
SET @@global.sql_mode = '';SET @@global.sql_mode = '';
-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table universitedb. mois
CREATE TABLE IF NOT EXISTS `mois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D6B08CB75E237E06` (`name`),
  UNIQUE KEY `UNIQ_D6B08CB777153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.mois : ~12 rows (environ)
DELETE FROM `mois`;
/*!40000 ALTER TABLE `mois` DISABLE KEYS */;
INSERT INTO `mois` (`id`, `name`, `code`) VALUES
	(1, 'Janvier', 1),
	(2, 'Fevrier', 2),
	(3, 'Mars', 3),
	(4, 'Avril', 4),
	(5, 'Mai', 5),
	(6, 'Juin', 6),
	(7, 'Juillet', 7),
	(8, 'Aout', 8),
	(9, 'Septembre', 9),
	(10, 'Octobre', 10),
	(11, 'Novembre', 11),
	(12, 'Decembre', 12);
/*!40000 ALTER TABLE `mois` ENABLE KEYS */;




/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table universitedb. jour_etude
CREATE TABLE IF NOT EXISTS `jour_etude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1FF608576C6E55B5` (`nom`),
  UNIQUE KEY `UNIQ_1FF6085777153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.jour_etude : ~2 rows (environ)
DELETE FROM `jour_etude`;
/*!40000 ALTER TABLE `jour_etude` DISABLE KEYS */;
INSERT INTO `jour_etude` (`id`, `nom`, `code`) VALUES
	(1, 'Lundi', 'lun'),
	(2, 'Mardi', 'mar'),
	(3, 'Mercredi', 'mer'),
	(4, 'Jeudi', 'jeu'),
	(5, 'Vendredi', 'ven'),
	(6, 'Samedi', 'sam'),
	(7, 'Dimanche', 'dim');
/*!40000 ALTER TABLE `jour_etude` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table universitedb. pecule
CREATE TABLE IF NOT EXISTS `pecule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valeurl1` double NOT NULL,
  `valeurl2` double NOT NULL,
  `valeurl3` double NOT NULL,
  `valeur4` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.pecule : ~1 rows (environ)
DELETE FROM `pecule`;
/*!40000 ALTER TABLE `pecule` DISABLE KEYS */;
INSERT INTO `pecule` (`id`, `valeurl1`, `valeurl2`, `valeurl3`, `valeur4`) VALUES
	(1, 150000, 160000, 170000, 180000);
/*!40000 ALTER TABLE `pecule` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table universitedb. frais_inscription
CREATE TABLE IF NOT EXISTS `frais_inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `montant` double NOT NULL,
  `encour` tinyint(1) DEFAULT NULL,
  `montantReinscription` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.frais_inscription : ~1 rows (environ)
DELETE FROM `frais_inscription`;
/*!40000 ALTER TABLE `frais_inscription` DISABLE KEYS */;
INSERT INTO `frais_inscription` (`id`, `montant`, `encour`, `montantReinscription`) VALUES
	(1, 250000, NULL, 350000);
/*!40000 ALTER TABLE `frais_inscription` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Export de la structure de la table universitedb. pecule
CREATE TABLE IF NOT EXISTS `pecule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valeurl1` double NOT NULL,
  `valeurl2` double NOT NULL,
  `valeurl3` double NOT NULL,
  `valeur4` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.pecule : ~1 rows (environ)
DELETE FROM `pecule`;
/*!40000 ALTER TABLE `pecule` DISABLE KEYS */;
INSERT INTO `pecule` (`id`, `valeurl1`, `valeurl2`, `valeurl3`, `valeur4`) VALUES
	(1, 150000, 160000, 170000, 180000);
/*!40000 ALTER TABLE `pecule` ENABLE KEYS */;

-- Export de la structure de la table universitedb. universite
CREATE TABLE IF NOT EXISTS `universite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `couverture_id` int(11) DEFAULT NULL,
  `nomUniversite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `nomPrenomRecteur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomDirecteurProgramme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomViceRecteurChargerEtude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomViceRecteurChargerRecherche` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boitePostal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siteWeb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B47BD9A3224B0A2F` (`nomUniversite`),
  UNIQUE KEY `UNIQ_B47BD9A34B685C80` (`nomPrenomRecteur`),
  UNIQUE KEY `UNIQ_B47BD9A3552A6E70` (`nomPrenomDirecteurProgramme`),
  UNIQUE KEY `UNIQ_B47BD9A3E33E8985` (`nomPrenomViceRecteurChargerEtude`),
  UNIQUE KEY `UNIQ_B47BD9A37DC60F31` (`nomPrenomViceRecteurChargerRecherche`),
  UNIQUE KEY `UNIQ_B47BD9A33F0A9AF5` (`couverture_id`),
  UNIQUE KEY `UNIQ_B47BD9A3F98F144A` (`logo_id`),
  UNIQUE KEY `UNIQ_B47BD9A347FB5A3F` (`boitePostal`),
  UNIQUE KEY `UNIQ_B47BD9A3BD78ADC7` (`siteWeb`),
  CONSTRAINT `FK_B47BD9A33F0A9AF5` FOREIGN KEY (`couverture_id`) REFERENCES `media` (`id`),
  CONSTRAINT `FK_B47BD9A3F98F144A` FOREIGN KEY (`logo_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.universite : ~1 rows (environ)
DELETE FROM `universite`;
/*!40000 ALTER TABLE `universite` DISABLE KEYS */;
INSERT INTO `universite` (`id`, `couverture_id`, `nomUniversite`, `logo_id`, `nomPrenomRecteur`, `nomPrenomDirecteurProgramme`, `nomPrenomViceRecteurChargerEtude`, `nomPrenomViceRecteurChargerRecherche`, `adresse`, `phone1`, `phone2`, `email`, `boitePostal`, `siteWeb`) VALUES
	(1, NULL, 'UNIVERSITE DE KINDIA', NULL, 'Docteur Jack Kourouma', 'Mr Drissa Conde', NULL, NULL, 'Kindia / Foulaya', '+224 628 29 56 09', '0224 00 00 00', 'rectorat@univ-kindia.org', '12 23 KINDIA', 'www.univ-kindia.org');
/*!40000 ALTER TABLE `universite` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;





