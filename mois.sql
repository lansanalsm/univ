-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table universitedb. mois
CREATE TABLE IF NOT EXISTS `mois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D6B08CB75E237E06` (`name`),
  UNIQUE KEY `UNIQ_D6B08CB777153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.mois : ~12 rows (environ)
DELETE FROM `mois`;
/*!40000 ALTER TABLE `mois` DISABLE KEYS */;
INSERT INTO `mois` (`id`, `name`, `code`) VALUES
	(1, 'Janvier', 1),
	(2, 'Fevrier', 2),
	(3, 'Mars', 3),
	(4, 'Avril', 4),
	(5, 'Mai', 5),
	(6, 'Juin', 6),
	(7, 'Juillet', 7),
	(8, 'Aout', 8),
	(9, 'Septembre', 9),
	(10, 'Octobre', 10),
	(11, 'Novembre', 11),
	(12, 'Decembre', 12);
/*!40000 ALTER TABLE `mois` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
