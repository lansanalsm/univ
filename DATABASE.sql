-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour universitedb
CREATE DATABASE IF NOT EXISTS `universitedb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `universitedb`;

-- Export de la structure de la table universitedb. admin_bac
CREATE TABLE IF NOT EXISTS `admin_bac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pv` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fraisFormation` double DEFAULT NULL,
  `avancePaye` double DEFAULT NULL,
  `ResteApaye` double DEFAULT NULL,
  `dateNaissance` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filiation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `centre` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origine` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idnational` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statut` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oldStatuBeforPaiement` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oldDepartementBeforPaiement` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departement` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `universite` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codeDep` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sessions` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flagIncrition` tinyint(1) DEFAULT NULL,
  `flagPaiement` tinyint(1) DEFAULT NULL,
  `contact` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationnalite` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `niveau` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codeNiveau` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unisite_pvSession_niveau` (`sessions`,`pv`)
) ENGINE=InnoDB AUTO_INCREMENT=478 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.admin_bac : ~159 rows (environ)
/*!40000 ALTER TABLE `admin_bac` DISABLE KEYS */;
INSERT INTO `admin_bac` (`id`, `pv`, `nomPrenom`, `fraisFormation`, `avancePaye`, `ResteApaye`, `dateNaissance`, `sexe`, `filiation`, `centre`, `origine`, `options`, `idnational`, `statut`, `oldStatuBeforPaiement`, `oldDepartementBeforPaiement`, `departement`, `universite`, `codeDep`, `sessions`, `etat`, `flagIncrition`, `flagPaiement`, `contact`, `nationnalite`, `niveau`, `codeNiveau`) VALUES
	(319, '21702', 'M\'BEMBA SYLLA', NULL, NULL, NULL, '08-17-91', 'M', 'Bagaly et de Djene SYLLA', 'LYCEE_KIPE', 'YAMASSAFOUBAH', 'SM', 'SS0225669456', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(320, '7687', 'ABOUBACAR GUIRASSY', NULL, NULL, NULL, '08-18-91', 'M', 'Bagaly et de Djene SYLLA', 'C-ALMAMY SAMORY', 'SEMYG', 'SM', 'SS0225669457', 'Boursier', NULL, NULL, 'PHYSIQUE', 'Univ  Kindia', '23', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(321, '13694', 'MAMOUDOU KOUROUMA', NULL, NULL, NULL, '08-19-91', 'M', 'Bagaly et de Djene SYLLA', 'EP SYLVANUS OLYMPIO', 'SEMYG', 'SM', 'SS0225669458', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(322, '11804', 'IBRAHIMA DIABY', NULL, NULL, NULL, '08-20-91', 'M', 'Bagaly et de Djene SYLLA', 'EP DABOMPA', 'G BASE', 'SM', 'SS0225669459', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(323, '6925', 'ABDOURAHAMANE TOURE', NULL, NULL, NULL, '08-21-91', 'M', 'Bagaly et de Djene SYLLA', 'ABOU FOTE', 'SEMYG', 'SM', 'SS0225669460', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(324, '21042', 'MAMADOU LAMINE DIALLO', NULL, NULL, NULL, '08-22-91', 'M', 'Bagaly et de Djene SYLLA', 'LYCEE_FELLA', 'LE SOUMBOUYAH', 'SM', 'SS0225669461', 'Boursier', NULL, NULL, 'PHYSIQUE', 'Univ  Kindia', '23', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(325, '18791', 'CESAR SOUA 7 SANGBARAMOU', NULL, NULL, NULL, '08-23-91', 'M', 'Bagaly et de Djene SYLLA', 'ENFANT_NOIR', 'MAHATMA GANDHI', 'SM', 'SS0225669462', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(326, '17321', 'ABDOULAYE BARRY', NULL, NULL, NULL, '08-24-91', 'M', 'Bagaly et de Djene SYLLA', 'AMADOU_SYLLA', 'EMMAUS', 'SM', 'SS0225669463', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(327, '15815', 'SEKOU CONDE', NULL, NULL, NULL, '05-09-89', 'M', 'Bagaly et de Djene SYLLA', 'MAMADOU SIDIBE', 'CDLEX', 'SM', 'SS0225669464', 'Boursier', NULL, NULL, 'PHYSIQUE', 'Univ  Kindia', '23', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(328, '18800', 'CHEICK AHMED TIDIANE DABO', NULL, NULL, NULL, '05-10-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'ENFANT_NOIR', 'SAINT GEORGES', 'SM', 'SS0225669465', 'Boursier', NULL, NULL, 'PHYSIQUE', 'Univ  Kindia', '23', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(329, '12624', 'LANSANA GUIRASSY', NULL, NULL, NULL, '05-11-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'EP KOICHIRO MATSURA', 'SEMYG', 'SS', 'SS0225669466', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(330, '18224', 'AMADOU OURY BARRY', NULL, NULL, NULL, '05-12-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'CS_LAMBANDJI', 'OUMOU DIABY', 'SS', 'SS0225669467', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(331, '6852', 'ABDOUL GADIRI GUIRASSY', NULL, NULL, NULL, '05-13-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'ABOU FOTE', 'SEMYG', 'SM', 'SS0225669468', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(332, '15884', 'SEYDOUBABANGOURA', NULL, NULL, NULL, '05-14-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'MAMADOU SIDIBE', 'TITI CAMARA', 'SE', 'SS0225669469', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(333, '4645', 'DJENABOU SOUMAH', NULL, NULL, NULL, '05-15-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'COLEAH CENTRE', 'KK1', 'SE', 'SS0225669470', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(334, '23511', 'ZENAB JEANNETTE DIALLO', NULL, NULL, NULL, '05-16-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'OLUSEGUN OBASS ANDJO', 'CANDIDAT LIBRE', 'SE', 'SS0225669471', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(335, '3275', 'SIA KOUROUMA', NULL, NULL, NULL, '05-17-89', 'F', 'Ansoumane et de LAMARANAH BAH', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SE', 'SS0225669472', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(336, '21703', 'IMIBENGUE WESS SOUKARN', NULL, NULL, NULL, '05-18-89', 'M', 'Ansoumane et de LAMARANAH BAH', 'LYCEE_KIPE', 'LYCEE KIPE', 'SE', 'SS0225669473', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(337, '15933', 'SIDIKI BAMBA', NULL, NULL, NULL, '05-19-89', 'M', 'Karamoko et de Odette ouendouno', 'MAMADOU SIDIBE', 'M L S', 'SE', 'SS0225669474', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(338, '22532', 'OUSMANE BERETE', NULL, NULL, NULL, '05-20-90', 'M', 'Karamoko et de Odette ouendouno', 'MAHATMA_GANDHI', 'LE SOUMBOUYAH', 'SE', 'SS0225669475', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(339, '27871', 'BAKARY CAMARA', NULL, NULL, NULL, '05-21-90', 'M', 'Karamoko et de Odette ouendouno', 'EL OUMAR TALL 2', 'CANDIDAT LIBRE', 'SE', 'SS0225669476', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(340, '20240', 'IDIATOU BELLA DIALLO', NULL, NULL, NULL, '05-22-90', 'M', 'Karamoko et de Odette ouendouno', 'HADJA MARIAMA SOUMAH', 'HABIBATA TOUNKARA', 'SE', 'SS0225669477', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(341, '22530', 'OUSMANE BARRY', NULL, NULL, NULL, '05-23-90', 'M', 'Karamoko et de Odette ouendouno', 'MAHATMA_GANDHI', 'SAFIA ECOLE', 'SE', 'SS0225669478', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(342, '11830', 'IBRAHIMA KALIL SOUMOUNOU', NULL, NULL, NULL, '05-24-90', 'M', 'Karamoko et de Odette ouendouno', 'EP DABOMPA', 'HUMANISTE SAGBE', 'SS', 'SS0225669479', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(343, '35056', 'MAMADOU TAHIROU SOW', NULL, NULL, NULL, '05-25-92', 'M', 'Fode et de Nanette TRAORE', 'GSP GLC KINDIA', 'LUNE DE SAM', 'SE', 'SS0225669480', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(344, '28535', 'MOUSSA KABA', NULL, NULL, NULL, '05-26-92', 'M', 'Fode et de Nanette TRAORE', 'ROI HASSAN 2-SIGUIRI', 'SIRA KOROMA', 'SE', 'SS0225669481', 'Boursier', NULL, NULL, 'PHYSIQUE', 'Univ  Kindia', '23', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(345, '18168', 'AMADOU BARRY', NULL, NULL, NULL, '05-27-92', 'M', 'Fode et de Nanette TRAORE', 'CS_LAMBANDJI', 'AMADOU SADIGOU DIALLO', 'SE', 'SS0225669482', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(346, '7485', 'MOHAMED DIAN TOURE', NULL, NULL, NULL, '05-28-92', 'M', 'Fode et de Nanette TRAORE', 'AHMED SEKOU TOURE', 'SEMYG', 'SS', 'SS0225669483', 'Boursier', NULL, NULL, 'HISTOIRE', 'Univ  Kindia', '32', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(347, '19356', 'FATOUMATA BINTA DICKO', NULL, NULL, NULL, '05-29-92', 'M', 'Fode et de Nanette TRAORE', 'FIDEL CASTRO RATOMA', 'HADJA OUMOU PS', 'SS', 'SS0225669484', 'Boursier', NULL, NULL, 'HISTOIRE', 'Univ  Kindia', '32', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(348, '12402', 'KALIFA GUIRASSY', NULL, NULL, NULL, '05-30-92', 'F', 'Fode et de Nanette TRAORE', 'EP DAR-ES-SALAM', 'SEMYG', 'SS', 'SS0225669485', 'Boursier', NULL, NULL, 'HISTOIRE', 'Univ  Kindia', '32', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(349, '20738', 'MAIMOUNATOU DIALLO', NULL, NULL, NULL, '05-31-92', 'M', 'Kalil et de OUMOU TOURE', 'LYCEE 2_LAMBANDJI', 'HABIBATA TOUNKARA', 'SS', 'SS0225669486', 'Boursier', NULL, NULL, 'HISTOIRE', 'Univ  Kindia', '32', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(350, '6179', 'MOUSTAPHA TOUNKARA', NULL, NULL, NULL, '06-01-92', 'F', 'Kalil et de OUMOU TOURE', 'MADINA-1', 'MAMOUNA', 'SS', 'SS0225669487', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(351, '6569', 'SOULEYMANE ALPHA BAH', NULL, NULL, NULL, '06-02-92', 'M', 'Kalil et de OUMOU TOURE', 'MATAM-1', 'LYCEE 1ER MARS', 'SS', 'SS0225669488', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(352, '21073', 'MAMADOU MOUCTAR DIALLO', NULL, NULL, NULL, '06-03-92', 'M', 'Kalil et de OUMOU TOURE', 'LYCEE_FELLA', 'LAMPEL', 'SS', 'SS0225669489', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(353, '14337', 'MOHAMED GAKOU', NULL, NULL, NULL, '06-04-92', 'M', 'Kalil et de OUMOU TOURE', 'JEAN MERMOZ', 'TITI CAMARA', 'SS', 'SS0225669490', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(354, '36612', 'THIERNO AMADOU BAILO DIALLO', NULL, NULL, NULL, '06-05-92', 'M', 'Kalil et de OUMOU TOURE', 'GS YACINE', 'WOURO', 'SS', 'SS0225669491', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(355, '15897', 'SEYDOUBA CISSE', NULL, NULL, NULL, '06-06-92', 'M', 'Kalil et de OUMOU TOURE', 'MAMADOU SIDIBE', 'SEMYG', 'SS', 'SS0225669492', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(356, '20231', 'IBRAHIMA YIRA DOUMBOUYA', NULL, NULL, NULL, '06-07-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'HADJA MARIAMA SOUMAH', 'HABIBATA TOUNKARA', 'SSFA', 'SS0225669493', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(357, '40530', 'ADAMACONDE', NULL, NULL, NULL, '06-08-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'DIVINE GRACE', 'BETHLEHEM', 'SSFA', 'SS0225669494', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(358, '12186', 'JEANNETTE LAMAH', NULL, NULL, NULL, '06-09-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'EP DABOMPA', 'CAK', 'SSFA', 'SS0225669495', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(359, '15936', 'SIDIKI CISSE', NULL, NULL, NULL, '06-10-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'MAMADOU SIDIBE', 'LA CIBLE DU FORMATEUR', 'SSFA', 'SS0225669496', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(360, '20778', 'MAMADI DONZO', NULL, NULL, NULL, '06-11-92', 'F', 'Mohamed et de AISSATA DOUMBOUYA', 'LYCEE 2_LAMBANDJI', 'LYCEE LAMBANDJI', 'SEFA', 'SS0225669497', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(361, '22850', 'SEKOU CHERIF', NULL, NULL, NULL, '06-12-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'ROLAND_PRE', 'SAINT GEORGES', 'SEFA', 'SS0225669498', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(362, '13387', 'MAMADOU SIDIBE', NULL, NULL, NULL, '06-13-92', 'M', 'Mohamed et de AISSATA DOUMBOUYA', 'EP MATOTO II', 'LYCEE M\'BALIA CAMARA', 'SM', 'SS0225669499', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(363, '20225', 'IBRAHIMA SOW', NULL, NULL, NULL, '06-14-91', 'F', 'Mohamed et de AISSATA DOUMBOUYA', 'HADJA MARIAMA SOUMAH', 'LYCEE LAMBANDJI', 'SM', 'SS0225669500', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(364, '12907', 'MAMA AISSATA YANSANE', NULL, NULL, NULL, '06-15-91', 'F', 'OUMAR et de ROUGUIATOU DIABY', 'EP KOICHIRO MATSURA', 'SEMYG', 'SM', 'SS0225669501', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(365, '17351', 'ABDOULAYE DJIBRIL BAH', NULL, NULL, NULL, '06-16-91', 'F', 'OUMAR et de ROUGUIATOU DIABY', 'AMADOU_SYLLA', 'MARIE ANTOINETTE', 'SM', 'SS0225669502', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(366, '19424', 'FATOUMATA DIARAYE DIALLO', NULL, NULL, NULL, '06-17-91', 'M', 'OUMAR et de ROUGUIATOU DIABY', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SM', 'SS0225669503', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(367, '12404', 'KALIL DAFFE', NULL, NULL, NULL, '06-18-91', 'F', 'OUMAR et de ROUGUIATOU DIABY', 'EP DAR-ES-SALAM', 'GSAMADOU TOUMANI TOURE', 'SM', 'SS0225669504', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(368, '40489', 'ABBE JOACHIN CAMARA', NULL, NULL, NULL, '06-19-91', 'F', 'OUMAR et de ROUGUIATOU DIABY', 'DIVINE GRACE', 'SAINT JEAN', 'SM', 'SS0225669505', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(369, '4850', 'FATOUMATA YARIE CAMARA', NULL, NULL, NULL, '06-20-91', 'M', 'OUMAR et de ROUGUIATOU DIABY', 'COLEAH CENTRE', 'KK1', 'SM', 'SS0225669506', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(370, '22001', 'MOHAMED LAMINE SANGARE', NULL, NULL, NULL, '06-21-91', 'M', 'OUMAR et de ROUGUIATOU DIABY', 'LYCEE_SONFONIA', 'HADJA OUMOU PS', 'SM', 'SS0225669507', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(371, '15978', 'SONAGBE KOUROUMA', NULL, NULL, NULL, '06-22-91', 'M', 'OUMAR et de ROUGUIATOU DIABY', 'MAMADOU SIDIBE', 'VICTOR HUGO', 'SM', 'SS0225669508', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(372, '19528', 'HAWA KEITA', NULL, NULL, NULL, '06-23-91', 'M', 'HASSANE  et de LILI KEITA', 'FIDEL CASTRO RATOMA', 'GRANDE ECOLE', 'SS', 'SS0225669509', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(373, '17681', 'AHMED TIDIANE DIALLO', NULL, NULL, NULL, '06-24-91', 'M', 'HASSANE  et de LILI KEITA', 'COLLEGE_KIPE', 'MAHATMA GANDHI', 'SS', 'SS0225669510', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(374, '17426', 'ABDOUL MADJID DIALLO', NULL, NULL, NULL, '06-25-91', 'M', 'HASSANE  et de LILI KEITA', 'AMADOU_SYLLA', 'MAARIF T-G KIPE', 'SM', 'SS0225669511', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(375, '35385', 'MORLAYE CAMARA', NULL, NULL, NULL, '06-26-91', 'F', 'HASSANE  et de LILI KEITA', 'GSP GLC KINDIA', 'LYCEE 28 SEPTEMBRE', 'SE', 'SS0225669512', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(376, '19005', 'ELHADJ OUSMANE DIALLO', NULL, NULL, NULL, '06-27-91', 'M', 'HASSANE  et de LILI KEITA', 'ENFANT_NOIR', 'SYLLA LAMINE', 'SE', 'SS0225669513', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(377, '41285', 'MOHAMED SOUMAH', NULL, NULL, NULL, '06-28-91', 'F', 'HASSANE  et de LILI KEITA', 'MARIAME CAMARA', 'MARIAME CAMARA', 'SE', 'SS0225669514', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(378, '3920', 'OUMAR DARABA', NULL, NULL, NULL, '06-29-91', 'F', 'HASSANE  et de LILI KEITA', 'TOMBO 2', 'LYCEE 2 OCTOBRE', 'SE', 'SS0225669515', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(379, '15935', 'SIDIKI CAMARA', NULL, NULL, NULL, '06-30-91', 'F', 'HASSANE  et de LILI KEITA', 'MAMADOU SIDIBE', '2HM', 'SE', 'SS0225669516', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(380, '19416', 'FATOUMATA DIARAYE BARRY', NULL, NULL, NULL, '07-01-91', 'F', 'HASSANE  et de LILI KEITA', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SE', 'SS0225669517', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(381, '3077', 'MAMADOU MADIOU DIALLO', NULL, NULL, NULL, '07-02-91', 'M', 'HASSANE  et de LILI KEITA', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SE', 'SS0225669518', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(382, '31408', 'ADRIEN TAMBA MILLIMONO', NULL, NULL, NULL, '07-03-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'ABDOU FAZA', 'GS SAINT JACOB', 'SE', 'SS0225669519', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(383, '12179', 'JEAN MARTHE KAMANO', NULL, NULL, NULL, '07-04-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'EP DABOMPA', 'BAPTISTE', 'SE', 'SS0225669520', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(384, '40668', 'AUBIN KOLAMOU', NULL, NULL, NULL, '07-05-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'DIVINE GRACE', 'BETHLEHEM', 'SE', 'SS0225669521', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(385, '15902', 'SEYDOUBA KEITA', NULL, NULL, NULL, '07-06-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'MAMADOU SIDIBE', 'SEMYG', 'SS', 'SS0225669522', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(386, '19415', 'FATOUMATA DIARAYE BARRY', NULL, NULL, NULL, '07-07-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SE', 'SS0225669523', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(387, '14273', 'M\'BEMBA DRAME', NULL, NULL, NULL, '07-08-91', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'JEAN MERMOZ', 'LYCEE D\'EXCELLENCE', 'SE', 'SS0225669524', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(388, '3277', 'SIBY MELANIE FRANCISCA LAWSON', NULL, NULL, NULL, '07-09-92', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SE', 'SS0225669525', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(389, '22279', 'MAMADOU DIOUHE DIALLO', NULL, NULL, NULL, '07-10-92', 'M', 'MAMADOU et de ROUGUIATOU SYLLA', 'MAHATMA_GANDHI', 'M\'MAH TOURE', 'SS', 'SS0225669526', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(390, '17414', 'ABDOUL KARIM DIALLO', NULL, NULL, NULL, '07-11-92', 'M', 'NABY et de FANTA SAOUMOURA', 'AMADOU_SYLLA', 'LE SOUMBOUYAH', 'SS', 'SS0225669527', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(391, '18764', 'BOUNTOU BANGOURA', NULL, NULL, NULL, '07-12-92', 'M', 'NABY et de FANTA SAOUMOURA', 'ENFANT_NOIR', 'SYLLA LAMINE', 'SS', 'SS0225669528', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(392, '15862', 'SEKOU SYLLA', NULL, NULL, NULL, '07-13-92', 'F', 'NABY et de FANTA SAOUMOURA', 'MAMADOU SIDIBE', 'LYCEE DR IBRAHIMA FOFANA', 'SS', 'SS0225669529', 'Boursier', NULL, NULL, 'SOCIOLOGIE', 'Univ  Kindia', '31', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(393, '5612', 'MAMADI KEITA', NULL, NULL, NULL, '07-14-92', 'M', 'NABY et de FANTA SAOUMOURA', 'LYCEE BONFI', 'M\'BALOU', 'SS', 'SS0225669530', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(394, '18805', 'CHEICK OUMAR BAH', NULL, NULL, NULL, '07-15-92', 'M', 'NABY et de FANTA SAOUMOURA', 'ENFANT_NOIR', 'SAINT GEORGES', 'SS', 'SS0225669531', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(395, '28327', 'MAMADI GUILAVOGUI', NULL, NULL, NULL, '07-16-92', 'M', 'NABY et de FANTA SAOUMOURA', 'KANKOU MOUSSA', 'EL HADJ OUMAR TALL', 'SS', 'SS0225669532', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(396, '18809', 'CHERIF YAYA SOW', NULL, NULL, NULL, '07-17-92', 'M', 'NABY et de FANTA SAOUMOURA', 'ENFANT_NOIR', 'HYNDAYE', 'SS', 'SS0225669533', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(397, '10365', 'MOHAMED MARIKO', NULL, NULL, NULL, '07-18-92', 'M', 'NABY et de FANTA SAOUMOURA', 'EL BOUBACAR BIRO', 'LA CIBLE DU FORMATEUR', 'SS', 'SS0225669534', 'Boursier', NULL, NULL, 'GEOGRAPHIE', 'Univ  Kindia', '33', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(398, '19020', 'EMMANUEL ABOUBACAR TOURE', NULL, NULL, NULL, '07-19-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'ENFANT_NOIR', 'HABIBATA TOUNKARA', 'SS', 'SS0225669535', 'Boursier', NULL, NULL, 'PHILOSOPHIE', 'Univ  Kindia', '34', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(399, '18315', 'AMINATA DIALLO', NULL, NULL, NULL, '07-20-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'CS_LAMBANDJI', 'SAINT GEORGES', 'SSFA', 'SS0225669536', 'Boursier', NULL, NULL, 'PHILOSOPHIE', 'Univ  Kindia', '34', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(400, '15706', 'MOHAMED LAMINE JACQUES CAMARA', NULL, NULL, NULL, '07-21-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'MAMADOU SIDIBE', 'GS IKK', 'SSFA', 'SS0225669537', 'Boursier', NULL, NULL, 'PHILOSOPHIE', 'Univ  Kindia', '34', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(401, '19347', 'FATOUMATA BINTA DIALLO', NULL, NULL, NULL, '07-22-92', 'F', 'YALAMI et de SANDRINE KOIVOGUI', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SSFA', 'SS0225669538', 'Boursier', NULL, NULL, 'PHILOSOPHIE', 'Univ  Kindia', '34', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(402, '10409', 'MOHAMED SEKOU FOFANA', NULL, NULL, NULL, '07-23-92', 'F', 'YALAMI et de SANDRINE KOIVOGUI', 'EL BOUBACAR BIRO', 'TITI CAMARA', 'SSFA', 'SS0225669539', 'Boursier', NULL, NULL, 'PHILOSOPHIE', 'Univ  Kindia', '34', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(403, '34553', 'HABIB SYLLA', NULL, NULL, NULL, '07-24-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'EP MANQUEPAS', 'EMMAUS', 'SEFA', 'SS0225669540', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(404, '40216', 'JEAN NOEL BAMY', NULL, NULL, NULL, '07-25-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'CAMP BEHANZIN', 'DIVINE GRACE', 'SEFA', 'SS0225669541', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(405, '22144', 'MOUSSASOUMAH', NULL, NULL, NULL, '07-26-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'LYCEE_SONFONIA', 'SAINT JOSEPH LAMBANDJI', 'SM', 'SS0225669542', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(406, '15859', 'SEKOU SYDIA DIALLO', NULL, NULL, NULL, '07-27-92', 'F', 'YALAMI et de SANDRINE KOIVOGUI', 'MAMADOU SIDIBE', 'HAKAFO', 'SM', 'SS0225669543', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(407, '19223', 'FANTA SYLLA', NULL, NULL, NULL, '07-28-92', 'M', 'YALAMI et de SANDRINE KOIVOGUI', 'FIDEL CASTRO RATOMA', 'SAINT BERNARD', 'SM', 'SS0225669544', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(408, '23466', 'TAMBA JULIEN MALANO', NULL, NULL, NULL, '07-29-92', 'F', 'YALAMI et de SANDRINE KOIVOGUI', 'OLUSEGUN OBASS ANDJO', 'CANDIDAT LIBRE', 'SM', 'SS0225669545', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(409, '6295', 'DAOUDA SYLLA', NULL, NULL, NULL, '07-30-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MATAM-1', 'LYCEE COLEGE BONFI', 'SM', 'SS0225669546', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(410, '35826', 'SEYDOUBASYLLA', NULL, NULL, NULL, '07-31-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'STEPHANE PLUS', 'C.A.D', 'SM', 'SS0225669547', 'Boursier', NULL, NULL, 'SCIENCES COMPTABLES', 'Univ  Kindia', '12', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(411, '37125', 'OUSMANE TANOU BAH', NULL, NULL, NULL, '08-01-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'LYCEE SIGON', 'LYCEE SIGON', 'SM', 'SS0225669548', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(412, '15729', 'NABY LAYE TOURE', NULL, NULL, NULL, '08-02-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'MAMADOU SIDIBE', 'CAM-SYL', 'SM', 'SS0225669549', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(413, '6955', 'ABDOULAYE DIALLO', NULL, NULL, NULL, '08-03-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'AHMED SEKOU TOURE', 'BILLY AVIATION', 'SM', 'SS0225669550', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(414, '28766', 'SIDI YAYA HAIDARA', NULL, NULL, NULL, '08-04-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'ROI HASSAN 2-SIGUIRI', 'NELSON MANDELA', 'SM', 'SS0225669551', 'Boursier', NULL, NULL, 'BIOLOGIE', 'Univ  Kindia', '21', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(415, '34079', 'ALPHACONDE', NULL, NULL, NULL, '08-05-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'EP CACIA', 'FRANCOPHONIE', 'SS', 'SS0225669552', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(416, '18803', 'CHEICK MAMADY CONDE', NULL, NULL, NULL, '08-06-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'ENFANT_NOIR', 'MOHAMED BARRY HAMDALLAYE', 'SS', 'SS0225669553', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(417, '27287', 'SEKOU TOURE', NULL, NULL, NULL, '08-07-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'LYCEE SEKOUBA SACKO', 'SEKOUBASACKO', 'SM', 'SS0225669554', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(418, '34493', 'FATOUMATA SYLLA', NULL, NULL, NULL, '08-08-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'EP MANQUEPAS', 'LA LUMIERE', 'SE', 'SS0225669555', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(419, '18955', 'DONALD IROMOU', NULL, NULL, NULL, '08-09-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'ENFANT_NOIR', 'HABIBATA TOUNKARA', 'SE', 'SS0225669556', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(420, '22648', 'RIAD CAMARA', NULL, NULL, NULL, '08-10-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MAHATMA_GANDHI', 'GS HAMAS', 'SE', 'SS0225669557', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(421, '20476', 'KADIATOU DIALLO', NULL, NULL, NULL, '08-11-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'LYCEE 2_LAMBANDJI', 'SAINT JOSEPH LAMBANDJI', 'SE', 'SS0225669558', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(422, '3166', 'MOHAMED KANFORY BANGOURA', NULL, NULL, NULL, '08-12-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'EP DIXINN CENTRE 2', 'LAVOISIER', 'SE', 'SS0225669559', 'Boursier', NULL, NULL, 'MATHEMATIQUES', 'Univ  Kindia', '24', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(423, '6626', 'YAMOUSSA CAMARA', NULL, NULL, NULL, '08-13-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MATAM-1', 'LEPRINCE', 'SE', 'SS0225669560', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(424, '36545', 'MAMADOU SAIDOU BAH', NULL, NULL, NULL, '08-14-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'GS YACINE', 'YACINE', 'SE', 'SS0225669561', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(425, '12201', 'JOSEPHINE SENY NIAMY', NULL, NULL, NULL, '08-15-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'EP DABOMPA', 'CAK', 'SE', 'SS0225669562', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(426, '22235', 'NENE ISSIAGA DIALLO', NULL, NULL, NULL, '08-16-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'LYCEE_SONFONIA', 'SAFIA ECOLE', 'SE', 'SS0225669563', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(427, '25880', 'ABDOULAYE BALDE', NULL, NULL, NULL, '08-17-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'ALPHA YAYA DIALLO', 'GSP-LAC LEMAN', 'SE', 'SS0225669564', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(428, '19095', 'ALPHA OUMAR BARRY', NULL, NULL, NULL, '08-18-92', 'F', 'JACQUES et de ROSALINE MAOMOU', 'FIDEL CASTRO RATOMA', 'CANDIDAT LIBRE', 'SS', 'SS0225669565', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(429, '15805', 'SEKOU CHERIF', NULL, NULL, NULL, '08-19-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MAMADOU SIDIBE', 'ROI HASSAN II', 'SE', 'SS0225669566', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(430, '35464', 'MOHAMED SYLLA', NULL, NULL, NULL, '08-20-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'LYCEE SAMBAYA', 'CHERIF KE', 'SE', 'SS0225669567', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(431, '3252', 'SAFIATOU KANN', NULL, NULL, NULL, '08-21-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SE', 'SS0225669568', 'Boursier', NULL, NULL, 'CHIMIE', 'Univ  Kindia', '22', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(432, '15929', 'SIA THERESE CONDE', NULL, NULL, NULL, '08-22-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MAMADOU SIDIBE', 'SEMYG', 'SS', 'SS0225669569', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(433, '12671', 'LONCENY CONDE', NULL, NULL, NULL, '08-23-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'EP KOICHIRO MATSURA', 'JEAN MERMOZ', 'SS', 'SS0225669570', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(434, '8354', 'ALAMATRAORE', NULL, NULL, NULL, '08-24-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'CHEICK ABDOUL KABA', 'TITI CAMARA', 'SS', 'SS0225669571', 'Boursier', NULL, NULL, 'ANGLAIS', 'Univ  Kindia', '43', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(435, '7612', 'ABOUBACAR CAMARA', NULL, NULL, NULL, '08-25-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'C-ALMAMY SAMORY', 'GANGAN KANIYA', 'SS', 'SS0225669572', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(436, '18342', 'AMINATOU BAH', NULL, NULL, NULL, '08-26-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'CS_LAMBANDJI', 'SAINT GEORGES', 'SS', 'SS0225669573', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(437, '6633', 'YAMOUSSASOUMAH', NULL, NULL, NULL, '08-27-92', 'M', 'JACQUES et de ROSALINE MAOMOU', 'MATAM-1', 'LEPRINCE', 'SS', 'SS0225669574', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(438, '21094', 'MAMADOU MOUSTAPHA DIALLO', NULL, NULL, NULL, '08-28-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'LYCEE_FELLA', 'HAMDALLAYE SECONDAIRE', 'SS', 'SS0225669575', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(439, '3296', 'THIERNO MARIAMA BALDE', NULL, NULL, NULL, '08-29-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SS', 'SS0225669576', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(440, '11687', 'IBRAHIMA BANGOURA', NULL, NULL, NULL, '08-30-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'EP CITE DE L\'AIR', 'MARIAMA DUBREKA', 'SS', 'SS0225669577', 'Boursier', NULL, NULL, 'ADMINISTRATION DES AFFAIRES', 'Univ  Kindia', '13', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(441, '10947', 'FATOUMATA CAMARA', NULL, NULL, NULL, '08-31-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'EP AICHA BAH', 'SEMYG', 'SS', 'SS0225669578', 'Boursier', NULL, NULL, 'LETTRES MODERNES', 'Univ  Kindia', '41', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(442, '11837', 'IBRAHIMA MAMADOU CAMARA', NULL, NULL, NULL, '09-01-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'EP DABOMPA', 'LYCEE D\'EXCELLENCE', 'SSFA', 'SS0225669579', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(443, '5631', 'MAMADOUBA TOURE', NULL, NULL, NULL, '09-02-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'LYCEE BONFI', 'LYCEE COLEGE BONFI', 'SSFA', 'SS0225669580', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(444, '37116', 'MAMADOU LAMARANA SOW', NULL, NULL, NULL, '09-03-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'LYCEE SIGON', 'LYCEE SIGON', 'SSFA', 'SS0225669581', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(445, '7880', 'ADENUS KOVA GUILAVOGUI', NULL, NULL, NULL, '09-04-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'C-ALMAMY SAMORY', 'LYCEE D\'EXCELLENCE', 'SSFA', 'SS0225669582', 'Boursier', NULL, NULL, 'SCIENCES DU LANGAGE', 'Univ  Kindia', '42', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(446, '34728', 'IBRAHIMA SORY KABA', NULL, NULL, NULL, '09-05-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'GSP GLC KINDIA', 'STEPHANE PLUS', 'SM', 'SS0225669583', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(447, '15953', 'SIKOWOU KALIVOGUI', NULL, NULL, NULL, '09-06-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'MAMADOU SIDIBE', 'LYCEE EL BOUBACAR BIRO', 'SM', 'SS0225669584', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(448, '19462', 'FATOUMATA KALOKO', NULL, NULL, NULL, '09-07-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'FIDEL CASTRO RATOMA', 'GRANDE ECOLE', 'SM', 'SS0225669585', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(449, '19345', 'FATOUMATA BINTA DIALLO', NULL, NULL, NULL, '09-08-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SM', 'SS0225669586', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(450, '18194', 'AMADOU DJOULDE DIALLO', NULL, NULL, NULL, '09-09-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'CS_LAMBANDJI', 'ABDOUL MALICK DIALLO', 'SM', 'SS0225669587', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(451, '34418', 'FATOUMATA BINTA BARRY', NULL, NULL, NULL, '09-10-92', 'F', 'KOUNA et de MAMADAMA TOURE', 'EP MANQUEPAS', 'C.A.D', 'SM', 'SS0225669588', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(452, '26432', 'KESSERY CONDE', NULL, NULL, NULL, '09-11-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'LYCEE-MARIEN-N\'G OUABI', 'LYCEE 3 AVRIL', 'SM', 'SS0225669589', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(453, '3118', 'MARIAMA KESSO BAH', NULL, NULL, NULL, '09-12-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'EP DIXINN CENTRE 2', 'SAINTE MARIE', 'SM', 'SS0225669590', 'Boursier', 'Bourssier', 'SCIENCES ECONOMIQUES', 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', 1, 1, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(454, '15931', 'SIBA KOLIE', NULL, NULL, NULL, '09-13-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'MAMADOU SIDIBE', 'CSAS', 'SM', 'SS0225669591', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(455, '19304', 'FATOUMATA BINTA BAH', NULL, NULL, NULL, '09-14-92', 'M', 'KOUNA et de MAMADAMA TOURE', 'FIDEL CASTRO RATOMA', 'GRANDE ECOLE', 'SE', 'SS0225669592', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(456, '16509', 'KADIATOU KANE', NULL, NULL, NULL, '09-15-92', 'M', 'IDRISSA et de MARIMA KANE', 'TITI CAMARA', 'DCE', 'SE', 'SS0225669593', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(457, '14121', 'MARIAME SAVANT CAMARA', NULL, NULL, NULL, '09-16-92', 'F', 'IDRISSA et de MARIMA KANE', 'JEAN MERMOZ', 'SEMYG', 'SE', 'SS0225669594', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(458, '28827', 'YAKOUBA DOUMBOUYA', NULL, NULL, NULL, '09-17-92', 'F', 'IDRISSA et de MARIMA KANE', 'ROI HASSAN 2-SIGUIRI', 'NELSON MANDELA', 'SE', 'SS0225669595', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(459, '6761', 'ABDOULAYE DJENAB SOUMAH', NULL, NULL, NULL, '09-18-92', 'M', 'IDRISSA et de MARIMA KANE', 'ABOU FOTE', 'LYCEE AHMED SEKOU TOURE', 'SE', 'SS0225669596', 'Boursier', 'Bourssier', 'SCIENCES ECONOMIQUES', 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', 1, 1, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(460, '40547', 'AHMADOU LAMARANA DIALLO', NULL, NULL, NULL, '09-19-92', 'F', 'IDRISSA et de MARIMA KANE', 'DIVINE GRACE', 'RAMATOULAYE KEITA', 'SE', 'SS0225669597', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(461, '18802', 'CHEICK KALIL KABA', NULL, NULL, NULL, '09-20-92', 'M', 'IDRISSA et de MARIMA KANE', 'ENFANT_NOIR', 'LA FARANDOLE', 'SE', 'SS0225669598', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(462, '13699', 'MAMOUDOU SYLLA', NULL, NULL, NULL, '09-21-88', 'M', 'IDRISSA et de MARIMA KANE', 'EP SYLVANUS OLYMPIO', 'CAK', 'SE', 'SS0225669599', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(463, '36613', 'THIERNO AMADOU DIALLO', NULL, NULL, NULL, '09-22-88', 'M', 'IDRISSA et de MARIMA KANE', 'GS YACINE', 'WOURO', 'SE', 'SS0225669600', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(464, '11029', 'FATOUMATA GUIRASSY', NULL, NULL, NULL, '09-23-88', 'F', 'IDRISSA et de MARIMA KANE', 'EP AICHA BAH', 'CSAS', 'SE', 'SS0225669601', 'Boursier', 'Bourssier', 'SCIENCES ECONOMIQUES', 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', 1, 1, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(465, '18330', 'AMINATA SANOH', NULL, NULL, NULL, '09-24-88', 'M', 'IDRISSA et de MARIMA KANE', 'CS_LAMBANDJI', 'LA FARANDOLE', 'SE', 'SS0225669602', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(466, '15980', 'SONAH TRAORE', NULL, NULL, NULL, '09-25-88', 'M', 'IDRISSA et de MARIMA KANE', 'MAMADOU SIDIBE', 'LYCEE DENIS GALEMA', 'SE', 'SS0225669603', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(467, '24979', 'SEKOU CAMARA', NULL, NULL, NULL, '09-26-88', 'M', 'IDRISSA et de MARIMA KANE', 'FIDEL CASTRO-KISSI', 'AMITE', 'SE', 'SS0225669604', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(468, '20824', 'MAMADOU ALPHA BAH', NULL, NULL, NULL, '09-27-88', 'M', 'IDRISSA et de MARIMA KANE', 'LYCEE 2_LAMBANDJI', 'GS YATTAYA', 'SE', 'SS0225669605', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(469, '11759', 'IBRAHIMA DRAME', NULL, NULL, NULL, '09-28-88', 'M', 'OUMAR et de M\'MAHAWA DRAME', 'EP CITE DE L\'AIR', 'EMMAUS', 'SE', 'SS0225669606', 'Boursier', 'Bourssier', 'SCIENCES ECONOMIQUES', 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', 1, 1, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(470, '19439', 'FATOUMATA DIARAYE BAH', NULL, NULL, NULL, '09-29-88', 'M', 'OUMAR et de M\'MAHAWA DRAME', 'FIDEL CASTRO RATOMA', 'GRANDE ECOLE', 'SE', 'SS0225669607', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(471, '12222', 'KABINET KEITA', NULL, NULL, NULL, '09-30-88', 'F', 'OUMAR et de M\'MAHAWA DRAME', 'EP DABOMPA', 'LYCEE D\'EXCELLENCE', 'SE', 'SS0225669608', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(472, '19190', 'FALILOU DIALLO', NULL, NULL, NULL, '10-01-88', 'F', 'OUMAR et de M\'MAHAWA DRAME', 'FIDEL CASTRO RATOMA', 'SABADOU', 'SE', 'SS0225669609', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(473, '16012', 'SOUADOU DIALLO', NULL, NULL, NULL, '10-02-88', 'M', 'OUMAR et de M\'MAHAWA DRAME', 'MAMADOU SIDIBE', 'LYCEE EL BOUBACAR BIRO', 'SM', 'SS0225669610', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(474, '19204', 'FANTA CONDE', NULL, NULL, NULL, '10-03-88', 'F', 'OUMAR et de M\'MAHAWA DRAME', 'FIDEL CASTRO RATOMA', 'SYLLA LAMINE', 'SM', 'SS0225669611', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(475, '22519', 'OUSMANE BAH', NULL, NULL, NULL, '10-04-88', 'M', 'OUMAR et de M\'MAHAWA DRAME', 'MAHATMA_GANDHI', 'AMADOU SADIGOU DIALLO', 'SM', 'SS0225669612', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(476, '19296', 'FATOUMATA BINTA 1 BARRY', NULL, NULL, NULL, '10-05-88', 'F', 'OUMAR et de M\'MAHAWA DRAME', 'FIDEL CASTRO RATOMA', 'VAN VOLLENHOVEN', 'SM', 'SS0225669613', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1'),
	(477, '14968', 'OUSMANE BAILO DIALLO', NULL, NULL, NULL, '10-06-88', 'M', 'OUMAR et de M\'MAHAWA DRAME', 'LYCEE M\'BALIA CAMARA', 'MICHELLE OBAMA', 'SM', 'SS0225669614', 'Boursier', NULL, NULL, 'SCIENCES ECONOMIQUES', 'Univ  Kindia', '11', '2018', '', NULL, NULL, '', 'GuinÃ©enne', 'Licence 1', '1');
/*!40000 ALTER TABLE `admin_bac` ENABLE KEYS */;

-- Export de la structure de la table universitedb. batiment
CREATE TABLE IF NOT EXISTS `batiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blocs_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F5FAB00C7C40FD7C` (`blocs_id`),
  CONSTRAINT `FK_F5FAB00C7C40FD7C` FOREIGN KEY (`blocs_id`) REFERENCES `blocs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.batiment : ~0 rows (environ)
/*!40000 ALTER TABLE `batiment` DISABLE KEYS */;
INSERT INTO `batiment` (`id`, `blocs_id`, `name`) VALUES
	(1, 1, 'Batiment A');
/*!40000 ALTER TABLE `batiment` ENABLE KEYS */;

-- Export de la structure de la table universitedb. blocs
CREATE TABLE IF NOT EXISTS `blocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombreBatment` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.blocs : ~0 rows (environ)
/*!40000 ALTER TABLE `blocs` DISABLE KEYS */;
INSERT INTO `blocs` (`id`, `name`, `nombreBatment`) VALUES
	(1, 'Bloc A', '3');
/*!40000 ALTER TABLE `blocs` ENABLE KEYS */;

-- Export de la structure de la table universitedb. calendrier
CREATE TABLE IF NOT EXISTS `calendrier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matiere_id` int(11) NOT NULL,
  `enseignant_id` int(11) NOT NULL,
  `departement_id` int(11) NOT NULL,
  `salle_id` int(11) NOT NULL,
  `jour_id` int(11) NOT NULL,
  `debut` time NOT NULL,
  `fin` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B2753CB9F46CD258` (`matiere_id`),
  KEY `IDX_B2753CB9E455FCC0` (`enseignant_id`),
  KEY `IDX_B2753CB9CCF9E01E` (`departement_id`),
  KEY `IDX_B2753CB9DC304035` (`salle_id`),
  KEY `IDX_B2753CB9220C6AD0` (`jour_id`),
  CONSTRAINT `FK_B2753CB9220C6AD0` FOREIGN KEY (`jour_id`) REFERENCES `jour_etude` (`id`),
  CONSTRAINT `FK_B2753CB9CCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`),
  CONSTRAINT `FK_B2753CB9DC304035` FOREIGN KEY (`salle_id`) REFERENCES `salles` (`id`),
  CONSTRAINT `FK_B2753CB9E455FCC0` FOREIGN KEY (`enseignant_id`) REFERENCES `enseignant` (`id`),
  CONSTRAINT `FK_B2753CB9F46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.calendrier : ~0 rows (environ)
/*!40000 ALTER TABLE `calendrier` DISABLE KEYS */;
INSERT INTO `calendrier` (`id`, `matiere_id`, `enseignant_id`, `departement_id`, `salle_id`, `jour_id`, `debut`, `fin`) VALUES
	(1, 10, 2, 5, 1, 1, '08:00:00', '11:00:00');
/*!40000 ALTER TABLE `calendrier` ENABLE KEYS */;

-- Export de la structure de la table universitedb. concentration
CREATE TABLE IF NOT EXISTS `concentration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departement_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unisite_conentration_departement` (`name`,`departement_id`),
  KEY `IDX_1CC78D18CCF9E01E` (`departement_id`),
  CONSTRAINT `FK_1CC78D18CCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.concentration : ~25 rows (environ)
/*!40000 ALTER TABLE `concentration` DISABLE KEYS */;
INSERT INTO `concentration` (`id`, `departement_id`, `name`) VALUES
	(25, 2, 'chimie organique'),
	(18, 7, 'Comptabilité Analytique d\'exploitation I'),
	(19, 7, 'Comptabilité Analytique d\'exploitation II'),
	(22, 7, 'Comptabilité des Entreprises'),
	(23, 7, 'Comptabilité des Sociétés'),
	(4, 5, 'Conjoncture Economique et Financière'),
	(24, 7, 'Contrôle de Gesrtion'),
	(17, 7, 'Droit des Affaires'),
	(5, 5, 'Econometrie II'),
	(3, 5, 'Economie Financière'),
	(7, 5, 'Economie Industrielle'),
	(10, 5, 'Finance Internationale'),
	(21, 7, 'Fiscalité des Entreprises'),
	(8, 5, 'Fiscalité Générale'),
	(12, 5, 'Gestion des Operations de production'),
	(15, 7, 'Gestion des Operations de production'),
	(13, 5, 'Gestion des Ressources Humaines'),
	(16, 7, 'Gestion des Ressources Humaines'),
	(20, 7, 'Gestion et Analyse Financière II'),
	(9, 5, 'Intelligence Economique'),
	(11, 5, 'Marketing Stratégique'),
	(14, 7, 'Marketing Stratégique'),
	(6, 5, 'Politique Economique'),
	(1, 5, 'Recheche operationnelle'),
	(2, 5, 'Relations Economiques Internationale');
/*!40000 ALTER TABLE `concentration` ENABLE KEYS */;

-- Export de la structure de la table universitedb. conge
CREATE TABLE IF NOT EXISTS `conge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeConge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2ED893486FB37687` (`typeConge_id`),
  CONSTRAINT `FK_2ED893486FB37687` FOREIGN KEY (`typeConge_id`) REFERENCES `type_conge` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.conge : ~0 rows (environ)
/*!40000 ALTER TABLE `conge` DISABLE KEYS */;
INSERT INTO `conge` (`id`, `titre`, `etat`, `contenu`, `typeConge_id`) VALUES
	(1, 'test', 0, '<p>test</p>', 3);
/*!40000 ALTER TABLE `conge` ENABLE KEYS */;

-- Export de la structure de la table universitedb. departement
CREATE TABLE IF NOT EXISTS `departement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facute_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C1765B635E237E06` (`name`),
  UNIQUE KEY `UNIQ_C1765B6377153098` (`code`),
  KEY `IDX_C1765B63AD2ADBB3` (`facute_id`),
  CONSTRAINT `FK_C1765B63AD2ADBB3` FOREIGN KEY (`facute_id`) REFERENCES `facultes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.departement : ~14 rows (environ)
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
INSERT INTO `departement` (`id`, `facute_id`, `name`, `code`) VALUES
	(1, 1, 'BIOLOGIE', '21'),
	(2, 1, 'CHIMIE', '22'),
	(3, 1, 'PHYSIQUE', '23'),
	(4, 1, 'MATHEMATIQUES', '24'),
	(5, 2, 'SCIENCES ECONOMIQUES', '11'),
	(6, 2, 'SCIENCES COMPTABLES', '12'),
	(7, 2, 'ADMINISTRATION DES AFFAIRES', '13'),
	(8, 3, 'SOCIOLOGIE', '31'),
	(9, 3, 'HISTOIRE', '32'),
	(10, 3, 'GEOGRAPHIE', '33'),
	(11, 3, 'PHILOSOPHIE', '34'),
	(12, 4, 'LETTRES MODERNES', '41'),
	(13, 4, 'SCIENCES DU LANGAGE', '42'),
	(14, 4, 'ANGLAIS', '43');
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;

-- Export de la structure de la table universitedb. detail_conge
CREATE TABLE IF NOT EXISTS `detail_conge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conge_id` int(11) DEFAULT NULL,
  `etudiant_id` int(11) DEFAULT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime DEFAULT NULL,
  `Sessions_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_53285811CAAC9A59` (`conge_id`),
  KEY `IDX_53285811DDEAB1A3` (`etudiant_id`),
  KEY `IDX_5328581177E0C25B` (`Sessions_id`),
  CONSTRAINT `FK_5328581177E0C25B` FOREIGN KEY (`Sessions_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_53285811CAAC9A59` FOREIGN KEY (`conge_id`) REFERENCES `conge` (`id`),
  CONSTRAINT `FK_53285811DDEAB1A3` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.detail_conge : ~0 rows (environ)
/*!40000 ALTER TABLE `detail_conge` DISABLE KEYS */;
INSERT INTO `detail_conge` (`id`, `conge_id`, `etudiant_id`, `dateDebut`, `dateFin`, `Sessions_id`) VALUES
	(1, 1, 8, '2018-11-06 00:00:00', '2018-11-06 00:00:00', 19);
/*!40000 ALTER TABLE `detail_conge` ENABLE KEYS */;

-- Export de la structure de la table universitedb. detail_notation
CREATE TABLE IF NOT EXISTS `detail_notation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matieres_id` int(11) DEFAULT NULL,
  `notation_id` int(11) DEFAULT NULL,
  `moyenneCours` double DEFAULT NULL,
  `moyenneExament` double DEFAULT NULL,
  `session1` double DEFAULT NULL,
  `session2` double DEFAULT NULL,
  `dateNotation` datetime DEFAULT NULL,
  `dateLeverSession` datetime DEFAULT NULL,
  `dateDelaitNotation` datetime DEFAULT NULL,
  `flagSession1` tinyint(1) DEFAULT NULL,
  `flagDelait` tinyint(1) DEFAULT NULL,
  `flagSession2` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F4B6554E82350831` (`matieres_id`),
  KEY `IDX_F4B6554E9680B7F7` (`notation_id`),
  CONSTRAINT `FK_F4B6554E82350831` FOREIGN KEY (`matieres_id`) REFERENCES `matiers` (`id`),
  CONSTRAINT `FK_F4B6554E9680B7F7` FOREIGN KEY (`notation_id`) REFERENCES `notation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.detail_notation : ~0 rows (environ)
/*!40000 ALTER TABLE `detail_notation` DISABLE KEYS */;
/*!40000 ALTER TABLE `detail_notation` ENABLE KEYS */;

-- Export de la structure de la table universitedb. detail_pecul
CREATE TABLE IF NOT EXISTS `detail_pecul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pecule_id` int(11) DEFAULT NULL,
  `mois_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_12250E03A62A32F1` (`pecule_id`),
  KEY `IDX_12250E03FA0749B8` (`mois_id`),
  CONSTRAINT `FK_12250E03A62A32F1` FOREIGN KEY (`pecule_id`) REFERENCES `pecule` (`id`),
  CONSTRAINT `FK_12250E03FA0749B8` FOREIGN KEY (`mois_id`) REFERENCES `mois` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.detail_pecul : ~0 rows (environ)
/*!40000 ALTER TABLE `detail_pecul` DISABLE KEYS */;
/*!40000 ALTER TABLE `detail_pecul` ENABLE KEYS */;

-- Export de la structure de la table universitedb. dossier
CREATE TABLE IF NOT EXISTS `dossier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3D48E0373DA5256D` (`image_id`),
  CONSTRAINT `FK_3D48E0373DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.dossier : ~0 rows (environ)
/*!40000 ALTER TABLE `dossier` DISABLE KEYS */;
INSERT INTO `dossier` (`id`, `image_id`) VALUES
	(3, 19);
/*!40000 ALTER TABLE `dossier` ENABLE KEYS */;

-- Export de la structure de la table universitedb. emploie
CREATE TABLE IF NOT EXISTS `emploie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salles_id` int(11) DEFAULT NULL,
  `matiere_id` int(11) DEFAULT NULL,
  `sessions_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `heurDebut` time NOT NULL,
  `heurFin` time NOT NULL,
  `interval` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datejour` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B2C6C1E8B11E4946` (`salles_id`),
  KEY `IDX_B2C6C1E8F46CD258` (`matiere_id`),
  KEY `IDX_B2C6C1E8F17C4D8C` (`sessions_id`),
  KEY `IDX_B2C6C1E8AFC2B591` (`module_id`),
  CONSTRAINT `FK_B2C6C1E8AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `FK_B2C6C1E8B11E4946` FOREIGN KEY (`salles_id`) REFERENCES `salles` (`id`),
  CONSTRAINT `FK_B2C6C1E8F17C4D8C` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_B2C6C1E8F46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.emploie : ~0 rows (environ)
/*!40000 ALTER TABLE `emploie` DISABLE KEYS */;
/*!40000 ALTER TABLE `emploie` ENABLE KEYS */;

-- Export de la structure de la table universitedb. enseignant
CREATE TABLE IF NOT EXISTS `enseignant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int(11) DEFAULT NULL,
  `departement_id` int(11) DEFAULT NULL,
  `contact` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diplome` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GradeAcademique_id` int(11) DEFAULT NULL,
  `typeEnseignant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_81A72FA1450FF010` (`telephone`),
  UNIQUE KEY `UNIQ_81A72FA1E7927C74` (`email`),
  UNIQUE KEY `UNIQ_81A72FA1FB88E14F` (`utilisateur_id`),
  KEY `IDX_81A72FA1CCF9E01E` (`departement_id`),
  KEY `IDX_81A72FA18A24FF74` (`GradeAcademique_id`),
  KEY `IDX_81A72FA18A336F99` (`typeEnseignant_id`),
  CONSTRAINT `FK_81A72FA18A24FF74` FOREIGN KEY (`GradeAcademique_id`) REFERENCES `grade_academique` (`id`),
  CONSTRAINT `FK_81A72FA18A336F99` FOREIGN KEY (`typeEnseignant_id`) REFERENCES `type_enseignant` (`id`),
  CONSTRAINT `FK_81A72FA1CCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`),
  CONSTRAINT `FK_81A72FA1FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.enseignant : ~2 rows (environ)
/*!40000 ALTER TABLE `enseignant` DISABLE KEYS */;
INSERT INTO `enseignant` (`id`, `utilisateur_id`, `departement_id`, `contact`, `telephone`, `email`, `name`, `prenom`, `diplome`, `GradeAcademique_id`, `typeEnseignant_id`) VALUES
	(2, 11, 5, 'gbessia', '68745223', 'bah3@gmail.com', 'Bah', 'Moussa', 'licence', 1, 2),
	(3, 20, 5, 'Gbessia', '06211346934', 'kant4476@gmail.com', 'Kante4', 'Mohamed4', 'Master', 1, 1);
/*!40000 ALTER TABLE `enseignant` ENABLE KEYS */;

-- Export de la structure de la table universitedb. enseigner
CREATE TABLE IF NOT EXISTS `enseigner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enseignant_id` int(11) DEFAULT NULL,
  `matiers_id` int(11) DEFAULT NULL,
  `Sessions_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matiere_session_unique` (`matiers_id`,`Sessions_id`),
  KEY `IDX_663E85CDE455FCC0` (`enseignant_id`),
  KEY `IDX_663E85CD811EDA1B` (`matiers_id`),
  KEY `IDX_663E85CD77E0C25B` (`Sessions_id`),
  CONSTRAINT `FK_663E85CD77E0C25B` FOREIGN KEY (`Sessions_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_663E85CD811EDA1B` FOREIGN KEY (`matiers_id`) REFERENCES `matiers` (`id`),
  CONSTRAINT `FK_663E85CDE455FCC0` FOREIGN KEY (`enseignant_id`) REFERENCES `enseignant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.enseigner : ~10 rows (environ)
/*!40000 ALTER TABLE `enseigner` DISABLE KEYS */;
INSERT INTO `enseigner` (`id`, `enseignant_id`, `matiers_id`, `Sessions_id`) VALUES
	(20, 2, 10, 19),
	(21, 2, 8, 19),
	(23, 2, 9, 19),
	(24, 2, 3, 19),
	(25, 2, 6, 19),
	(26, 3, 5, 19),
	(27, 3, 2, 19),
	(28, 3, 7, 19),
	(29, 3, 4, 19),
	(30, 3, 1, 19);
/*!40000 ALTER TABLE `enseigner` ENABLE KEYS */;

-- Export de la structure de la table universitedb. etudiants
CREATE TABLE IF NOT EXISTS `etudiants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) DEFAULT NULL,
  `departement_id` int(11) DEFAULT NULL,
  `licence_id` int(11) DEFAULT NULL,
  `semestre_id` int(11) DEFAULT NULL,
  `roles_id` int(11) DEFAULT NULL,
  `concentration_id` int(11) DEFAULT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `matricule` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statut` longtext COLLATE utf8mb4_unicode_ci,
  `nomPrenom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` longtext COLLATE utf8mb4_unicode_ci,
  `filiation` longtext COLLATE utf8mb4_unicode_ci,
  `contact` longtext COLLATE utf8mb4_unicode_ci,
  `nationnalite` longtext COLLATE utf8mb4_unicode_ci,
  `dateNaissance` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_227C02EB12B2DC9C` (`matricule`),
  UNIQUE KEY `UNIQ_227C02EB3DA5256D` (`image_id`),
  UNIQUE KEY `UNIQ_227C02EBFB88E14F` (`utilisateur_id`),
  KEY `IDX_227C02EBCCF9E01E` (`departement_id`),
  KEY `IDX_227C02EB26EF07C9` (`licence_id`),
  KEY `IDX_227C02EB5577AFDB` (`semestre_id`),
  KEY `IDX_227C02EB38C751C4` (`roles_id`),
  KEY `IDX_227C02EBFF5986F1` (`concentration_id`),
  CONSTRAINT `FK_227C02EB26EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`),
  CONSTRAINT `FK_227C02EB38C751C4` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_227C02EB3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media` (`id`),
  CONSTRAINT `FK_227C02EB5577AFDB` FOREIGN KEY (`semestre_id`) REFERENCES `semetre` (`id`),
  CONSTRAINT `FK_227C02EBCCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`),
  CONSTRAINT `FK_227C02EBFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `FK_227C02EBFF5986F1` FOREIGN KEY (`concentration_id`) REFERENCES `concentration` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.etudiants : ~4 rows (environ)
/*!40000 ALTER TABLE `etudiants` DISABLE KEYS */;
INSERT INTO `etudiants` (`id`, `image_id`, `departement_id`, `licence_id`, `semestre_id`, `roles_id`, `concentration_id`, `utilisateur_id`, `matricule`, `statut`, `nomPrenom`, `sexe`, `filiation`, `contact`, `nationnalite`, `dateNaissance`) VALUES
	(7, 20, 5, 1, NULL, NULL, NULL, 16, '2113118181840H', 'Boursier', 'MARIAMA KESSO BAH', 'M', 'KOUNA et de MAMADAMA TOURE', '', 'GuinÃ©enne', '09-12-92'),
	(8, 23, 5, 1, NULL, NULL, NULL, 17, '2116761189326H', 'Boursier', 'ABDOULAYE DJENAB SOUMAH', 'Masculin', 'IDRISSA et de MARIMA KANE', NULL, 'GuinÃ©enne', '09-18-92'),
	(9, 21, 5, 1, NULL, NULL, NULL, 18, '2111102184748Y', 'Boursier', 'FATOUMATA GUIRASSY', 'F', 'IDRISSA et de MARIMA KANE', '', 'GuinÃ©enne', '09-23-88'),
	(10, 22, 5, 2, NULL, NULL, NULL, 19, '2111175189114E', 'Boursier', 'IBRAHIMA DRAME', 'M', 'OUMAR et de M\'MAHAWA DRAME', '', 'GuinÃ©enne', '09-28-88');
/*!40000 ALTER TABLE `etudiants` ENABLE KEYS */;

-- Export de la structure de la table universitedb. ext_log_entries
CREATE TABLE IF NOT EXISTS `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_class` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Export de données de la table universitedb.ext_log_entries : ~0 rows (environ)
/*!40000 ALTER TABLE `ext_log_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ext_log_entries` ENABLE KEYS */;

-- Export de la structure de la table universitedb. ext_translations
CREATE TABLE IF NOT EXISTS `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_class` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Export de données de la table universitedb.ext_translations : ~0 rows (environ)
/*!40000 ALTER TABLE `ext_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ext_translations` ENABLE KEYS */;

-- Export de la structure de la table universitedb. facultes
CREATE TABLE IF NOT EXISTS `facultes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_12BB141C5E237E06` (`name`),
  UNIQUE KEY `UNIQ_12BB141C77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.facultes : ~4 rows (environ)
/*!40000 ALTER TABLE `facultes` DISABLE KEYS */;
INSERT INTO `facultes` (`id`, `name`, `code`) VALUES
	(1, 'FACULTE DES SCIENCES', '20'),
	(2, 'FACULTE DES SCIENCES ECONOMIQUES ET DE GESTION', '10'),
	(3, 'FACULTE DES SCIENCES SOCIALES', '30'),
	(4, 'FACULTE DES LANGUES ET LETTRES', '40');
/*!40000 ALTER TABLE `facultes` ENABLE KEYS */;

-- Export de la structure de la table universitedb. fonction
CREATE TABLE IF NOT EXISTS `fonction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_900D5BD5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.fonction : ~5 rows (environ)
/*!40000 ALTER TABLE `fonction` DISABLE KEYS */;
INSERT INTO `fonction` (`id`, `name`) VALUES
	(5, 'Chef Comptable'),
	(1, 'Chef de Departement'),
	(4, 'Chef Service de Solarite'),
	(3, 'Doyen Faculte'),
	(2, 'Recteur');
/*!40000 ALTER TABLE `fonction` ENABLE KEYS */;

-- Export de la structure de la table universitedb. frais_inscription
CREATE TABLE IF NOT EXISTS `frais_inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `montant` double NOT NULL,
  `montantReinscription` double NOT NULL,
  `encour` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.frais_inscription : ~0 rows (environ)
/*!40000 ALTER TABLE `frais_inscription` DISABLE KEYS */;
INSERT INTO `frais_inscription` (`id`, `montant`, `montantReinscription`, `encour`) VALUES
	(1, 250000, 350000, NULL);
/*!40000 ALTER TABLE `frais_inscription` ENABLE KEYS */;

-- Export de la structure de la table universitedb. grade_academique
CREATE TABLE IF NOT EXISTS `grade_academique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7F932C945E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.grade_academique : ~4 rows (environ)
/*!40000 ALTER TABLE `grade_academique` DISABLE KEYS */;
INSERT INTO `grade_academique` (`id`, `name`) VALUES
	(1, 'ASSISTANT'),
	(2, 'MAÎTRE ASSISTANT'),
	(3, 'MAÎTRE DE CONFÉRENCE'),
	(4, 'PROFESSEUR');
/*!40000 ALTER TABLE `grade_academique` ENABLE KEYS */;

-- Export de la structure de la table universitedb. historique_affectation_matiere
CREATE TABLE IF NOT EXISTS `historique_affectation_matiere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.historique_affectation_matiere : ~0 rows (environ)
/*!40000 ALTER TABLE `historique_affectation_matiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `historique_affectation_matiere` ENABLE KEYS */;

-- Export de la structure de la table universitedb. inscription
CREATE TABLE IF NOT EXISTS `inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concentration_id` int(11) DEFAULT NULL,
  `etudiant_id` int(11) DEFAULT NULL,
  `paiement_id` int(11) DEFAULT NULL,
  `licence_id` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `dateInscription` datetime NOT NULL,
  `annee` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5E90F6D62A4C4478` (`paiement_id`),
  UNIQUE KEY `unisite_inscription_niveau_Session_Etudiant` (`etudiant_id`,`licence_id`,`session_id`),
  UNIQUE KEY `unisite_inscription_session_etudiant` (`etudiant_id`,`session_id`),
  KEY `IDX_5E90F6D6FF5986F1` (`concentration_id`),
  KEY `IDX_5E90F6D6DDEAB1A3` (`etudiant_id`),
  KEY `IDX_5E90F6D626EF07C9` (`licence_id`),
  KEY `IDX_5E90F6D6613FECDF` (`session_id`),
  CONSTRAINT `FK_5E90F6D626EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`),
  CONSTRAINT `FK_5E90F6D62A4C4478` FOREIGN KEY (`paiement_id`) REFERENCES `paiement` (`id`),
  CONSTRAINT `FK_5E90F6D6613FECDF` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_5E90F6D6DDEAB1A3` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiants` (`id`),
  CONSTRAINT `FK_5E90F6D6FF5986F1` FOREIGN KEY (`concentration_id`) REFERENCES `concentration` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.inscription : ~8 rows (environ)
/*!40000 ALTER TABLE `inscription` DISABLE KEYS */;
INSERT INTO `inscription` (`id`, `concentration_id`, `etudiant_id`, `paiement_id`, `licence_id`, `session_id`, `dateInscription`, `annee`) VALUES
	(8, NULL, 7, NULL, 1, 19, '2018-11-06 00:00:00', '2018'),
	(9, NULL, 8, NULL, 1, 19, '2018-11-06 00:00:00', '2018'),
	(10, NULL, 9, NULL, 1, 19, '2018-11-06 00:00:00', '2018'),
	(11, NULL, 10, NULL, 1, 19, '2018-11-06 00:00:00', '2018'),
	(12, NULL, 7, NULL, 1, 20, '2018-11-07 02:05:51', '2018'),
	(13, NULL, 8, NULL, 1, 20, '2018-11-07 02:06:01', '2018'),
	(14, NULL, 9, NULL, 1, 20, '2018-11-07 02:06:10', '2018'),
	(15, 2, 10, NULL, 2, 20, '2018-11-07 02:06:30', '2018'),
	(16, NULL, 7, NULL, 1, 22, '2019-01-14 20:45:53', '2019');
/*!40000 ALTER TABLE `inscription` ENABLE KEYS */;

-- Export de la structure de la table universitedb. jour_etude
CREATE TABLE IF NOT EXISTS `jour_etude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1FF608576C6E55B5` (`nom`),
  UNIQUE KEY `UNIQ_1FF6085777153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.jour_etude : ~7 rows (environ)
/*!40000 ALTER TABLE `jour_etude` DISABLE KEYS */;
INSERT INTO `jour_etude` (`id`, `nom`, `code`) VALUES
	(1, 'Lundi', 'lun'),
	(2, 'Mardi', 'mar'),
	(3, 'Mercredi', 'mer'),
	(4, 'Jeudi', 'jeu'),
	(5, 'Vendredi', 'ven'),
	(6, 'Samedi', 'sam'),
	(7, 'Dimanche', 'dim');
/*!40000 ALTER TABLE `jour_etude` ENABLE KEYS */;

-- Export de la structure de la table universitedb. licence
CREATE TABLE IF NOT EXISTS `licence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1DAAE6485E237E06` (`name`),
  UNIQUE KEY `UNIQ_1DAAE64877153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.licence : ~3 rows (environ)
/*!40000 ALTER TABLE `licence` DISABLE KEYS */;
INSERT INTO `licence` (`id`, `name`, `code`) VALUES
	(1, 'LICENCE 1', '1'),
	(2, 'LICENCE 2', '2'),
	(3, 'LICENCE 3', '3');
/*!40000 ALTER TABLE `licence` ENABLE KEYS */;

-- Export de la structure de la table universitedb. matiers
CREATE TABLE IF NOT EXISTS `matiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departement_id` int(11) DEFAULT NULL,
  `semetre_id` int(11) DEFAULT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombreCredit` int(11) DEFAULT NULL,
  `typeMatiere` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombreHeure` int(11) NOT NULL,
  `baremInf` double DEFAULT NULL,
  `baremSup` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_64C0E21B77153098` (`code`),
  UNIQUE KEY `unisite_matiers_departement_semestre` (`departement_id`,`semetre_id`,`name`),
  KEY `IDX_64C0E21BCCF9E01E` (`departement_id`),
  KEY `IDX_64C0E21BFC39A345` (`semetre_id`),
  CONSTRAINT `FK_64C0E21BCCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`),
  CONSTRAINT `FK_64C0E21BFC39A345` FOREIGN KEY (`semetre_id`) REFERENCES `semetre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.matiers : ~45 rows (environ)
/*!40000 ALTER TABLE `matiers` DISABLE KEYS */;
INSERT INTO `matiers` (`id`, `departement_id`, `semetre_id`, `code`, `nombreCredit`, `typeMatiere`, `name`, `nombreHeure`, `baremInf`, `baremSup`) VALUES
	(1, 5, 2, 'MI4457SC111', NULL, 'Fondamentale', 'Micro économie', 48, 3.5, 5),
	(2, 5, 2, 'MA2465SC112', NULL, 'Fondamentale', 'Macro économie', 48, 3.5, 5),
	(3, 5, 1, 'EC5069SC113', NULL, 'Fondamentale', 'Economie Générale', 48, 3.5, 5),
	(4, 5, 2, 'ME9606SC114', NULL, 'Fondamentale', 'Methodes Statistiques', 48, 3.5, 5),
	(5, 5, 2, 'HI4311SC115', NULL, 'Fondamentale', 'Histoire des faits économiques et sociaux', 48, 3.5, 5),
	(6, 5, 1, 'FI1925SC116', NULL, 'Fondamentale', 'Finances publiques', 48, 3.5, 5),
	(7, 5, 2, 'MA5298SC117', NULL, 'Fondamentale', 'Mathématiques économiques', 48, 3.5, 5),
	(8, 5, 1, 'CO8491SC118', NULL, 'Fondamentale', 'Contexte Econmique et social', 48, 3.5, 5),
	(9, 5, 1, 'EC4166SC119', NULL, 'Fondamentale', 'Economie d\'entreprise', 48, 3.5, 5),
	(10, 5, 1, 'CO6319SC1110', NULL, 'Fondamentale', 'Comptabilité générale', 48, 3.5, 5),
	(11, 5, 3, 'MA4230SC1111', NULL, 'Fondamentale', 'Mathématique financière', 48, 3.5, 5),
	(12, 5, 3, 'GE9033SC1112', NULL, 'Fondamentale', 'Gestion et Analyse financière I', 48, 3.5, 5),
	(13, 5, 3, 'EC4381SC1113', NULL, 'Fondamentale', 'Economie et Commerce International', 48, 3.5, 5),
	(14, 5, 3, 'MO8964SC1114', NULL, 'Fondamentale', 'Monnaie et Institutions financière', 48, 3.5, 5),
	(15, 5, 3, 'EC6825SC1115', NULL, 'Fondamentale', 'Economie de l\'environnement', 48, 3.5, 5),
	(16, 5, 4, 'EC5804SC1116', NULL, 'Fondamentale', 'Econometrie I', 48, 3.5, 5),
	(17, 5, 4, 'TE1338SC1117', NULL, 'Fondamentale', 'Techniques bancaires', 48, 3.5, 5),
	(18, 5, 4, 'CO2127SC1118', 6, 'Fondamentale', 'Comptabilité Nationale', 48, 3.5, 5),
	(19, 5, 4, 'EC1283SC1119', NULL, 'Fondamentale', 'Economie du developpement', 48, 3.5, 5),
	(20, 5, 4, 'GE7690SC1120', NULL, 'Fondamentale', 'Gestion des projets', 48, 3.5, 5),
	(21, 5, 5, 'RE8320SC1121', NULL, 'Fondamentale', 'Recherche Operationnelle', 48, 3.5, 5),
	(22, 5, 5, 'RE3024SC1122', NULL, 'Fondamentale', 'Relations Economiques Internationales', 48, 3.5, 5),
	(23, 5, 5, 'EC5087SC1123', NULL, 'Fondamentale', 'Economie financière', 48, 3.5, 5),
	(24, 5, 5, 'CO3679SC1124', NULL, 'Fondamentale', 'Conjoncture Economique et financière', 48, 3.5, 5),
	(25, 5, 5, 'EC9068SC1125', NULL, 'Fondamentale', 'Econometrie II', 48, 3.5, 5),
	(26, 5, 6, 'PO7816SC1126', NULL, 'Fondamentale', 'Politique Economique', 48, 3.5, 5),
	(27, 5, 6, 'EC3149SC1127', NULL, 'Fondamentale', 'Economie Industrielle', 48, 3.5, 5),
	(28, 5, 6, 'FI7802SC1128', NULL, 'Fondamentale', 'Fiscalité générale', 48, 3.5, 5),
	(29, 5, 6, 'IN8207SC1129', NULL, 'Fondamentale', 'Intelligence Economique', 48, 3.5, 5),
	(30, 5, 6, 'FI7254SC1130', NULL, 'Fondamentale', 'Finance Internationale', 48, 3.5, 5),
	(31, 7, 5, 'MA8333AD1331', NULL, '', 'Marketing Stratégique et Operationnel', 48, 3.5, 5),
	(32, 7, 5, 'GE3535AD1332', NULL, '', 'Gestion des Operations de production', 48, 3.5, 5),
	(33, 5, 5, 'GE4625SC1133', NULL, 'Fondamentale', 'Gestion des Ressources Humaines', 48, 3.5, 5),
	(34, 7, 5, 'DR8796AD1334', NULL, '', 'Droit des Affaires', 48, 3.5, 5),
	(35, 7, 5, 'CO2141AD1335', NULL, '', 'Comptabilité Analytique d\'exploitation I', 48, 3.5, 5),
	(36, 7, 6, 'CO8905AD1336', NULL, '', 'Comptabilité Analytique d\'exploitation II', 48, 3.5, 5),
	(37, 7, 6, 'GE2317AD1337', NULL, '', 'Gestion et Analyse financière', 48, 3.5, 5),
	(38, 7, 6, 'FI7709AD1338', NULL, '', 'Fiscalité des Entreprises', 48, 3.5, 5),
	(39, 7, 6, 'CO4509AD1339', NULL, '', 'Comptabilité des sociétés', 48, 3.5, 5),
	(40, 7, 6, 'CO5790AD1340', NULL, '', 'Contrôle de Gestion', 48, 3.5, 5),
	(41, 10, 1, 'G', NULL, '', 'Géographie Général', 51, 3.5, 5),
	(42, 14, 1, 'AN3829AN4342', NULL, '', 'Anglais de Base', 60, 3.5, 5),
	(43, 14, 1, 'AN6595AN4343', NULL, '', 'Anglais niveau intermediare', 65, 3.5, 5),
	(44, 14, 1, 'AN1702AN4344', NULL, '', 'Anglais niveau avance', 72, 3.5, 5),
	(45, 14, 1, 'EN1049AN4345', NULL, '', 'ENglish for Expert', 72, 3.5, 5);
/*!40000 ALTER TABLE `matiers` ENABLE KEYS */;

-- Export de la structure de la table universitedb. media
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `path` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6A2CA10C611C0C56` (`dossier_id`),
  CONSTRAINT `FK_6A2CA10C611C0C56` FOREIGN KEY (`dossier_id`) REFERENCES `dossier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.media : ~10 rows (environ)
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` (`id`, `dossier_id`, `updated_at`, `path`) VALUES
	(3, NULL, '2018-11-04 00:05:47', 'f17df5c7bdbde1666eeb7a20d48d868278492e97.png'),
	(4, NULL, '2018-11-04 00:16:08', 'ad2e3ae95ca289b7a2b037f9ddbf2fcc46473f9d.png'),
	(5, NULL, '2018-11-04 00:16:53', '26bb2cec8542fe4ecd3b9662443025bc88dad901.png'),
	(6, NULL, '2018-11-04 00:13:06', 'a488e84268cc807c38ca94a4b20db744d8aac7f3.png'),
	(7, NULL, '2018-11-04 00:15:39', 'a9def21c63acef29fb1f460a347b9a777b873c8b.png'),
	(19, NULL, '2018-11-06 14:41:52', '533557d23494f4cee2126d4e0d7af956130241b5.xlsx'),
	(20, NULL, '2018-11-06 15:12:51', '79daead1a26bbe5cff0532a977a73224c8254ed1.1541517163.jpg'),
	(21, NULL, '2018-11-06 15:16:16', '15cfc54f0279928d28152f6be34f16b177e0442c.1541517371.jpg'),
	(22, NULL, '2018-11-06 15:17:31', '5598c2c1221785984d02983085e8c5c4ebe7bc19.1541517443.jpg'),
	(23, NULL, '2018-11-06 22:13:44', '036be254e299b5e26a0afa7225cd08178e21513f.png');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;

-- Export de la structure de la table universitedb. module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.module : ~2 rows (environ)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `name`) VALUES
	(1, 'Module 1'),
	(4, 'Module 2');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Export de la structure de la table universitedb. mois
CREATE TABLE IF NOT EXISTS `mois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D6B08CB75E237E06` (`name`),
  UNIQUE KEY `UNIQ_D6B08CB777153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.mois : ~12 rows (environ)
/*!40000 ALTER TABLE `mois` DISABLE KEYS */;
INSERT INTO `mois` (`id`, `name`, `code`) VALUES
	(1, 'Janvier', 1),
	(2, 'Fevrier', 2),
	(3, 'Mars', 3),
	(4, 'Avril', 4),
	(5, 'Mai', 5),
	(6, 'Juin', 6),
	(7, 'Juillet', 7),
	(8, 'Aout', 8),
	(9, 'Septembre', 9),
	(10, 'Octobre', 10),
	(11, 'Novembre', 11),
	(12, 'Decembre', 12);
/*!40000 ALTER TABLE `mois` ENABLE KEYS */;

-- Export de la structure de la table universitedb. notation
CREATE TABLE IF NOT EXISTS `notation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `etudiant_id` int(11) DEFAULT NULL,
  `licence_id` int(11) DEFAULT NULL,
  `matieres_id` int(11) DEFAULT NULL,
  `moyenneCours` double DEFAULT NULL,
  `moyenneExament` double DEFAULT NULL,
  `session1` double DEFAULT NULL,
  `session2` double DEFAULT NULL,
  `dateNotation` datetime DEFAULT NULL,
  `dateLeverSession` datetime DEFAULT NULL,
  `dateDelaitNotation` datetime DEFAULT NULL,
  `flagSession1` tinyint(1) DEFAULT NULL,
  `flagDelait` tinyint(1) DEFAULT NULL,
  `flagSession2` tinyint(1) DEFAULT NULL,
  `Semestre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unisite_notation_etudiant_matiere` (`etudiant_id`,`matieres_id`,`session_id`),
  KEY `IDX_268BC95613FECDF` (`session_id`),
  KEY `IDX_268BC95DDEAB1A3` (`etudiant_id`),
  KEY `IDX_268BC9526EF07C9` (`licence_id`),
  KEY `IDX_268BC95D3EB200C` (`Semestre_id`),
  KEY `IDX_268BC9582350831` (`matieres_id`),
  CONSTRAINT `FK_268BC9526EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`),
  CONSTRAINT `FK_268BC95613FECDF` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_268BC9582350831` FOREIGN KEY (`matieres_id`) REFERENCES `matiers` (`id`),
  CONSTRAINT `FK_268BC95D3EB200C` FOREIGN KEY (`Semestre_id`) REFERENCES `semetre` (`id`),
  CONSTRAINT `FK_268BC95DDEAB1A3` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.notation : ~19 rows (environ)
/*!40000 ALTER TABLE `notation` DISABLE KEYS */;
INSERT INTO `notation` (`id`, `session_id`, `etudiant_id`, `licence_id`, `matieres_id`, `moyenneCours`, `moyenneExament`, `session1`, `session2`, `dateNotation`, `dateLeverSession`, `dateDelaitNotation`, `flagSession1`, `flagDelait`, `flagSession2`, `Semestre_id`) VALUES
	(159, 19, 7, 1, 10, 10, 5, 5, 0, '2019-01-13 11:01:35', NULL, '2019-01-13 12:01:35', 0, NULL, 0, 1),
	(160, 19, 8, 1, 10, 5, 7, 5.8, 0, '2019-01-13 11:01:35', NULL, '2019-01-13 12:01:35', 0, NULL, 0, 1),
	(161, 19, 9, 1, 10, 4, 2, 3.2, 3.2, '2019-01-13 11:01:35', NULL, '2019-01-13 12:01:35', 0, NULL, 0, 1),
	(162, 19, 10, 1, 10, 6, 4, 5.2, 0, '2019-01-13 11:01:35', NULL, '2019-01-13 12:01:35', 0, NULL, 0, 1),
	(163, 20, 10, 2, 15, 8, 8, 8, 0, '2018-11-11 04:11:20', NULL, '2018-11-10 15:11:50', 0, NULL, 0, 3),
	(164, 20, 7, 1, 10, 10, 8, 9.2, 0, '2019-01-14 20:01:55', NULL, '2019-01-14 21:01:55', 0, NULL, 0, 1),
	(165, 20, 8, 1, 10, 8, 8, 8, 0, '2019-01-14 20:01:56', NULL, '2019-01-14 21:01:56', 0, NULL, 0, 1),
	(166, 20, 9, 1, 10, 8, 8, 8, 0, '2019-01-14 20:01:56', NULL, '2019-01-14 21:01:56', 0, NULL, 0, 1),
	(167, 19, 7, 1, 6, 10, 7, 7, 0, '2018-11-10 15:11:36', NULL, '2018-11-10 16:11:36', 0, NULL, 0, 1),
	(168, 19, 8, 1, 6, 4, 4, 4, 0, '2018-11-10 15:11:36', NULL, '2018-11-10 16:11:36', 0, NULL, 0, 1),
	(169, 19, 9, 1, 6, 5, 5, 5, 0, '2018-11-10 15:11:36', NULL, '2018-11-10 16:11:36', 0, NULL, 0, 1),
	(170, 19, 10, 1, 6, 6, 6, 6, 0, '2018-11-10 15:11:36', NULL, '2018-11-10 16:11:36', 0, NULL, 0, 1),
	(171, 20, 7, 1, 9, 10, 3, 3, 3, '2018-11-10 15:11:17', NULL, '2018-11-10 16:11:17', 0, NULL, 0, 1),
	(172, 20, 8, 1, 9, 5, 5, 5, 0, '2018-11-11 04:11:54', NULL, '2018-11-10 16:11:17', 0, NULL, 0, 1),
	(173, 20, 9, 1, 9, 3, 3, 3, 3, '2018-11-10 15:11:17', NULL, '2018-11-10 16:11:17', 0, NULL, 0, 1),
	(178, 20, 10, 2, 13, 9, 9, 9, 0, '2018-11-11 03:11:06', NULL, NULL, 0, NULL, 0, 3),
	(179, 19, 7, 1, 3, 10, 1, 1, 1, '2018-11-11 03:11:33', NULL, NULL, 0, NULL, 0, 1),
	(180, 19, 9, 1, 3, 6, 6, 6, 0, '2018-11-11 04:11:15', NULL, NULL, 0, NULL, 0, 1),
	(181, 19, 7, 1, 5, 10, 10, 7, 0, '2018-11-11 04:11:27', NULL, NULL, 0, NULL, 0, 2),
	(182, 22, 7, 1, 10, 10, 8, 8, 0, '2019-01-14 20:01:54', NULL, '2019-01-14 21:01:54', 0, NULL, 0, 1);
/*!40000 ALTER TABLE `notation` ENABLE KEYS */;

-- Export de la structure de la table universitedb. paiement
CREATE TABLE IF NOT EXISTS `paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessions_id` int(11) DEFAULT NULL,
  `inscriptions_id` int(11) DEFAULT NULL,
  `etudiants_id` int(11) DEFAULT NULL,
  `datepaiement` datetime NOT NULL,
  `montantPayer` double DEFAULT NULL,
  `fraisScolarite` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numeroRecu` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flagInscription` tinyint(1) DEFAULT NULL,
  `admisBac_id` int(11) DEFAULT NULL,
  `fraisInscription_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B1DC7A1E8E2AD382` (`inscriptions_id`),
  KEY `IDX_B1DC7A1ECB88FA9E` (`admisBac_id`),
  KEY `IDX_B1DC7A1EF17C4D8C` (`sessions_id`),
  KEY `IDX_B1DC7A1EA873A5C6` (`etudiants_id`),
  KEY `IDX_B1DC7A1EA6CF50C6` (`fraisInscription_id`),
  CONSTRAINT `FK_B1DC7A1E8E2AD382` FOREIGN KEY (`inscriptions_id`) REFERENCES `inscription` (`id`),
  CONSTRAINT `FK_B1DC7A1EA6CF50C6` FOREIGN KEY (`fraisInscription_id`) REFERENCES `frais_inscription` (`id`),
  CONSTRAINT `FK_B1DC7A1EA873A5C6` FOREIGN KEY (`etudiants_id`) REFERENCES `etudiants` (`id`),
  CONSTRAINT `FK_B1DC7A1ECB88FA9E` FOREIGN KEY (`admisBac_id`) REFERENCES `admin_bac` (`id`),
  CONSTRAINT `FK_B1DC7A1EF17C4D8C` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.paiement : ~9 rows (environ)
/*!40000 ALTER TABLE `paiement` DISABLE KEYS */;
INSERT INTO `paiement` (`id`, `sessions_id`, `inscriptions_id`, `etudiants_id`, `datepaiement`, `montantPayer`, `fraisScolarite`, `numeroRecu`, `flagInscription`, `admisBac_id`, `fraisInscription_id`) VALUES
	(12, 19, 8, 7, '2018-11-06 00:00:00', 250000, NULL, '0', 1, 453, 1),
	(13, 19, 9, 8, '2018-11-06 00:00:00', 250000, NULL, '0', 1, 459, 1),
	(14, 19, 10, 9, '2018-11-06 00:00:00', 250000, NULL, '0', 1, 464, 1),
	(15, 19, 11, 10, '2018-11-06 00:00:00', 250000, NULL, '0', 1, 469, 1),
	(16, 20, 12, 7, '2018-11-07 00:00:00', 250000, NULL, NULL, 1, NULL, 1),
	(17, 20, 13, 8, '2018-11-07 00:00:00', 250000, NULL, NULL, 1, NULL, 1),
	(18, 20, 14, 9, '2018-11-07 00:00:00', 250000, NULL, NULL, 1, NULL, 1),
	(19, 20, 15, 10, '2018-11-07 00:00:00', 250000, NULL, NULL, 1, NULL, 1),
	(20, 21, NULL, 7, '2019-01-13 00:00:00', 250000, NULL, NULL, 0, NULL, 1);
/*!40000 ALTER TABLE `paiement` ENABLE KEYS */;

-- Export de la structure de la table universitedb. pecule
CREATE TABLE IF NOT EXISTS `pecule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valeurl1` double NOT NULL,
  `valeurl2` double NOT NULL,
  `valeurl3` double NOT NULL,
  `valeur4` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.pecule : ~0 rows (environ)
/*!40000 ALTER TABLE `pecule` DISABLE KEYS */;
INSERT INTO `pecule` (`id`, `valeurl1`, `valeurl2`, `valeurl3`, `valeur4`) VALUES
	(1, 150000, 160000, 170000, 180000);
/*!40000 ALTER TABLE `pecule` ENABLE KEYS */;

-- Export de la structure de la table universitedb. personnel
CREATE TABLE IF NOT EXISTS `personnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int(11) DEFAULT NULL,
  `departement_id` int(11) DEFAULT NULL,
  `fonction_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `roles` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempwd` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel1` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel2` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GradeAcademique_id` int(11) DEFAULT NULL,
  `typePersonnel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A6BCF3DE1393C58D` (`tel1`),
  UNIQUE KEY `UNIQ_A6BCF3DEE7927C74` (`email`),
  UNIQUE KEY `UNIQ_A6BCF3DE8A9A9437` (`tel2`),
  UNIQUE KEY `UNIQ_A6BCF3DEFB88E14F` (`utilisateur_id`),
  UNIQUE KEY `UNIQ_A6BCF3DE3DA5256D` (`image_id`),
  UNIQUE KEY `unisite_personnel_departement` (`departement_id`,`fonction_id`),
  KEY `IDX_A6BCF3DECCF9E01E` (`departement_id`),
  KEY `IDX_A6BCF3DE8A24FF74` (`GradeAcademique_id`),
  KEY `IDX_A6BCF3DE11852EE2` (`typePersonnel_id`),
  KEY `IDX_A6BCF3DE57889920` (`fonction_id`),
  CONSTRAINT `FK_A6BCF3DE11852EE2` FOREIGN KEY (`typePersonnel_id`) REFERENCES `type_personnel` (`id`),
  CONSTRAINT `FK_A6BCF3DE3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media` (`id`),
  CONSTRAINT `FK_A6BCF3DE57889920` FOREIGN KEY (`fonction_id`) REFERENCES `fonction` (`id`),
  CONSTRAINT `FK_A6BCF3DE8A24FF74` FOREIGN KEY (`GradeAcademique_id`) REFERENCES `grade_academique` (`id`),
  CONSTRAINT `FK_A6BCF3DECCF9E01E` FOREIGN KEY (`departement_id`) REFERENCES `departement` (`id`),
  CONSTRAINT `FK_A6BCF3DEFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.personnel : ~5 rows (environ)
/*!40000 ALTER TABLE `personnel` DISABLE KEYS */;
INSERT INTO `personnel` (`id`, `utilisateur_id`, `departement_id`, `fonction_id`, `image_id`, `roles`, `tempwd`, `name`, `prenom`, `tel1`, `tel2`, `adresse`, `email`, `GradeAcademique_id`, `typePersonnel_id`) VALUES
	(1, 4, NULL, 4, 3, 'ROLE_SCOLARITE', 'PERSMK1', 'Keita', 'Mama', '83948398537', '4587635845', 'Matoto', 'keita@gmail.com', 2, 1),
	(2, 5, 5, 1, 4, 'ROLE_CHEF_DEPARTEMENT', 'PERSMD112', 'Diabate', 'Mohamed', '839483985373', '458763584514', 'Gbessia', 'daibate@gmail.com', 4, 1),
	(3, 6, NULL, 5, 5, 'ROLE_COMPTABILITE', 'PERSMK3', 'Kante', 'Mohamed', '839483985372', '9385398', 'Gbessia', 'kant@gmail.com', 1, 1),
	(4, 7, NULL, 2, 6, 'ROLE_SUPERVISEUR', 'PERSMD4', 'Diawarra', 'Mansa', '49849683053', '458763584522', 'Matoto', 'diawarr2a@gmail.com', 3, 1),
	(5, 8, 5, 3, 7, 'ROLE_DIRECTEUR_PROGRAMME', 'PERSMD115', 'Diallo', 'Mamadou', '785558888', '8547885588', 'Kaloum', 'diallo@gmail.com', 3, 2);
/*!40000 ALTER TABLE `personnel` ENABLE KEYS */;

-- Export de la structure de la table universitedb. presence
CREATE TABLE IF NOT EXISTS `presence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etudiant_id` int(11) DEFAULT NULL,
  `matieres_id` int(11) DEFAULT NULL,
  `datepresence` datetime NOT NULL,
  `flagConge` tinyint(1) DEFAULT NULL,
  `flagPresence` tinyint(1) DEFAULT NULL,
  `flagAbsence` tinyint(1) DEFAULT NULL,
  `flagMaladie` tinyint(1) DEFAULT NULL,
  `Sessions_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6977C7A5DDEAB1A3` (`etudiant_id`),
  KEY `IDX_6977C7A582350831` (`matieres_id`),
  KEY `IDX_6977C7A577E0C25B` (`Sessions_id`),
  CONSTRAINT `FK_6977C7A577E0C25B` FOREIGN KEY (`Sessions_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_6977C7A582350831` FOREIGN KEY (`matieres_id`) REFERENCES `matiers` (`id`),
  CONSTRAINT `FK_6977C7A5DDEAB1A3` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.presence : ~8 rows (environ)
/*!40000 ALTER TABLE `presence` DISABLE KEYS */;
INSERT INTO `presence` (`id`, `etudiant_id`, `matieres_id`, `datepresence`, `flagConge`, `flagPresence`, `flagAbsence`, `flagMaladie`, `Sessions_id`) VALUES
	(1, 7, 10, '2018-11-06 22:44:41', 0, 1, 0, 0, 19),
	(2, 8, 10, '2018-11-06 22:44:41', 0, 1, 0, 0, 19),
	(3, 9, 10, '2018-11-06 22:44:41', 0, 1, 0, 0, 19),
	(4, 10, 10, '2018-11-06 22:44:41', 0, 1, 0, 0, 19),
	(5, 7, 5, '2018-12-21 11:25:53', 0, 0, 1, 0, 20),
	(6, 8, 5, '2018-12-21 11:25:53', 0, 0, 1, 0, 20),
	(7, 9, 5, '2018-12-21 11:25:53', 1, 0, 0, 0, 20),
	(8, 7, 5, '2018-12-21 11:50:34', 0, 1, 0, 0, 20),
	(9, 8, 5, '2018-12-21 11:50:34', 0, 1, 0, 0, 20),
	(10, 9, 5, '2018-12-21 11:50:34', 0, 1, 0, 0, 20);
/*!40000 ALTER TABLE `presence` ENABLE KEYS */;

-- Export de la structure de la table universitedb. programmer
CREATE TABLE IF NOT EXISTS `programmer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessions_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programmation_module_unique` (`module_id`,`sessions_id`),
  KEY `IDX_4136CCA9F17C4D8C` (`sessions_id`),
  KEY `IDX_4136CCA9AFC2B591` (`module_id`),
  CONSTRAINT `FK_4136CCA9AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `FK_4136CCA9F17C4D8C` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.programmer : ~0 rows (environ)
/*!40000 ALTER TABLE `programmer` DISABLE KEYS */;
/*!40000 ALTER TABLE `programmer` ENABLE KEYS */;

-- Export de la structure de la table universitedb. recu_pecule
CREATE TABLE IF NOT EXISTS `recu_pecule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etudiants_id` int(11) DEFAULT NULL,
  `pecule_id` int(11) DEFAULT NULL,
  `mois_id` int(11) DEFAULT NULL,
  `sessions_id` int(11) DEFAULT NULL,
  `datepaie` datetime NOT NULL,
  `montantRecu` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `etudiant_session_mois_unique` (`etudiants_id`,`sessions_id`,`mois_id`),
  KEY `IDX_7B0D98F7A873A5C6` (`etudiants_id`),
  KEY `IDX_7B0D98F7A62A32F1` (`pecule_id`),
  KEY `IDX_7B0D98F7FA0749B8` (`mois_id`),
  KEY `IDX_7B0D98F7F17C4D8C` (`sessions_id`),
  CONSTRAINT `FK_7B0D98F7A62A32F1` FOREIGN KEY (`pecule_id`) REFERENCES `pecule` (`id`),
  CONSTRAINT `FK_7B0D98F7A873A5C6` FOREIGN KEY (`etudiants_id`) REFERENCES `etudiants` (`id`),
  CONSTRAINT `FK_7B0D98F7F17C4D8C` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_7B0D98F7FA0749B8` FOREIGN KEY (`mois_id`) REFERENCES `mois` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.recu_pecule : ~0 rows (environ)
/*!40000 ALTER TABLE `recu_pecule` DISABLE KEYS */;
/*!40000 ALTER TABLE `recu_pecule` ENABLE KEYS */;

-- Export de la structure de la table universitedb. resultat
CREATE TABLE IF NOT EXISTS `resultat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etudiant_id` int(11) NOT NULL,
  `licence_id` int(11) NOT NULL,
  `matieres_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `moyenneGenerale` double NOT NULL,
  `dateCalcul` date NOT NULL,
  `Semestre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E7DB5DE2DDEAB1A3` (`etudiant_id`),
  KEY `IDX_E7DB5DE226EF07C9` (`licence_id`),
  KEY `IDX_E7DB5DE2D3EB200C` (`Semestre_id`),
  KEY `IDX_E7DB5DE282350831` (`matieres_id`),
  KEY `IDX_E7DB5DE2613FECDF` (`session_id`),
  KEY `IDX_E7DB5DE2FB88E14F` (`utilisateur_id`),
  CONSTRAINT `FK_E7DB5DE226EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`),
  CONSTRAINT `FK_E7DB5DE2613FECDF` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`),
  CONSTRAINT `FK_E7DB5DE282350831` FOREIGN KEY (`matieres_id`) REFERENCES `matiers` (`id`),
  CONSTRAINT `FK_E7DB5DE2D3EB200C` FOREIGN KEY (`Semestre_id`) REFERENCES `semetre` (`id`),
  CONSTRAINT `FK_E7DB5DE2DDEAB1A3` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiants` (`id`),
  CONSTRAINT `FK_E7DB5DE2FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.resultat : ~0 rows (environ)
/*!40000 ALTER TABLE `resultat` DISABLE KEYS */;
INSERT INTO `resultat` (`id`, `etudiant_id`, `licence_id`, `matieres_id`, `session_id`, `utilisateur_id`, `moyenneGenerale`, `dateCalcul`, `Semestre_id`) VALUES
	(5, 7, 1, 10, 19, 4, 8, '2019-01-13', 1),
	(7, 8, 1, 10, 19, 4, 5.8, '2019-01-13', 1),
	(8, 9, 1, 10, 19, 4, 3.2, '2019-01-13', 1),
	(9, 10, 1, 10, 19, 4, 5.2, '2019-01-13', 1),
	(11, 7, 1, 13, 19, 4, 7, '2019-01-13', 1),
	(12, 7, 1, 3, 19, 4, 6.4, '2019-01-14', 1),
	(13, 9, 1, 3, 19, 4, 6, '2019-01-14', 1);
/*!40000 ALTER TABLE `resultat` ENABLE KEYS */;

-- Export de la structure de la table universitedb. roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B63E2EC75E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.roles : ~0 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Export de la structure de la table universitedb. salles
CREATE TABLE IF NOT EXISTS `salles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batiement_id` int(11) DEFAULT NULL,
  `numSalle` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombrePlace` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_799D45AA508890CB` (`batiement_id`),
  CONSTRAINT `FK_799D45AA508890CB` FOREIGN KEY (`batiement_id`) REFERENCES `batiment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.salles : ~2 rows (environ)
/*!40000 ALTER TABLE `salles` DISABLE KEYS */;
INSERT INTO `salles` (`id`, `batiement_id`, `numSalle`, `name`, `nombrePlace`) VALUES
	(1, 1, 'BA001', NULL, 74),
	(2, 1, 'BA002', NULL, 90);
/*!40000 ALTER TABLE `salles` ENABLE KEYS */;

-- Export de la structure de la table universitedb. semetre
CREATE TABLE IF NOT EXISTS `semetre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licence_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6A671C35E237E06` (`name`),
  KEY `IDX_6A671C326EF07C9` (`licence_id`),
  KEY `IDX_6A671C3AFC2B591` (`module_id`),
  CONSTRAINT `FK_6A671C326EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`),
  CONSTRAINT `FK_6A671C3AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.semetre : ~6 rows (environ)
/*!40000 ALTER TABLE `semetre` DISABLE KEYS */;
INSERT INTO `semetre` (`id`, `licence_id`, `module_id`, `name`) VALUES
	(1, 1, 1, 'SEMESTRE 1'),
	(2, 1, 4, 'SEMESTRE 2'),
	(3, 2, 1, 'Semestre 3'),
	(4, 2, NULL, 'Semestre 4'),
	(5, 3, 4, 'Semestre 5'),
	(6, 3, NULL, 'Semestre 6');
/*!40000 ALTER TABLE `semetre` ENABLE KEYS */;

-- Export de la structure de la table universitedb. sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encour` tinyint(1) DEFAULT NULL,
  `sessions` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9A609D139A609D13` (`sessions`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.sessions : ~5 rows (environ)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`id`, `encour`, `sessions`) VALUES
	(19, 0, '2018'),
	(20, 0, '2019'),
	(21, 0, '2020'),
	(22, 1, '2021'),
	(23, 0, '2022');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Export de la structure de la table universitedb. type_conge
CREATE TABLE IF NOT EXISTS `type_conge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_20D414BF5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.type_conge : ~3 rows (environ)
/*!40000 ALTER TABLE `type_conge` DISABLE KEYS */;
INSERT INTO `type_conge` (`id`, `name`) VALUES
	(2, 'Autre'),
	(1, 'Conge Accademique'),
	(3, 'Maladie');
/*!40000 ALTER TABLE `type_conge` ENABLE KEYS */;

-- Export de la structure de la table universitedb. type_enseignant
CREATE TABLE IF NOT EXISTS `type_enseignant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9CA4F9845E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.type_enseignant : ~2 rows (environ)
/*!40000 ALTER TABLE `type_enseignant` DISABLE KEYS */;
INSERT INTO `type_enseignant` (`id`, `name`) VALUES
	(1, 'TITULAIRE'),
	(2, 'VACATAIRE');
/*!40000 ALTER TABLE `type_enseignant` ENABLE KEYS */;

-- Export de la structure de la table universitedb. type_personnel
CREATE TABLE IF NOT EXISTS `type_personnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_127AB2CE5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.type_personnel : ~2 rows (environ)
/*!40000 ALTER TABLE `type_personnel` DISABLE KEYS */;
INSERT INTO `type_personnel` (`id`, `name`) VALUES
	(1, 'ADMINISTRATEUR'),
	(2, 'AUTRE');
/*!40000 ALTER TABLE `type_personnel` ENABLE KEYS */;

-- Export de la structure de la table universitedb. universite
CREATE TABLE IF NOT EXISTS `universite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `couverture_id` int(11) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `nomUniversite` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boitePostal` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siteWeb` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomRecteur` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomDirecteurProgramme` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomViceRecteurChargerEtude` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomPrenomViceRecteurChargerRecherche` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B47BD9A3224B0A2F` (`nomUniversite`),
  UNIQUE KEY `UNIQ_B47BD9A347FB5A3F` (`boitePostal`),
  UNIQUE KEY `UNIQ_B47BD9A3BD78ADC7` (`siteWeb`),
  UNIQUE KEY `UNIQ_B47BD9A34B685C80` (`nomPrenomRecteur`),
  UNIQUE KEY `UNIQ_B47BD9A3552A6E70` (`nomPrenomDirecteurProgramme`),
  UNIQUE KEY `UNIQ_B47BD9A3E33E8985` (`nomPrenomViceRecteurChargerEtude`),
  UNIQUE KEY `UNIQ_B47BD9A37DC60F31` (`nomPrenomViceRecteurChargerRecherche`),
  UNIQUE KEY `UNIQ_B47BD9A33F0A9AF5` (`couverture_id`),
  UNIQUE KEY `UNIQ_B47BD9A3F98F144A` (`logo_id`),
  CONSTRAINT `FK_B47BD9A33F0A9AF5` FOREIGN KEY (`couverture_id`) REFERENCES `media` (`id`),
  CONSTRAINT `FK_B47BD9A3F98F144A` FOREIGN KEY (`logo_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.universite : ~0 rows (environ)
/*!40000 ALTER TABLE `universite` DISABLE KEYS */;
INSERT INTO `universite` (`id`, `couverture_id`, `logo_id`, `nomUniversite`, `boitePostal`, `siteWeb`, `nomPrenomRecteur`, `nomPrenomDirecteurProgramme`, `nomPrenomViceRecteurChargerEtude`, `nomPrenomViceRecteurChargerRecherche`, `adresse`, `phone1`, `phone2`, `email`) VALUES
	(1, NULL, NULL, 'UNIVERSITE DE KINDIA', '12 23 KINDIA', 'www.univ-kindia.org', 'Docteur Jack Kourouma', 'Mr Drissa Conde', NULL, NULL, 'Kindia / Foulaya', '+224 628 29 56 09', '0224 00 00 00', 'rectorat@univ-kindia.org');
/*!40000 ALTER TABLE `universite` ENABLE KEYS */;

-- Export de la structure de la table universitedb. utilisateurs
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personnel_id` int(11) DEFAULT NULL,
  `enseignant_id` int(11) DEFAULT NULL,
  `etudiants_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `rolesUser` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `prenom` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_497B315E92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_497B315EA0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_497B315EC05FB297` (`confirmation_token`),
  UNIQUE KEY `UNIQ_497B315E1C109075` (`personnel_id`),
  UNIQUE KEY `UNIQ_497B315EE455FCC0` (`enseignant_id`),
  UNIQUE KEY `UNIQ_497B315EA873A5C6` (`etudiants_id`),
  CONSTRAINT `FK_497B315E1C109075` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`id`),
  CONSTRAINT `FK_497B315EA873A5C6` FOREIGN KEY (`etudiants_id`) REFERENCES `etudiants` (`id`),
  CONSTRAINT `FK_497B315EE455FCC0` FOREIGN KEY (`enseignant_id`) REFERENCES `enseignant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table universitedb.utilisateurs : ~12 rows (environ)
/*!40000 ALTER TABLE `utilisateurs` DISABLE KEYS */;
INSERT INTO `utilisateurs` (`id`, `personnel_id`, `enseignant_id`, `etudiants_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `rolesUser`, `nom`, `locked`, `prenom`) VALUES
	(3, NULL, NULL, NULL, 'super', 'super', 'kenneh65@gmail.com', 'kenneh65@gmail.com', 1, 'bgBTNqni9DzQNqcmvtY.zGT0V9FiuzEbQXeKYiKCQDU', '4mx5p6sSNzpwCds/twfwkrbOe6dk0n5sjS9vItgxZz9ydXbEZB41VoRyM7cBALsMLnVyda9rduvgV+KF2pKPxw==', '2019-01-14 16:04:15', NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', NULL, NULL, NULL, NULL),
	(4, 1, NULL, NULL, 'PERSMK1', 'persmk1', 'PERSMK1@.univ.com', 'persmk1@.univ.com', 1, 'lnGn6y66i08Q5XvoOLdZPKNnDYCoUWG4aArsk8.4i3M', 'PSTMrXItR6b+z9mYffW77PgvIDR8hLsWWvGUfAZB+BkqYpnGWTp2CmWmu79hIey1hI5Ypyx0TUtzRl7c68Tzig==', '2019-01-14 20:31:44', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_SCOLARITE";}', 'ROLE_SCOLARITE', 'Keita', NULL, 'Mama'),
	(5, 2, NULL, NULL, 'PERSMD112', 'persmd112', 'PERSMD112@.univ.com', 'persmd112@.univ.com', 1, 'If3GOcd3BA9AHZM5JhnZY.wcv.Aed9ysJxDm/d8WtC0', '1IHXsTRSMYtFOaCSFBZqesvB5TnTv5m1HQ5mzcx+Dh7D88GrOX755KBqipnhjR36RaOdXbkmFMeQLvqyr0bqdQ==', '2019-01-14 18:19:39', NULL, NULL, 'a:1:{i:0;s:21:"ROLE_CHEF_DEPARTEMENT";}', 'ROLE_CHEF_DEPARTEMENT', 'Diabate', NULL, 'Mohaned'),
	(6, 3, NULL, NULL, 'PERSMK3', 'persmk3', 'PERSMK3@.univ.com', 'persmk3@.univ.com', 1, 'cBaV/Jasnos4HoNNcKyO50pl9MKYczBZIxlRvTE.qoM', 'uSZxF5tmKq09zjGh1eYjFK3Z5G+yXGeYTehPa3iBFDSOsv7hm7jHYdhj9c9Ai/OJGu/c9nLn+Y9ilfKZhMIBOQ==', '2019-01-14 16:07:24', NULL, NULL, 'a:1:{i:0;s:17:"ROLE_COMPTABILITE";}', 'ROLE_COMPTABILITE', 'Kante', NULL, 'Mohaned'),
	(7, 4, NULL, NULL, 'PERSMD4', 'persmd4', 'PERSMD4@.univ.com', 'persmd4@.univ.com', 1, '2Fhjfjr27EEl8oxRKbovZyp/AHVKf.vdsVmyoXPy/jo', 'aHFNs1Fgm0/aTo582QS4ky+rlm30btmxhoX9fxPuHxO36rp550CvcjxDgpf5W6JH1psdsPi/8iDrTO08cQHhMQ==', NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPERVISEUR";}', 'ROLE_SUPERVISEUR', 'Diawarra', NULL, 'Mansa'),
	(8, 5, NULL, NULL, 'PERSMD115', 'persmd115', 'PERSMD115@.univ.com', 'persmd115@.univ.com', 1, 'dBoOPRzEHLPE5tjbKNPa3GGRgbI3cg6CDsHY14useJg', 'uT9IcPrKhEAhvfovPv7cmwKI0jNl1TV9zsFLoGpXmF/L+7BDkvoevy7WdJd5h0DFm0ORSFcemE+QvGYkMxpUmQ==', '2019-01-05 21:47:49', NULL, NULL, 'a:1:{i:0;s:24:"ROLE_DIRECTEUR_PROGRAMME";}', 'ROLE_DIRECTEUR_PROGRAMME', 'Diallo', NULL, 'Mamadou'),
	(11, NULL, 2, NULL, 'ENSMB112', 'ensmb112', 'ENSMB112@.univ.com', 'ensmb112@.univ.com', 1, 'P6.exsAKXUuF3teCtvbX3vEkYjqfkTMaAUWo8gTMN7c', 'T4WPPf1GKwSNXHH52nLXwoeOMNhNF6yKTOaFtCHkApzaIcnc73uI0/2nwDzajuQE4ke/lg4S0JkVUoYLp5s2fQ==', '2018-11-07 12:11:38', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_ENSEGNANT";}', 'ROLE_ENSEGNANT', 'Bah', NULL, 'Moussa'),
	(16, NULL, NULL, 7, '2113118181840H', '2113118181840h', '2113118181840H@.univ.com', '2113118181840h@.univ.com', 1, 'ZGHmkQw.3hVhTA71suxHzeyoWminou8UFfWpQm7m.1A', 'G5dOI4vPw5yPXcZUu31a6g1X46ggKSCGlLiYuuTxet5DKErqONcalhhE8sSCAZFxJo6mtnXkFwJJPZSw3V7qYA==', '2019-01-05 22:05:20', NULL, NULL, 'a:1:{i:0;s:13:"ROLE_ETUDIANT";}', 'ROLE_ETUDIANT', 'MARIAMA KESSO BAH', NULL, 'MARIAMA KESSO BAH'),
	(17, NULL, NULL, 8, '2116761189326H', '2116761189326h', '2116761189326H@.univ.com', '2116761189326h@.univ.com', 1, 'Krzh//geUPEzCQ.T39sUTVTWoNfy0SKNBDypxBlZtiI', 'LL32yZj6LK+K/60hLJK9zt6EzdQRjbv+2xaxQ1NsRR/ddGfv85Hr+epNrIVY7Xw45/SuF4a+O8Mhwx/6EEwtIQ==', '2018-11-06 22:58:20', NULL, NULL, 'a:1:{i:0;s:13:"ROLE_ETUDIANT";}', 'ROLE_ETUDIANT', 'ABDOULAYE DJENAB SOUMAH', NULL, 'ABDOULAYE DJENAB SOUMAH'),
	(18, NULL, NULL, 9, '2111102184748Y', '2111102184748y', '2111102184748Y@.univ.com', '2111102184748y@.univ.com', 1, 'AoiBooYZeLV/ED55crkeD/PyR0z6IT9smxxDuehwYAg', 'WCF+RdmPvMIqwA4OmCWLXyCIAzVaChdp6wgSpDbm07GabrPP7/RrhDRGp/hg5G/NzWLNp+oLHrAvLC3ceYjSNg==', '2018-11-07 00:18:26', NULL, NULL, 'a:1:{i:0;s:13:"ROLE_ETUDIANT";}', 'ROLE_ETUDIANT', 'FATOUMATA GUIRASSY', NULL, 'FATOUMATA GUIRASSY'),
	(19, NULL, NULL, 10, '2111175189114E', '2111175189114e', '2111175189114E@.univ.com', '2111175189114e@.univ.com', 1, '0aRsHHXBa8I4AP0YPphWmm4XK9T/Mpp.HF2UuOloOPQ', 'ZAmY8azUfeRsq1ho8l0eGIC7qAL3xoK+Rlmq1ydu0AjtgLYzFgxV4BzNud1kBnsHbUQO0P63XlsFEU7u5JHwvg==', '2018-11-07 00:19:03', NULL, NULL, 'a:1:{i:0;s:13:"ROLE_ETUDIANT";}', 'ROLE_ETUDIANT', 'IBRAHIMA DRAME', NULL, 'IBRAHIMA DRAME'),
	(20, NULL, 3, NULL, 'ENSMK113', 'ensmk113', 'ENSMK113@.univ.com', 'ensmk113@.univ.com', 1, '/US7BanEXjYHX8x9thCPLSFVEiH9qSFJflO51noGagY', 'wmH87i+UCGdNspJ64zTRtQxPDmYaJURc6pXB0hFB+2YmHQDE4zVCG5TnqCP4ZNmulX4YZULLAT9Uboa3cgUydw==', '2018-12-21 11:25:02', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_ENSEGNANT";}', 'ROLE_ENSEGNANT', 'Kante4', NULL, 'Mohamed4');
/*!40000 ALTER TABLE `utilisateurs` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
