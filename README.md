phptestproject
==============

After clone:
- composer install
- import DATABASE.sql into `universitedb`
- run `php bin/console doctrine:schema:update --dump-sql`
- run `php bin/console server:start`
- http://127.0.0.1:8000/user/new
