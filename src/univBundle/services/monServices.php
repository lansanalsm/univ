<?php
/**
 * Created by PhpStorm.
 * User: Mohamed
 * Date: 19/05/2018
 * Time: 01:05
 */

namespace univBundle\services;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use univBundle\Entity\Notation;
use univBundle\Form\NotationType;

class monServices extends Controller
{
    public function mesVariableEtTable(Request $request)
    {
        $newsForm = $this->newsLetterForm($request);
        return array(
            'newsForm' => $newsForm,
        );

    }
//    public function newsLetterForm(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//        $entity = new Notation();
//        $form = $this->createForm(NotationType::class, $entity);
//        $form->handleRequest($request);
//        if ($form->isValid()) {
//            $em->persist($entity);
//            $em->flush();
//            $this->get('session')->getFlashBag()->add('success', 'Operation Effectuer avec Success !!!');
//        } else {
//            $error = $form->getErrors(true, true);
//            $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
//            $this->get('session')->getFlashBag()->add('success', $errorMessage);
//        }
//        return $formview = $form->createView();
//    }

    public function newsLetterForm(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Notation();
        $form = $this->createForm(NotationType::class, $entity);
        $form->handleRequest($request);
        $noteCour = $form->get('moyenneCours')->getData();
        $noteExament = $form->get('moyenneExament')->getData();
        $mattricule = $request->get('matriculeEtudiant');
        $postLicence = $request->get('univbundle_niveaux');
        $postSemestre = $request->get('univbundle_semestre');
        $postMatiere = $request->get('univbundle_matieres');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $connection = $em->getConnection();
                $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id)
            ON DUPLICATE KEY UPDATE
            moyenneCours=VALUES(moyenneCours),
            moyenneExament=VALUES(moyenneExament),
            session1=VALUES(session1),
             session2=VALUES(session2),
            dateNotation=VALUES(dateNotation),
            flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
            etudiant_id=VALUES(etudiant_id),
            matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
              Semestre_id=VALUES(Semestre_id)
             ");
                $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule);
                $statement->bindValue("moyenneCours", $noteCour);
                $statement->bindValue("moyenneExament", $noteExament);
                $statement->bindValue("session1", ($noteCour * 0.6 + $noteExament * 0.4));
                $statement->bindValue("session2", 0);
                $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
                $statement->bindValue("flagSession1", 0);
                $statement->bindValue("flagSession2", 0);
                $statement->bindValue("etudiant_id", $etudiant->getId());
                $statement->bindValue("matieres_id", $postMatiere);

                $statement->bindValue("licence_id", $postLicence);
                $statement->bindValue("Semestre_id", $postSemestre);
                $result = $statement->execute();

                $this->get('session')->getFlashBag()->add('success', 'Operation Effectuer avec Success !!!');
            } else {
                $error = $form->getErrors(true, true);
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
        }
        return $formview = $form->createView();
    }

    public function getSessionEncour($defaultVal)
    {

        $em = $this->getDoctrine()->getManager();
        $querySession = $em->createQuery(
            'SELECT s FROM univBundle:Sessions s
                    WHERE s.encour =1');
        $session = $querySession->getOneOrNullResult();
        if (is_null($session)) {
            return $defaultVal;
        } else {
            return $session;
        }
    }

    public function generationMatricule($codeFaculte, $codeDepartement, $pv, $session, $numerDordre)
    {
//        $em = $this->getDoctrine()->getManager();
//        $query = $em->createQuery(
//            'SELECT  COUNT (e.etudiant) as maxid FROM univBundle:Etudiants  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
//        $MaxIdInscriptionLicence = $query->getResult();

        if (iconv_strlen($pv) == 4) {
            $quatreChiffrePv = substr($pv, 0, 4);
        } elseif ((iconv_strlen($pv) == 3)) {
            $quatreChiffrePv = "0" . $pv;
        } elseif ((iconv_strlen($pv) == 2)) {
            $quatreChiffrePv = "00" . $pv;
        } elseif ((iconv_strlen($pv) == 1)) {
            $quatreChiffrePv = "000" . $pv;
        } elseif ((iconv_strlen($pv) > 4)) {
            $quatreChiffrePv = substr($pv, 0, 4);
        }
        $deux_denierChiffreSession = substr($session, -2);
        $matricule = $codeFaculte . $codeDepartement . $quatreChiffrePv . $deux_denierChiffreSession . $numerDordre;
        return $matricule;
    }

    public function tester()
    {
        return 1;
    }
}