<?php

namespace univBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use univBundle\Entity\enseignant;
use univBundle\Entity\Presence;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Enseignant controller.
 *
 */
class enseignantController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
//        $departements=$user->getPersonnel();
        $enseignants = $em->getRepository('univBundle:enseignant')->findAll();
//        $MesEnseignants = $em->getRepository('univBundle:enseignant')->findByDepartement($departements);

        return $this->render('enseignant/index.html.twig', array(
            'enseignants' => $enseignants,
//            'MesEnseignants'=>$MesEnseignants,
//            'departement'=>$departements
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $enseignant = new enseignant();
        $form = $this->createForm('univBundle\Form\enseignantType', $enseignant);
        $form->handleRequest($request);
        $chefDep = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!is_null($chefDep->getPersonnel())) {
                $enseignant->setDepartement($chefDep->getPersonnel()->getDepartement());
            } else {
                $enseignant->setDepartement(null);
            }
            $enseignant->setTelephone($form->get('telephone')->getData());
            $em->persist($enseignant);
            $em->flush();

            if (!is_null($chefDep->getPersonnel())) {
                $code = "ENS" . (substr($enseignant->getPrenom(), 0, 1) . substr($enseignant->getName(), 0, 1) . $enseignant->getDepartement()->getCode() . $enseignant->getId());

            } else {
                $code = "ENS" . (substr($enseignant->getPrenom(), 0, 1) . substr($enseignant->getName(), 0, 1) . "NODEP" . $enseignant->getId());
            }

            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setUsername($code);
            $user->setNom($enseignant->getName());
            $user->setPrenom($enseignant->getPrenom());
            $user->setEmail($code . '@.univ.com');
            $user->setPlainPassword($code);
            $tableRole = array();
            $tableRole[0] = "ROLE_ENSEGNANT";
            $user->setrolesUser('ROLE_ENSEGNANT');
            $user->setRoles($tableRole);
            $userManager->updateUser($user);
            $em->flush();

            $enseignant->setUtilisateur($user);
            $em->persist($enseignant);
            $em->flush();

            $user->setEnseignant($enseignant);
            $userManager->updateUser($user);
            $em->flush();


            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);

            if (!is_null($enseignant->getDepartement())) {
                return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseignant->getId()));
            } else {
                return $this->redirectToRoute('enseignant_index');
            }
//            return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseignant->getId()));
        }

        return $this->render('enseignant/new.html.twig', array(
            'enseignant' => $enseignant,
            'form' => $form->createView(),
        ));
    }

    public function showAction(enseignant $enseignant)
    {
        $deleteForm = $this->createDeleteForm($enseignant);

        return $this->render('enseignant/show.html.twig', array(
            'enseignant' => $enseignant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, enseignant $enseignant)
    {
        $deleteForm = $this->createDeleteForm($enseignant);
        $editForm = $this->createForm('univBundle\Form\enseignantType', $enseignant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->getFlashBag()->add('success', 'Operation Effectuer avec Success !!!');
//            return $this->redirectToRoute('enseignant_edit', array('id' => $enseignant->getId()));
            return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseignant->getId()));
        }

        return $this->render('enseignant/edit.html.twig', array(
            'enseignant' => $enseignant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a enseignant entity.
     *
     */
    public function deleteAction(Request $request, enseignant $enseignant)
    {
        $form = $this->createDeleteForm($enseignant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($enseignant);
            $em->flush();
        }

        return $this->redirectToRoute('enseignant_index');
    }

    /**
     * Creates a form to delete a enseignant entity.
     *
     * @param enseignant $enseignant The enseignant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(enseignant $enseignant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('enseignant_delete', array('id' => $enseignant->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function matiereByEnseignantAction()
    {
        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $MatiereByEnseignant = $em->getRepository('univBundle:enseigner')->findByEnseignant($enseignant);
        return $this->render('matiers/indexMatiereByEnseignant.html.twig', array(
            'matieres' => $MatiereByEnseignant,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function EnseignantEtudiantsNotationByMatiereAction(Request $request, $codeMat)

    {

        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //============================================================================
        $noteCour = $request->get('noteCour');
        $noteExament = $request->get('noteExament');
        $mattricule = $request->get('mattricule');
        if (!is_null($noteCour) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $connection = $em->getConnection();
//            for ($i = 0; $i < count($mattricule); $i++) {
//                for ($j = 0; $j < count($noteCour); $j++) {
//                    if ($i == $j) {
//
//                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
//
//                        $statement = $connection->prepare(" INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id)
//            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id)
//            ON DUPLICATE KEY UPDATE
//            moyenneCours=VALUES(moyenneCours),
//            moyenneExament=VALUES(moyenneExament),
//            session1=VALUES(session1),
//             session2=VALUES(session2),
//            dateNotation=VALUES(dateNotation),
//            flagSession1=VALUES(flagSession1),
//             flagSession2=VALUES(flagSession2),
//            etudiant_id=VALUES(etudiant_id),
//            matieres_id=VALUES(matieres_id),
//             licence_id=VALUES(licence_id),
//              Semestre_id=VALUES(Semestre_id)
//             ");
//                        $statement->bindValue("moyenneCours", $noteCour[$j]);
//                        $statement->bindValue("moyenneExament", $noteExament[$j]);
//                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
//                        $statement->bindValue("session2", 0);
//                        $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
//                        $statement->bindValue("flagSession1", 0);
//                        $statement->bindValue("flagSession2", 0);
//                        $statement->bindValue("etudiant_id", $etu->getId());
//                        $statement->bindValue("matieres_id", $matieresOne->getId());
//                        $statement->bindValue("licence_id", $licenceOne->getId());
//                        $statement->bindValue("Semestre_id", $semestreOne->getId());
//                        $result = $statement->execute();
//                    }
//                }
//            }

            for ($i = 0; $i < count($mattricule); $i++) {
                for ($j = 0; $j < count($noteCour); $j++) {
                    if ($i == $j) {
                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
                        $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,dateDelaitNotation,Semestre_id,session_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:dateDelaitNotation,:Semestre_id,:session_id)
            ON DUPLICATE KEY UPDATE
             moyenneCours=VALUES(moyenneCours),
             moyenneExament=VALUES(moyenneExament),
             session1=VALUES(session1),
             session2=VALUES(session2),
             dateNotation=VALUES(dateNotation),
             flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
             etudiant_id=VALUES(etudiant_id),
             matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
             dateDelaitNotation=VALUES(dateDelaitNotation),
             Semestre_id=VALUES(Semestre_id),
             session_id=VALUES(session_id)
             ");

                        $iAnne = 0;
                        $iMois = 0;
                        $iJour = 0;
                        $iHeure = 1;
                        $iMinute = 0;
                        $iSeconde = 0;
                        $delais = 'P' . $iAnne . 'Y' . $iMois . 'M' . $iJour . 'D' . 'T' . $iHeure . 'H' . $iMinute . 'M' . $iSeconde . 'S';
                        $dateJour = new \DateTime();
                        $dateDelait = $dateJour->add(new \DateInterval($delais));
                        $dateDelaitNotation = (date_format($dateDelait, "Y-m-d H:m:s"));
                        $dateNotation = date_format(new \ DateTime(), "Y-m-d H:m:s");
                        $statement->bindValue("moyenneCours", $noteCour[$j]);
                        $statement->bindValue("moyenneExament", $noteExament[$j]);
                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
                        if ($matieresOne->getBaremInf() > ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4)) {
                            $statement->bindValue("session2", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
                        } else {
                            $statement->bindValue("session2", 0);
                        }
                        $statement->bindValue("dateNotation", $dateNotation);
                        $statement->bindValue("flagSession1", 0);
                        $statement->bindValue("flagSession2", 0);
                        $statement->bindValue("etudiant_id", $etu->getId());
                        $statement->bindValue("matieres_id", $matieresOne->getId());
                        $statement->bindValue("licence_id", $licenceOne->getId());
                        $statement->bindValue("Semestre_id", $semestreOne->getId());
                        $statement->bindValue("session_id", $sessionEncour->getId());
                        $statement->bindValue("dateDelaitNotation", $dateDelaitNotation);
                        $result = $statement->execute();


                    }
                }

            }

            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $parame = array('idDepartement' => $departementOne->getId(), 'idNiveau' => $licenceOne->getId(), 'idSemetre' => $semestreOne->getId(), 'idmatieres' => $matieresOne->getId());
            return $this->redirectToRoute('univ_EnseignantmatierFiche_de_Note', $parame);
        }
        //=============================================================================
//
//        $querryEtudianteaNoter2 = $em->createQuery(
//            'SELECT e
//                         FROM univBundle:Etudiants e
//                         INNER JOIN univBundle:Inscription I  WITH  e.id= IDENTITY(I.etudiant)
//                         LEFT JOIN  univBundle:Notation N WITH e.id=IDENTITY(N.etudiant)
//                         AND IDENTITY (I.licence)=:licence
//                         AND IDENTITY (e.departement)=:departement
//                         AND IDENTITY(I.sessions)=:sessions
//                         HAVING ((e.departement)=:departement)
//                         ')
//            ->setParameter('licence', $licenceOne->getId())
//            ->setParameter('departement', $departementOne->getId())
//            ->setParameter('sessions', $sessionEncour->getId());
//        $listeEtudiants = $querryEtudianteaNoter2->getResult();

        $querryEtudianteaNoter2 = $em->createQuery(
            'SELECT e
                         FROM univBundle:Etudiants e 
                         INNER JOIN univBundle:Inscription I  WITH  e.id= IDENTITY(I.etudiant)
                         LEFT JOIN  univBundle:Notation N WITH e.id=IDENTITY(N.etudiant) 
                         AND IDENTITY (I.licence)=:licence
                         AND IDENTITY(I.sessions)=:sessions
                         ')
            ->setParameter('licence', $licenceOne->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $listeEtudiants = $querryEtudianteaNoter2->getResult();
        ///////////////////lliste des Note =============================
        ///

//
//        $querrylisteNote= $em->createQuery(
//            'SELECT N
//                         FROM univBundle:Notation N WHERE
//                         IDENTITY(N.matiers)=:matiers
//                         AND IDENTITY (N.licence)=:licence
//                         AND IDENTITY(N.sessions)=:sessions
//                         ')
//            ->setParameter('licence', $licenceOne->getId())
//            ->setParameter('matiers', $matieresOne->getId())
//            ->setParameter('sessions', $sessionEncour->getId());
//        $listeNotes = $querrylisteNote->getResult();
//
//

        $querrylisteNote= $em->createQuery(
            'SELECT N
                         FROM univBundle:Notation N WHERE 
                         IDENTITY(N.matiers)=:matiers
                         AND IDENTITY (N.licence)=:licence
                         AND IDENTITY(N.sessions)=:sessions
                         ')
            ->setParameter('licence', $licenceOne->getId())
            ->setParameter('matiers', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $listeNotes = $querrylisteNote->getResult();

        return $this->render('univBundle:Default:notationEtudiantByEnseignant.html.twig', array(
            'listeEtudiants' => $listeEtudiants,
            'listeNotes'=>$listeNotes,
            'idMatiere' => $matieresOne->getId(),
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionEncour,

        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationFicheDeNotesAction($idDepartement, $idNiveau, $idSemetre, $idmatieres)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($idmatieres);
//        $sessionOne =($sessionEncour);
        //////////////////end
//        $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
        $matiererId = $idmatieres;

        $querryEtudianteaNoter2 = $em->createQuery(
            'SELECT e
                         FROM univBundle:Etudiants e 
                         INNER JOIN univBundle:Inscription I  WITH  e.id= IDENTITY(I.etudiant)
                         LEFT JOIN  univBundle:Notation N WITH e.id=IDENTITY(N.etudiant) 
                         AND IDENTITY (I.licence)=:licence
                         AND IDENTITY (e.departement)=:departement
                         AND IDENTITY(I.sessions)=:sessions
                          HAVING  (e.departement)=:departement
                         ')
            ->setParameter('licence', $licenceOne->getId())
            ->setParameter('departement', $departementOne->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $listeEtudiants = $querryEtudianteaNoter2->getResult();


        $querrylisteNote= $em->createQuery(
            'SELECT N
                         FROM univBundle:Notation N WHERE 
                         IDENTITY(N.matiers)=:matiers
                         AND IDENTITY (N.licence)=:licence
                         AND IDENTITY(N.sessions)=:sessions
                         ')
            ->setParameter('licence', $licenceOne->getId())
            ->setParameter('matiers', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $listeNotes = $querrylisteNote->getResult();


        return $this->render('enseignant/fiche_de_noteEnseignantEtudiant.html.twig', array(
//            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'sessionOne' => $sessionEncour,
            'listeEtudiants' => $listeEtudiants,
            'listeNotes'=>$listeNotes,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function EnseignantEtudiantsNotationByMatiereListeNoteAction(Request $request, $codeMat)
    {

        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //============================================================================
        $noteCour = $request->get('noteCour');
        $noteExament = $request->get('noteExament');
        $mattricule = $request->get('mattricule');
        if (!is_null($noteCour) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $connection = $em->getConnection();
            for ($i = 0; $i < count($mattricule); $i++) {
                for ($j = 0; $j < count($noteCour); $j++) {
                    if ($i == $j) {

                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);

                        $statement = $connection->prepare(" INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id)
            ON DUPLICATE KEY UPDATE
            moyenneCours=VALUES(moyenneCours),
            moyenneExament=VALUES(moyenneExament),
            session1=VALUES(session1),
             session2=VALUES(session2),
            dateNotation=VALUES(dateNotation),
            flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
            etudiant_id=VALUES(etudiant_id),
            matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
              Semestre_id=VALUES(Semestre_id)
             ");
                        $statement->bindValue("moyenneCours", $noteCour[$j]);
                        $statement->bindValue("moyenneExament", $noteExament[$j]);
                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
                        $statement->bindValue("session2", 0);
                        $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
                        $statement->bindValue("flagSession1", 0);
                        $statement->bindValue("flagSession2", 0);
                        $statement->bindValue("etudiant_id", $etu->getId());
                        $statement->bindValue("matieres_id", $matieresOne->getId());
                        $statement->bindValue("licence_id", $licenceOne->getId());
                        $statement->bindValue("Semestre_id", $semestreOne->getId());
                        $result = $statement->execute();
                    }
                }
            }

            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $parame = array('idDepartement' => $departementOne->getId(), 'idNiveau' => $licenceOne->getId(), 'idSemetre' => $semestreOne->getId(), 'idmatieres' => $matieresOne->getId());
            return $this->redirectToRoute('univ_EnseignantmatierFiche_de_Note', $parame);
        }
        //=============================================================================
        //////////// //////////
        $querryEtudiant = $em->createQuery(
            'SELECT e  FROM univBundle:Etudiants e
                         INNER JOIN univBundle:Inscription I  WITH  e.id= IDENTITY(I.etudiant)
                         INNER JOIN univBundle:departement d   WITH  d.id= IDENTITY(e.departement)
                         AND IDENTITY(e.departement)=:departement 
                         AND IDENTITY (I.licence)=:licence
                         AND IDENTITY(I.sessions)=:sessions
                         ')
            ->setParameter('departement', $departementOne->getId())
            ->setParameter('licence', $licenceOne->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $notesEtudiants = $querryEtudiant->getResult();
        $etudiantsByDepartement = $notesEtudiants;

        return $this->render('univBundle:Default:notationEtudiantByEnseignant.html.twig', array(
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matieresOne->getId(),
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionEncour,

        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function matiereByEnseignanForListeNotetAction()
    {
        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $MatiereByEnseignant = $em->getRepository('univBundle:enseigner')->findByEnseignant($enseignant);
        return $this->render('matiers/indexMatiereByEnseignantForListeNote.html.twig', array(
            'matieres' => $MatiereByEnseignant,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeMatieresPourPresenceEtudiantAction()
    {
        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $MatiereByEnseignant = $em->getRepository('univBundle:enseigner')->findByEnseignant($enseignant);
        return $this->render('matiers/listeMatiereForPresenceEnseignant.html.twig', array(
            'matieres' => $MatiereByEnseignant,
        ));
    }
    public function listeMatieresPourStatistiquePresenceEtudiantAction()
    {
        $enseignant = $this->getUser()->getEnseignant();
        $em = $this->getDoctrine()->getManager();
        $MatiereByEnseignant = $em->getRepository('univBundle:enseigner')->findByEnseignant($enseignant);
        return $this->render('matiers/listeMatiereForStatistiquePresenceEnseignant.html.twig', array(
            'matieres' => $MatiereByEnseignant
        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function PresenceDesEtudiantsAction(Request $request, $codeMat)
    {
        $em = $this->getDoctrine()->getManager();
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //============================================================================
        $present = $request->get('present');
        $absent = $request->get('absent');
        $malade = $request->get('malade');
        $conge = $request->get('conge');
        $mattricule = $request->get('mattricule');
        if (!is_null($mattricule) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            for ($i = 0; $i < count($mattricule); $i++) {
                $presence = new Presence();
                $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
                $presence->setEtudiant($etu);
                $presence->setSessions($sessionEncour);
                $presence->setMatiers($matieresOne);
                $presence->setDatepresence(new \DateTime());
                $presence->setFlagPresence(0);
                $presence->setFlagAbsence(0);
                $presence->setFlagMaladie(0);
                $presence->setFlagConge(0);
                $em->persist($presence);
                $em->flush();
                foreach ( (array)$present as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagPresence(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }
                foreach ((array)$absent as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagAbsence(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }
                foreach ((array)$malade as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagMaladie(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }
                foreach ((array)$conge as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagConge(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }
            }
            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $parame = array('idDepartement' => $departementOne->getId(), 'idNiveau' => $licenceOne->getId(), 'idSemetre' => $semestreOne->getId(), 'idmatieres' => $matieresOne->getId());
            return $this->redirectToRoute('enseignant_matiereEnseignerForPresenceEtudiant');
        }
        //=============================================================================
        $sql = 'SELECT  E.`matricule`,E.`nomPrenom`,N.`flagConge`, N.`flagMaladie`, N.`flagPresence`, N.`flagAbsence`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
                                  N.datepresence
                                FROM     
                                           `etudiants`   E 
                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
                                LEFT JOIN  `presence`    N  ON     E.id=N.etudiant_id
                                AND N.matieres_id=:matieres_id 
                                AND I.session_id=:session_id
                                AND I.licence_id=:licence_id
                                AND E.departement_id =:departement_id
                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
        $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionEncour->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId());
        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
//            var_dump($etudiant);die();
        $etudiantsByDepartement = $etudiant;
//        $matiererId = $univbundle_matieres;
        return $this->render('presence/presenceEtudiant.html.twig', array(
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matieresOne->getId(),
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionEncour,

        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function statistiquePresenceDesEtudiantsAction(Request $request, $codeMat)
    {
        $em = $this->getDoctrine()->getManager();
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //============================================================================
        //=============================================================================
        $sql = 'SELECT count(N.`flagPresence`) as tauxP,count(N.`flagAbsence`) as tauxAb,count(N.`flagConge`) as tauxCg,count(N.`flagMaladie`) as tauxMal, E.`matricule`,E.`nomPrenom`,N.`flagConge`, N.`flagMaladie`, N.`flagPresence`, N.`flagAbsence`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
                                  N.datepresence
                                FROM     
                                           `etudiants`   E 
                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
                                LEFT JOIN  `presence`    N  ON     E.id=N.etudiant_id
                                AND N.matieres_id=:matieres_id 
                                AND I.session_id=:session_id
                                AND I.licence_id=:licence_id
                                AND E.departement_id =:departement_id 
                                AND N.flagPresence=true 
                        
                                GROUP BY I.etudiant_id';

        $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionEncour->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId());
        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
        $etudiantsByDepartement = $etudiant;


        //////////////StatiquePresence
        $querryStatiquePresence = $em->createQuery(
            'SELECT IDENTITY (P.etudiant), E.matricule, count(P.flagPresence) as taux
                   FROM univBundle:Presence P
                   INNER JOIN  univBundle:Etudiants E WITH E.id=IDENTITY (P.etudiant)
                   INNER JOIN  univBundle:Sessions S WITH S.id=IDENTITY (P.sessions)
                   INNER JOIN univBundle:Matiers M WITH M.id=IDENTITY (P.matiers)
                   AND   IDENTITY(P.matiers)=:matiere  
                   AND   IDENTITY(P.sessions)=:sessions
                   AND P.flagPresence=TRUE 
                  GROUP BY E.id
                  ')
            ->setParameter('matiere', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour);
        $statistiqueflagPresence = $querryStatiquePresence->getResult();
        /// /////////////////////////
        ///
        //////////////StatiquePresence
        $querryStatiquePresence = $em->createQuery(
            'SELECT IDENTITY (P.etudiant), E.matricule, count(P.flagAbsence) as taux
                   FROM univBundle:Presence P
                   INNER JOIN  univBundle:Etudiants E WITH E.id=IDENTITY (P.etudiant)
                   INNER JOIN  univBundle:Sessions S WITH S.id=IDENTITY (P.sessions)
                   INNER JOIN univBundle:Matiers M WITH M.id=IDENTITY (P.matiers)
                   AND   IDENTITY(P.matiers)=:matiere  
                   AND   IDENTITY(P.sessions)=:sessions
                   AND P.flagAbsence=TRUE 
                  GROUP BY E.id
                  ')
            ->setParameter('matiere', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour);
        $statistiqueflagAbsence = $querryStatiquePresence->getResult();
        /// /////////////////////////
        ///
        //////////////StatiquePresence
        $querryStatiquePresence = $em->createQuery(
            'SELECT IDENTITY (P.etudiant), E.matricule, count(P.flagMaladie) as taux
                   FROM univBundle:Presence P
                   INNER JOIN  univBundle:Etudiants E WITH E.id=IDENTITY (P.etudiant)
                   INNER JOIN  univBundle:Sessions S WITH S.id=IDENTITY (P.sessions)
                   INNER JOIN univBundle:Matiers M WITH M.id=IDENTITY (P.matiers)
                   AND   IDENTITY(P.matiers)=:matiere  
                   AND   IDENTITY(P.sessions)=:sessions
                   AND P.flagMaladie=TRUE 
                  GROUP BY E.id
                  ')
            ->setParameter('matiere', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour);
        $statistiqueflagMaladie = $querryStatiquePresence->getResult();
        /// /////////////////////////

        $querryStatiquePresence = $em->createQuery(
            'SELECT IDENTITY (P.etudiant), E.matricule, count(P.flagConge) as taux
                   FROM univBundle:Presence P
                   INNER JOIN  univBundle:Etudiants E WITH E.id=IDENTITY (P.etudiant)
                   INNER JOIN  univBundle:Sessions S WITH S.id=IDENTITY (P.sessions)
                   INNER JOIN univBundle:Matiers M WITH M.id=IDENTITY (P.matiers)
                   AND   IDENTITY(P.matiers)=:matiere  
                   AND   IDENTITY(P.sessions)=:sessions
                   AND P.flagConge=TRUE 
                  GROUP BY E.id
                  ')
            ->setParameter('matiere', $matieresOne->getId())
            ->setParameter('sessions', $sessionEncour);
        $statistiqueflagConge = $querryStatiquePresence->getResult();
        /// /////////////////////////

        return $this->render('presence/statistiquePresenceEtudiant.html.twig', array(
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matieresOne->getId(),
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionEncour,
            'statistiqueflagPresence'=>$statistiqueflagPresence,
            'statistiqueflagAbsence'=>$statistiqueflagAbsence,
            'statistiqueflagMaladie'=>$statistiqueflagMaladie,
            'statistiqueflagConge'=>$statistiqueflagConge



        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function testdevAction(Request $request, $codeMat)
    {
        $em = $this->getDoctrine()->getManager();
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //============================================================================
        $present = $request->get('present');
        $absent = $request->get('absent');
        $malade = $request->get('malade');
        $conge = $request->get('conge');
        $mattricule = $request->get('mattricule');
        if (!is_null($mattricule) && $_SERVER['REQUEST_METHOD'] == 'POST') {
//            var_dump($mattricule);die();
            for ($i = 0; $i < count($mattricule); $i++) {
                $presence = new Presence();
                $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
                $presence->setEtudiant($etu);
                $presence->setSessions($sessionEncour);
                $presence->setMatiers($matieresOne);
                $presence->setDatepresence(new \DateTime());
                $presence->setFlagPresence(0);
                $presence->setFlagAbsence(0);
                $presence->setFlagMaladie(0);
                $presence->setFlagConge(0);
                $em->persist($presence);
                $em->flush();


                foreach ( (array)$present as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagPresence(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }

                foreach ((array)$absent as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagAbsence(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }

                foreach ((array)$malade as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagMaladie(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }

                foreach ((array)$conge as $key) {
                    if ($key == $mattricule[$i]) {
                        $presence->setFlagConge(1);
                        $em->persist($presence);
                        $em->flush();
                    }
                }

            }

            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $parame = array('idDepartement' => $departementOne->getId(), 'idNiveau' => $licenceOne->getId(), 'idSemetre' => $semestreOne->getId(), 'idmatieres' => $matieresOne->getId());
            return $this->redirectToRoute('enseignant_matiereEnseignerForPresenceEtudiant');

        }
        //=============================================================================
        $sql = 'SELECT  E.`matricule`,E.`nomPrenom`,N.`flagConge`, N.`flagMaladie`, N.`flagPresence`, N.`flagAbsence`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
                                  N.datepresence
                                FROM     
                                           `etudiants`   E 
                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
                                LEFT JOIN  `presence`    N  ON     E.id=N.etudiant_id
                                AND N.matieres_id=:matieres_id 
                                AND I.session_id=:session_id
                                AND I.licence_id=:licence_id
                                AND E.departement_id =:departement_id
                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
        $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionEncour->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId());
        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
//            var_dump($etudiant);die();
        $etudiantsByDepartement = $etudiant;
//        $matiererId = $univbundle_matieres;
        return $this->render('presence/presenceEtudiant.html.twig', array(
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matieresOne->getId(),
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionEncour,

        ));
    }
//    public function PresenceEtudiantByMatiereAction(Request $request, $codeMat)
//
//    {
//
//        $enseignant = $this->getUser()->getEnseignant();
//        $em = $this->getDoctrine()->getManager();
//        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneByCode($codeMat);
//        $departementOne = $em->getRepository('univBundle:departement')->findOneById($matieresOne->getDepartement()->getId());
//        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($matieresOne->getSemestre()->getId());
//        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($semestreOne->getLicence()->getId());
//        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
//        //============================================================================
//        $noteCour = $request->get('noteCour');
//        $noteExament = $request->get('noteExament');
//        $mattricule = $request->get('mattricule');
//        if (!is_null($noteCour) && $_SERVER['REQUEST_METHOD'] == 'POST') {
//            $connection = $em->getConnection();
////            for ($i = 0; $i < count($mattricule); $i++) {
////                for ($j = 0; $j < count($noteCour); $j++) {
////                    if ($i == $j) {
////
////                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
////
////                        $statement = $connection->prepare(" INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id)
////            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id)
////            ON DUPLICATE KEY UPDATE
////            moyenneCours=VALUES(moyenneCours),
////            moyenneExament=VALUES(moyenneExament),
////            session1=VALUES(session1),
////             session2=VALUES(session2),
////            dateNotation=VALUES(dateNotation),
////            flagSession1=VALUES(flagSession1),
////             flagSession2=VALUES(flagSession2),
////            etudiant_id=VALUES(etudiant_id),
////            matieres_id=VALUES(matieres_id),
////             licence_id=VALUES(licence_id),
////              Semestre_id=VALUES(Semestre_id)
////             ");
////                        $statement->bindValue("moyenneCours", $noteCour[$j]);
////                        $statement->bindValue("moyenneExament", $noteExament[$j]);
////                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
////                        $statement->bindValue("session2", 0);
////                        $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
////                        $statement->bindValue("flagSession1", 0);
////                        $statement->bindValue("flagSession2", 0);
////                        $statement->bindValue("etudiant_id", $etu->getId());
////                        $statement->bindValue("matieres_id", $matieresOne->getId());
////                        $statement->bindValue("licence_id", $licenceOne->getId());
////                        $statement->bindValue("Semestre_id", $semestreOne->getId());
////                        $result = $statement->execute();
////                    }
////                }
////            }
//
//            for ($i = 0; $i < count($mattricule); $i++) {
//                for ($j = 0; $j < count($noteCour); $j++) {
//                    if ($i == $j) {
//                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
//                        $statement = $connection->prepare("
//            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,dateDelaitNotation,Semestre_id)
//            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:dateDelaitNotation,:Semestre_id)
//            ON DUPLICATE KEY UPDATE
//            moyenneCours=VALUES(moyenneCours),
//            moyenneExament=VALUES(moyenneExament),
//            session1=VALUES(session1),
//             session2=VALUES(session2),
//            dateNotation=VALUES(dateNotation),
//            flagSession1=VALUES(flagSession1),
//             flagSession2=VALUES(flagSession2),
//            etudiant_id=VALUES(etudiant_id),
//            matieres_id=VALUES(matieres_id),
//             licence_id=VALUES(licence_id),
//             dateDelaitNotation=VALUES(dateDelaitNotation),
//              Semestre_id=VALUES(Semestre_id)
//             ");
//
//                        $iAnne=0;
//                        $iMois=0;
//                        $iJour=7;
//                        $iHeure=0;
//                        $iMinute=0;
//                        $iSeconde=0;
//                        $delais='P'.$iAnne.'Y'.$iMois.'M'.$iJour.'D'.'T'.$iHeure.'H'.$iMinute.'M'.$iSeconde.'S';
//                        $dateJour = new \DateTime();
//                        $dateDelait=$dateJour->add(new \DateInterval($delais));
//
//                        $dateDelaitNotation=(date_format($dateDelait, "Y-m-d H:m:s"));
//
//                        $dateNotation= date_format(new \ DateTime(), "Y-m-d H:m:s");
//                        $statement->bindValue("moyenneCours", $noteCour[$j]);
//                        $statement->bindValue("moyenneExament", $noteExament[$j]);
//                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
//                        $statement->bindValue("session2", 0);
//                        $statement->bindValue("dateNotation", $dateNotation);
//                        $statement->bindValue("flagSession1", 0);
//                        $statement->bindValue("flagSession2", 0);
//                        $statement->bindValue("etudiant_id", $etu->getId());
//                        $statement->bindValue("etudiant_id", $etu->getId());
//                        $statement->bindValue("matieres_id", $matieresOne->getId());
//                        $statement->bindValue("licence_id", $licenceOne->getId());
//                        $statement->bindValue("Semestre_id", $semestreOne->getId());
//                        $statement->bindValue("dateDelaitNotation",$dateDelaitNotation);
//                        $result = $statement->execute();
//
//                    }
//                }
//
//            }
//
//            $error = "Operation effectuer avec success ";
//            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
//            $this->get('session')->getFlashBag()->add('success', $errorMessage);
//            $parame = array('idDepartement' => $departementOne->getId(), 'idNiveau' => $licenceOne->getId(), 'idSemetre' => $semestreOne->getId(), 'idmatieres' => $matieresOne->getId());
//            return $this->redirectToRoute('univ_EnseignantmatierFiche_de_Note', $parame);
//        }
//        //=============================================================================
//
//        $sql = 'SELECT  E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
//                                  N.dateDelaitNotation,N.dateNotation
//                                FROM
//                                           `etudiants`   E
//                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
//                                LEFT JOIN  `notation`    N  ON     E.id=N.etudiant_id
//                                AND N.matieres_id=:matieres_id
//                                AND I.session_id=:session_id
//                                AND I.licence_id=:licence_id
//                                AND E.departement_id =:departement_id
//                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
//        $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionEncour->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId(),);
//        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
////            var_dump($etudiant);die();
//        $etudiantsByDepartement = $etudiant;
////        $matiererId = $univbundle_matieres;
//
//        return $this->render('presence/presenceEtudiant.html.twig', array(
//            'etudiantsByDepartement' => $etudiantsByDepartement,
//            'idMatiere' => $matieresOne->getId(),
//            'departementOne' => $departementOne,
//            'licenceOne' => $licenceOne,
//            'semestreOne' => $semestreOne,
//            'matieresOne' => $matieresOne,
//            'seeionOne' => $sessionEncour,
//
//        ));
//    }
    public function getUser()
    {
        return parent::getUser(); // TODO: Change the autogenerated stub
    }
}
