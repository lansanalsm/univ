<?php

namespace univBundle\Controller;

use univBundle\Entity\AdminBac;
use univBundle\Entity\Paiement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Paiement controller.
 *
 */
class PaiementController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $paiements = $em->getRepository('univBundle:Paiement')->findAll();

        return $this->render('paiement/index.html.twig', array(
            'paiements' => $paiements,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $paiement = new Paiement();
        $form = $this->createForm('univBundle\Form\PaiementType', $paiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paiement);
            $em->flush();

            return $this->redirectToRoute('paiement_show', array('id' => $paiement->getId()));
        }

        return $this->render('paiement/new.html.twig', array(
            'paiement' => $paiement,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function paiementInscriptionAction(Request $request,$idAdmisBac)
    {
        $em = $this->getDoctrine()->getManager();
        $AdmisBac = $em->getRepository('univBundle:AdminBac')->findOneById($idAdmisBac);
        $montant = $em->getRepository('univBundle:FraisInscription')->findOneById(1);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $paiement = new Paiement();
        if ($AdmisBac->getStatut()=="Boursier"){
            $form = $this->createForm('univBundle\Form\PaiemenFraisInscriptiontEtudiantBourssierType', $paiement);
        }else{
            $form = $this->createForm('univBundle\Form\PaiementFraisInscriptionEtudiantNonBourssierType', $paiement);
        }

        $form->handleRequest($request);
//
//        $querryEtudiant = $em->createQuery(
//            'SELECT n  FROM univBundle:AdminBac n
//                  WHERE
//                  n.idnational=:pv AND
//                  n.sessions=:sessionBac
//                   ')
//            ->setParameter('pv',$AdmisBac->getIdnational())
//            ->setParameter('sessionBac',$AdmisBac->getSessions());
//        $admin = $querryEtudiant->getOneOrNullResult();
        if (!is_null($AdmisBac)) {
            if ($AdmisBac->getFlagPaiement() != 1 || $AdmisBac->getFlagPaiement() == null) {
                 if ($form->isSubmitted() && $form->isValid()) {

                     if ($AdmisBac->getStatut()=="Boursier"){
                         if ($form->get('departement')->getData()->getCode()==$AdmisBac->getCodeDep()){
                             $statu="Boursier";
                         }else{
                             $statu="Non Boursier";
                         }
                     }else{
                         $statu="Non Boursier";
                     }


                     if ($AdmisBac->getStatut()=="Non Boursier"){
                         $paiement->setFraisScolarite($form->get('fraisScolarite')->getData());
                         $paiement->setNumeroRecu($form->get('numeroRecu')->getData());
                     }

                    $paiement->setAdmisBac($AdmisBac);
                    $paiement->setMontantPayer($montant->getMontant());
                    $paiement->setSessions($sessionEncour);
                    $paiement->setFraisInscription($montant);
                    $paiement->setFlagInscription(false);
                    $em->persist($paiement);
                    $em->flush();


                     $AdmisBac->setFlagPaiement(1);
                     $AdmisBac->setCodeDep($form->get('departement')->getData()->getCode());
                     $AdmisBac->setOldDepartementBeforPaiement($AdmisBac->getDepartement());
                     $AdmisBac->setOldStatuBeforPaiement($AdmisBac->getStatut());
                     $AdmisBac->setDepartement($form->get('departement')->getData()->getName());
                     $AdmisBac->setStatut($statu);



                     $em->persist($AdmisBac);
                     $em->flush();

                    $mserror = "Operation effectuer avec success";
                    $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('BulletinPaiementpaiementFraisAction', array('idPaiement' =>$paiement->getId()));
                }
            }
        }else{
            $mserror = "Desole  etudiant inexistant ";
            $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('frais_inscription');
        }

        if ($AdmisBac->getStatut()=="Non Boursier"){
            $view="univBundle:comptabilte:validation_fraisPaiement_inscriptionEtudiantNonBourssier.html.twig";
        }else{
          $view = "univBundle:comptabilte:validation_fraisPaiement_inscription.html.twig";
    }

        return $this->render($view, array(
            'infos' => $AdmisBac,
            'form' => $form->createView(),
            'paiement' => $paiement,

        ));

    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function paiementReinscriptionAction(Request $request,$matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $montant = $em->getRepository('univBundle:FraisInscription')->findOneById(1);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $paiement = new Paiement();
        if ($etudiant->getStatut()=="Boursier"){
            $form = $this->createForm('univBundle\Form\PaiementFraisReinscriptionEtudiantBoussierType', $paiement);
        }else{
            $form = $this->createForm('univBundle\Form\PaiementFraisReinscriptionEtudiantNonBoussierType', $paiement);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($etudiant->getStatut()=="Non Boursier"){
                $paiement->setFraisScolarite($form->get('fraisScolarite')->getData());
                $paiement->setNumeroRecu($form->get('numeroRecu')->getData());
            }
            $paiement->setMontantPayer($montant->getMontant());
            $paiement->setSessions($sessionEncour);
            $paiement->setFraisInscription($montant);
            $paiement->setFlagInscription(false);
            $paiement->setEtudiant($etudiant);
            $em->persist($paiement);
            $em->flush();

            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('BulletinPaiementpaiementFraisReinscriptionAction', array('idPaiement' =>$paiement->getId()));
        }

        if ($etudiant->getStatut()=="Non Boursier"){
            $view="univBundle:comptabilte:validation_fraisPaiement_ReinscriptionEtudiantNonBourssier.html.twig";
        }else {
            $view = "univBundle:comptabilte:validation_fraisPaiement_Reinscription.html.twig";
        }

        return $this->render($view, array(
            'paiement' => $paiement,
            'form' => $form->createView(),
            'infos'=>$etudiant
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Paiement $paiement)
    {
        $deleteForm = $this->createDeleteForm($paiement);

        return $this->render('paiement/show.html.twig', array(
            'paiement' => $paiement,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Paiement $paiement)
    {
        $deleteForm = $this->createDeleteForm($paiement);
        $editForm = $this->createForm('univBundle\Form\PaiementType', $paiement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('paiement_edit', array('id' => $paiement->getId()));
        }

        return $this->render('paiement/edit.html.twig', array(
            'paiement' => $paiement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Paiement $paiement)
    {
        $form = $this->createDeleteForm($paiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paiement);
            $em->flush();
        }

        return $this->redirectToRoute('paiement_index');
    }

    /**
     * Creates a form to delete a paiement entity.
     *
     * @param Paiement $paiement The paiement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Paiement $paiement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paiement_delete', array('id' => $paiement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
