<?php

namespace univBundle\Controller;

use univBundle\Entity\Etudiants;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use univBundle\Entity\Inscription;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Etudiant controller.
 *
 */
class EtudiantsController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $licencer = $request->get('licence');
        $sessionr = $request->get('sessions');
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionr);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licencer);

        //////////////////end

        if (is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequest = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                  ');
            $listeEtudiants = $querryEtudiantSansPparamRequest->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = 0;
        } elseif (!is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequestSessionS = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestSessionS->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = 0;
        } elseif (is_null($licence) && !is_null($session)) {
            $querryEtudiantSansPparamRequestLicence = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions ')
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestLicence->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = $session->getId();

        } elseif (!is_null($licence) && !is_null($session)) {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        } else {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        }


        $etudiants = $em->getRepository('univBundle:Etudiants')->findAll();
        $facultes = $em->getRepository('univBundle:Facultes')->findAll();
        $departements = $em->getRepository('univBundle:departement')->findAll();
        $licences = $em->getRepository('univBundle:Licence')->findAll();
        $sessions = $em->getRepository('univBundle:Sessions')->findAll();

        return $this->render('etudiants/index.html.twig', array(
            'etudiants' => $listeEtudiants,
            'facultes' => $facultes,
            'departements' => $departements,
            'licences' => $licences,
            'sessions' => $sessions,
            'licenceForUrl' => $licenceForUrl,
            'sessionForUrl' => $sessionForUrl

        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeEtudiantpourAttestationInscriptionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $licencer = $request->get('licence');
        $sessionr = $request->get('sessions');
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionr);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licencer);

        //////////////////end

        if (is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequest = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                  ');
            $listeEtudiants = $querryEtudiantSansPparamRequest->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = 0;
        } elseif (!is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequestSessionS = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestSessionS->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = 0;
        } elseif (is_null($licence) && !is_null($session)) {
            $querryEtudiantSansPparamRequestLicence = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions ')
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestLicence->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = $session->getId();

        } elseif (!is_null($licence) && !is_null($session)) {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        } else {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        }


        $etudiants = $em->getRepository('univBundle:Etudiants')->findAll();
        $facultes = $em->getRepository('univBundle:Facultes')->findAll();
        $departements = $em->getRepository('univBundle:departement')->findAll();
        $licences = $em->getRepository('univBundle:Licence')->findAll();
        $sessions = $em->getRepository('univBundle:Sessions')->findAll();

        return $this->render('etudiants/listeEtudiantpourAttestationInscription.html.twig', array(
            'etudiants' => $listeEtudiants,
            'facultes' => $facultes,
            'departements' => $departements,
            'licences' => $licences,
            'sessions' => $sessions,
            'licenceForUrl' => $licenceForUrl,
            'sessionForUrl' => $sessionForUrl

        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $etudiant = new Etudiants();
        $form = $this->createForm('univBundle\Form\EtudiantsType', $etudiant);
        $form->handleRequest($request);
        $departement = $form->get('departement')->getData();

        $session = $form->get('Sessions')->getData();
        $niveau = $form->get('licence')->getData();
//        $semestre=$form->get('Semestre')->getData();
        $pv = $form->get('PV')->getData();
        $concentration = $form->get('concentration')->getData();


        if ($form->isSubmitted() && $form->isValid()) {
            $numAleatoire = $randon = random_int(999, 9999);
            $matEtidiant = $this->get('monServices')->generationMatricule("777", $departement->getCode(), $pv, $session->getSessions(), $numAleatoire);
            $em = $this->getDoctrine()->getManager();
            $etudiant->setMatricule($matEtidiant);
            $etudiant->setStatut('Non Boursier');
            $etudiant->setDateNaissance($form->get('dateNaissance')->getData()->format('d/m/Y'));
            if ($niveau->getId() >= 3) {
                $etudiant->setConcentration($concentration);
            } else {
                $etudiant->setConcentration(null);
            }
            $em->persist($etudiant);
            $em->flush();
            $inscrire = new Inscription();
            $inscrire->setEtudiant($etudiant);
            $inscrire->setLicence($niveau);
            $inscrire->setDateInscription(new \DateTime());
            $inscrire->setSessions($session);
            $em->persist($inscrire);
            $em->flush();
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setUsername($etudiant->getMatricule());
            $user->setNom($etudiant->getNomPrenom());
            $user->setPrenom($etudiant->getNomPrenom());
            $user->setEmail($etudiant->getMatricule() . '@.univ.com');
            $user->setPlainPassword($etudiant->getMatricule());
            $tableRole = array();
            $tableRole[0] = "ROLE_ETUDIANT";
            $user->setrolesUser('ROLE_ETUDIANT');
            $user->setRoles($tableRole);
            $userManager->updateUser($user);
            $em->flush();

            $etudiant->setUtilisateur($user);
            $em->persist($etudiant);
            $em->flush();

            $user->setEtudiants($etudiant);
            $userManager->updateUser($user);
            $em->flush();

            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('etudiants_show', array('id' => $etudiant->getId()));
        }
        return $this->render('etudiants/new.html.twig', array(
            'etudiant' => $etudiant,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Etudiants $etudiant, Request $request)
    {
        $deleteForm = $this->createDeleteForm($etudiant);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $em = $this->getDoctrine()->getManager();
        $querLastLicence = $em->createQuery(
            'SELECT  max(e.id) as id FROM univBundle:Inscription e WHERE IDENTITY (e.etudiant)=:etudiant'
        )
            ->setParameter('etudiant', $etudiant->getId());
        $LastLicence = $querLastLicence->getResult();

        $QuerylicenceActuel = $em->createQuery(
            'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
            ->setParameter('maxid', $LastLicence[0]['id']);
        $niveauActuel = $QuerylicenceActuel->getOneOrNullResult();
//        var_dump($lastImage->getLicence()->getName());die();
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('etudiants/show.html.twig', array(
            'etudiant' => $etudiant, 'niveauActuel' => $niveauActuel,
            'sessionsEncour'=>$sessionEncour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Etudiants $etudiant)
    {
        $deleteForm = $this->createDeleteForm($etudiant);
        $editForm = $this->createForm('univBundle\Form\EtudiantsType', $etudiant);
        $editForm->handleRequest($request);
        $niveau = $editForm->get('licence')->getData();
        $concentration = $editForm->get('concentration')->getData();
        $em = $this->getDoctrine()->getManager();
        $queryMaxIdInscriptionLicence = $em->createQuery(
            'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
        $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
        $queryLastInscriptionLicence = $em->createQuery(
            'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
            ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);
        $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if ($niveau->getId() >= 2) {
                $etudiant->setConcentration($concentration);
            } else {
                $etudiant->setConcentration(null);
            }
            $this->getDoctrine()->getManager()->flush();
            $lastInscriptionlicence->setLicence($niveau);
            $em->persist($lastInscriptionlicence);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('etudiants_show', array('id' => $etudiant->getId()));
        }



        return $this->render('etudiants/edit.html.twig', array(
            'etudiant' => $etudiant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'lastInscriptionlicence' => $lastInscriptionlicence,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Etudiants $etudiant)
    {
        $form = $this->createDeleteForm($etudiant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etudiant);
            $em->flush();
        }

        return $this->redirectToRoute('etudiants_index');
    }

    /**
     * Creates a form to delete a etudiant entity.
     *
     * @param Etudiants $etudiant The etudiant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Etudiants $etudiant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etudiants_delete', array('id' => $etudiant->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeEtudiantPourleBadgeEtudiantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);

        //////////////////end

        if ( !is_null($departementOne) && !is_null($sessionOne) && $_SERVER['REQUEST_METHOD'] == 'GET') {

            $sql = 'SELECT P.path, E.departement_id as departement_id, E.filiation,E.sexe, E.`matricule`,E.`nomPrenom`,I.etudiant_id ,I.session_id,E.departement_id,I.licence_id
                                FROM     
                                           `etudiants`   E 
                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
                                 LEFT JOIN  `media` P    ON     P.id=E.image_id
                                AND I.session_id=:session_id
                                AND I.licence_id=:licence_id
                                AND E.departement_id =:departement_id
                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
            $params = array('session_id' => $sessionOne->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId(),);
            $etudiantListe = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
            $etudiantsByDepartement = $etudiantListe;
        } else {
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);

        }

        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render('univBundle:Default:listeEtudiantForbadgeEtudiant.html.twig', array(
            'etudiants' => $etudiantsByDepartement,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'seeionOne' => $sessionOne,
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeEtudiantPourlaReinscriptionAction(Request $request)
    {
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT E  FROM
                          univBundle:Etudiants E 
                  INNER JOIN  univBundle:Paiement P WITH  IDENTITY (P.etudiant)= E.id
                  INNER JOIN univBundle:Sessions S WITH  IDENTITY (P.sessions)=S.id
                  AND P.flagInscription!=1
                   ');
        $ListeEtudiants = $querryEtudiant->getResult();
        return $this->render('univBundle:Default:listeEtudiantForPaiementReinscrition.html.twig', array(
            'etudiants'=>$ListeEtudiants
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationMesNotesAction(Request $request)
    {
        $user=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $etudiant =  $user->getEtudiants();
        ////////////////// la dernier inscription
        $queryMaxIdInscriptionLicence = $em->createQuery(
            'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
        $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
        $queryLastInscriptionLicence = $em->createQuery(
            'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
            ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);

        $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();

        /////////////////end //////////////////////////////
        $requestLicence = $request->get('licence');
        if (!is_null($requestLicence) && $_SERVER['REQUEST_METHOD'] == 'GET') {
            $licence = $em->getRepository('univBundle:Licence')->findOneById($requestLicence);
            $selectedLicenceId = $requestLicence;
        } else {
            $licence = $lastInscriptionlicence->getLicence();
            $selectedLicenceId = $licence->getId();
        }

        //////////// notes en fonction  de l'etudiant de la licence  deniere licence par defau//////////
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:Notation n
                  WHERE 
                  n.licence=:licence
                  AND IDENTITY (n.etudiant)=:etudiant
                   ')
            ->setParameter('licence', $licence->getId())
            ->setParameter('etudiant', $etudiant->getId());
        $notesEtudiants = $querryEtudiant->getResult();

        //////////////////end
        $licences = $em->getRepository('univBundle:Licence')->findAll();
        //////////  Recuperation de la session  en fonction de la derniere inscription et de la licence /////////////////
        $querrysession = $em->createQuery(
            'SELECT I  FROM univBundle:Inscription I
                  WHERE 
                  IDENTITY(I.etudiant)=:etudiant
                   AND 
                   IDENTITY(I.licence)=:licence')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('licence', $licence->getId());
        $session = $querrysession->getOneOrNullResult();

        //////////////////////////en //////////////////////////////////////////////
//  var_dump($lastInscriptionlicence->getLicence()->getId());die();
        return $this->render('etudiants/mesNotes.html.twig', array(
            'notesEtudiants' => $notesEtudiants,
            'etudiant' => $etudiant,
            'licence' => $licence,
            'licences' => $licences,
            'ins_session' => $session,
            'temoinIdCodeLicence' => $lastInscriptionlicence->getLicence()->getId(),
            'selectedLicenceId' => $selectedLicenceId
        ));
    }

    public function getUser()
    {
        return parent::getUser(); // TODO: Change the autogenerated stub
    }
}
