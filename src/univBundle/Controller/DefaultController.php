<?php

namespace univBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use univBundle\Entity\Etudiants;
use univBundle\Entity\Inscription;
use univBundle\Entity\Media;
use univBundle\Entity\Notation;
use univBundle\Entity\noteN;
use univBundle\Entity\Resultat;
use univBundle\Form\InscriptionType;
use univBundle\Form\NotationType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $etudiantIscrit = $em->getRepository('univBundle:Etudiants')->findAll();
        $nbrPersonnel = $em->getRepository('univBundle:Personnel')->findAll();
        $etudiantOrientes = $em->getRepository('univBundle:AdminBac')->findAll();
        return $this->render('univBundle:Default:dashbord.html.twig', array(
            'etudiantIscrit' => $etudiantIscrit,
            'etudiantOrientes' => $etudiantOrientes,
            'nbrPersonnel' => $nbrPersonnel

        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function verificationPvCandidatAction(Request $request, $pv, $sessionBac)
    {
        $up = $request->get('uplo');
        $em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.idnational=:pv AND 
                  n.sessions=:sessionBac
                  AND n.flagIncrition is NULL 
                   ')
            ->setParameter('pv', $pv)
            ->setParameter('sessionBac', $sessionBac);
        $admin = $querryEtudiant->getOneOrNullResult();
        $Listesessions = $em->getRepository('univBundle:Sessions')->findAll();
        if (is_null($admin)) {
            $mserror = "Désolé ce PV n'existe pas ";
            $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);


        } else {
            if (!is_null($admin) && $admin->getFlagIncrition() == 1) {
                $mserror = "Désolé cet etudiant est déjà inscrit";
                $errorMessage = '<span style="color: #ffae47;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('univ_inscription', array('pv' => $pv, 'sessionBac' => $sessionBac));
            }
        }

        $last_entity = $em->createQueryBuilder()
            ->select('e')
            ->from('univBundle:Media', 'e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('univBundle:Default:inscription.html.twig', array(
            'infos' => $admin,
            'up' => $up,
            'lastImage' => $last_entity,
            'pvElection' => $pv,
            'sessions' => $Listesessions,
            'newsForm' => $monServices['newsForm'],
            'sessionEncour' => $monServices = $this->get('monServices')->getSessionEncour(1)
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function inscriptionEtudiantBourssierAction(Request $request, $pvSelection, $sessionBac)
    {
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.idnational=:pv AND 
                  n.sessions=:sessionBac
              
                   ')
            ->setParameter('pv', $pvSelection)
            ->setParameter('sessionBac', $sessionBac);
        $admin = $querryEtudiant->getOneOrNullResult();

        $idimg = $request->get('idImage');
        $codeNiveau = $request->get('codeNiveau');
        $codeDep = $request->get('codeDep');
        $sessionIns = $request->get('sessionInscription');
        $dateIns = $request->get('dateInscription');
//        $codeSemestre=$request->get('semestresInscription');
        $date = new \DateTime($dateIns);
//        var_dump($sessionIns);die();

        $imageEtudiant = $em->getRepository('univBundle:Media')->findOneById($idimg);
        $niveau = $em->getRepository('univBundle:Licence')->findOneByCode($codeNiveau);
        $sessions = $em->getRepository('univBundle:Sessions')->findOneById($sessionIns);
//        $semestreOne = $em->getRepository('univBundle:Semetre')->findOneById($codeSemestre);
        $departement = $em->getRepository('univBundle:departement')->findOneByCode($codeDep);

        if (!is_null($admin)) {
            if ($admin->getFlagIncrition() != 1) {
                /////////Letudiant nest pas inscrit et il a paie les frais inscription
                ///
                $etudiant = new Etudiants();
                $numAleatoire = $randon = random_int(999, 9999);
                $matEtidiant = $this->get('monServices')->generationMatricule($departement->getFaculte()->getId(), $admin->getCodeDep(), $admin->getPv(), $admin->getSessions(), $numAleatoire);
//        $matEtidiant = $this->generationMatricule("777", $admin->getCodeDep(), $admin->getPv(), $admin->getSessions(), $numAleatoire);
                $etudiant->setMatricule(strtoupper($matEtidiant . substr($admin->getNomPrenom(), -1)));
                $etudiant->setNomPrenom($admin->getNomPrenom());
                $etudiant->setSexe($admin->getSexe());
                $etudiant->setDateNaissance($admin->getDateNaissance());
                $etudiant->setFiliation($admin->getFiliation());
                $etudiant->setNationnalite($admin->getNationnalite());
                $etudiant->setContact($admin->getContact());
                $etudiant->setDepartement($departement);
                $etudiant->setStatut($admin->getStatut());
                $etudiant->setLicence($niveau);
//        $etudiant->setSemestre($semestreOne);
                if ($imageEtudiant != null) {
                    $etudiant->setImage($imageEtudiant);
                }
                $em->persist($etudiant);
                $em->flush();

                $inscrire = new Inscription();
                $inscrire->setEtudiant($etudiant);
                $inscrire->setLicence($niveau);
                $inscrire->setDateInscription($date);
                $inscrire->setSessions($sessions);
                $em->persist($inscrire);
                $em->flush();
                $admin->setFlagIncrition(1);
                $em->persist($admin);
                $em->flush();
//                  $p=new Paiement();$p->setFlagInscription();$p->setInscriptions();$p->setEtudiant();
                $paiement = $em->getRepository('univBundle:Paiement')->findOneByAdmisBac($admin);
                $paiement->setInscriptions($inscrire);
                $paiement->setFlagInscription(true);
                $paiement->setEtudiant($etudiant);
                $em->persist($paiement);
                $em->flush();

                /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                $userManager = $this->get('fos_user.user_manager');
                $user = $userManager->createUser();
                $user->setEnabled(true);
                $user->setUsername($etudiant->getMatricule());
                $user->setNom($etudiant->getNomPrenom());
                $user->setPrenom($etudiant->getNomPrenom());
                $user->setEmail($etudiant->getMatricule() . '@.univ.com');
                $user->setPlainPassword($etudiant->getMatricule());
                $tableRole = array();
                $tableRole[0] = "ROLE_ETUDIANT";
                $user->setrolesUser('ROLE_ETUDIANT');
                $user->setRoles($tableRole);
                $userManager->updateUser($user);
                $em->flush();

                $etudiant->setUtilisateur($user);
                $em->persist($etudiant);
                $em->flush();

                $user->setEtudiants($etudiant);
                $userManager->updateUser($user);
                $em->flush();

                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
//                return $this->redirectToRoute('adminbac_index');
                ///
                ///  fin de l'inscription
                return $this->redirectToRoute('attestation_attestationDinscription', array('etudiant' => $etudiant->getId()));
            } else {
                $mserror = "Desole cet etudiant est deja inscrit";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('adminbac_index');
            }
        } else {
            $mserror = "Desole cet etudiant n'existe pas";
            $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('adminbac_index');
        }


    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function attestationDinscriptionAction($etudiant)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneById($etudiant);

        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        return $this->render('univBundle:attestation:attestationInscription.html.twig', array(
            'etudiant' => $etudiant,
            'chefService' => $chefService
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function generationattestationDinscriptionPdfAction($etudiant)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneById($etudiant);
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        $html = $this->renderView('univBundle:attestation:attesttationPDF.html.twig', array(
            'etudiant' => $etudiant,
            'Universite' => $chefService
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,

            )),
            'attestation-d-inscription-' . $etudiant->getMatricule() . '.pdf'
        );

//        return $this->render('univBundle:attestation:attesttationPDF.html.twig', array(
//            'etudiant' => $etudiant, 'Universite'=>$chefService
//        ));

    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function reInscriptionEtudiantAction(Request $request, $matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        if (is_null($etudiant)) {
            if (is_null($etudiant) && $_SERVER['REQUEST_METHOD'] == 'POST') {
                $mserror = "Desole ce Matricule n'existe pas ";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
            $lastInscriptionlicence = $em->getRepository('univBundle:Inscription')->find(1);
        } else {

            $queryMaxIdInscriptionLicence = $em->createQuery(
                'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
            $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
            $queryLastInscriptionLicence = $em->createQuery(
                'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
                ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);
            $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();
        }

        $Listesessions = $em->getRepository('univBundle:Sessions')->findAll();
        $entity = new Inscription();
        $form = $this->createForm(InscriptionType::class, $entity);
        $form->handleRequest($request);
        $niveau = $form->get('licence')->getData();
        $sessions = $form->get('sessions')->getData();
        $concentration = $form->get('concentration')->getData();
        ///////////////////Mi a jour de paiemenr
        $em = $this->getDoctrine()->getManager();
        $querrypaiement = $em->createQuery(
            'SELECT P  FROM
                          univBundle:Etudiants E 
                  INNER JOIN  univBundle:Paiement P WITH  IDENTITY (P.etudiant)= E.id
                  INNER JOIN univBundle:Sessions S WITH  IDENTITY (P.sessions)=S.id
                  AND IDENTITY (P.etudiant)=:etudiant
                  AND IDENTITY (P.sessions)=:sessions
                  AND P.flagInscription=0
                   ')->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $paiement = $querrypaiement->getOneOrNullResult();


        ////////////////////////////////
        ///
        $querryNbrMatiereValider = $em->createQuery(
            "SELECT  N.moyenneCours,
                     N.moyenneExament,
                     N.session1,
                     N.session2,
                     IDENTITY(N.sessions)  as session_id ,
                     IDENTITY(N.etudiant)  as etudiant_id,
                     IDENTITY(N.licence)   as licence_id,
                     IDENTITY(N.matiers)   as matieres_id,
                     IDENTITY(N.semestre)  as Semestre_id,M.typeMatiere
              FROM univBundle:Notation N inner JOIN univBundle:Matiers M WITH M.id=IDENTITY (N.matiers)
              WHERE
                     IDENTITY (N.licence)=:licence
                   AND   IDENTITY (N.etudiant)=:etudiant
                   and IDENTITY(N.licence)=:licence
                   and IDENTITY (N.etudiant)=:etudiant
                   and  ( M.typeMatiere) = 'Fondamentale'
                   AND  (N.session1) >=M.baremSup
                   OR   ( N.session1 >=M.baremInf AND  N.session2=M.baremSup)
                   having ( (IDENTITY (N.etudiant)=:etudiant and IDENTITY(N.licence)=:licence and IDENTITY (N.sessions)=:sessions) )")
            ->setParameter('licence', $etudiant->getLicence()->getId())
            ->setParameter('sessions', $lastInscriptionlicence->getSessions()->getId())
            ->setParameter('etudiant', $etudiant->getId());
        $MatiereValider1 = $querryNbrMatiereValider->getResult();

//        var_dump($MatiereValider1[0]['session_id']);die();


//        $sqlMatiereValider = "select N.id,N.etudiant_id,N.matieres_id,N.session1,N.session2,N.session_id,N.licence_id, M.departement_id,M.typeMatiere,M.semetre_id from notation N ,matiers M
//                        where  M.id=N.matieres_id
//                         and
//                        N.licence_id=:licence
//                        and   N.etudiant_id =:etudiant
//                        AND ( N.session1>=M.baremSup)
//                        or  ( N.session1 >= M.baremInf  and  N.session2=M.baremSup)
//                        GROUP by N.id,N.etudiant_id,N.matieres_id,N.session1,N.session2,N.session_id,N.licence_id having (N.etudiant_id=:etudiant and N.licence_id=:licence   and M.departement_id=:departement  AND M.typeMatiere='Fondamentale')";

//        $params = array('licence' => $etudiant->getLicence()->getId(), 'etudiant' => $etudiant->getId(), 'departement' => $etudiant->getDepartement()->getId());
//        $MatiereValider = $em->getConnection()->executeQuery($sqlMatiereValider, $params)->fetchAll();

//        var_dump(count($MatiereValider1));die();

        $sqlAllmatieres = "select M.* from matiers M
                                inner join semetre S on S.id=M.semetre_id
                                inner join licence L on L.id=S.licence_id
                                and M.departement_id=:departement
                                and L.id=:licence AND M.typeMatiere='Fondamentale'";
        $params = array('licence' => $etudiant->getLicence()->getId(), 'departement' => $etudiant->getDepartement()->getId());
        $Allmatieres = $em->getConnection()->executeQuery($sqlAllmatieres, $params)->fetchAll();

        $nombreMatiereFondenmentalValider = (count($MatiereValider1));
        $nombreMatiereFondenmentale = (count($Allmatieres));
        $nombreMatiereFondenmentalNonValider = ($nombreMatiereFondenmentale - $nombreMatiereFondenmentalValider);
//        var_dump($nombreMatiereFondenmentalNonValider);die();

        ///////////////////////////  on reporte les matiere non valide dans la session suivant si l'etudiant est en dette dans plus de 4 matiere
        ///

        if ($form->isSubmitted() && $form->isValid()) {

//            var_dump(($nombreMatiereFondenmentale));die();
            try {
                $etudiantForm = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
                $em = $this->getDoctrine()->getManager();
                $etudiantForm->setLicence($niveau);
                if ($niveau->getId() >= 3) {
                    $etudiantForm->setConcentration($concentration);
                } else {
                    $etudiantForm->setConcentration(null);
                }
                $inscrire = new Inscription();
                $inscrire->setEtudiant($etudiantForm);
                $inscrire->setLicence($niveau);
                $inscrire->setSessions($sessions);
                if ($niveau->getId() >= 2) {
                    $inscrire->setConcentration($concentration);
                } else {
                    $inscrire->setConcentration(null);
                }

                $em->persist($inscrire);
                $em->flush();
                $em->persist($etudiantForm);
                $em->flush();
                if (!is_null($paiement)) {
                    $paiement->setFlagInscription(true);
                    $paiement->setInscriptions($inscrire);
                    $em->persist($paiement);
                    $em->flush();
                }

                if ($nombreMatiereFondenmentalNonValider >= 4) {
                    $connection = $em->getConnection();
                    for ($j = 0; $j < count($MatiereValider1); $j++) {
                        $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,dateDelaitNotation,Semestre_id,session_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:dateDelaitNotation,:Semestre_id,:session_id)
            ON DUPLICATE KEY UPDATE
             moyenneCours=VALUES(moyenneCours),
             moyenneExament=VALUES(moyenneExament),
             session1=VALUES(session1),
             session2=VALUES(session2),
             dateNotation=VALUES(dateNotation),
             flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
             etudiant_id=VALUES(etudiant_id),
             matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
             dateDelaitNotation=VALUES(dateDelaitNotation),
             Semestre_id=VALUES(Semestre_id),
             session_id=VALUES(session_id)
             ");

                        $iAnne = 0;
                        $iMois = 0;
                        $iJour = 0;
                        $iHeure = 1;
                        $iMinute = 0;
                        $iSeconde = 0;
                        $delais = 'P' . $iAnne . 'Y' . $iMois . 'M' . $iJour . 'D' . 'T' . $iHeure . 'H' . $iMinute . 'M' . $iSeconde . 'S';
                        $dateJour = new \DateTime();
                        $dateDelait = $dateJour->add(new \DateInterval($delais));
                        $dateDelaitNotation = (date_format($dateDelait, "Y-m-d H:m:s"));
                        $dateNotation = date_format(new \ DateTime(), "Y-m-d H:m:s");
                        $statement->bindValue("moyenneCours", $MatiereValider1[$j]['moyenneCours']);
                        $statement->bindValue("moyenneExament", $MatiereValider1[$j]['moyenneExament']);
                        $statement->bindValue("session1", ($MatiereValider1[$j]['session1']));
                        $statement->bindValue("session2", ($MatiereValider1[$j]['session2']));
                        $statement->bindValue("dateNotation", $dateNotation);
                        $statement->bindValue("flagSession1", 0);
                        $statement->bindValue("flagSession2", 0);
                        $statement->bindValue("etudiant_id", $etudiant->getId());
                        $statement->bindValue("matieres_id", $MatiereValider1[$j]['matieres_id']);
                        $statement->bindValue("licence_id", $MatiereValider1[$j]['licence_id']);
                        $statement->bindValue("Semestre_id", $MatiereValider1[$j]['Semestre_id']);
                        $statement->bindValue("session_id", $sessionEncour->getId());
                        $statement->bindValue("dateDelaitNotation", $dateDelaitNotation);
                        $result = $statement->execute();
                    }
                }
                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('ims_listeEtudiantForReinscription');
            } catch (\Doctrine\DBAL\DBALException $e) {

                $licence = $form->get('licence')->getData()->getName();
                $sessions = $form->get('sessions')->getData()->getSessions();
                $error = "Desole Cet etudiant est déjà réinscrit  pour la session " . $sessions;
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }

        }


        return $this->render("univBundle:Default:Reinscription.html.twig", array(
            'infos' => $etudiant,
            'sessions' => $Listesessions,
            'matricule' => $matricule,
            'niveaux' => $niveaux,
            'formNews' => $form->createView(),
            'lastInscriptionlicence' => $lastInscriptionlicence,
            'sessionEncour' => $sessionEncour,
            'nombreMatiereFondenmentalNonValider' => $nombreMatiereFondenmentalNonValider,
            'MatiereValider' => $MatiereValider1,
            'Allmatieres' => $Allmatieres
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationByDepartementNiveauMatierAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $noteCour = $request->get('noteCour');
        $noteExament = $request->get('noteExament');
        $mattricule = $request->get('mattricule');
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        //            //////////// //////////
        if (!is_null($departementOne) && !is_null($sessionOne) && !is_null($semestreOne) && !is_null($matieresOne) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $querryEtudianteaNoter2 = $em->createQuery(
                'SELECT e
                         FROM univBundle:Etudiants e 
                         INNER JOIN univBundle:Inscription I  WITH  e.id= IDENTITY(I.etudiant)
                         LEFT JOIN  univBundle:Notation N WITH e.id=IDENTITY(N.etudiant) 
                         AND IDENTITY (I.licence)=:licence
                         AND IDENTITY(I.sessions)=:sessions
                         ')
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $listeEtudiants = $querryEtudianteaNoter2->getResult();
            ///////////////////lliste des Note =============================
            ///
            $querrylisteNote = $em->createQuery(
                'SELECT N
                         FROM univBundle:Notation N WHERE 
                         IDENTITY(N.matiers)=:matiers
                         AND IDENTITY (N.licence)=:licence
                         AND IDENTITY(N.sessions)=:sessions
                         ')
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('matiers', $matieresOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $listeNotes = $querrylisteNote->getResult();
            ///
            /// ==================end ================
            $matiererId = $univbundle_matieres;
        } else {
//            $error = "Desole Aucun enregistrements trouvés ";
//            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $error . '</span>';
//            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $listeEtudiants = $em->getRepository('univBundle:Etudiants')->find(0);
            $listeNotes = $em->getRepository('univBundle:Notation')->find(0);
        }
        $postLicence = $request->get('licenceOne');
        $postSemestre = $request->get('semestreOne');
        $postMatiere = $request->get('matieresOne');
        $postSession = $request->get('seeionOneId');
        if (!is_null($noteCour) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $matieresNotation = $em->getRepository('univBundle:Matiers')->findOneById($postMatiere);
            $connection = $em->getConnection();
            for ($i = 0; $i < count($mattricule); $i++) {
                for ($j = 0; $j < count($noteCour); $j++) {
                    if ($i == $j) {
                        $etu = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule[$i]);
                        $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,dateDelaitNotation,Semestre_id,session_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:dateDelaitNotation,:Semestre_id,:session_id)
            ON DUPLICATE KEY UPDATE
             moyenneCours=VALUES(moyenneCours),
             moyenneExament=VALUES(moyenneExament),
             session1=VALUES(session1),
             session2=VALUES(session2),
             dateNotation=VALUES(dateNotation),
             flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
             etudiant_id=VALUES(etudiant_id),
             matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
             dateDelaitNotation=VALUES(dateDelaitNotation),
             Semestre_id=VALUES(Semestre_id),
             session_id=VALUES(session_id)
             ");

                        $iAnne = 0;
                        $iMois = 0;
                        $iJour = 0;
                        $iHeure = 1;
                        $iMinute = 0;
                        $iSeconde = 0;
                        $delais = 'P' . $iAnne . 'Y' . $iMois . 'M' . $iJour . 'D' . 'T' . $iHeure . 'H' . $iMinute . 'M' . $iSeconde . 'S';
                        $dateJour = new \DateTime();
                        $dateDelait = $dateJour->add(new \DateInterval($delais));
                        $dateDelaitNotation = (date_format($dateDelait, "Y-m-d H:m:s"));
                        $dateNotation = date_format(new \ DateTime(), "Y-m-d H:m:s");
                        $statement->bindValue("moyenneCours", $noteCour[$j]);
                        $statement->bindValue("moyenneExament", $noteExament[$j]);
                        $statement->bindValue("session1", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
                        if ($matieresNotation->getBaremInf() > ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4)) {
                            $statement->bindValue("session2", ($noteCour[$j] * 0.6 + $noteExament[$j] * 0.4));
                        } else {
                            $statement->bindValue("session2", 0);
                        }
                        $statement->bindValue("dateNotation", $dateNotation);
                        $statement->bindValue("flagSession1", 0);
                        $statement->bindValue("flagSession2", 0);
                        $statement->bindValue("etudiant_id", $etu->getId());
                        $statement->bindValue("matieres_id", $postMatiere);
                        $statement->bindValue("licence_id", $postLicence);
                        $statement->bindValue("Semestre_id", $postSemestre);
                        $statement->bindValue("session_id", $postSession);
                        $statement->bindValue("dateDelaitNotation", $dateDelaitNotation);
                        $result = $statement->execute();


                    }
                }

            }
            $error = "Operation effectuer avec success ";
            $errorMessage = '<span style="color: #ffaf64;font-weight: bold ;font-size: larger">' . $error . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('univ_notation_departement_niveau_matier');
        }
        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();

        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:notation.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:notation.html.twig";
        } else {
            $view = "univBundle:superAdmin:notation.html.twig";
        }

        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionOne,
            'listeEtudiants' => $listeEtudiants,
            'listeNotes' => $listeNotes


        ));
    }

    public function getUser()
    {
        return parent::getUser(); // TODO: Change the autogenerated stub
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationSimpleBisAction(Request $request)
    {

        $entity = new Notation();
        $form = $this->createForm(NotationType::class, $entity);
        $form->handleRequest($request);
        $noteCour = $form->get('moyenneCours')->getData();
        $noteExament = $form->get('moyenneExament')->getData();
        $idMatiereChoisir = $request->get('idMatiereChoisir');
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        if (!is_null($departementOne) && !is_null($sessionOne) && !is_null($semestreOne) && !is_null($matieresOne) && $_SERVER['REQUEST_METHOD'] == 'GET') {

            $sql = 'SELECT  E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id
                                FROM     
                                           `etudiants`   E 
                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
                                LEFT JOIN  `notation`    N  ON     E.id=N.etudiant_id
                                AND N.matieres_id=:matieres_id 
                                AND I.session_id=:session_id
                                AND I.licence_id=:licence_id
                                AND E.departement_id =:departement_id
                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
            $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionOne->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId(),);
            $etudiantListe = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
            $etudiantsByDepartement = $etudiantListe;
            $matiererId = $univbundle_matieres;

//            $temoin=false;
//            $valion=0;
//            foreach ($etudiantsByDepartement as $etudiantsByDepartement){
//                if(    $etudiantsByDepartement['session_id']==$sessionOne->getId()
//                    && $etudiantsByDepartement['departement_id']== $departementOne->getId()
//                    && $etudiantsByDepartement['licence_id']==$licenceOne->getId()){
//                    $temoin=true;
//                    $valion=1;
//                }else{
//                    $temoin=false;
//                    $valion=0;
//                }
//            }
//
//            if ( $temoin==false){
//                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . "Aucun enregistrements trouvés" . '</span>';
//                $this->get('session')->getFlashBag()->add('success', $errorMessage);
//            }
        } else {
//            $error = $form->getErrors(true, true);
//            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . "Aucun enregistrements trouvés" . '</span>';
//            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);
//            return $this->redirectToRoute('univ_notation_simpleBis');

        }
        $mattricule = $request->get('matriculeEtudiant');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $idSemestre = ($request->get('idSemestre'));
            $idSession = ($request->get('idSession'));
            $idLicence = ($request->get('idLicence'));
            $idMatiereChoisir = ($request->get('idMatiereChoisir'));
            $idetudiantForNote = ($request->get('idetudiantForNote'));

            if ($form->isSubmitted() && $form->isValid()) {


                $connection = $em->getConnection();
                $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id,session_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id,:session_id)
            ON DUPLICATE KEY UPDATE
            moyenneCours=VALUES(moyenneCours),
            moyenneExament=VALUES(moyenneExament),
            session1=VALUES(session1),
             session2=VALUES(session2),
            dateNotation=VALUES(dateNotation),
            flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
            etudiant_id=VALUES(etudiant_id),
            matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
              Semestre_id=VALUES(Semestre_id),
               session_id=VALUES(session_id)
             ");
                $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule);
                $statement->bindValue("moyenneCours", $noteCour);
                $statement->bindValue("moyenneExament", $noteExament);
                $statement->bindValue("session1", ($noteCour * 0.6 + $noteExament * 0.4));
                if ($matieresOne->getBaremInf() > ($noteCour * 0.6 + $noteExament * 0.4)) {
                    $statement->bindValue("session2", ($noteCour * 0.6 + $noteExament * 0.4));
                } else {
                    $statement->bindValue("session2", 0);
                }
//                $statement->bindValue("session2", 0);
                $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
                $statement->bindValue("flagSession1", 0);
                $statement->bindValue("flagSession2", 0);
                $statement->bindValue("etudiant_id", $idetudiantForNote);
                $statement->bindValue("matieres_id", $idMatiereChoisir);

                $statement->bindValue("licence_id", $idLicence);

                $statement->bindValue("Semestre_id", $idSemestre);
                $statement->bindValue("session_id", $idSession);
                $result = $statement->execute();

                $error = $form->getErrors(true, true);
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . "Operation Effectuer avec Success" . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('univ_notation_simpleBis');
            } else {
                $error = $form->getErrors(true, true);
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
        }
        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:notationSimpleBis.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:notationSimpleBis.html.twig";
        } else {
            $view = "univBundle:superAdmin:notationSimpleBis.html.twig";
        }
        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionOne,
            'newsForm' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notatationSimpleAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $entity = new Notation();
        $form = $this->createForm(NotationType::class, $entity);
        $form->handleRequest($request);
        $noteCour = $form->get('moyenneCours')->getData();
        $noteExament = $form->get('moyenneExament')->getData();
        $mattricule = $request->get('matriculeEtudiant');
        $postLicence = $request->get('univbundle_niveaux');
        $postSemestre = $request->get('univbundle_semestre');
        $postMatiere = $request->get('univbundle_matieres');
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($postMatiere);
            if ($form->isSubmitted() && $form->isValid()) {
                $connection = $em->getConnection();
                $statement = $connection->prepare("
            INSERT INTO notation(moyenneCours,moyenneExament,session1,session2,dateNotation,flagSession1,flagSession2,etudiant_id,matieres_id,licence_id,Semestre_id,session_id)
            VALUES (:moyenneCours,:moyenneExament, :session1, :session2,:dateNotation,:flagSession1,:flagSession2,:etudiant_id,:matieres_id,:licence_id,:Semestre_id,:session_id)
            ON DUPLICATE KEY UPDATE
            moyenneCours=VALUES(moyenneCours),
            moyenneExament=VALUES(moyenneExament),
            session1=VALUES(session1),
             session2=VALUES(session2),
            dateNotation=VALUES(dateNotation),
            flagSession1=VALUES(flagSession1),
             flagSession2=VALUES(flagSession2),
            etudiant_id=VALUES(etudiant_id),
            matieres_id=VALUES(matieres_id),
             licence_id=VALUES(licence_id),
              Semestre_id=VALUES(Semestre_id),
                session_id=VALUES(session_id)
             ");
                $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($mattricule);
                $statement->bindValue("moyenneCours", $noteCour);
                $statement->bindValue("moyenneExament", $noteExament);
                $statement->bindValue("session1", ($noteCour * 0.6 + $noteExament * 0.4));
                if ($matieresOne->getBaremInf() > ($noteCour * 0.6 + $noteExament * 0.4)) {
                    $statement->bindValue("session2", ($noteCour * 0.6 + $noteExament * 0.4));
                } else {
                    $statement->bindValue("session2", 0);
                }
                $statement->bindValue("dateNotation", date_format(new \ DateTime(), "Y-m-d H:m:s"));
                $statement->bindValue("flagSession1", 0);
                $statement->bindValue("flagSession2", 0);
                $statement->bindValue("etudiant_id", $etudiant->getId());
                $statement->bindValue("matieres_id", $postMatiere);

                $statement->bindValue("licence_id", $postLicence);
                $statement->bindValue("Semestre_id", $postSemestre);
                $statement->bindValue("session_id", $sessionEncour->getId());
                $result = $statement->execute();
                return $this->redirectToRoute('univ_notation_simple');
                $this->get('session')->getFlashBag()->add('success', 'Operation Effectuer avec Success !!!');
            } else {
                $error = $form->getErrors(true, true);
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
        }


        $etudiants = $em->getRepository('univBundle:Etudiants')->findAll();
        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:notationSimple.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:notationSimple.html.twig";
        } else {
            $view = "univBundle:superAdmin:notationSimple.html.twig";
        }

        return $this->render($view, array(
            'etudiants' => $etudiants,
            'newsForm' => $form->createView()
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationFicheDeNotesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        if (!is_null($idDepartement) && $_SERVER['REQUEST_METHOD'] == 'GET') {

            //////////////////end
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
//            $matiererId = $univbundle_matieres;
//            $querryEtudiant = $em->createQuery(
//                'SELECT n
//                   FROM univBundle:Notation n
//                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
//                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
//                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
//                   AND   IDENTITY(n.matiers)=:matiere
//                   AND   IDENTITY(n.licence) =:licence
//                   AND   IDENTITY(n.semestre) =:semestre
//                   AND   I.sessions=:sessions
//                  ')
//                ->setParameter('matiere', $matiererId)
//                ->setParameter('licence', $licenceOne)
//                ->setParameter('semestre', $semestreOne)
//                ->setParameter('sessions', $sessionOne);
//            $notesEtudiants = $querryEtudiant->getResult();
////            var_dump($notesEtudiants);die();


//            $sql = 'SELECT N.session_id, N.id,E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
//                                  N.dateDelaitNotation,N.dateNotation
//                                FROM
//                                           `etudiants`   E
//                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
//                                RIGHT JOIN  `notation`    N  ON     E.id=N.etudiant_id
//                                AND N.matieres_id=:matieres_id
//                                AND I.session_id=:session_id
//                                AND I.licence_id=:licence_id
//                                AND E.departement_id =:departement_id
//                                GROUP BY I.etudiant_id,I.session_id,I.licence_id  HAVING (N.session_id=:session_id)';
//            $params = array('matieres_id' => $matieresOne->getId(), 'session_id' => $sessionOne->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId(),);
//
////            $notesEtudiants = $em->getConnection()->executeQuery($sql, $params)->fetchAll();

            $querryEtudiant = $em->createQuery(
                'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
                ->setParameter('matiere', $matieresOne->getId())
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('semestre', $semestreOne)
                ->setParameter('sessions', $sessionOne->getId());
            $notesEtudiants = $querryEtudiant->getResult();


        } else {
            $notesEtudiants = $em->getRepository('univBundle:Notation')->find(0);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);
        }

        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:fiche_de_note.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:fiche_de_note.html.twig";
        } else {
            $view = "univBundle:superAdmin:fiche_de_note.html.twig";
        }

        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'notesEtudiants' => $notesEtudiants,
            'sessionOne' => $sessionOne
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function FicheDeNotesViergeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        if (!is_null($idDepartement) && $_SERVER['REQUEST_METHOD'] == 'GET') {

            //////////////////end
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
            $querryEtudiant = $em->createQuery(
                'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
                ->setParameter('matiere', $matieresOne->getId())
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('semestre', $semestreOne)
                ->setParameter('sessions', $sessionOne->getId());
            $notesEtudiants = $querryEtudiant->getResult();


        } else {
            $notesEtudiants = $em->getRepository('univBundle:Notation')->find(0);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);
        }

        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:fiche_de_note_vierge.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:fiche_de_note_vierge.html.twig";
        } else {
            $view = "univBundle:superAdmin:fiche_de_note_vierge.html.twig";
        }

        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'notesEtudiants' => $notesEtudiants,
            'sessionOne' => $sessionOne
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function leverSessionAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $noteSession2 = $request->get('noteSession2');
        $idNotation = $request->get('validNotation');
        if (!is_null($noteSession2) && !is_null($idNotation) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $Notation = $em->getRepository('univBundle:Notation')->findOneById($idNotation);
            $Notation->setSession2($noteSession2);
            $Notation->setDateLeverSession(date_format(new \ DateTime(), "Y-m-d H:m:s"));
            $em->persist($Notation);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('univ_notation_Etudiant_En_Session1');
        }
        return $this->redirectToRoute('univ_notation_Etudiant_En_Session1');
    }


    public function calculResultatAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');

        $idNiveau = $request->get('univbundle_niveaux');

        $idSemetre = $request->get('univbundle_semestre');

        $idSession = $request->get('univbundle_session');

        $moduleRequest = $request->get('univbundle_module');


        $user = $this->getUser();

        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);

        $modules = $em->getRepository('univBundle:Module')->findAll();
        if (!empty($moduleRequest) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $moduleOne = $em->getRepository('univBundle:Module')->findOneById($moduleRequest);
            $semestres = $em->getRepository('univBundle:semetre')->findByModule($moduleOne);
            $listeNoteEtudiants = $em->getRepository('univBundle:Notation')->findBy(array('semestre' => $semestres, 'sessions' => $sessionEncour->getId()));
            if (count($listeNoteEtudiants) > 0) {

                foreach ($listeNoteEtudiants as $liste) {
                    $moyenneGenerale = $liste->getMoyenneCours() * 0.6 + $liste->getMoyenneExament() * 0.4;
                    $querryResultat = $em->createQuery(
                        'SELECT r
                   FROM univBundle:Resultat r 
                   WHERE  
                         IDENTITY(r.matiers)=:matiere 
                   AND   IDENTITY(r.licence) =:licence
                   AND   IDENTITY(r.semestre) =:semestre 
                   AND   IDENTITY(r.sessions)=:sessions 
                    AND  IDENTITY(r.etudiant)=:etudiant 
                  ')
                        ->setParameter('matiere', $liste->getMatiers()->getId())
                        ->setParameter('licence', $liste->getLicence()->getId())
                        ->setParameter('semestre', $liste->getSemestre()->getId())
                        ->setParameter('etudiant', $liste->getEtudiant()->getId())
                        ->setParameter('sessions', $sessionEncour->getId());
                    $resultat = $querryResultat->getOneOrNullResult();
                    if (count($resultat) > 0) {
                        $resultat->setMoyenneGenerale($moyenneGenerale);
                        $em->persist($resultat);
                        $em->flush();


                    } else {
                        $resultat = Resultat::createResultat(
                            $liste->getEtudiant(),
                            $liste->getLicence(),
                            $liste->getSemestre(),
                            $liste->getMatiers(),
                            $liste->getSessions(),
                            $moyenneGenerale,
                            $user);
                        $em->persist($resultat);
                        $em->flush();
                    }
                }

                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            } else {
                $mserror = "Aucun etudiant ne correspond a ce critere";
                $errorMessage = '<span style="color: #ff021e;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
        }else if (empty($moduleRequest) && $_SERVER['REQUEST_METHOD'] == 'POST'){

            $semestres = $em->getRepository('univBundle:semetre')->findAll();
            $listeNoteEtudiants = $em->getRepository('univBundle:Notation')->findBy(array('semestre' => $semestres, 'sessions' => $sessionEncour->getId()));
//            var_dump($listeNoteEtudiants);die();
            if (count($listeNoteEtudiants) > 0) {

                foreach ($listeNoteEtudiants as $liste) {
                    $moyenneGenerale = $liste->getMoyenneCours() * 0.6 + $liste->getMoyenneExament() * 0.4;
                    $querryResultat = $em->createQuery(
                        'SELECT r
                   FROM univBundle:Resultat r 
                   WHERE  
                         IDENTITY(r.matiers)=:matiere 
                   AND   IDENTITY(r.licence) =:licence
                   AND   IDENTITY(r.semestre) =:semestre 
                   AND   IDENTITY(r.sessions)=:sessions 
                    AND  IDENTITY(r.etudiant)=:etudiant 
                  ')
                        ->setParameter('matiere', $liste->getMatiers()->getId())
                        ->setParameter('licence', $liste->getLicence()->getId())
                        ->setParameter('semestre', $liste->getSemestre()->getId())
                        ->setParameter('etudiant', $liste->getEtudiant()->getId())
                        ->setParameter('sessions', $sessionEncour->getId());
                    $resultat = $querryResultat->getOneOrNullResult();
                    if (count($resultat) > 0) {
                        $resultat->setMoyenneGenerale($moyenneGenerale);
                        $em->persist($resultat);
                        $em->flush();


                    } else {
                        $resultat = Resultat::createResultat(
                            $liste->getEtudiant(),
                            $liste->getLicence(),
                            $liste->getSemestre(),
                            $liste->getMatiers(),
                            $liste->getSessions(),
                            $moyenneGenerale,
                            $user);
                        $em->persist($resultat);
                        $em->flush();
                    }
                }

                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            } else {
                $mserror = "Aucun etudiant ne correspond a ce critere";
                $errorMessage = '<span style="color: #ff021e;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }

        }


        return $this->render('univBundle:resultat:calcultatResultat.html.twig', array(
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'sessionOne' => $sessionOne,
            'departements' => $departements,
            'sessionEncour' => $sessionEncour,
            'modules' => $modules
        ));
    }

    public function consultationResultatAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $matiererId = $request->get('univbundle_matieres');
        $idDepartement = $request->get('univbundle_departement');

        $idNiveau = $request->get('univbundle_niveaux');

        $idSemetre = $request->get('univbundle_semestre');

        $idSession = $request->get('univbundle_session');

        $user = $this->getUser();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);

        $departements = $em->getRepository('univBundle:departement')->findAll();

        $resultats = array();

        $contenuParam="";
        if (!empty($idSemetre)){
            if (!empty($matiererId)) {
                $contenuParam=" AND IDENTITY(r.semestre)= ".$idSemetre." AND IDENTITY(r.matiers)= ".$matiererId;
            }else{
                $contenuParam=" AND IDENTITY(r.semestre)= ".$idSemetre;
            }
        }

        if (!is_null($idDepartement) && !is_null($idNiveau)) {

            $querryResultat = $em->createQuery(
                'SELECT IDENTITY (r.etudiant) AS etudiantId, count(IDENTITY (r.etudiant)) as nombre, avg (r.moyenneGenerale) as sumMoyenneGenerale,
                             IDENTITY(r.licence) , 
                            IDENTITY(r.semestre),IDENTITY(r.sessions),e.matricule,e.nomPrenom
                   FROM univBundle:Resultat r,univBundle:Etudiants e
                   WHERE e.id= IDENTITY(r.etudiant)
                      AND  IDENTITY(r.licence) =:licence
                      AND   IDENTITY(r.sessions)=:sessions
                     '.$contenuParam.'GROUP BY etudiantId ORDER BY sumMoyenneGenerale asc')
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $resultats = $querryResultat->getResult();

        }

        return $this->render('univBundle:resultat:listeResultat.html.twig', array(
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'sessionOne' => $sessionOne,
            'departements' => $departements,
            'sessionEncour' => $sessionEncour,
            'resultats' => $resultats
        ));
    }


    public function consultationFicheResultatAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $matiererId = $request->get('univbundle_matieres');
        $idDepartement = $request->get('univbundle_departement');

        $idNiveau = $request->get('univbundle_niveaux');

        $idSemetre = $request->get('univbundle_semestre');

        $idSession = $request->get('univbundle_session');

        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresBySemestre = $em->getRepository('univBundle:Matiers')->findBySemestre($semestreOne);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        $resultats = array();
//       var_dump($idSemetre);die();
        $contenuParam="";
        if (!empty($idSemetre)){
                $contenuParam=" AND IDENTITY(r.semestre)= ".$idSemetre;
        }

        if (!is_null($idDepartement) && !is_null($idNiveau)  && !is_null($sessionOne)) {
            $querryResultat = $em->createQuery(
                'SELECT IDENTITY (r.etudiant) AS etudiantId, count(IDENTITY (r.etudiant)) as nombre ,avg(r.moyenneGenerale) as sumMoyenneGenerale,
                             IDENTITY(r.licence) , 
                            IDENTITY(r.semestre),IDENTITY(r.sessions),e.matricule,e.nomPrenom
                   FROM univBundle:Resultat r,univBundle:Etudiants e
                   WHERE e.id= IDENTITY(r.etudiant)
                      AND  IDENTITY(r.licence) =:licence
                      AND   IDENTITY(r.sessions)=:sessions
                      '.$contenuParam.'
                     GROUP BY etudiantId ORDER BY sumMoyenneGenerale asc')
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $resultats = $querryResultat->getResult();

            $querryMatiere = $em->createQuery(
                'SELECT  IDENTITY (r.etudiant) AS etudiantId,r.moyenneGenerale as sumMoyenneGenerale, IDENTITY(r.licence) , 
                            IDENTITY(r.semestre),IDENTITY(r.sessions),m.name,m.id as idMatiere
                   FROM univBundle:Resultat r,univBundle:Matiers m
                   WHERE m.id= IDENTITY(r.matiers)
                      AND     IDENTITY(r.licence) =:licence
                      AND   IDENTITY(r.sessions)=:sessions
                      '.$contenuParam.'
                    ')->setParameter('licence', $licenceOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $resultatMatiere=$querryMatiere->getResult();
            $r = [];
            foreach ($resultatMatiere as $m) {
                if (!isset($r[$m['name']])){
                    $r[$m['name']] = array();
                    $r[$m['name']][$m['etudiantId']] = $m['sumMoyenneGenerale'];
                }else {
                    $r[$m['name']][$m['etudiantId']] = $m['sumMoyenneGenerale'];
                }

            }
        }else{
            $r = [];
        }





//        var_dump($r);die();

        return $this->render('univBundle:resultat:consultationFicheResultat.html.twig', array(
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieres' => $matieresBySemestre,
            'sessionOne' => $sessionOne,
            'departements' => $departements,
            'sessionEncour' => $sessionEncour,
            'resultats' => $resultats,
            'resultatMatiere'=>$r
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationDeNotesEtuiantEnSessionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        if (!is_null($idDepartement) && $_SERVER['REQUEST_METHOD'] == 'POST') {

            //////////////////end
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
            $matiererId = $univbundle_matieres;
            $querryEtudiant = $em->createQuery(
                'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   IDENTITY (n.sessions)=:sessions
                   AND   I.sessions=:sessions  AND n.session1 < m.baremSup  AND n.session1>m.baremInf AND  n.session2=0
                  ')
                ->setParameter('matiere', $matiererId)
                ->setParameter('licence', $licenceOne)
                ->setParameter('semestre', $semestreOne)
                ->setParameter('sessions', $sessionOne);
            $notesEtudiants = $querryEtudiant->getResult();


        } else {
            $notesEtudiants = $em->getRepository('univBundle:Notation')->find(0);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);
        }

        $noteSession2 = $request->get('noteSession2');
        $idNotation = $request->get('validNotation');

        if (!is_null($noteSession2) && !is_null($idNotation) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $Notation = $em->getRepository('univBundle:Notation')->findOneById($idNotation);
            $Notation->setSession2($noteSession2);
            $em->persist($Notation);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
//            return $this->redirectToRoute('univ_notation_Etudiant_En_Session1');

            //////////////////end
            $univbundle_matieres = $request->get('SSmatiere');
            $idDepartement = $request->get('SSdepartement');
            $idNiveau = $request->get('SSlicence');
            $idSemetre = $request->get('SSsemestre');
            $idSession = $request->get('SSsession');

            $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
            $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
            $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
            $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
            $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
            $matiererId = $univbundle_matieres;
            $querryEtudiant = $em->createQuery(
                'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   I.sessions=:sessions  AND n.session1< m.baremSup  AND n.session1>m.baremInf AND  n.session2=0
                  ')
                ->setParameter('matiere', $matiererId)
                ->setParameter('licence', $licenceOne)
                ->setParameter('semestre', $semestreOne)
                ->setParameter('sessions', $sessionOne);
            $notesEtudiants = $querryEtudiant->getResult();

        }


        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:Etudiant_En_Session1.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:Etudiant_En_Session1.html.twig";
        } else {
            $view = "univBundle:superAdmin:Etudiant_En_Session1.html.twig";
        }

        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'notesEtudiants' => $notesEtudiants,
            'sessionOne' => $sessionOne
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationDeNotesEtuiantEnDetteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);
        if (!is_null($idDepartement) && $_SERVER['REQUEST_METHOD'] == 'POST') {

            //////////////////end
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->findByDepartement($departementOne);
            $matiererId = $univbundle_matieres;
            $querryEtudiant = $em->createQuery(
                'SELECT n 
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre
                    AND   n.sessions=:sessions
                   AND   I.sessions=:sessions AND n.session2 < m.baremSup  AND n.session1 <m.baremInf
                  ')
                ->setParameter('matiere', $matiererId)
                ->setParameter('licence', $licenceOne)
                ->setParameter('semestre', $semestreOne)
                ->setParameter('sessions', $sessionOne);
            $notesEtudiants = $querryEtudiant->getResult();


        } else {
            $notesEtudiants = $em->getRepository('univBundle:Notation')->find(0);
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);
        }

        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:Etudiant_En_Dette.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:Etudiant_En_Dette.html.twig";
        } else {
            $view = "univBundle:Default:Etudiant_En_Session1.html.twig";
        }

        $niveaux = $em->getRepository('univBundle:Licence')->findAll();
        $matieres = $em->getRepository('univBundle:Matiers')->findAll();
        $etudiants = $em->getRepository('univBundle:etudiants')->findAll();
        $semestres = $em->getRepository('univBundle:semetre')->findAll();
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render($view, array(
            'semestres' => $semestres,
            'niveaux' => $niveaux,
            'matieres' => $matieres,
            'etudiants' => $etudiants,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'notesEtudiants' => $notesEtudiants,
            'sessionOne' => $sessionOne
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function modificationNotesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $noteCour = $request->get('moyenneCours');
        $noteExament = $request->get('moyenneExament');

        $idDepartement = $request->get('idDepartement');
        $idMatiere = $request->get('idMatiere');
        $idSemestre = $request->get('idSemestre');
        $idNiveau = $request->get('idNiveau');


        $idNotation = $request->get('idNotation');
        if (!is_null($idNotation) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $notesEtudiant = $em->getRepository('univBundle:Notation')->findOneById($idNotation);
            $notesEtudiant->setMoyenneCours($noteCour);
            $notesEtudiant->setMoyenneExament($noteExament);
            $notesEtudiant->setSession1($noteCour * 0.6 + $noteExament * 0.4);

            $em->persist($notesEtudiant);
            $em->flush();
        }
        $parame = array('idDepartement' => $idDepartement, 'idNiveau' => $idNiveau, 'idSemetre' => $idSemestre, 'idmatieres' => $idMatiere);
        $mserror = "Operation effectuer avec success";
        $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
        $this->get('session')->getFlashBag()->add('success', $errorMessage);
        return $this->redirectToRoute('univ_EnseignantmatierFiche_de_Note', $parame);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationlistsEtudiantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $univbundle_matieres = $request->get('univbundle_matieres');
        $matiererId = $request->get('idMatiere');
        $idDepartement = $request->get('univbundle_departement');
        $idNiveau = $request->get('univbundle_niveaux');
        $idSemetre = $request->get('univbundle_semestre');
        $idSession = $request->get('univbundle_session');
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($idDepartement);
        $licenceOne = $em->getRepository('univBundle:Licence')->findOneById($idNiveau);
        $semestreOne = $em->getRepository('univBundle:semetre')->findOneById($idSemetre);
        $matieresOne = $em->getRepository('univBundle:Matiers')->findOneById($univbundle_matieres);
        $sessionOne = $em->getRepository('univBundle:Sessions')->findOneById($idSession);

        //////////////////end

        if (!is_null($departementOne) && !is_null($sessionOne) && !is_null($licenceOne) && $_SERVER['REQUEST_METHOD'] == 'GET') {

//            $sql = 'SELECT E.departement_id as departement_id, E.filiation,E.sexe, E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id ,I.session_id,E.departement_id,I.licence_id
//                                FROM
//                                           `etudiants`   E
//                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
//                                LEFT JOIN  `notation`    N  ON     E.id=N.etudiant_id
//                                AND I.session_id=:session_id
//                                AND I.licence_id=:licence_id
//                                AND E.departement_id =:departement_id
//                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
//            $params = array('session_id' => $sessionOne->getId(), 'licence_id' => $licenceOne->getId(), 'departement_id' => $departementOne->getId(),);
//            $etudiantListe = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
            $querryEtudiant = $em->createQuery(
                'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.licence) =:licence
                   AND   n.sessions=:sessions 
                    GROUP by e.id
                  ')
                ->setParameter('licence', $licenceOne->getId())
                ->setParameter('sessions', $sessionOne->getId());
            $notes2 = $querryEtudiant->getResult();

            $etudiantsByDepartement = $notes2;
            $matiererId = $univbundle_matieres;
        } else {
            $etudiantsByDepartement = $em->getRepository('univBundle:Etudiants')->find(0);

        }
        if ($this->getUser()->getRolesUser() == "ROLE_CHEF_DEPARTEMENT") {
            $view = "univBundle:chefDepartement:notationlisteEtudiants.html.twig";
        } elseif ($this->getUser()->getRolesUser() == "ROLE_SUPER_ADMIN") {
            $view = "univBundle:superAdmin:notationlisteEtudiants.html.twig";
        } else {
            $view = "univBundle:superAdmin:notationlisteEtudiants.html.twig";
        }


        $departements = $em->getRepository('univBundle:departement')->findAll();
        return $this->render($view, array(
            'etudiants' => $etudiantsByDepartement,
            'departements' => $departements,
            'etudiantsByDepartement' => $etudiantsByDepartement,
            'idMatiere' => $matiererId,
            'departementOne' => $departementOne,
            'licenceOne' => $licenceOne,
            'semestreOne' => $semestreOne,
            'matieresOne' => $matieresOne,
            'seeionOne' => $sessionOne,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function notationBulletinDeNotesAction(Request $request, $matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
//
//        ////////////////// la dernier inscription
//        $queryMaxIdInscriptionLicence = $em->createQuery(
//            'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
//        $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
//        $queryLastInscriptionLicence = $em->createQuery(
//            'SELECT e  FROM univBundle:Inscription e
//                    WHERE e.id=:maxid')
//            ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);
//        $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();
        /////////////////end //////////////////////////////
        $requestLicence = $request->get('licence');
        $requestSession = $request->get('reqSession');
        if (!is_null($requestLicence) && $_SERVER['REQUEST_METHOD'] == 'GET') {
            ////////////////// la dernier inscription
            $queryMaxIdInscriptionLicence = $em->createQuery(
                'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant AND IDENTITY(e.sessions)=:sessions')->setParameter('etudiant', $etudiant->getId())
                ->setParameter('sessions', $requestSession);
            $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
            $queryLastInscriptionLicence = $em->createQuery(
                'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
                ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);
            $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();


            $licence = $em->getRepository('univBundle:Licence')->findOneById($requestLicence);
            $session = $em->getRepository('univBundle:Sessions')->findOneById($requestSession);
            $selectedLicenceId = $requestLicence;
            $selectedSessionId = $requestSession;

        } else {

            ////////////////// la dernier inscription
            $queryMaxIdInscriptionLicence = $em->createQuery(
                'SELECT  max(e.id) as maxid FROM univBundle:Inscription  e WHERE IDENTITY (e.etudiant)=:etudiant')->setParameter('etudiant', $etudiant->getId());
            $MaxIdInscriptionLicence = $queryMaxIdInscriptionLicence->getResult();
            $queryLastInscriptionLicence = $em->createQuery(
                'SELECT e  FROM univBundle:Inscription e
                    WHERE e.id=:maxid')
                ->setParameter('maxid', $MaxIdInscriptionLicence[0]['maxid']);
            $lastInscriptionlicence = $queryLastInscriptionLicence->getOneOrNullResult();

            $licence = $lastInscriptionlicence->getLicence();
            $selectedLicenceId = $licence->getId();

            $session = $lastInscriptionlicence->getSessions();
            $selectedSessionId = $session->getId();
        }

        //////////// notes en fonction  de l'etudiant de la licence  deniere licence par defau//////////
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:Notation n
                  WHERE 
                  n.licence=:licence
                  AND IDENTITY (n.etudiant)=:etudiant
                  AND IDENTITY (n.sessions)=:sessions
                   ')
            ->setParameter('licence', $licence->getId())
            ->setParameter('sessions', $lastInscriptionlicence->getSessions()->getId())
            ->setParameter('etudiant', $etudiant->getId());
        $notesEtudiants = $querryEtudiant->getResult();

        //////////////////end


        $licences = $em->getRepository('univBundle:Licence')->findAll();
        $sessions = $em->getRepository('univBundle:Sessions')->findAll();


        //////////  Recuperation de la session  en fonction de la derniere inscription et de la licence /////////////////
        $querrysession = $em->createQuery(
            'SELECT I  FROM univBundle:Inscription I
                  WHERE 
                  IDENTITY(I.etudiant)=:etudiant
       
                   AND IDENTITY (I.sessions)=:sessions
                   and 
                   IDENTITY(I.licence)=:licence')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $lastInscriptionlicence->getSessions()->getId())
            ->setParameter('licence', $licence->getId());
        $session = $querrysession->getOneOrNullResult();
//        $lastInscriptionlicence
        //////////////////////////en //////////////////////////////////////////////
//  var_dump($lastInscriptionlicence->getLicence()->getId());die();
        return $this->render('univBundle:Default:bulletin_de_note.html.twig', array(
            'notesEtudiants' => $notesEtudiants,
            'etudiant' => $etudiant,
            'licence' => $licence,
            'licences' => $licences,
            'ins_session' => $lastInscriptionlicence->getSessions(),
            'sessions' => $sessions,
            'temoinIdCodeLicence' => $lastInscriptionlicence->getLicence()->getId(),
            'selectedLicenceId' => $selectedLicenceId,
            'selectedSessionId' => $selectedSessionId
        ));
    }

    public function webcameAction()
    {
        #=====================Capture webcme==================
        $em = $this->getDoctrine()->getManager();
        $filename = sha1(uniqid(mt_rand(), true)) . '.' . time() . '.jpg';
        $filepath = "uploads/";
        if (!is_dir($filepath))
            mkdir($filepath);

        if (isset($_FILES['webcam'])) {
            move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath . $filename);
            $ima = new Media();
            $ima->setPath($filename);
            $em->persist($ima);
            $em->flush();
        }

        #=====================End web cam==================
        return new JsonResponse(array('photo' => $ima));
    }

    public function latestImageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $queryMaxIdImage = $em->createQuery(
            'SELECT  max(e.id) as maxid FROM univBundle:Media e');
        $maxidImage = $queryMaxIdImage->getResult();
        $queryLastImage = $em->createQuery(
            'SELECT e  FROM univBundle:Media e
                    WHERE e.id=:maxid')
            ->setParameter('maxid', $maxidImage[0]['maxid']);
        $lastImage = $queryLastImage->getOneOrNullResult();

        return new JsonResponse(array('pathi' => $lastImage->getPath(), 'id' => $lastImage->getId()));
    }

    public function matiereByDepartementjaxAction(Request $request)
    {
        $departement_id = $request->request->get('departement_id');
        $semetre_id = $request->request->get('semestre_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $sql = "SELECT * FROM matiers
               ";
//        WHERE matiers.id not  in  ( select en.matiers_id  FROM enseigner en )
        if ($departement_id != '' && $semetre_id != '')
            $sql .= " where matiers.departement_id = :departement_id AND matiers.semetre_id = :semestre_id  ";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($departement_id != '' && $semetre_id != '')
            $query
                ->setParameter('departement_id', $departement_id)
                ->setParameter('semestre_id', $semetre_id);

        $matiers = $query->getResult();

        $matiersList = array();
        foreach ($matiers as $matiers) {
            $p = array();
            $p['id'] = $matiers['id'];
            $p['name'] = $matiers['name'];
            $matiersList[] = $p;
        }

        return new JsonResponse($matiersList);


    }

    public function lesEnseignantAffecterparteMatiereDuDepartementjaxAction(Request $request)
    {
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $departement_id = $request->request->get('departement_id');
        $matiers_id = $request->request->get('matiers_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('MatiereId', 'MatiereId');
        $rsm->addScalarResult('matiereName', 'matiereName');
        $rsm->addScalarResult('enseignantId', 'enseignantId');
        $rsm->addScalarResult('enseignantName', 'enseignantName');
        $rsm->addScalarResult('nomComplet', 'nomComplet');


        $sql = "SELECT ENS.name as enseignantName, ENS.id as enseignantId ,CONCAT(ENS.name,' ',ENS.prenom,' ','( TEL: ',ENS.telephone,')')as nomComplet
                  FROM          enseignant  ENS
                 INNER JOIN enseigner     EN ON ENS.id=EN.enseignant_id
                 INNER JOIN matiers       M  ON M.id=EN.matiers_id
                 INNER JOIN sessions      S ON S.id= EN.Sessions_id
                 INNER JOIN departement   D ON D.id=M.departement_id
                        ";

        if ($departement_id != '')
            $sql .= "AND EN.Sessions_id=:Sessions_id  AND M.departement_id =:departement_id
                      AND  M.id=:matiers_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($departement_id != '')
            $query
                ->setParameter('Sessions_id', $sessionEncour->getId())
                ->setParameter('matiers_id', $matiers_id)
                ->setParameter('departement_id', $departement_id);

        $enseignants = $query->getResult();

        $enseignantsList = array();
        foreach ($enseignants as $enseignants) {
            $p = array();
            $p['enseignantId'] = $enseignants['enseignantId'];
            $p['enseignantName'] = $enseignants['enseignantName'];
            $p['nomComplet'] = $enseignants['nomComplet'];
            $enseignantsList[] = $p;
        }
        if (count($enseignantsList) > 0) {
            return new JsonResponse($enseignantsList);
        } else {
            return new JsonResponse();
        }


    }


    public function lesMatiereLierAEnseignantDepartementjaxAction(Request $request)
    {
        $departement_id = $request->request->get('departement_id');
        $semetre_id = $request->request->get('semestre_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $sql = "SELECT M.* FROM
                  matiers M
                  INNER JOIN departement   D ON D.id=M.departement_id
                  INNER JOIN semetre S ON S.id=M.semetre_id ";
        if ($departement_id != '' && $semetre_id != '')
            $sql .= "AND S.id=:semetre_id  AND M.departement_id =:departement_id";
        $query = $em->createNativeQuery($sql, $rsm);

        if ($departement_id != '' && $semetre_id != '')
            $query
                ->setParameter('semetre_id', $semetre_id)
                ->setParameter('departement_id', $departement_id);
        $enseignants = $query->getResult();
        $enseignantsList = array();
        foreach ($enseignants as $enseignants) {
            $p = array();
            $p['id'] = $enseignants['id'];
            $p['name'] = $enseignants['name'];
            $enseignantsList[] = $p;
        }

        return new JsonResponse($enseignantsList);

    }

    public function matiereByEnseignantjaxAction(Request $request)
    {
        $enseignant_id = $request->request->get('enseignant_id');
        $semetre_id = $request->request->get('semetre_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = "SELECT M.* from matiers M 
                        INNER JOIN enseigner E ON M.id = E.matiers_id
                        INNER JOIN enseignant A ON A.id= E.enseignant_id 
                        INNER JOIN semetre S ON S.id=M.semetre_id
                        ";

        if ($enseignant_id != '' && $semetre_id != '')
            $sql .= "AND A.id=:enseignant_id = :enseignant_id AND S.id=:semetre_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($enseignant_id != '' && $semetre_id != '')
            $query
                ->setParameter('enseignant_id', $enseignant_id)
                ->setParameter('semetre_id', $semetre_id);

        $matiers = $query->getResult();

        $matiersList = array();
        foreach ($matiers as $matiers) {
            $p = array();
            $p['id'] = $matiers['id'];
            $p['name'] = $matiers['name'];
            $matiersList[] = $p;
        }

        return new JsonResponse($matiersList);

    }

    public function semestreByNiveaujaxAction(Request $request)
    {
        $licence_id = $request->request->get('licence_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = "SELECT * FROM semetre s
                WHERE 1 = 1 ";

        if ($licence_id != '')
            $sql .= " AND s.licence_id = :licence_id AND s.licence_id = :licence_id  ";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($licence_id != '')
            $query
                ->setParameter('licence_id', $licence_id);

        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['name'] = $semestres['name'];
            $semestresList[] = $p;
        }

        return new JsonResponse($semestresList);


    }

    public function NiveaujaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $sql = "SELECT * FROM licence s
                WHERE 1 = 1 ";
        $query = $em->createNativeQuery($sql, $rsm);
        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['name'] = $semestres['name'];
            $semestresList[] = $p;
        }
        return new JsonResponse($semestresList);
    }

    public function sessionsjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('sessions', 'sessions');
        $sql = "SELECT * FROM sessions s
                WHERE 1 = 1 ";
        $query = $em->createNativeQuery($sql, $rsm);
        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['sessions'] = $semestres['sessions'];
            $semestresList[] = $p;
        }
        return new JsonResponse($semestresList);
    }

    public function ficheNoteExcelAction($matiererId, $licenceOne, $semestreOne, $sessionOne, $departementOne)
    {
        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $departement = $em->getRepository('univBundle:departement')->findOneById($departementOne);
        $semestre = $em->getRepository('univBundle:semetre')->findOneById($semestreOne);
        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();
//       var_dump($enseignant->getPrenom());die();
        $querryEtudiant = $em->createQuery(
            'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
            ->setParameter('matiere', $matiere->getId())
            ->setParameter('licence', $licence->getId())
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $session->getId());
        $notes2 = $querryEtudiant->getResult();
//
//        $sql = 'SELECT N.id,  E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
//                                  N.dateDelaitNotation,N.dateNotation
//                                FROM
//                                           `etudiants`   E
//                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
//                                LEFT JOIN  `notation`    N  ON     E.id=N.etudiant_id
//                                AND N.matieres_id=:matieres_id
//                                AND I.session_id=:session_id
//                                AND I.licence_id=:licence_id
//                                AND E.departement_id =:departement_id
//                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
//        $params = array('matieres_id' => $matiere->getId(), 'session_id' => $session->getId(), 'licence_id' => $licence->getId(), 'departement_id' => $departement->getId(),);
//        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
////            var_dump($etudiant);die();
///
///

        return $this->render('univBundle:Default:ficheNote_excel.xlsx.twig', [
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant,
            'etudiants' => $notes2,
            'idMatiere' => $matiererId,
            'departementOne' => $departement,
            'licenceOne' => $licence,
            'semestreOne' => $semestre,
            'sessionOne' => $session
        ]);
    }

    public function ficheNoteExcelViergeAction($matiererId, $licenceOne, $semestreOne, $sessionOne, $departementOne)
    {
        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $departement = $em->getRepository('univBundle:departement')->findOneById($departementOne);
        $semestre = $em->getRepository('univBundle:semetre')->findOneById($semestreOne);
        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();
//       var_dump($enseignant->getPrenom());die();
        $querryEtudiant = $em->createQuery(
            'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
            ->setParameter('matiere', $matiere->getId())
            ->setParameter('licence', $licence->getId())
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $session->getId());
        $notes2 = $querryEtudiant->getResult();
        return $this->render('univBundle:Default:ficheNote_excel_vierge.xlsx.twig', [
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant,
            'etudiants' => $notes2,
            'idMatiere' => $matiererId,
            'departementOne' => $departement,
            'licenceOne' => $licence,
            'semestreOne' => $semestre,
            'sessionOne' => $session
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function ficheNotePdfAction($matiererId, $licenceOne, $semestreOne, $sessionOne, $departementOne)
    {

        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $departement = $em->getRepository('univBundle:departement')->findOneById($departementOne);
        $semestre = $em->getRepository('univBundle:semetre')->findOneById($semestreOne);

        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();

//        //////////////////end
//        $querryEtudiant = $em->createQuery(
//            'SELECT n
//                   FROM univBundle:Notation n
//                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
//                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
//                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
//                   AND   IDENTITY(n.matiers)=:matiere
//                   AND   IDENTITY(n.licence) =:licence
//                   AND   IDENTITY(n.semestre) =:semestre
//                   AND   I.sessions=:sessions
//                  ')
//            ->setParameter('matiere', $matiererId)
//            ->setParameter('licence', $licenceOne)
//            ->setParameter('semestre', $semestreOne)
//            ->setParameter('sessions', $sessionOne);
//        $notesEtudiants = $querryEtudiant->getResult();

//        $sql = 'SELECT N.id,  E.`matricule`,E.`nomPrenom`,N.`moyenneCours`, N.`moyenneExament`, N.`session1`, N.`session2`,I.etudiant_id,I.session_id,E.departement_id,I.licence_id,
//                                  N.dateDelaitNotation,N.dateNotation
//                                FROM
//                                           `etudiants`   E
//                                LEFT JOIN `inscription` I  ON     E.id=I.etudiant_id
//                                LEFT JOIN  `notation`    N  ON     E.id=N.etudiant_id
//                                AND N.matieres_id=:matieres_id
//                                AND I.session_id=:session_id
//                                AND I.licence_id=:licence_id
//                                AND E.departement_id =:departement_id
//                                GROUP BY I.etudiant_id,I.session_id,I.licence_id';
//        $params = array('matieres_id' => $matiere->getId(), 'session_id' => $session->getId(), 'licence_id' => $licence->getId(), 'departement_id' => $departement->getId(),);
//        $etudiant = $em->getConnection()->executeQuery($sql, $params)->fetchAll();
////            var_dump($etudiant);die();
//

        $querryEtudiant = $em->createQuery(
            'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
            ->setParameter('matiere', $matiere->getId())
            ->setParameter('licence', $licence->getId())
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $session->getId());
        $notes2 = $querryEtudiant->getResult();

        $ref = $matiere->getDepartement()->getFaculte()->getName() . "  " . $matiere->getName();
        $randon = random_int(1, 9999);
        $nomFichier = 'Fiche de Notes' . $randon . '.pdf';

        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();

        $html = $this->renderView('univBundle:Default:ficheNotePDF.html.twig', array(
            'notes' => $notes2,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant,
            'Universite' => $chefService,

            'idMatiere' => $matiererId,
            'departementOne' => $departement,
            'licenceOne' => $licence,
            'semestreOne' => $semestre,
            'sessionOne' => $session

        ));
        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
            'encoding' => 'utf-8',
            'images' => true,
            'cookie' => array(),
            'enable-external-links' => true,
            'enable-internal-links' => true,
            'enable-javascript' => true,)), $nomFichier);
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function ficheNoteViergePdfAction($matiererId, $licenceOne, $semestreOne, $sessionOne, $departementOne)
    {

        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $departement = $em->getRepository('univBundle:departement')->findOneById($departementOne);
        $semestre = $em->getRepository('univBundle:semetre')->findOneById($semestreOne);

        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();

        $querryEtudiant = $em->createQuery(
            'SELECT n
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre 
                   AND   n.sessions=:sessions 
                  ')
            ->setParameter('matiere', $matiere->getId())
            ->setParameter('licence', $licence->getId())
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $session->getId());
        $notes2 = $querryEtudiant->getResult();

        $ref = $matiere->getDepartement()->getFaculte()->getName() . "  " . $matiere->getName();
        $randon = random_int(1, 9999);
        $nomFichier = 'Fiche de Notes' . $randon . '.pdf';

        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();

        $html = $this->renderView('univBundle:Default:ficheNoteViergePDF.html.twig', array(
            'notes' => $notes2,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant,
            'Universite' => $chefService,

            'idMatiere' => $matiererId,
            'departementOne' => $departement,
            'licenceOne' => $licence,
            'semestreOne' => $semestre,
            'sessionOne' => $session
        ));
        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
            'encoding' => 'utf-8',
            'images' => true,
            'cookie' => array(),
            'enable-external-links' => true,
            'enable-internal-links' => true,
            'enable-javascript' => true,)), $nomFichier);
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function ListeEtudiantEnSessionExcelAction($matiererId, $licenceOne, $semestreOne, $sessionOne)
    {
        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();
//       var_dump($enseignant->getPrenom());die();
        $querryEtudiant = $em->createQuery(
            'SELECT n 
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre
                    AND   n.sessions=:sessions 
                  AND   I.sessions=:sessions  AND n.session1< m.baremSup  AND n.session1>m.baremInf AND  n.session2=0
                  ')
            ->setParameter('matiere', $matiererId)
            ->setParameter('licence', $licenceOne)
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $sessionOne);
        $notesEtudiants = $querryEtudiant->getResult();
        return $this->render('univBundle:chefDepartement:SessionnaireNote_excel.xlsx.twig', [
            'notes' => $notesEtudiants,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant
        ]);
    }

    public function ListeNoteEtudiantEnDettePdfAction($matiererId, $licenceOne, $semestreOne, $sessionOne)
    {

        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);


        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();

        //////////////////end
        $querryEtudiant = $em->createQuery(
            'SELECT n 
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre
                   AND   n.sessions=:sessions
                   AND   I.sessions=:sessions AND n.session2 < m.baremSup  AND n.session2!=0
                  ')
            ->setParameter('matiere', $matiererId)
            ->setParameter('licence', $licenceOne)
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $sessionOne);
        $notesEtudiants = $querryEtudiant->getResult();

//        $html = $this->container->get('templating')->render('univBundle:chefDepartement:EndetteNotePDF.html.twig',
//            array(
//                'notes' => $notesEtudiants,
//                'matiere' => $matiere,
//                'licence' => $licence,
//                'session' => $session,
//                'enseignant' => $enseignant
//            ));
//        //Le format (A4), la langue(fr)
//        $html2pdf = new \Html2Pdf_Html2Pdf('P', 'A4', 'fr');
//        //L'auteur ici Moi
//        $html2pdf->pdf->SetAuthor('GEST-UNIV');
//        $ref = $matiere->getDepartement()->getFaculte()->getName() . " / " . $matiere->getName();
//        //Le titre du fichie,  ici le numero de la facture
//        $html2pdf->pdf->SetTitle('Fiche de notes ' . $ref);
//        //
//        $html2pdf->pdf->SetSubject('Fiche de notes');
//        //
//        $html2pdf->pdf->SetKeywords('Fiche de note,GEST-UNIV');
//        //
//        $html2pdf->pdf->SetDisplayMode('real');
//        //
//        $html2pdf->writeHTML($html);
//        $randon = random_int(1, 9999);
//        //En fin on ecrite le fichier
//        $html2pdf->Output('Fiche de Notes' . $ref . $randon . '.pdf');
////        var_dump($html);
////        die();
//        $response = new Response();
//
//        //Entete du navigateur (un pdf)
//        $response->headers->set('Content-type', 'application/pdf');
//
//        //On retourne la reponse
//        return $response;
//        return $html2pdf;

        $randon = random_int(1, 9999);
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        $html = $this->renderView('univBundle:chefDepartement:EndetteNotePDF.html.twig', array(
            'Universite' => $chefService,
            'notes' => $notesEtudiants,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,

            )),
            'Fiche de Notes etudiants en dette' . $randon . '.pdf'
        );


    }

    public function ListeEtudiantEnDetteExcelAction($matiererId, $licenceOne, $semestreOne, $sessionOne)
    {
        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();
//       var_dump($enseignant->getPrenom());die();
        $querryEtudiant = $em->createQuery(
            'SELECT n 
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre
                  AND  n.sessions=:sessions 
                  AND   I.sessions=:sessions AND n.session2 < m.baremSup  AND n.session2!=0
                  ')
            ->setParameter('matiere', $matiererId)
            ->setParameter('licence', $licenceOne)
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $sessionOne);
        $notesEtudiants = $querryEtudiant->getResult();
        return $this->render('univBundle:chefDepartement:EnDetteNote_excel.xlsx.twig', [
            'notes' => $notesEtudiants,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function ListeNoteEtudiantEnSessionPdfAction($matiererId, $licenceOne, $semestreOne, $sessionOne)
    {

        $em = $this->getDoctrine()->getManager();
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceOne);
        $matiere = $em->getRepository('univBundle:Matiers')->findOneById($matiererId);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionOne);
        $querryenseignatMatieres = $em->createQuery(
            'SELECT ens
                   FROM univBundle:Enseignant ens 
                   INNER JOIN  univBundle:enseigner er WITH ens.id=IDENTITY (er.enseignant)
                   INNER JOIN  univBundle:matiers m WITH m.id=IDENTITY (er.matiers)
                   AND   (m.id) =:matiers
                  ')
            ->setParameter('matiers', $matiererId);
        $enseignant = $querryenseignatMatieres->getOneOrNullResult();

        //////////////////end
        $querryEtudiant = $em->createQuery(
            'SELECT n 
                   FROM univBundle:Notation n 
                   INNER JOIN  univBundle:Matiers m WITH m.id=IDENTITY (n.matiers)
                   INNER JOIN  univBundle:Etudiants e WITH e.id=IDENTITY (n.etudiant)
                   INNER JOIN univBundle:Inscription I WITH e.id=IDENTITY (I.etudiant)
                   AND   IDENTITY(n.matiers)=:matiere 
                   AND   IDENTITY(n.licence) =:licence
                   AND   IDENTITY(n.semestre) =:semestre
                   AND   n.sessions=:sessions 
                   AND   I.sessions=:sessions  AND n.session1< m.baremSup  AND n.session1>m.baremInf AND  n.session2=0
                  ')
            ->setParameter('matiere', $matiererId)
            ->setParameter('licence', $licenceOne)
            ->setParameter('semestre', $semestreOne)
            ->setParameter('sessions', $sessionOne);
        $notesEtudiants = $querryEtudiant->getResult();

//        $html = $this->container->get('templating')->render('univBundle:chefDepartement:SessionaireNotePDF.html.twig',
//            array(
//                'notes' => $notesEtudiants,
//                'matiere' => $matiere,
//                'licence' => $licence,
//                'session' => $session,
//                'enseignant' => $enseignant
//            ));
//        //Le format (A4), la langue(fr)
//        $html2pdf = new \Html2Pdf_Html2Pdf('P', 'A4', 'fr');
//        //L'auteur ici Moi
//        $html2pdf->pdf->SetAuthor('GEST-UNIV');
//        $ref = $matiere->getDepartement()->getFaculte()->getName() . " / " . $matiere->getName();
//        //Le titre du fichie,  ici le numero de la facture
//        $html2pdf->pdf->SetTitle('Fiche de notes ' . $ref);
//        //
//        $html2pdf->pdf->SetSubject('Fiche de notes');
//        //
//        $html2pdf->pdf->SetKeywords('Fiche de note,GEST-UNIV');
//        //
//        $html2pdf->pdf->SetDisplayMode('real');
//        //
//        $html2pdf->writeHTML($html);
//        $randon = random_int(1, 9999);
//        //En fin on ecrite le fichier
//        $html2pdf->Output('Fiche de Notes' . $ref . $randon . '.pdf');
////        var_dump($html);
////        die();
//        $response = new Response();
//
//        //Entete du navigateur (un pdf)
//        $response->headers->set('Content-type', 'application/pdf');
//
//        //On retourne la reponse
//        return $response;
//        return $html2pdf;
//        $ref = $matiere->getDepartement()->getFaculte()->getName() . " - " . $matiere->getName();
        $randon = random_int(1, 9999);
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        $html = $this->renderView('univBundle:chefDepartement:SessionaireNotePDF.html.twig', array(
            'Universite' => $chefService,
            'notes' => $notesEtudiants,
            'matiere' => $matiere,
            'licence' => $licence,
            'session' => $session,
            'enseignant' => $enseignant
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,

            )),
            'Fiche de Notes' . $randon . '.pdf'
        );


    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function bulletinNotePdfAction($requestLicence, $matricule, $sessionsId)
    {
        $em = $this->getDoctrine()->getManager();

        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionsId);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($requestLicence);
        //////////// notes en fonction  de l'etudiant de la licence  deniere licence par defau//////////
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:Notation n
                  WHERE 
                  n.licence=:licence
                  AND  IDENTITY(n.sessions)=:sessions
                  AND IDENTITY (n.etudiant)=:etudiant
                   ')
            ->setParameter('licence', $licence->getId())
            ->setParameter('sessions', $sessionsId)
            ->setParameter('etudiant', $etudiant->getId());
        $notesEtudiants = $querryEtudiant->getResult();

        //////////////////end
        ///
        //////////  Recuperation de la session  en fonction de la derniere inscription et de la licence /////////////////
//        $querrysession = $em->createQuery(
//            'SELECT I  FROM univBundle:Inscription I
//                  WHERE
//                  IDENTITY(I.etudiant)=:etudiant
//                   AND
//                   IDENTITY(I.licence)=:licence')
//            ->setParameter('etudiant', $etudiant->getId())
//            ->setParameter('licence', $licence->getId());
//        $session = $querrysession->getOneOrNullResult();

        //////////////////////////en //////////////////////////////////////////////

//        $html = $this->container->get('templating')->render('univBundle:Default:bulletinNotePDF.html.twig',
//            array(
//                'notesEtudiants' => $notesEtudiants,
//                'etudiant' => $etudiant,
//                'licence' => $licence,
//                'ins_session' => $session,
//                'Universite'=>$chefService
//            ));
//        //Le format (A4), la langue(fr)
//        $html2pdf = new \Html2Pdf_Html2Pdf('P', 'A4', 'fr');
//        //L'auteur ici Moi
//        $html2pdf->pdf->SetAuthor('GEST-UNIV');
        $ref = $etudiant->getMatricule() . '_' . $session->getSessions();
////        $ref = "Bulletin";
//        //Le titre du fichie,  ici le numero de la facture
//        $html2pdf->pdf->SetTitle('Bulletin de notes ' . $ref);
//        //
//        $html2pdf->pdf->SetSubject('Bulletin de notes');
//        //
//        $html2pdf->pdf->SetKeywords('Bulletin de note,GEST-UNIV');
//        //
//        $html2pdf->pdf->SetDisplayMode('real');
//        //
//        $html2pdf->writeHTML($html);
//        $randon = random_int(1, 9999);
//        //En fin on ecrite le fichier
//        $html2pdf->Output('Bulletin de Notes' . '.pdf');
////        var_dump($html);
////        die();
//        $response = new Response();
//
//        //Entete du navigateur (un pdf)
//        $response->headers->set('Content-type', 'application/pdf');
//        //On retourne la reponse
//        return $response;
//        return $html2pdf;

        $html = $this->renderView('univBundle:Default:bulletinNotePDF.html.twig', array(
            'notesEtudiants' => $notesEtudiants,
            'etudiant' => $etudiant,
            'licence' => $licence,
            'ins_session' => $session,
            'Universite' => $chefService
        ));
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,

            )),
            'Bulletin de Notes' . $ref . '.pdf'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function bulletinNoteExcelAction($requestLicence, $matricule, $sessionsId)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($requestLicence);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionsId);
        //////////// notes en fonction  de l'etudiant de la licence  deniere licence par defau//////////
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:Notation n
                  WHERE 
                  n.licence=:licence
                  AND  IDENTITY(n.sessions)=:sessions
                  AND IDENTITY (n.etudiant)=:etudiant
                   ')
            ->setParameter('licence', $licence->getId())
            ->setParameter('sessions', $sessionsId)
            ->setParameter('etudiant', $etudiant->getId());
        $notesEtudiants = $querryEtudiant->getResult();

        //////////////////end
        return $this->render('univBundle:Default:bulletinNote_excel.xlsx.twig', [
            'notesEtudiants' => $notesEtudiants,
            'etudiant' => $etudiant,
            'licence' => $licence,
            'ins_session' => $session,
        ]);
    }

    public function listeEtudiantByDepartementExcelAction($departementId, $licenceForUrl, $sessionForUrl)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionForUrl);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceForUrl);
        if (is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequest = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                  ');
            $listeEtudiants = $querryEtudiantSansPparamRequest->getResult();
            $sessionOne = 0;
        } elseif (!is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequestSessionS = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestSessionS->getResult();
            $sessionOne = 0;
        } elseif (is_null($licence) && !is_null($session)) {
            $querryEtudiantSansPparamRequestLicence = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions ')
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestLicence->getResult();
            $sessionOne = $session->getSessions();
        } elseif (!is_null($licence) && !is_null($session)) {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $sessionOne = $session->getSessions();
        } else {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $sessionOne = 0;
        }
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($departementId);

        return $this->render('univBundle:Default:liste_desEtudiantByDepartement_excel.xlsx.twig', [
            'etudiants' => $listeEtudiants,
            'departement' => $departementOne,
            'session' => $sessionOne
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeEtudiantByDepartementPdfAction($departementId, $licenceForUrl, $sessionForUrl)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionForUrl);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licenceForUrl);
        if (is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequest = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                  ');
            $listeEtudiants = $querryEtudiantSansPparamRequest->getResult();
            $sessionOne = 0;
        } elseif (!is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequestSessionS = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestSessionS->getResult();
            $sessionOne = 0;
        } elseif (is_null($licence) && !is_null($session)) {
            $querryEtudiantSansPparamRequestLicence = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions ')
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestLicence->getResult();
            $sessionOne = $session->getSessions();
        } elseif (!is_null($licence) && !is_null($session)) {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $sessionOne = $session->getSessions();
        } else {
            $querryEtudiant = $em->createQuery(
                'SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence')
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $sessionOne = 0;
        }
        $departementOne = $em->getRepository('univBundle:departement')->findOneById($departementId);

        $html = $this->renderView('univBundle:Default:listeEtudiantBydepartementPDF.html.twig', array(
            'etudiants' => $listeEtudiants,
            'departement' => $departementOne,
            'session' => $sessionOne
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,
//                'page-height' => 200,
//                'page-width' => 120,

            )),
            'LISTE DES ETUDIANTS' . $departementOne->getFaculte()->getName() . " Departement :" . $departementOne->getName() . '.pdf'
        );

//        $html = $this->container->get('templating')->render('univBundle:Default:listeEtudiantBydepartementPDF.html.twig',
//            array(
//                'etudiants' => $listeEtudiants,
//                'departement' => $departementOne,
//                'session' => $sessionOne
//            ));
//        //Le format (A4), la langue(fr)
//        $html2pdf = new \Html2Pdf_Html2Pdf('P', 'A4', 'fr');
//        //L'auteur ici Moi
//        $html2pdf->pdf->SetAuthor('GEST-UNIV');
//        $ref = $departementOne->getFaculte()->getName() . " Departement :" . $departementOne->getName();
//        //Le titre du fichie,  ici le numero de la facture
//        $html2pdf->pdf->SetTitle('LISTE DES ETUDIANTS :' . $ref);
//        //
//        $html2pdf->pdf->SetSubject('LISTE DES ETUDIANTS');
//        //
//        $html2pdf->pdf->SetKeywords('LISTE DES ETUDIANTS,GEST-UNIV');
//        //
//        $html2pdf->pdf->SetDisplayMode('real');
//        //
//        $html2pdf->writeHTML($html);
//        $randon = random_int(1, 9999);
//        //En fin on ecrite le fichier
//        $html2pdf->Output('LISTE DES ETUDIANTS' . $ref . '.pdf');
////        var_dump($html);
////        die();
//        $response = new Response();
//
//        //Entete du navigateur (un pdf)
//        $response->headers->set('Content-type', 'application/pdf');
//
//        //On retourne la reponse
//        return $response;
////        return $html2pdf;
    }

    public function concentrationjaxAction(Request $request)
    {
        $departement_id = $request->request->get('departement_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
//        $sql = "SELECT * FROM concentration c
//                WHERE 1 = 1 ";
        if ($departement_id != '')
            $sql = "SELECT * FROM concentration c
                WHERE  c.departement_id = :departement_id ";
        $query = $em->createNativeQuery($sql, $rsm);
        if ($departement_id != '')
            $query
                ->setParameter('departement_id', $departement_id);
        $concentration = $query->getResult();
        $concentrationList = array();
        foreach ($concentration as $concentration) {
            $p = array();
            $p['id'] = $concentration['id'];
            $p['name'] = $concentration['name'];
            $concentrationList[] = $p;
        }

        return new JsonResponse($concentrationList);

    }

    public function etudiantByDepartementjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('matricule', 'matricule');
        $sql = "SELECT * FROM etudiants s
                WHERE 1 = 1 ";
        $query = $em->createNativeQuery($sql, $rsm);
        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['matricule'] = $semestres['matricule'];
            $semestresList[] = $p;
        }
        return new JsonResponse($semestresList);
    }

    public function depByfaculteAjaxAction(Request $request)
    {
        $faculte_id = $request->request->get('faculte_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = "SELECT * FROM departement s
                WHERE 1 = 1 ";

        if ($faculte_id != '')
            $sql .= " AND s.facute_id = :faculte_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($faculte_id != '')
            $query
                ->setParameter('faculte_id', $faculte_id);

        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['name'] = $semestres['name'];
            $semestresList[] = $p;
        }

        return new JsonResponse($semestresList);


    }

    public function departementjaxAction(Request $request)
    {
////        $idFaculte=1;
//        $em = $this->getDoctrine()->getManager();
//        $faculte = $em->getRepository('univBundle:Facultes')->findOneById(1);
//        $departements = $em->getRepository('univBundle:departement')->findByFaculte($faculte);
//
//        $data = $this->get('jms_serializer')->serialize($departements, 'json');
//        $response = new Response($data);
//        $response->headers->set('Content-Type', 'application/json');
//        return $response;
//

        $licence_id = $request->request->get('facute_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $sql = "SELECT * FROM departement s
                WHERE 1 = 1 ";

        if ($licence_id != '')
            $sql .= "AND s.facute_id = :facute_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($licence_id != '')
            $query
                ->setParameter('facute_id', $licence_id);

        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['name'] = $semestres['name'];
            $semestresList[] = $p;
        }

        return new JsonResponse($semestresList);

    }

    public function testAction()
    {

        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:Etudiants n
                  WHERE 
                  n.id=458
                   ');
//        $BulletinPaimentEtudiant = $querryEtudiant->getOneOrNullResult();
        $etudiant = $em->getRepository('univBundle:Etudiants')->find(2);
//        return $this->render('univBundle:Default:test.html.twig', array(
//
//        ));

        return $this->render('univBundle:Default:test.html.twig', array(
            'etudiant' => $etudiant,
        ));
//        $html = $this->renderView('univBundle:Default:test.html.twig', array(
//            'etudiant' => $etudiant,
//        ));
//
//        return new PdfResponse(
//            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
//                'encoding' => 'utf-8',
//                'images' => true,
//                'cookie' => array(),
//                'enable-external-links' => true,
//                'enable-internal-links' => true,
//                'enable-javascript' => true,
////                'page-height' => 200,
////                'page-width' => 120,
//
//            )),
//            'badge.pdf'
//        );
//
//        //Entete du navigateur (un pdf)
//        $response->headers->set('Content-type', 'application/pdf');
//        //On retourne la reponse
//        return $response;
//        return $html2pdf;

//        $html = $this->renderView('univBundle:Default:test.html.twig', array(
//            'etudiant'  => $BulletinPaimentEtudiant
//        ));
//
//        return new PdfResponse(
//            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
////                'page-height' => 200,
////                'page-width' => 50,
//                 'encoding' => 'utf-8',
//                'images' => true,
//                'cookie' => array(),
//                'dpi' => 300,
//                'image-dpi' => 300,
//                'enable-external-links' => true,
//                'enable-internal-links' => true,
////                'orientation' => 'landscape',
//                'enable-javascript' => true,
//                'no-background' => false,
//                 'password'=>true,
//                'title'=>'mohamed',
//                'header-line'=>true
//            )),
//            'file.pdf'
//        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function badgeEtudiantAction($matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $debuAnne = $sessionEncour->getSessions();
        $finAnne = $debuAnne + 2;
        $annneUniversitaire = $debuAnne . '-' . $finAnne;
        $nameTable = explode(' ', $etudiant->getNomPrenom());
        $nom = end($nameTable);
        $prenom = '';
        for ($i = 0; $i < count($nameTable); $i++) {
            if ($nameTable[$i] != $nom) {
                continue;
                $prenom = $prenom . ' ' . $nameTable[$i];

            }
        }
//        var_dump($prenom);die();
        $html = $this->renderView('univBundle:Default:badge.html.twig', array(
            'etudiant' => $etudiant,
            'anneeUniv' => $annneUniversitaire,
            'nom' => $nom,
            'prenom' => $prenom
        ));
        return new PdfResponse($this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
            'encoding' => 'utf-8',
            'images' => true,
            'cookie' => array(),
            'enable-external-links' => true,
            'enable-internal-links' => true,
            'enable-javascript' => true,
//                'page-height' => 200,
//                'page-width' => 120,

        )), 'badge_' . $matricule . '.pdf');
    }

    public function NoteSimpleEtudiantByDepartementNiveauMatiereSessionAjaxAction(Request $request)
    {
        $etudiant_id = $request->request->get('etudiant_id');
        $licence_id = $request->request->get('licence_id');
        $matieres_id = $request->request->get('matieres_id');
        $Semestre_id = $request->request->get('Semestre_id');

        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('moyenneCours', 'moyenneCours');
        $rsm->addScalarResult('moyenneExament', 'moyenneExament');

        $sql = "SELECT * FROM notation n
                WHERE 1 = 1 ";

        if ($etudiant_id != '' && $licence_id != '' && $matieres_id != '' && $Semestre_id != '')
            $sql .= " AND n.etudiant_id = :etudiant_id AND n.licence_id=:licence_id AND n.matieres_id=:matieres_id AND n.Semestre_id=:Semestre_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($etudiant_id != '' && $licence_id != '' && $matieres_id != '' && $Semestre_id != '')
            $query
                ->setParameter('etudiant_id', $etudiant_id)
                ->setParameter('licence_id', $licence_id)
                ->setParameter('matieres_id', $matieres_id)
                ->setParameter('Semestre_id', $Semestre_id);

        $notation = $query->getResult();

        $notationList = array();
        foreach ($notation as $notation) {
            $p = array();
            $p['id'] = $notation['id'];
            $p['moyenneCours'] = $notation['moyenneCours'];
            $p['moyenneExament'] = $notation['moyenneExament'];
            $notationList[] = $p;
        }

        return new JsonResponse($notationList);


    }

    public function NoteSimpleEtudiantRecuperationByEtudiantSessionLicenceMatierAjaxAction(Request $request)
    {
        $etudiant_id = $request->request->get('etudiant_id');
        $licence_id = $request->request->get('licence_id');
        $matieres_id = $request->request->get('matieres_id');
        $session_id = $request->request->get('session_id');

        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('moyenneCours', 'moyenneCours');
        $rsm->addScalarResult('moyenneExament', 'moyenneExament');
//        if ($etudiant_id != '' && $licence_id != '' && $matieres_id != '' && $session_id != '')
        $sql = "SELECT  n.*   FROM notation n 
               WHERE
                n.etudiant_id = :etudiant_id AND n.licence_id=:licence_id AND n.matieres_id=:matieres_id AND n.session_id=:session_id";
        $query = $em->createNativeQuery($sql, $rsm)
            ->setParameter('etudiant_id', $etudiant_id)
            ->setParameter('licence_id', $licence_id)
            ->setParameter('matieres_id', $matieres_id)
            ->setParameter('session_id', $session_id);
        $notation = $query->getResult();

        $notationList = array();
        foreach ($notation as $notation) {
            $p = array();
            $p['id'] = $notation['id'];
            $p['moyenneCours'] = $notation['moyenneCours'];
            $p['moyenneExament'] = $notation['moyenneExament'];
            $notationList[] = $p;
        }
        if (count($notationList) > 0) {
            return new JsonResponse($notationList);
        } else {
            return new JsonResponse(null);
        }
//        return new JsonResponse($notationList);


    }

}
