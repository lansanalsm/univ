<?php

namespace univBundle\Controller;

use univBundle\Entity\AdminBac;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Adminbac controller.
 *
 */
class AdminBacController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.flagPaiement =1 AND n.pv is NOT NULL 
                   ');
        $adminBacs = $querryEtudiant->getResult();
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('adminbac/index.html.twig', array(
            'adminBacs' => $adminBacs,
            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $adminBac = new Adminbac();
        $form = $this->createForm('univBundle\Form\AdminBacType', $adminBac);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{

                $numAleatoire = $randon = random_int(999, 9999);
//                $fraisTotal=$form->get('fraisFormation')->getData();
//                $avancePaye=$form->get('avancePaye')->getData();
                $pvSelection = $this->get('monServices')->generationMatricule($form->get('departement')->getData()->getFaculte()->getCode(),$form->get('departement')->getData()->getCode(), $form->get('pv')->getData(), $form->get('sessions')->getData(), $numAleatoire);
                $em = $this->getDoctrine()->getManager();
                $adminBac->setCodeDep($form->get('departement')->getData()->getCode());
                $adminBac->setCodeNiveau($form->get('licence')->getData()->getCode());
                $adminBac->setDepartement($form->get('departement')->getData()->getName());
//                $adminBac->setResteApaye($fraisTotal-$avancePaye);
                $adminBac->setIdnational($pvSelection);
                $adminBac->setNiveau($form->get('licence')->getData()->getName());
                $adminBac->setSessions($form->get('sessions')->getData());
                $adminBac->setDateNaissance(date_format($form->get('dateNaissance')->getData(),'Y-m-d'));
                $adminBac->setUniversite("Univ  Kindia");
                $em->persist($adminBac);
                $em->flush();

                $adminBac->setIdnational($pvSelection.$adminBac->getId());
                $em->persist($adminBac);
                $em->flush();
                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('adminbac_show', array('id' => $adminBac->getId()));

            }catch (\Doctrine\DBAL\DBALException $e){
//                $licence = $form->get('licence')->getData()->getName();
//                $sessions = $form->get('sessions')->getData()->getSessions();
                $error = "Desole Cet etudiant est déjà Préinscrit";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
//                return $this->redirectToRoute('adminbac_show', array('id' => $adminBac->getId()));
            }
        }
        return $this->render('adminbac/new.html.twig', array(
            'adminBac' => $adminBac,
            'form' => $form->createView(),
        ));
    }



    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function recycleNewAction(Request $request)
    {
        $adminBac = new Adminbac();
        $form = $this->createForm('univBundle\Form\RecycleType', $adminBac);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try{

                $numAleatoire = $randon = random_int(999, 9999);
                $em = $this->getDoctrine()->getManager();
                $adminBac->setCodeDep($form->get('departement')->getData()->getCode());
                $adminBac->setCodeNiveau($form->get('licence')->getData()->getCode());
                $adminBac->setDepartement($form->get('departement')->getData()->getName());

                $adminBac->setNiveau($form->get('licence')->getData()->getName());
                $adminBac->setSessions($form->get('sessions')->getData());
                $adminBac->setDateNaissance(date_format($form->get('dateNaissance')->getData(),'Y-m-d'));
                $adminBac->setUniversite("Univ  Kindia");
                $em->persist($adminBac);
                $em->flush();
                $pvSelection = $this->get('monServices')->generationMatricule($form->get('departement')->getData()->getFaculte()->getCode(),$form->get('departement')->getData()->getCode(),"REC".$numAleatoire.$adminBac->getId()."CYLE", $form->get('sessions')->getData(), $numAleatoire);
                $adminBac->setIdnational($pvSelection.$adminBac->getId());
                $adminBac->setPv($numAleatoire.$adminBac->getId());
                $adminBac->setIdnational($pvSelection.$adminBac->getId());
                $em->persist($adminBac);
                $em->flush();
                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('adminbac_show', array('id' => $adminBac->getId()));

            }catch (\Doctrine\DBAL\DBALException $e){
//                $licence = $form->get('licence')->getData()->getName();
//                $sessions = $form->get('sessions')->getData()->getSessions();
                $error = "Desole Cet etudiant est déjà Préinscrit";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
//                return $this->redirectToRoute('adminbac_show', array('id' => $adminBac->getId()));
            }
        }
        return $this->render('adminbac/recycle_new.html.twig', array(
            'adminBac' => $adminBac,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(AdminBac $adminBac)
    {
        $deleteForm = $this->createDeleteForm($adminBac);

        return $this->render('adminbac/show.html.twig', array(
            'adminBac' => $adminBac,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, AdminBac $adminBac)
    {
        $deleteForm = $this->createDeleteForm($adminBac);
        $editForm = $this->createForm('univBundle\Form\AdminBacType', $adminBac);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminbac_edit', array('id' => $adminBac->getId()));
        }

        return $this->render('adminbac/edit.html.twig', array(
            'adminBac' => $adminBac,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, AdminBac $adminBac)
    {
        $form = $this->createDeleteForm($adminBac);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($adminBac);
            $em->flush();
        }

        return $this->redirectToRoute('adminbac_index');
    }

    /**
     * Creates a form to delete a adminBac entity.
     *
     * @param AdminBac $adminBac The adminBac entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AdminBac $adminBac)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminbac_delete', array('id' => $adminBac->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
