<?php

namespace univBundle\Controller;

use univBundle\Entity\typeConge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Typeconge controller.
 *
 */
class typeCongeController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeConges = $em->getRepository('univBundle:typeConge')->findAll();

        return $this->render('typeconge/index.html.twig', array(
            'typeConges' => $typeConges,
        ));
    }

    /**
     * Creates a new typeConge entity.
     *
     */
    public function newAction(Request $request)
    {
        $typeConge = new Typeconge();
        $form = $this->createForm('univBundle\Form\typeCongeType', $typeConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeConge);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typeconge_index', array('id' => $typeConge->getId()));
        }

        return $this->render('typeconge/new.html.twig', array(
            'typeConge' => $typeConge,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeConge entity.
     *
     */
    public function showAction(typeConge $typeConge)
    {
        $deleteForm = $this->createDeleteForm($typeConge);

        return $this->render('typeconge/show.html.twig', array(
            'typeConge' => $typeConge,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeConge entity.
     *
     */
    public function editAction(Request $request, typeConge $typeConge)
    {
        $deleteForm = $this->createDeleteForm($typeConge);
        $editForm = $this->createForm('univBundle\Form\typeCongeType', $typeConge);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typeconge_index', array('id' => $typeConge->getId()));
        }

        return $this->render('typeconge/edit.html.twig', array(
            'typeConge' => $typeConge,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeConge entity.
     *
     */
    public function deleteAction(Request $request, typeConge $typeConge)
    {
        $form = $this->createDeleteForm($typeConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeConge);
            $em->flush();
        }

        return $this->redirectToRoute('typeconge_index');
    }

    /**
     * Creates a form to delete a typeConge entity.
     *
     * @param typeConge $typeConge The typeConge entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(typeConge $typeConge)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typeconge_delete', array('id' => $typeConge->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
