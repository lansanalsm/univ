<?php

namespace univBundle\Controller;

use univBundle\Entity\etage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Etage controller.
 *
 */
class etageController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etages = $em->getRepository('univBundle:etage')->findAll();

        return $this->render('etage/index.html.twig', array(
            'etages' => $etages,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $etage = new Etage();
        $form = $this->createForm('univBundle\Form\etageType', $etage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etage);
            $em->flush();

            return $this->redirectToRoute('etage_show', array('id' => $etage->getId()));
        }

        return $this->render('etage/new.html.twig', array(
            'etage' => $etage,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(etage $etage)
    {
        $deleteForm = $this->createDeleteForm($etage);

        return $this->render('etage/show.html.twig', array(
            'etage' => $etage,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, etage $etage)
    {
        $deleteForm = $this->createDeleteForm($etage);
        $editForm = $this->createForm('univBundle\Form\etageType', $etage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('etage_edit', array('id' => $etage->getId()));
        }

        return $this->render('etage/edit.html.twig', array(
            'etage' => $etage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a etage entity.
     *
     */
    public function deleteAction(Request $request, etage $etage)
    {
        $form = $this->createDeleteForm($etage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etage);
            $em->flush();
        }

        return $this->redirectToRoute('etage_index');
    }

    /**
     * Creates a form to delete a etage entity.
     *
     * @param etage $etage The etage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(etage $etage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etage_delete', array('id' => $etage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
