<?php

namespace univBundle\Controller;

use univBundle\Entity\typePersonnel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Typepersonnel controller.
 *
 */
class typePersonnelController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typePersonnels = $em->getRepository('univBundle:typePersonnel')->findAll();

        return $this->render('typepersonnel/index.html.twig', array(
            'typePersonnels' => $typePersonnels,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $typePersonnel = new typePersonnel();
        $form = $this->createForm('univBundle\Form\typePersonnelType', $typePersonnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typePersonnel);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typepersonnel_index');
//            return $this->redirectToRoute('typepersonnel_show', array('id' => $typePersonnel->getId()));
        }

        return $this->render('typepersonnel/new.html.twig', array(
            'typePersonnel' => $typePersonnel,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(typePersonnel $typePersonnel)
    {
        $deleteForm = $this->createDeleteForm($typePersonnel);

        return $this->render('typepersonnel/show.html.twig', array(
            'typePersonnel' => $typePersonnel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, typePersonnel $typePersonnel)
    {
        $deleteForm = $this->createDeleteForm($typePersonnel);
        $editForm = $this->createForm('univBundle\Form\typePersonnelType', $typePersonnel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typepersonnel_index');
        }

        return $this->render('typepersonnel/edit.html.twig', array(
            'typePersonnel' => $typePersonnel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, typePersonnel $typePersonnel)
    {
        $form = $this->createDeleteForm($typePersonnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typePersonnel);
            $em->flush();
        }

        return $this->redirectToRoute('typepersonnel_index');
    }

    /**
     * Creates a form to delete a typePersonnel entity.
     *
     * @param typePersonnel $typePersonnel The typePersonnel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(typePersonnel $typePersonnel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typepersonnel_delete', array('id' => $typePersonnel->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
