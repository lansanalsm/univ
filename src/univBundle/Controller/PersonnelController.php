<?php

namespace univBundle\Controller;

use Doctrine\DBAL\DBALException;
use univBundle\Entity\Personnel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Utilisateurs;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Personnel controller.
 *
 */
class PersonnelController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $personnels = $em->getRepository('univBundle:Personnel')->findAll();

        return $this->render('personnel/index.html.twig', array(
            'personnels' => $personnels,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $personnel = new Personnel();
        $form = $this->createForm('univBundle\Form\PersonnelType', $personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $em = $this->getDoctrine()->getManager();
                $personnel->setRoles($form->get('roles')->getData());
                $em->persist($personnel);
                $em->flush();
                if (! is_null($form->get('departement')->getData())){
                    $code = "PERS" . (substr($personnel->getPrenom(), 0, 1) . substr($personnel->getName(), 0, 1) . $personnel->getDepartement()->getCode() . $personnel->getId());
                }else{
                    $code = "PERS" . (substr($personnel->getPrenom(), 0, 1) . substr($personnel->getName(), 0, 1). $personnel->getId());
                }


                /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                $userManager = $this->get('fos_user.user_manager');
                $user = $userManager->createUser();
                $user->setEnabled(true);
                $user->setUsername($code);
                $user->setNom($personnel->getName());
                $user->setPrenom($personnel->getPrenom());
                $user->setEmail($code . '@.univ.com');
                $user->setPlainPassword($code);
                $tableRole = array();
                $tableRole[0] = $form->get('roles')->getData();
                $user->setrolesUser($form->get('roles')->getData());
                $user->setRoles($tableRole);
                $userManager->updateUser($user);
                $em->flush();

                $personnel->setUtilisateur($user);
                $personnel->setTempwd($code);
                $em->persist($personnel);
                $em->flush();
                $user->setPersonnel($personnel);
                $userManager->updateUser($user);
                $em->flush();

                $error = "Operation effectuer avec success ";
                $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('personnel_show', array('id' => $personnel->getId()));
            }catch (\Doctrine\DBAL\DBALException $exception){
                $departement = $form->get('departement')->getData()->getName();
                $error = "Desole Ce Departement : " . $departement . " est déjà affecter a un personnel";
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
            return $this->redirectToRoute('personnel_new');

        }

        return $this->render('personnel/new.html.twig', array(
            'personnel' => $personnel,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);

        return $this->render('personnel/show.html.twig', array(
            'personnel' => $personnel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);
        $editForm = $this->createForm('univBundle\Form\PersonnelType', $personnel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user = $personnel->getUtilisateur();
            $user->setRoles(array($editForm->get('roles')->getData()));
            $personnel->setRoles($editForm->get('roles')->getData());
            $user->setrolesUser($editForm->get('roles')->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('personnel_show', array('id' => $personnel->getId()));
        }

        return $this->render('personnel/edit.html.twig', array(
            'personnel' => $personnel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a personnel entity.
     *
     */
    public function deleteAction(Request $request, Personnel $personnel)
    {
        $form = $this->createDeleteForm($personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personnel);
            $em->flush();
        }

        return $this->redirectToRoute('personnel_index');
    }

    /**
     * Creates a form to delete a personnel entity.
     *
     * @param Personnel $personnel The personnel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personnel $personnel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personnel_delete', array('id' => $personnel->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
