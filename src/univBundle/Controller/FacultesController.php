<?php

namespace univBundle\Controller;

use univBundle\Entity\Facultes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Faculte controller.
 *
 */
class FacultesController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $facultes = $em->getRepository('univBundle:Facultes')->findAll();

        return $this->render('facultes/index.html.twig', array(
            'facultes' => $facultes,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $faculte = new Facultes();
        $form = $this->createForm('univBundle\Form\FacultesType', $faculte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($faculte);
            $em->flush();

            return $this->redirectToRoute('facultes_show', array('id' => $faculte->getId()));
        }

        return $this->render('facultes/new.html.twig', array(
            'faculte' => $faculte,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Facultes $faculte)
    {
        $deleteForm = $this->createDeleteForm($faculte);

        return $this->render('facultes/show.html.twig', array(
            'faculte' => $faculte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Facultes $faculte)
    {
        $deleteForm = $this->createDeleteForm($faculte);
        $editForm = $this->createForm('univBundle\Form\FacultesType', $faculte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facultes_edit', array('id' => $faculte->getId()));
        }

        return $this->render('facultes/edit.html.twig', array(
            'faculte' => $faculte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Facultes $faculte)
    {
        $form = $this->createDeleteForm($faculte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($faculte);
            $em->flush();
        }

        return $this->redirectToRoute('facultes_index');
    }

    /**
     * Creates a form to delete a faculte entity.
     *
     * @param Facultes $faculte The faculte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Facultes $faculte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facultes_delete', array('id' => $faculte->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
