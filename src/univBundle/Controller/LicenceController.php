<?php

namespace univBundle\Controller;

use univBundle\Entity\Licence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Licence controller.
 *
 */
class LicenceController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $licences = $em->getRepository('univBundle:Licence')->findAll();
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('licence/index.html.twig', array(
            'licences' => $licences,
            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $licence = new Licence();
        $form = $this->createForm('univBundle\Form\LicenceType', $licence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($licence);
            $em->flush();

            return $this->redirectToRoute('licence_show', array('id' => $licence->getId()));
        }
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('licence/new.html.twig', array(
            'licence' => $licence,
            'form' => $form->createView(),
            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Licence $licence, Request $request)
    {
        $deleteForm = $this->createDeleteForm($licence);
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('licence/show.html.twig', array(
            'licence' => $licence,
            'delete_form' => $deleteForm->createView(),
            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Licence $licence)
    {
        $deleteForm = $this->createDeleteForm($licence);
        $editForm = $this->createForm('univBundle\Form\LicenceType', $licence);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('licence_edit', array('id' => $licence->getId()));
        }
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('licence/edit.html.twig', array(
            'licence' => $licence,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Licence $licence)
    {
        $form = $this->createDeleteForm($licence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($licence);
            $em->flush();
        }

        return $this->redirectToRoute('licence_index');
    }

    /**
     * Creates a form to delete a licence entity.
     *
     * @param Licence $licence The licence entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Licence $licence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('licence_delete', array('id' => $licence->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
