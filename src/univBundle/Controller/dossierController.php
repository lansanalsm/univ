<?php

namespace univBundle\Controller;

use SimpleExcel\SimpleExcel;
use univBundle\Entity\AdminBac;
use univBundle\Entity\dossier;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use League\Csv\Reader;
use League\Csv\Writer;
use League\Csv\CharsetConverter;
use PHPExcel;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Dossier controller.
 *
 */
class dossierController extends Controller
{

    public function excel_to_csv($inputFileName)
    {
        $inputFileType = 'Excel2007';
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcelReader = $objReader->load($inputFileName);
        $objPHPExcelReader->setActiveSheetIndex(0)
            ->setCellValue('A1', 'pv')
            ->setCellValue('B1', 'nomPrenom')
            ->setCellValue('C1', 'dateNaiss')
            ->setCellValue('D1', 'sexe')
            ->setCellValue('E1', 'filiation')
            ->setCellValue('F1', 'centre')
            ->setCellValue('G1', 'origine')
            ->setCellValue('H1', 'option')
            ->setCellValue('I1', 'idNationnal')
            ->setCellValue('J1', 'statut')
            ->setCellValue('K1', 'departement')
            ->setCellValue('L1', 'uniersite')
            ->setCellValue('M1', 'codeDep')
            ->setCellValue('N1', 'session')
            ->setCellValue('O1', 'etat')
            ->setCellValue('P1', 'contact')
            ->setCellValue('Q1', 'nationalite')
            ->setCellValue('R1', 'niveau')
            ->setCellValue('S1', 'codeNiv')
        ;

        $loadedSheetNames = $objPHPExcelReader->getSheetNames();
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');
        foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {
            $objWriter->setSheetIndex($sheetIndex);
            $objWriter->save('uploads/' . $loadedSheetName . '.csv');
        }
        return 'uploads/' . $loadedSheetName . '.csv';
    }

    public function DelleteAllFille($path)
    {
//       $dir = $path;
//       foreach(glob($dir.'*.*') as $v){
//           if(is_file($v)) {
//               if (chmod($v, 0777)) {
//                   unlink($v); // delete file
//               } else {
//                   echo "Couldn't do it.";
//               }
//           }}
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dossiers = $em->getRepository('univBundle:dossier')->findAll();

        return $this->render('dossier/index.html.twig', array(
            'dossiers' => $dossiers,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $dossier = new dossier();
        $form = $this->createForm('univBundle\Form\dossierType', $dossier);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->DelleteAllFille('uploads/');
            $em = $this->getDoctrine()->getManager();
            $em->persist($dossier);
            $em->flush();
            $path = $dossier->getImage()->getPath();
            $chemin = $this->excel_to_csv('uploads/' . $path);
            $reader = Reader::createFromPath($chemin, 'rw');
            $result = $reader->fetchAssoc();
            mb_detect_encoding($reader, 'UTF-8'); // 'UTF-8'
            mb_detect_encoding($reader, 'UTF-8', true); // false
            $countUpdate = 0;
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
           $isError=false;

            $i = 1;

            foreach ($result as $row) {
                if($i == 1) { 
                    if (isset($row['pv'])!==true){
                       $isError=true;
                    }elseif (isset($row['nomPrenom'])!==true){
                       $isError=true;
                    }
                    elseif (isset($row['dateNaiss'])!==true){
                       $isError=true;
                    }
                    elseif (isset($row['sexe'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['filiation'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['centre'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['origine'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['option'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['idNationnal'])!==true){
                       $isError=true;

                    }
                    elseif (isset($row['statut'])!==true){
                       $isError=true;
                    }
                    elseif (isset($row['departement'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['uniersite'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['codeDep'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['session'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['etat'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['contact'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['nationalite'])!==true){
                        $isError=true;
                    }

                    elseif (isset($row['niveau'])!==true){
                        $isError=true;
                    }
                    elseif (isset($row['codeNiv'])!==true){
                        $isError=true;
                    }
                    
                }

                $i = $i+  1;


                if ($isError==false) {
                    $statement = $connection->prepare("
                INSERT INTO admin_bac( pv, nomPrenom, dateNaissance, sexe,filiation,centre,origine,options,idnational,statut,departement,universite,codeDep,sessions,etat,contact,nationnalite,niveau,codeNiveau)
                VALUES (:pv,:nomPrenom, :dateNaissance, :sexe,:filiation,:centre,:origine,:options,:idnational,:statut,:departement,:universite,:codeDep,:sessions,:etat,:contact,:nationnalite,:niveau,:codeNiveau)
                ON DUPLICATE KEY UPDATE
                pv=VALUES(pv),
                nomPrenom=VALUES(nomPrenom),
                dateNaissance=VALUES(dateNaissance),
                 sexe=VALUES(sexe),
                filiation=VALUES(filiation),
                centre=VALUES(centre),
                 origine=VALUES(origine),
                options=VALUES(options),
                idnational=VALUES(idnational),
                 statut=VALUES(statut),
                departement=VALUES(departement),
                universite=VALUES(universite),
                 codeDep=VALUES(codeDep),
                sessions=VALUES(sessions),
                etat=VALUES(etat),
                 contact=VALUES(contact),
                nationnalite=VALUES(nationnalite),
                niveau=VALUES(niveau),
                 codeNiveau=VALUES(codeNiveau)
    
                ");

                    $statement->bindValue("pv", utf8_encode($row['pv']));
                    $statement->bindValue("nomPrenom", utf8_encode($row['nomPrenom']));
                    $statement->bindValue("dateNaissance", utf8_encode($row['dateNaiss']));
                    $statement->bindValue("sexe", utf8_encode($row['sexe']));

                    $statement->bindValue("filiation", utf8_encode($row['filiation']));
                    $statement->bindValue("centre", utf8_encode($row['centre']));
                    $statement->bindValue("origine", utf8_encode($row['origine']));
                    $statement->bindValue("options", utf8_encode($row['option']));

                    $statement->bindValue("idnational", utf8_encode($row['idNationnal']));
                    $statement->bindValue("statut", utf8_encode($row['statut']));
                    $statement->bindValue("departement", utf8_encode($row['departement']));
                    $statement->bindValue("universite", utf8_encode($row['uniersite']));

                    $statement->bindValue("codeDep", utf8_encode($row['codeDep']));
                    $statement->bindValue("sessions", utf8_encode($row['session']));
                    $statement->bindValue("etat", utf8_encode($row['etat']));
                    $statement->bindValue("contact", utf8_encode($row['contact']));

                    $statement->bindValue("nationnalite", utf8_encode($row['nationalite']));
                    $statement->bindValue("niveau", utf8_encode($row['niveau']));
                    $statement->bindValue("codeNiveau", utf8_encode($row['codeNiv']));

                    $result = $statement->execute();

                    //if ($result) $countUpdate++;

                    $error = "Importation effectuer avec success ";
                    $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('adminbac_index');

                 }
                 else {
                    $error = "Desole fichier invalide";
                    $errorMessage = '<span style="color: #ff021e;font-weight: bold ;font-size: larger;text-transform: none">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('dossier_new');
                     }
                }

        }
        return $this->render('dossier/new.html.twig', array(
            'dossier' => $dossier,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(dossier $dossier, Request $request)
    {
        $deleteForm = $this->createDeleteForm($dossier);
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('dossier/show.html.twig', array(
            'dossier' => $dossier,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, dossier $dossier)
    {
        $deleteForm = $this->createDeleteForm($dossier);
        $editForm = $this->createForm('univBundle\Form\dossierType', $dossier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_edit', array('id' => $dossier->getId()));
        }

        return $this->render('dossier/edit.html.twig', array(
            'dossier' => $dossier,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, dossier $dossier)
    {
        $form = $this->createDeleteForm($dossier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dossier);
            $em->flush();
        }

        return $this->redirectToRoute('dossier_index');
    }

    /**
     * Creates a form to delete a dossier entity.
     *
     * @param dossier $dossier The dossier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(dossier $dossier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dossier_delete', array('id' => $dossier->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
