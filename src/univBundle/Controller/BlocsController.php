<?php

namespace univBundle\Controller;

use univBundle\Entity\Blocs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Bloc controller.
 *
 */
class BlocsController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blocs = $em->getRepository('univBundle:Blocs')->findAll();

        return $this->render('blocs/index.html.twig', array(
            'blocs' => $blocs,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $bloc = new Blocs();
        $form = $this->createForm('univBundle\Form\BlocsType', $bloc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bloc);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('blocs_index', array('id' => $bloc->getId()));
        }

        return $this->render('blocs/new.html.twig', array(
            'bloc' => $bloc,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Blocs $bloc)
    {
        $deleteForm = $this->createDeleteForm($bloc);

        return $this->render('blocs/show.html.twig', array(
            'bloc' => $bloc,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Blocs $bloc)
    {
        $deleteForm = $this->createDeleteForm($bloc);
        $editForm = $this->createForm('univBundle\Form\BlocsType', $bloc);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('blocs_index', array('id' => $bloc->getId()));
        }

        return $this->render('blocs/edit.html.twig', array(
            'bloc' => $bloc,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Blocs $bloc)
    {
        $form = $this->createDeleteForm($bloc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bloc);
            $em->flush();
        }

        return $this->redirectToRoute('blocs_index');
    }

    /**
     * Creates a form to delete a bloc entity.
     *
     * @param Blocs $bloc The bloc entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Blocs $bloc)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blocs_delete', array('id' => $bloc->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
