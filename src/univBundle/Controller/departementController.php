<?php

namespace univBundle\Controller;

use univBundle\Entity\departement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Departement controller.
 *
 */
class departementController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $departements = $em->getRepository('univBundle:departement')->findAll();
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('departement/index.html.twig', array(
            'departements' => $departements,
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $departement = new Departement();
        $form = $this->createForm('univBundle\Form\departementType', $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departement);
            $em->flush();

            return $this->redirectToRoute('departement_show', array('id' => $departement->getId()));
        }
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('departement/new.html.twig', array(
            'departement' => $departement,
            'form' => $form->createView(),
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(departement $departement)
    {
        $deleteForm = $this->createDeleteForm($departement);

        return $this->render('departement/show.html.twig', array(
            'departement' => $departement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, departement $departement)
    {
        $deleteForm = $this->createDeleteForm($departement);
        $editForm = $this->createForm('univBundle\Form\departementType', $departement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('departement_edit', array('id' => $departement->getId()));
        }

        return $this->render('departement/edit.html.twig', array(
            'departement' => $departement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, departement $departement)
    {
        $form = $this->createDeleteForm($departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departement);
            $em->flush();
        }

        return $this->redirectToRoute('departement_index');
    }

    /**
     * Creates a form to delete a departement entity.
     *
     * @param departement $departement The departement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(departement $departement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('departement_delete', array('id' => $departement->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
