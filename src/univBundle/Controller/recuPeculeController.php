<?php

namespace univBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use univBundle\Entity\recuPecule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use univBundle\Form\recuPeculeType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Recupecule controller.
 *
 */
class recuPeculeController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $recuPecules = $em->getRepository('univBundle:recuPecule')->findAll();



        return $this->render('recupecule/index.html.twig', array(
            'recuPecules' => $recuPecules,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function listeEtudiantPaiementPeculeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $licencer = $request->get('licence');
        $sessionr = $request->get('sessions');
        $session = $em->getRepository('univBundle:Sessions')->findOneById($sessionr);
        $licence = $em->getRepository('univBundle:Licence')->findOneById($licencer);

        //////////////////end

        if (is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequest = $em->createQuery(
                "SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND E.statut='Boursier'
                  ");
            $listeEtudiants = $querryEtudiantSansPparamRequest->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = 0;
        } elseif (!is_null($licence) && is_null($session)) {
            $querryEtudiantSansPparamRequestSessionS = $em->createQuery(
                "SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(E.licence) =:licence
                   AND E.statut='Boursier'
                   ")
                ->setParameter('licence', $licence->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestSessionS->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = 0;
        } elseif (is_null($licence) && !is_null($session)) {
            $querryEtudiantSansPparamRequestLicence = $em->createQuery(
                "SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND  E.statut='Boursier'
                   ")
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiantSansPparamRequestLicence->getResult();
            $licenceForUrl = 0;
            $sessionForUrl = $session->getId();

        } elseif (!is_null($licence) && !is_null($session)) {
            $querryEtudiant = $em->createQuery(
                "SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence
                   AND E.statut='Boursier'
                   ")
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        } else {
            $querryEtudiant = $em->createQuery(
                "SELECT E  
                   FROM univBundle:Etudiants E 
                   INNER JOIN  univBundle:departement D WITH D.id=IDENTITY (E.departement)
                   INNER JOIN  univBundle:Inscription I  WITH E.id=IDENTITY (I.etudiant)
                   INNER JOIN univBundle:Sessions S WITH S.id=IDENTITY (I.sessions)
                   AND   IDENTITY(I.sessions)=:sessions 
                   AND   IDENTITY(E.licence) =:licence
                  AND E.statut='Boursier'
                   ")
                ->setParameter('licence', $licence->getId())
                ->setParameter('sessions', $session->getId());
            $listeEtudiants = $querryEtudiant->getResult();
            $licenceForUrl = $licence->getId();
            $sessionForUrl = $session->getId();
        }


        $etudiants = $em->getRepository('univBundle:Etudiants')->findAll();
        $facultes = $em->getRepository('univBundle:Facultes')->findAll();
        $departements = $em->getRepository('univBundle:departement')->findAll();
        $licences = $em->getRepository('univBundle:Licence')->findAll();
        $sessions = $em->getRepository('univBundle:Sessions')->findAll();
        return $this->render('etudiants/listeEtudiantPaiementPecule.html.twig', array(
            'etudiants' => $listeEtudiants,
            'facultes' => $facultes,
            'departements' => $departements,
            'licences' => $licences,
            'sessions' => $sessions,
            'licenceForUrl' => $licenceForUrl,
            'sessionForUrl' => $sessionForUrl
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request,$matricule)
    {

        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);

        $form = $this->createForm('univBundle\Form\recuPeculeType');
        $form->handleRequest($request);
        $choix=$request->get('choix');
        $pecule = $em->getRepository('univBundle:pecule')->findOneById(1);
        if ($etudiant->getLicence()->getCode()==1){
            $montant=$pecule->getValeurl1();
        }elseif ($etudiant->getLicence()->getCode()==2){
            $montant=$pecule->getValeurl2();
        }
        elseif ($etudiant->getLicence()->getCode()==3){
            $montant=$pecule->getValeurl3();
        }
        elseif ($etudiant->getLicence()->getCode()==4){
            $montant=$pecule->getValeurl4();
        }else{
            $montant=150000;
        }
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);

        ////////////Nombre de mois paie//////////
        $querrynombrMois = $em->createQuery(
            'SELECT  (IDENTITY(r.sessions))   FROM univBundle:recuPecule r
                 WHERE  IDENTITY (r.etudiant)=:etudiant AND   IDENTITY ( r.sessions)=:sessions ')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $ombrMois = $querrynombrMois->getResult();
        //////////////////end
        if ($form->isSubmitted() && $form->isValid()) {

            if (!is_null($choix)){
                try {
                    $nombrePaie=count($ombrMois);
                    $count = 0;
                    $max = (9 -$nombrePaie);
                    foreach($choix  as $key) {
                        $count++;
                        $recuPecule = new Recupecule();
                        $recuPecule->setMois($em->getRepository('univBundle:mois')->findOneById($key));
                        $recuPecule->setEtudiant($etudiant);
                        $recuPecule->setDatepaie($form->get('datepaie')->getData());
                        $recuPecule->setSessions($sessionEncour);
                        $recuPecule->setPecule($pecule);
                        $recuPecule->setMontantRecu($montant);
                        $em->persist($recuPecule);
                        $em->flush();
                        if ($count == $max) break; // stop looping if $max value is reached
                    }
                    $error = "Operation effectuer avec success ";
                    $errorMessage = '<span style="color: #ffaf64;font-weight: bold ;font-size: larger">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('recupecule_carnet_paiement', array('matricule' => $matricule));
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $moi = $form->get('mois')->getData()->getName();
                    $sessions = $sessionEncour->getSessions();
                    $error = "Desole Cet etudiant a déjà recu son pucule pour ce mois de " . $moi . " pour la session " . $sessions;
                    $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                }
            }else{
                $error = "Veillez selectionner au moins un mois";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }
        }
        //////////// liste des mois non paie //////////
        $querrymois = $em->createQuery(
            'SELECT m  FROM univBundle:mois m
                  WHERE 
                  m.id  NOT IN ( SELECT  IDENTITY(r.mois) FROM 
                  univBundle:recuPecule r WHERE  IDENTITY (r.etudiant)=:etudiant AND  IDENTITY ( r.sessions)=:sessions)
                
                   ')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $mois = $querrymois->getResult();
//        var_dump($mois2);die();
        //////////////////end

//        var_dump(count($ombrMois));die();
        return $this->render('recupecule/new.html.twig', array(
//            'recuPecule' => $recuPecule,
            'form' => $form->createView(),
            'infos' => $etudiant,
            'mois'=>$mois,
            'NombreMoisPaie'=>count($ombrMois),
            'montant'=>$montant
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(recuPecule $recuPecule)
    {
        $deleteForm = $this->createDeleteForm($recuPecule);

        return $this->render('recupecule/show.html.twig', array(
            'recuPecule' => $recuPecule,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing recuPecule entity.
     *
     */
    public function editAction(Request $request, recuPecule $recuPecule)
    {
        $deleteForm = $this->createDeleteForm($recuPecule);
        $editForm = $this->createForm('univBundle\Form\recuPeculeType', $recuPecule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recupecule_edit', array('id' => $recuPecule->getId()));
        }

        return $this->render('recupecule/edit.html.twig', array(
            'recuPecule' => $recuPecule,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, recuPecule $recuPecule)
    {
        $form = $this->createDeleteForm($recuPecule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($recuPecule);
            $em->flush();
        }

        return $this->redirectToRoute('recupecule_index');
    }

    /**
     * Creates a form to delete a recuPecule entity.
     *
     * @param recuPecule $recuPecule The recuPecule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(recuPecule $recuPecule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recupecule_delete', array('id' => $recuPecule->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function etudiantByMatriculeAjaxAction(Request $request)
    {
        $matricule = $request->request->get('matricule');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('matricule', 'matricule');

        $sql = "SELECT * FROM etudiants s LIMIT 1";

        if ($matricule != '')
            $sql .= "AND s.matricule = :matricule";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($matricule != '')
            $query
                ->setParameter('matricule', $matricule);

        $semestres = $query->getResult();
        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['matricule'] = $semestres['matricule'];
            $semestresList[] = $p;
        }

        return new JsonResponse($semestresList);


    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function carnetPaiementPeculeAction($matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();

        ////////////Nombre de mois paie//////////
        $querrynombrMois = $em->createQuery(
            'SELECT r FROM univBundle:recuPecule r
                 WHERE  IDENTITY (r.etudiant)=:etudiant AND   IDENTITY ( r.sessions)=:sessions ')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $PeculePayerMois = $querrynombrMois->getResult();

        return $this->render('univBundle:pecule:paiementPecule.html.twig', array(
            'recuPecule'=>$PeculePayerMois,
            'chefService'=>$chefService,
            'etudiant'=>$etudiant
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function generationcarnetPaiementPeculePdfAction($matricule)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant = $em->getRepository('univBundle:Etudiants')->findOneByMatricule($matricule);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();

        ////////////Nombre de mois paie//////////
        $querrynombrMois = $em->createQuery(
            'SELECT r FROM univBundle:recuPecule r
                 WHERE  IDENTITY (r.etudiant)=:etudiant AND   IDENTITY ( r.sessions)=:sessions
                  ORDER  BY IDENTITY(r.mois) ')
            ->setParameter('etudiant', $etudiant->getId())
            ->setParameter('sessions', $sessionEncour->getId());
        $PeculePayerMois = $querrynombrMois->getResult();
        $html = $this->renderView('univBundle:pecule:carnetPaiementPeculePDF.html.twig', array(
            'etudiant' => $etudiant,
            'Universite'=>$chefService,
            'recuPecule'=>$PeculePayerMois,
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,

            )),
            'carnet-d-paiement-pecule-'.$etudiant->getMatricule().'.pdf'
        );

//        return $this->render('univBundle:attestation:attesttationPDF.html.twig', array(
//            'etudiant' => $etudiant, 'Universite'=>$chefService
//        ));

    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function historiquePaiementPeculeAction(){

    }
}
