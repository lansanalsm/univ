<?php

namespace univBundle\Controller;

use univBundle\Entity\conge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Conge controller.
 *
 */
class congeController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $conges = $em->getRepository('univBundle:conge')->findAll();

        return $this->render('conge/index.html.twig', array(
            'conges' => $conges,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $conge = new Conge();
        $form = $this->createForm('univBundle\Form\congeType', $conge);
        $form->handleRequest($request);
         $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conge);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('conge_show', array('id' => $conge->getId()));
        }

        return $this->render('conge/new.html.twig', array(
            'conge' => $conge,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function ValidationCongeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $idConge = $request->get('congeId');
        $etatConge = $request->get('etatConge');
        if (!is_null($idConge)&& $_SERVER['REQUEST_METHOD'] == 'POST') {
            $conge = $em->getRepository('univBundle:conge')->findOneById($idConge);
            $conge->setEtat($etatConge);
            $em->persist($conge);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);

        }
        return $this->redirectToRoute('detailconge_index');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(conge $conge)
    {
        $deleteForm = $this->createDeleteForm($conge);

        return $this->render('conge/show.html.twig', array(
            'conge' => $conge,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, conge $conge)
    {
        $deleteForm = $this->createDeleteForm($conge);
        $editForm = $this->createForm('univBundle\Form\congeType', $conge);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('conge_edit', array('id' => $conge->getId()));
        }

        return $this->render('conge/edit.html.twig', array(
            'conge' => $conge,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, conge $conge)
    {
        $form = $this->createDeleteForm($conge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($conge);
            $em->flush();
        }

        return $this->redirectToRoute('conge_index');
    }

    /**
     * Creates a form to delete a conge entity.
     *
     * @param conge $conge The conge entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(conge $conge)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('conge_delete', array('id' => $conge->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
