<?php

namespace univBundle\Controller;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use univBundle\Entity\mois;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Mois controller.
 *
 */
class moisController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mois = $em->getRepository('univBundle:mois')->findAll();

        return $this->render('mois/index.html.twig', array(
            'mois' => $mois,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $mois = new Mois();
        $form = $this->createForm('univBundle\Form\moisType', $mois);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mois);
            $em->flush();

            return $this->redirectToRoute('mois_show', array('id' => $mois->getId()));
        }

        return $this->render('mois/new.html.twig', array(
            'mois' => $mois,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(mois $mois)
    {
        $deleteForm = $this->createDeleteForm($mois);

        return $this->render('mois/show.html.twig', array(
            'mois' => $mois,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, mois $mois)
    {
        $deleteForm = $this->createDeleteForm($mois);
        $editForm = $this->createForm('univBundle\Form\moisType', $mois);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mois_edit', array('id' => $mois->getId()));
        }

        return $this->render('mois/edit.html.twig', array(
            'mois' => $mois,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, mois $mois)
    {
        $form = $this->createDeleteForm($mois);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mois);
            $em->flush();
        }

        return $this->redirectToRoute('mois_index');
    }

    /**
     * Creates a form to delete a mois entity.
     *
     * @param mois $mois The mois entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(mois $mois)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mois_delete', array('id' => $mois->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



}
