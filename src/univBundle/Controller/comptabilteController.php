<?php

namespace univBundle\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use univBundle\Entity\AdminBac;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class comptabilteController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function fraisInscriptionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.flagPaiement IS NULL 
                   ');
        $adminBacs = $querryEtudiant->getResult();
//        $adminBacs = $em->getRepository('univBundle:AdminBac')->findAll();
        return $this->render('univBundle:comptabilte:frais_inscription.html.twig', array(
            'adminBacs' => $adminBacs,
        ));
    }
//AND P.flagInscription=0
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function fraisReinscriptionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT E  FROM
                          univBundle:Etudiants E 
                  INNER JOIN  univBundle:Inscription I WITH  IDENTITY (I.etudiant)= E.id
                  INNER JOIN univBundle:Sessions S WITH  IDENTITY (I.sessions)=S.id
                
                   ');
        $ListeEtudiants = $querryEtudiant->getResult();
        return $this->render('univBundle:comptabilte:frais_reinscription.html.twig', array(
            'etudiants'=>$ListeEtudiants
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function validationFraisInscriptionAction(Request $request, $pv,$sessionBac)
    {
         $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.idnational=:pv AND 
                  n.sessions=:sessionBac
                   ')
            ->setParameter('pv', $pv)
            ->setParameter('sessionBac',$sessionBac);
        $admin = $querryEtudiant->getOneOrNullResult();
        $departementInscription = $em->getRepository('univBundle:departement')->findAll();
        $sessions = $em->getRepository('univBundle:Sessions')->findAll();
        return $this->render('univBundle:comptabilte:validation_frais_inscription.html.twig', array(
            'infos' => $admin,
            'pvElection' => $pv,
            'sessionEncour' => $monServices = $this->get('monServices')->getSessionEncour(1),
            'departementInscription'=>$departementInscription,
            'sessions'=>$sessions
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function paiementFraisAction($pv,$codeDepartement,$statut,$sessionBac)
    {
        $em = $this->getDoctrine()->getManager();
         $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.idnational=:pv AND 
                  n.sessions=:sessionBac
                   ')
            ->setParameter('pv', $pv)
            ->setParameter('sessionBac',$sessionBac);
        $admin = $querryEtudiant->getOneOrNullResult();
        if (!is_null($admin)){
             if($admin->getFlagPaiement()!=1 || $admin->getFlagPaiement()==null){
                 $codeDepartement = $em->getRepository('univBundle:departement')->findOneByCode($codeDepartement);
                 $admin->setFlagPaiement(1);
                 $admin->setCodeDep($codeDepartement->getCode());
                 $admin->setOldDepartementBeforPaiement($admin->getDepartement());
                 $admin->setDepartement($codeDepartement->getName());
                 $admin->setOldStatuBeforPaiement($admin->getStatut());
                 $admin->setStatut($statut);
                 $em->persist($admin);
                 $em->flush();
                 $error = "Operation effectuer avec success ";
                 $errorMessage = '<span style="color: #ffaf64;font-weight: bold ;font-size: larger">' . $error . '</span>';
                 $this->get('session')->getFlashBag()->add('success', $errorMessage);
                 return $this->redirectToRoute('BulletinPaiementpaiementFraisAction', array('pv' =>$pv ,'sessionBac'=>$sessionBac));
             } elseif ($admin->getFlagPaiement()==1){

                 $mserror = "Desole Il semble que cet etudiant a deja effectuer son paiement";
                 $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                 $this->get('session')->getFlashBag()->add('success', $errorMessage);
                 return $this->redirectToRoute('validation_frais_inscription', array('pv' =>$pv ,'sessionBac'=>$sessionBac));
             }
        }{
        $mserror = "Desole  etudiant inexistant ";
        $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
        $this->get('session')->getFlashBag()->add('success', $errorMessage);
        return $this->redirectToRoute('frais_inscription');
    }



    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function BulletinPaiementAction($idPaiement)
    {
        $em = $this->getDoctrine()->getManager();
        $recuPaiement = $em->getRepository('univBundle:Paiement')->findOneById($idPaiement);
        return $this->render('univBundle:comptabilte:bulletinPaiement.html.twig', array(
            'recu'=>$recuPaiement
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function BulletinPaiementReinscriptionAction($idPaiement)
    {
        $em = $this->getDoctrine()->getManager();
        $recuPaiement = $em->getRepository('univBundle:Paiement')->findOneById($idPaiement);
        return $this->render('univBundle:comptabilte:bulletinPaiementReinscription.html.twig', array(
            'recu'=>$recuPaiement
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function generationBulletinPaiementPdfAction( $pv,$sessionBac,$recu)
    {
        $em = $this->getDoctrine()->getManager();
        $querryEtudiant = $em->createQuery(
            'SELECT n  FROM univBundle:AdminBac n
                  WHERE 
                  n.idnational=:pv AND 
                  n.sessions=:sessionBac
                   ')
            ->setParameter('pv', $pv)
            ->setParameter('sessionBac',$sessionBac);
        $BulletinPaimentEtudiant = $querryEtudiant->getOneOrNullResult();
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();

        $recuPaiement = $em->getRepository('univBundle:Paiement')->findOneById($recu);
        $html = $this->renderView('univBundle:comptabilte:bulletinPaiementPDF.html.twig', array(
            'etudiant'  => $BulletinPaimentEtudiant,
            'recuPaiement'=>$recuPaiement,
            'Universite'=>$chefService
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,
                'page-height' => 175,
                'page-width' => 175,

            )),
            'BulletinPaieme'.$BulletinPaimentEtudiant->getIdnational().'.pdf'
        );

    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function generationBulletinPaiementReinscriptionPdfAction($recu)
    {
        $em = $this->getDoctrine()->getManager();
        $querryUniversite = $em->createQuery(
            'SELECT U FROM univBundle:Universite U');
        $chefService = $querryUniversite->getOneOrNullResult();
        $recuPaiement = $em->getRepository('univBundle:Paiement')->findOneById($recu);
        $html = $this->renderView('univBundle:comptabilte:bulletinPaiementReinscriptionPDF.html.twig', array(
            'recuPaiement'=>$recuPaiement,
            'Universite'=>$chefService
        ));
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,
                'page-height' => 175,
                'page-width' => 175,

            )),
            'RecuPaiement'.$recuPaiement->getEtudiant()->getMatricule().'.pdf'
        );

    }


}
