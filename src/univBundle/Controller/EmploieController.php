<?php

namespace univBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use univBundle\Entity\Emploie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Emploie controller.
 *
 */
class EmploieController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $emploies = $em->getRepository('univBundle:Emploie')->findAll();
        return $this->render('emploie/index.html.twig', array(
            'emploies' => $emploies,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $emploie = new Emploie();
        $form = $this->createForm('univBundle\Form\EmploieType', $emploie);
        $form->handleRequest($request);

        $dateJour=$form->get('datejour')->getData();
        $HeuDeputForm=$form->get('heurDebut')->getData();
        $HeuFinForm=$form->get('heurFin')->getData();
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);

        if (!is_null($dateJour)){
            $compareDate= date_format($dateJour,'Y-m-d');
            $queryEmploie= $em->createQuery(
                'SELECT  E.id,E.heurDebut as heurDebut ,E.heurFin as heurFin,E.datejour as datejour,M.name as matiere ,SA.name as salle,SE.sessions as sessions  FROM
                         univBundle:Emploie E
                         INNER JOIN univBundle:Salles SA  WITH SA.id= IDENTITY (E.salles)
                         INNER JOIN univBundle:Matiers M  WITH M.id= IDENTITY (E.matiers)
                         INNER JOIN univBundle:Sessions SE WITH SE.id= IDENTITY (E.sessions)
                          AND   E.sessions=:sessions
                        GROUP BY E.heurDebut
                        ')
//                ->setParameter('datejour',$compareDate)
                ->setParameter('sessions', $sessionEncour->getId())  ;
            $emploies = $queryEmploie->getResult();
        }else{
            $compareDate= date_format(new \DateTime(),'Y-m-d');
            $queryEmploie= $em->createQuery(
                'SELECT  E.id,E.heurDebut,E.heurFin,E.datejour,M.name,SA.name,SE.sessions  FROM
                         univBundle:Emploie E
                         INNER JOIN univBundle:Salles SA  WITH SA.id= IDENTITY (E.salles)
                         INNER JOIN univBundle:Matiers M  WITH M.id= IDENTITY (E.matiers)
                         INNER JOIN univBundle:Sessions SE WITH SE.id= IDENTITY (E.sessions)
                          AND   E.sessions=:sessions
                        GROUP BY E.heurDebut')
//                ->setParameter('datejour',$compareDate)
                ->setParameter('sessions', $sessionEncour->getId())  ;
            $emploies = $queryEmploie->getResult();
        }

//        $compareDate= date_format(new \DateTime(),'Y-m-d');
//        $queryEmploie= $em->createQuery(
//            'SELECT M.name, E.heurDebut, E.heurFin, E.datejour,E.interval, count(E.id) as Nombre  FROM
//                         univBundle:Emploie E
//                         LEFT OUTER JOIN univBundle:Salles SA  WITH SA.id= IDENTITY (E.salles)
//                         LEFT OUTER JOIN univBundle:Matiers M  WITH M.id= IDENTITY (E.matiers)
//                         LEFT  JOIN univBundle:Sessions SE WITH SE.id= IDENTITY (E.sessions)
//                          AND   E.sessions=:sessions
//                         GROUP BY E.heurDebut,E.heurFin,E.id
//                         ORDER BY E.heurDebut ASC ')
////                ->setParameter('datejour',$compareDate)
//            ->setParameter('sessions', $sessionEncour->getId())  ;
//        $emploies = $queryEmploie->getResult();

//        $sql = "SELECT   M.`name`, E.`heurDebut`, E.`heurFin`, E.`datejour`,  E.`interval`, count(E.id) as Nombre FROM
//                    emploie E
//                LEFT JOIN  salles SA ON SA.id=E.salles_id
//                LEFT OUTER   JOIN matiers as M ON M.id=E.matiere_id
//                 GROUP BY E.`interval`";
        // Inventaire columme
        $rsm = new  ResultSetMapping();
        $rsm->addScalarResult( 'name', 'name');
        $rsm->addScalarResult( 'heurDebut', 'heurDebut');
        $rsm->addScalarResult('heurFin', 'heurFin');
        $rsm->addScalarResult('datejour' ,  'datejour');
        $rsm->addScalarResult('interval', 'interval');
//        $query = $em->createNativeQuery($sql, $rsm);
//        $users = $query->getResult();
        if ($form->isSubmitted() && $form->isValid()) {
            $Heurdebut=date_format($HeuDeputForm,'H');
            $Minutdebut=date_format($HeuDeputForm,'i');
            $Heurfin=date_format($HeuFinForm,'H');
            $Minufin=date_format($HeuFinForm,'i');

            $ValDebutEnMinute= $this->calculeHeurMinute($Heurdebut,$Minutdebut);
            $ValFinEnMinute= $this->calculeHeurMinute($Heurfin,$Minufin);

            $temoin=false;
//            var_dump($compareDate);die();
            foreach ($emploies as $emploies) {
                $finbase = ($this->calculeHeurMinute((date_format($emploies->getHeurFin(), 'H')), (date_format($emploies->getHeurFin(), 'i'))));
                $debutbase = ($this->calculeHeurMinute((date_format($emploies->getHeurDebut(), 'H')), (date_format($emploies->getHeurDebut(), 'i'))));
                if (  ($ValDebutEnMinute>=$finbase) || ($ValFinEnMinute<=$debutbase  ) ) {

                    $temoin = false;
                } else {
                    $temoin = true;
                }
            }

            if($temoin==true){
                $mserror = "Operation non effectuer risque de doublon";
                $errorMessage = '<span style="color: #ea200f;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('emploie_new');
            }else{
                $emploie->setSessions($sessionEncour);
                $emploie->setModule($form->get('matiers')->getData()->getSemestre()->getModule());
                $emploie->setDatejour($dateJour);
                $em->persist($emploie);
                $em->flush();
                $mserror = "Operation  effectuer  acvec success ";
                $errorMessage = '<span style="color: #ffaf64;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('emploie_new');
            }


        }

        return $this->render('emploie/new.html.twig', array(
            'emploie' => $emploie,
            'emploies'=>$emploies,
            'form' => $form->createView(),
        ));
    }

    public  function calculeHeurMinute($heur,$minute){
        $valMinute=$minute+$heur*60;
        return $valMinute;
    }

    /**
     * Finds and displays a emploie entity.
     *
     */
    public function showAction(Emploie $emploie)
    {
        $deleteForm = $this->createDeleteForm($emploie);

        return $this->render('emploie/show.html.twig', array(
            'emploie' => $emploie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing emploie entity.
     *
     */
    public function editAction(Request $request, Emploie $emploie)
    {
        $deleteForm = $this->createDeleteForm($emploie);
        $editForm = $this->createForm('univBundle\Form\EmploieType', $emploie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('emploie_edit', array('id' => $emploie->getId()));
        }

        return $this->render('emploie/edit.html.twig', array(
            'emploie' => $emploie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a emploie entity.
     *
     */
    public function deleteAction(Request $request, Emploie $emploie)
    {
        $form = $this->createDeleteForm($emploie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($emploie);
            $em->flush();
        }

        return $this->redirectToRoute('emploie_index');
    }

    /**
     * Creates a form to delete a emploie entity.
     *
     * @param Emploie $emploie The emploie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Emploie $emploie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('emploie_delete', array('id' => $emploie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
