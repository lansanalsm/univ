<?php

namespace univBundle\Controller;

use univBundle\Entity\Universite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Universite controller.
 *
 */
class UniversiteController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $universites = $em->getRepository('univBundle:Universite')->findAll();

        return $this->render('universite/index.html.twig', array(
            'universites' => $universites,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $universite = new Universite();
        $form = $this->createForm('univBundle\Form\UniversiteType', $universite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($universite);
            $em->flush();

            return $this->redirectToRoute('universite_show', array('id' => $universite->getId()));
        }

        return $this->render('universite/new.html.twig', array(
            'universite' => $universite,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Universite $universite)
    {
        $deleteForm = $this->createDeleteForm($universite);

        return $this->render('universite/show.html.twig', array(
            'universite' => $universite,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Universite $universite)
    {
        $deleteForm = $this->createDeleteForm($universite);
        $editForm = $this->createForm('univBundle\Form\UniversiteType', $universite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);

            return $this->redirectToRoute('universite_index', array('id' => $universite->getId()));
        }

        return $this->render('universite/edit.html.twig', array(
            'universite' => $universite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Universite $universite)
    {
        $form = $this->createDeleteForm($universite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($universite);
            $em->flush();
        }

        return $this->redirectToRoute('universite_index');
    }

    /**
     * Creates a form to delete a universite entity.
     *
     * @param Universite $universite The universite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Universite $universite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('universite_delete', array('id' => $universite->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
