<?php

namespace univBundle\Controller;

use univBundle\Entity\programmer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Programmer controller.
 *
 */
class programmerController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $programmers = $em->getRepository('univBundle:programmer')->findAll();

        return $this->render('programmer/index.html.twig', array(
            'programmers' => $programmers,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request,$module)
    {
        $em = $this->getDoctrine()->getManager();

        $moduleOne = $em->getRepository('univBundle:Module')->findOneById($module);
        $programmer = new Programmer();
        $form = $this->createForm('univBundle\Form\programmerType', $programmer);
        $form->handleRequest($request);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $dateDebut=$form->get('dateDebut')->getData();
        $dateFin=$form->get('dateFin')->getData();

        if ($form->isSubmitted() && $form->isValid()) {


            try {
                if ($dateFin<= $dateDebut){

                    $error = "Desole la  date debut doit etre inferieur a la date Fin";
                    $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('programmer_new', array('module' => $module));
                }else{
                    $programmer->setSessions($sessionEncour);
                    $programmer->setDateDebut($dateDebut);
                    $programmer->setDateFin($dateFin);
                    $programmer->setModule($moduleOne);
                    $em->persist($programmer);
                    $em->flush();
                    $error = "Operation effectuer avec success";
                    $errorMessage = '<span style="color: #ffaf64;font-weight: bold ;font-size: larger">' . $error . '</span>';
                    $this->get('session')->getFlashBag()->add('success', $errorMessage);
                    return $this->redirectToRoute('programmer_new', array('module' => $module));
                }
            } catch (\Doctrine\DBAL\DBALException $e) {
                $error = "Desole Ce Module est deja programmer pour la session : " . $sessionEncour->getSessions() ;
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('programmer_new', array('module' => $module));
            }



        }
        $programmationModule = $em->getRepository('univBundle:programmer')->findByModule($moduleOne);
        return $this->render('programmer/new.html.twig', array(
            'programmer' => $programmer,
            'form' => $form->createView(),
            'programmationModule'=>$programmationModule
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(programmer $programmer)
    {
        $deleteForm = $this->createDeleteForm($programmer);

        return $this->render('programmer/show.html.twig', array(
            'programmer' => $programmer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, programmer $programmer)
    {
        $deleteForm = $this->createDeleteForm($programmer);
        $editForm = $this->createForm('univBundle\Form\programmerType', $programmer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('programmer_edit', array('id' => $programmer->getId()));
        }

        return $this->render('programmer/edit.html.twig', array(
            'programmer' => $programmer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, programmer $programmer)
    {
        $form = $this->createDeleteForm($programmer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($programmer);
            $em->flush();
        }

        return $this->redirectToRoute('programmer_index');
    }

    /**
     * Creates a form to delete a programmer entity.
     *
     * @param programmer $programmer The programmer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(programmer $programmer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('programmer_delete', array('id' => $programmer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
