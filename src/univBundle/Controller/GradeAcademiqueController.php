<?php

namespace univBundle\Controller;

use univBundle\Entity\GradeAcademique;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Gradeacademique controller.
 *
 */
class GradeAcademiqueController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $gradeAcademiques = $em->getRepository('univBundle:GradeAcademique')->findAll();

        return $this->render('gradeacademique/index.html.twig', array(
            'gradeAcademiques' => $gradeAcademiques,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $gradeAcademique = new Gradeacademique();
        $form = $this->createForm('univBundle\Form\GradeAcademiqueType', $gradeAcademique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gradeAcademique);
            $em->flush();

            return $this->redirectToRoute('gradeacademique_show', array('id' => $gradeAcademique->getId()));
        }

        return $this->render('gradeacademique/new.html.twig', array(
            'gradeAcademique' => $gradeAcademique,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(GradeAcademique $gradeAcademique)
    {
        $deleteForm = $this->createDeleteForm($gradeAcademique);

        return $this->render('gradeacademique/show.html.twig', array(
            'gradeAcademique' => $gradeAcademique,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, GradeAcademique $gradeAcademique)
    {
        $deleteForm = $this->createDeleteForm($gradeAcademique);
        $editForm = $this->createForm('univBundle\Form\GradeAcademiqueType', $gradeAcademique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gradeacademique_show', array('id' => $gradeAcademique->getId()));
        }

        return $this->render('gradeacademique/edit.html.twig', array(
            'gradeAcademique' => $gradeAcademique,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, GradeAcademique $gradeAcademique)
    {
        $form = $this->createDeleteForm($gradeAcademique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gradeAcademique);
            $em->flush();
        }

        return $this->redirectToRoute('gradeacademique_index');
    }

    /**
     * Creates a form to delete a gradeAcademique entity.
     *
     * @param GradeAcademique $gradeAcademique The gradeAcademique entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GradeAcademique $gradeAcademique)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gradeacademique_delete', array('id' => $gradeAcademique->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
