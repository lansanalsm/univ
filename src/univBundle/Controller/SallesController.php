<?php

namespace univBundle\Controller;

use univBundle\Entity\Salles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Salle controller.
 *
 */
class SallesController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $salles = $em->getRepository('univBundle:Salles')->findAll();

        return $this->render('salles/index.html.twig', array(
            'salles' => $salles,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $salle = new Salles();
        $form = $this->createForm('univBundle\Form\SallesType', $salle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($salle);
            $em->flush();
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('salles_index', array('id' => $salle->getId()));
        }

        return $this->render('salles/new.html.twig', array(
            'salle' => $salle,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Salles $salle)
    {
        $deleteForm = $this->createDeleteForm($salle);

        return $this->render('salles/show.html.twig', array(
            'salle' => $salle,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Salles $salle)
    {
        $deleteForm = $this->createDeleteForm($salle);
        $editForm = $this->createForm('univBundle\Form\SallesType', $salle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('salles_index');
        }

        return $this->render('salles/edit.html.twig', array(
            'salle' => $salle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Salles $salle)
    {
        $form = $this->createDeleteForm($salle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($salle);
            $em->flush();
        }

        return $this->redirectToRoute('salles_index');
    }

    /**
     * Creates a form to delete a salle entity.
     *
     * @param Salles $salle The salle entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Salles $salle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('salles_delete', array('id' => $salle->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
