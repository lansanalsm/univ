<?php

namespace univBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class testController extends Controller
{

    /**
     * @Route("/method1", name="app_method1")
     */

    private function method1Action()
    {
        return $this->render('test/method1.html.twig');
    }

    /**
     * @Route("/method2", name="app_method2")
     */
    private function method2Action()
    {
        return $this->render('test/method2.html.twig');
    }

    public function __call($method, $args)
    {
        $user = $this->get('security.tokenStorage')->getToken()->getUser();
        dump($user->getUsername());
        return call_user_func_array(array($this, $method), $args);
    }
}
