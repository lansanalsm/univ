<?php

namespace univBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class ParamUnivController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function moisParamAction()
    {
//        $req = file_get_contents("parametrage/mois.sql");
//        $array = explode(PHP_EOL, $req);
//        foreach ($array as $sql) {
//            if ($sql != '') {
//                Sql($sql);
//            }
//        }
        $em = $this->getDoctrine()->getManager();
        $this->load($em);
        return $this->render('univBundle:ParamUniv:mois_param.html.twig', array(
        ));
    }

    public function load(ObjectManager $manager)
    {
        // Bundle to manage file and directories
        $finder = new Finder();
        $finder->in('parametrage/');
        $finder->name('paramTable.sql');

        foreach( $finder as $file ){
            $content = $file->getContents();

            $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare($content);
            $stmt->execute();
        }
    }

    public function pdfAction()
    {
        $kernel = $this->get('kernel');
        $path = $kernel->locateResource('@AppBundle/Resources/pdf-files/cv.pdf');

        $response = new BinaryFileResponse($path);

        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE, //use ResponseHeaderBag::DISPOSITION_ATTACHMENT to save as an attachement
            'cv.pdf'
        );

        return $response;
    }

}
