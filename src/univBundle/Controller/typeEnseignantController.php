<?php

namespace univBundle\Controller;

use univBundle\Entity\typeEnseignant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Typeenseignant controller.
 *
 */
class typeEnseignantController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeEnseignants = $em->getRepository('univBundle:typeEnseignant')->findAll();

        return $this->render('typeenseignant/index.html.twig', array(
            'typeEnseignants' => $typeEnseignants,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $typeEnseignant = new Typeenseignant();
        $form = $this->createForm('univBundle\Form\typeEnseignantType', $typeEnseignant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeEnseignant);
            $em->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typeenseignant_index');
//            return $this->redirectToRoute('typeenseignant_show', array('id' => $typeEnseignant->getId()));
        }

        return $this->render('typeenseignant/new.html.twig', array(
            'typeEnseignant' => $typeEnseignant,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(typeEnseignant $typeEnseignant)
    {
        $deleteForm = $this->createDeleteForm($typeEnseignant);

        return $this->render('typeenseignant/show.html.twig', array(
            'typeEnseignant' => $typeEnseignant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, typeEnseignant $typeEnseignant)
    {
        $deleteForm = $this->createDeleteForm($typeEnseignant);
        $editForm = $this->createForm('univBundle\Form\typeEnseignantType', $typeEnseignant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('typeenseignant_index');
        }

        return $this->render('typeenseignant/edit.html.twig', array(
            'typeEnseignant' => $typeEnseignant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, typeEnseignant $typeEnseignant)
    {
        $form = $this->createDeleteForm($typeEnseignant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeEnseignant);
            $em->flush();
        }

        return $this->redirectToRoute('typeenseignant_index');
    }

    /**
     * Creates a form to delete a typeEnseignant entity.
     *
     * @param typeEnseignant $typeEnseignant The typeEnseignant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(typeEnseignant $typeEnseignant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typeenseignant_delete', array('id' => $typeEnseignant->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
