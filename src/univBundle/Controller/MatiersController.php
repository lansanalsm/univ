<?php

namespace univBundle\Controller;

use univBundle\Entity\Matiers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Matier controller.
 *
 */
class MatiersController extends Controller
{
    /**
     * Lists all matier entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $matiers = $em->getRepository('univBundle:Matiers')->findAll();
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('matiers/index.html.twig', array(
            'matiers' => $matiers,
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $matier = new Matiers();
        $form = $this->createForm('univBundle\Form\MatiersType', $matier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nomMatier = $form->get('name')->getData();
            $nomDepartement = $form->get('departement')->getData()->getName();
            $codeDepartement = $form->get('departement')->getData()->getCode();
            $numAleatoire = $randon = random_int(999, 9999);
            $codeMatier = (substr($nomMatier, 0, 2) . $numAleatoire . substr($nomDepartement, 0, 2) . $codeDepartement);
            $em = $this->getDoctrine()->getManager();
            $em->persist($matier);
            $em->flush();
            $this->UpdateCodeMatier($matier->getId(), strtoupper($codeMatier));
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('matiers_new');
        }

        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('matiers/new.html.twig', array(
            'matier' => $matier,
            'form' => $form->createView(),
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    public function UpdateCodeMatier($idMatier, $code)
    {
        $em = $this->getDoctrine()->getManager();
        $matiers = $em->getRepository('univBundle:Matiers')->findOneById($idMatier);
        $matiers->setCode($code . $idMatier);
        $em->persist($matiers);
        $em->flush();
        return $code;
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Matiers $matier, Request $request)
    {
        $deleteForm = $this->createDeleteForm($matier);
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('matiers/show.html.twig', array(
            'matier' => $matier,
            'delete_form' => $deleteForm->createView(),
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Matiers $matier)
    {
        $deleteForm = $this->createDeleteForm($matier);
        $editForm = $this->createForm('univBundle\Form\MatiersType', $matier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('matiers_show', array('id' => $matier->getId()));
        }
        $monServices = $this->get('monServices')->mesVariableEtTable($request);
        return $this->render('matiers/edit.html.twig', array(
            'matier' => $matier,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
//            'newsForm' => $monServices['newsForm'],
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Matiers $matier)
    {
        $form = $this->createDeleteForm($matier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($matier);
            $em->flush();
        }

        return $this->redirectToRoute('matiers_index');
    }

    /**
     * Creates a form to delete a matier entity.
     *
     * @param Matiers $matier The matier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Matiers $matier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('matiers_delete', array('id' => $matier->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
