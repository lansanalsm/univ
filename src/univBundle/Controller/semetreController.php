<?php

namespace univBundle\Controller;

use univBundle\Entity\Semetre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Semetre controller.
 *
 */
class semetreController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $semetres = $em->getRepository('univBundle:Semetre')->findAll();

        return $this->render('semetre/index.html.twig', array(
            'semetres' => $semetres,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $semetre = new Semetre();
        $form = $this->createForm('univBundle\Form\semetreType', $semetre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($semetre);
            $em->flush();

            return $this->redirectToRoute('semetre_show', array('id' => $semetre->getId()));
        }

        return $this->render('semetre/new.html.twig', array(
            'semetre' => $semetre,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(semetre $semetre)
    {
        $deleteForm = $this->createDeleteForm($semetre);

        return $this->render('semetre/show.html.twig', array(
            'semetre' => $semetre,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, semetre $semetre)
    {
        $deleteForm = $this->createDeleteForm($semetre);
        $editForm = $this->createForm('univBundle\Form\semetreType', $semetre);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('semetre_edit', array('id' => $semetre->getId()));
        }

        return $this->render('semetre/edit.html.twig', array(
            'semetre' => $semetre,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, semetre $semetre)
    {
        $form = $this->createDeleteForm($semetre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($semetre);
            $em->flush();
        }

        return $this->redirectToRoute('semetre_index');
    }

    /**
     * Creates a form to delete a semetre entity.
     *
     * @param semetre $semetre The semetre entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(semetre $semetre)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('semetre_delete', array('id' => $semetre->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
