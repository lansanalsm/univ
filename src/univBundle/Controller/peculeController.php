<?php

namespace univBundle\Controller;

use univBundle\Entity\pecule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Pecule controller.
 *
 */
class peculeController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pecules = $em->getRepository('univBundle:pecule')->findAll();

        return $this->render('pecule/index.html.twig', array(
            'pecules' => $pecules,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $pecule = new Pecule();
        $form = $this->createForm('univBundle\Form\peculeType', $pecule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pecule);
            $em->flush();

            return $this->redirectToRoute('pecule_show', array('id' => $pecule->getId()));
        }

        return $this->render('pecule/new.html.twig', array(
            'pecule' => $pecule,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(pecule $pecule)
    {
        $deleteForm = $this->createDeleteForm($pecule);

        return $this->render('pecule/show.html.twig', array(
            'pecule' => $pecule,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, pecule $pecule)
    {
        $deleteForm = $this->createDeleteForm($pecule);
        $editForm = $this->createForm('univBundle\Form\peculeType', $pecule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('pecule_index', array('id' => $pecule->getId()));
        }

        return $this->render('pecule/edit.html.twig', array(
            'pecule' => $pecule,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, pecule $pecule)
    {
        $form = $this->createDeleteForm($pecule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pecule);
            $em->flush();
        }

        return $this->redirectToRoute('pecule_index');
    }

    /**
     * Creates a form to delete a pecule entity.
     *
     * @param pecule $pecule The pecule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(pecule $pecule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pecule_delete', array('id' => $pecule->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
