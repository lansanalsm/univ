<?php

namespace univBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\JsonResponse;
use univBundle\Entity\enseigner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use univBundle\Form\enseignerType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Enseigner controller.
 *
 */
class enseignerController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $enseigners = $em->getRepository('univBundle:enseigner')->findAll();

        return $this->render('enseigner/index.html.twig', array(
            'enseigners' => $enseigners,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request, $enseignantId)
    {
        $em = $this->getDoctrine()->getManager();
        $enseignant = $em->getRepository('univBundle:enseignant')->findOneById($enseignantId);
        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        $enseigner = new Enseigner();
        $form = $this->createForm('univBundle\Form\enseignerType', $enseigner);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $enseigner->setEnseignant($enseignant);
                $enseigner->setSessions($sessionEncour);
                $em->persist($enseigner);
                $em->flush();
                $mserror = "Operation effectuer avec success";
                $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
                return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseigner->getEnseignant()->getId()));
            } catch (\Doctrine\DBAL\DBALException $e) {
                $matiere = $form->get('matiers')->getData()->getName();
                $error = "Desole Cette Matiere : " . $matiere . " est déjà affecter ";
                $errorMessage = '<span style="color: red;font-weight: bold ;font-size: larger">' . $error . '</span>';
                $this->get('session')->getFlashBag()->add('success', $errorMessage);
            }

            return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseignant->getId()));
        }
        $MatiereByEnseignant = $em->getRepository('univBundle:enseigner')->findByEnseignant($enseignant);
        return $this->render('enseigner/new.html.twig', array(
            'enseigner' => $enseigner,
            'form' => $form->createView(),
            'enseignant' => $enseignant,
            'matieres' => $MatiereByEnseignant,
            'sessionEncour' => $sessionEncour


        ));
    }

    /**
     * Finds and displays a enseigner entity.
     *
     */
    public function showAction(enseigner $enseigner)
    {
        $deleteForm = $this->createDeleteForm($enseigner);

        return $this->render('enseigner/show.html.twig', array(
            'enseigner' => $enseigner,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing enseigner entity.
     *
     */
    public function editAction(Request $request, enseigner $enseigner)
    {
        $deleteForm = $this->createDeleteForm($enseigner);
        $editForm = $this->createForm('univBundle\Form\enseignerType', $enseigner);
        $editForm->handleRequest($request);
          $sessionEncour = $this->get('monServices')->getSessionEncour(1);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $enseigner->setSessions($sessionEncour);
            $this->getDoctrine()->getManager()->flush();

            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseigner->getEnseignant()->getId()));
        }

        return $this->render('enseigner/edit.html.twig', array(
            'enseigner' => $enseigner,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'matiere' => $enseigner->getMatiers()
        ));
    }

    /**
     * Deletes a enseigner entity.
     *
     */
    public function deleteAction($id, $dienseignant)
    {
        $em = $this->getDoctrine()->getManager();
        $enseignerById = $em->getRepository('univBundle:enseigner')->findOneById($id);
        $enseignantById = $em->getRepository('univBundle:enseignant')->findOneById($dienseignant);
        $em->remove($enseignerById);
        $em->flush();
        $error = "Suppression effectuer avec success ";
        $errorMessage = '<span style="color: #7eff86;font-weight: bold ;font-size: larger">' . $error . '</span>';
        $this->get('session')->getFlashBag()->add('success', $errorMessage);
        return $this->redirectToRoute('enseigner_new', array('enseignantId' => $enseignantById->getId()));
    }

    /**
     * Creates a form to delete a enseigner entity.
     *
     * @param enseigner $enseigner The enseigner entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(enseigner $enseigner)
    {
        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('enseigner_delete', array('id' => $enseigner->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function getUser()
    {
        return parent::getUser(); // TODO: Change the autogenerated stub
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function semestreByMatier(Request $request)
    {

        $semestre_id = $request->request->get('semestre_id');
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = "SELECT * FROM semetre s
                WHERE 1 = 1 ";

        if ($semestre_id != '')
            $sql .= " AND s.id = :semestre_id";

        $query = $em->createNativeQuery($sql, $rsm);

        if ($semestre_id != '')
            $query
                ->setParameter('semestre_id', $semestre_id);

        $semestres = $query->getResult();

        $semestresList = array();
        foreach ($semestres as $semestres) {
            $p = array();
            $p['id'] = $semestres['id'];
            $p['name'] = $semestres['name'];
            $semestresList[] = $p;
        }

        return new JsonResponse($semestresList);


    }

    public function licenceBysemetre()
    {
    }
}
