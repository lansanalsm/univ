<?php

namespace univBundle\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use univBundle\Entity\calendrier;
use univBundle\Entity\departement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// $productRepository = $entityManager->getRepository('Product');

/**
 * calendrier controller.
 *
 */
class calendrierController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('departement');
        
        $departement = null;
        $calendriers = null; 
        
        if (is_null($id) == false) {
            $departement = $em->getRepository('univBundle:departement')->find($id);
        }
        
        // les departemens pour le dropdown
        $departements = $em->getRepository('univBundle:departement')->findAll();

        //$calendriers = $em->getRepository('univBundle:calendrier')->findAll();
         if (!is_null($departement)) {
             $calendriers = $em->getRepository("univBundle:calendrier")
             ->createQueryBuilder('o')
             ->where('o.departement = :departement')
             ->setParameter('departement', $departement)
             ->getQuery()
             ->getResult();
         }

        return $this->render(
            'calendrier/index.html.twig', 
            array(
                'calendriers' => $calendriers,
                'departements' => $departements,
                'departement' => $departement // la selection
            )
        );
    }

    public function calendrierPdfAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $departement=$this->getUser()->getPersonnel()->getDepartement();
        $calendriers = $em->getRepository("univBundle:calendrier")
            ->createQueryBuilder('o')
            ->where('o.departement = :departement')
            ->setParameter('departement', $departement)
            ->getQuery()
            ->getResult();
//        return $this->render(
//            'calendrier/index.html.twig',
//            array(
//                'calendriers' => $calendriers,
//            )
//        );
//

        $html = $this->renderView('calendrier/indexPdf.html.twig', array(
            'calendriers' => $calendriers,
            'departement'=>$departement
        ));
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'enable-external-links' => true,
                'enable-internal-links' => true,
                'enable-javascript' => true,
//                'page-height' => 200,
//                'page-width' => 120,

            )),
            'EMPLOIE DU TEMPS'.$departement->getFaculte()->getName() . " Departement :" . $departement->getName().'.pdf'
        );

    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $calendrier = new calendrier();
        $form = $this->createForm('univBundle\Form\calendrierForm', $calendrier);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {

            
            $em->persist($calendrier);
            $em->flush();

            return $this->redirectToRoute('calendrier_new'); //  array('id' => $calendrier->getId()));
        }
        $departement=$this->getUser()->getPersonnel()->getDepartement();
        $calendriers = $em->getRepository("univBundle:calendrier")
            ->createQueryBuilder('o')
            ->where('o.departement = :departement')
            ->setParameter('departement', $departement)
            ->getQuery()
            ->getResult();
        return $this->render('calendrier/new.html.twig', array(
            'calendrier' => $calendrier,
            'form' => $form->createView(),
            'calendriers' => $calendriers,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Calendrier $calendrier)
    {
        $deleteForm = $this->createDeleteForm($calendrier);

        return $this->render('calendrier/show.html.twig', array(
            'calendrier' => $calendrier,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Calendrier $calendrier)
    {
        $deleteForm = $this->createDeleteForm($calendrier);
        $editForm = $this->createForm('univBundle\Form\calendrierForm', $calendrier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('calendrier_edit', array('id' => $calendrier->getId()));
        }

        return $this->render('calendrier/edit.html.twig', array(
            'calendrier' => $calendrier,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, calendrier $calendrier)
    {
        $form = $this->createDeleteForm($calendrier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($calendrier);
            $em->flush();
        }

        return $this->redirectToRoute('calendrier_index');
    }

    /**
     * Creates a form to delete a calendrier entity.
     *
     * @param calendrier $calendrier The calendrier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(calendrier $calendrier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('calendrier_delete', array('id' => $calendrier->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function getUser()
    {
        return parent::getUser();
    }
}
