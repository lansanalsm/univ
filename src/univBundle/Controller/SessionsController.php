<?php

namespace univBundle\Controller;

use univBundle\Entity\Sessions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Session controller.
 *
 */
class SessionsController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $querySession = $em->createQuery(
            'SELECT s.id,s.sessions,s.encour FROM univBundle:Sessions s
                ORDER  BY s.id DESC ');
        $sessions = $querySession->getResult();

        return $this->render('sessions/index.html.twig', array(
            'sessions' => $sessions,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $session = new Sessions();
        $form = $this->createForm('univBundle\Form\SessionsType', $session);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $sessionEncour = $this->get('monServices')->getSessionEncour(1);
            $sessionBool=$form->get('encour')->getData();
            $valeurSession=$form->get('sessions')->getData();
            if ($sessionBool==true && $valeurSession >$sessionEncour->getSessions() ){
                    $sessionEncour->setEncour(false);
                    $em->persist($sessionEncour);
                    $em->flush();
                $session->setEncour(true);
                $em->persist($session);
                $em->flush();
            }elseif ($sessionBool==false && $valeurSession > $sessionEncour->getSessions()){
                $em = $this->getDoctrine()->getManager();
                $session->setEncour(false);
                $em->persist($session);
                $em->flush();
            }

            return $this->redirectToRoute('sessions_show', array('id' => $session->getId()));
        }

        return $this->render('sessions/new.html.twig', array(
            'session' => $session,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Sessions $session)
    {
        $deleteForm = $this->createDeleteForm($session);

        return $this->render('sessions/show.html.twig', array(
            'session' => $session,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Sessions $session)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($session);
        $editForm = $this->createForm('univBundle\Form\SessionsType', $session);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($editForm->get('encour')==true){
                $sessions = $em->getRepository('univBundle:Sessions')->findAll();
                foreach ($sessions as $sessions){
                    $sessions->setEncour(false);
                    $em->flush();
                }
            }

           $session->setEncour(true);
            $em->persist($session);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sessions_edit', array('id' => $session->getId()));
        }

        return $this->render('sessions/edit.html.twig', array(
            'session' => $session,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Sessions $session)
    {
        $form = $this->createDeleteForm($session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($session);
            $em->flush();
        }

        return $this->redirectToRoute('sessions_index');
    }

    /**
     * Creates a form to delete a session entity.
     *
     * @param Sessions $session The session entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sessions $session)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sessions_delete', array('id' => $session->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
