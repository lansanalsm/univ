<?php

namespace univBundle\Controller;

use univBundle\Entity\concentration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Concentration controller.
 *
 */
class concentrationController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $concentrations = $em->getRepository('univBundle:concentration')->findAll();

        return $this->render('concentration/index.html.twig', array(
            'concentrations' => $concentrations,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $concentration = new Concentration();
        $form = $this->createForm('univBundle\Form\concentrationType', $concentration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concentration);
            $em->flush();

            return $this->redirectToRoute('concentration_index');
        }

        return $this->render('concentration/new.html.twig', array(
            'concentration' => $concentration,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(concentration $concentration)
    {
        $deleteForm = $this->createDeleteForm($concentration);

        return $this->render('concentration/show.html.twig', array(
            'concentration' => $concentration,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, concentration $concentration)
    {
        $deleteForm = $this->createDeleteForm($concentration);
        $editForm = $this->createForm('univBundle\Form\concentrationType', $concentration);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('concentration_index');
        }

        return $this->render('concentration/edit.html.twig', array(
            'concentration' => $concentration,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, concentration $concentration)
    {
        $form = $this->createDeleteForm($concentration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($concentration);
            $em->flush();
        }

        return $this->redirectToRoute('concentration_index');
    }

    /**
     * Creates a form to delete a concentration entity.
     *
     * @param concentration $concentration The concentration entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(concentration $concentration)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('concentration_delete', array('id' => $concentration->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
