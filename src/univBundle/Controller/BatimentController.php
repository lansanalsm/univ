<?php

namespace univBundle\Controller;

use univBundle\Entity\Batiment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Batiment controller.
 *
 */
class BatimentController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $batiments = $em->getRepository('univBundle:Batiment')->findAll();

        return $this->render('batiment/index.html.twig', array(
            'batiments' => $batiments,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $batiment = new Batiment();
        $form = $this->createForm('univBundle\Form\BatimentType', $batiment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($batiment);
            $em->flush();

            return $this->redirectToRoute('batiment_index', array('id' => $batiment->getId()));
        }

        return $this->render('batiment/new.html.twig', array(
            'batiment' => $batiment,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(Batiment $batiment)
    {
        $deleteForm = $this->createDeleteForm($batiment);

        return $this->render('batiment/show.html.twig', array(
            'batiment' => $batiment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, Batiment $batiment)
    {
        $deleteForm = $this->createDeleteForm($batiment);
        $editForm = $this->createForm('univBundle\Form\BatimentType', $batiment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('batiment_index', array('id' => $batiment->getId()));
        }

        return $this->render('batiment/edit.html.twig', array(
            'batiment' => $batiment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, Batiment $batiment)
    {
        $form = $this->createDeleteForm($batiment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($batiment);
            $em->flush();
        }
        $mserror = "Operation effectuer avec success";
        $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
        $this->get('session')->getFlashBag()->add('success', $errorMessage);
        return $this->redirectToRoute('batiment_index');
    }

    /**
     * Creates a form to delete a batiment entity.
     *
     * @param Batiment $batiment The batiment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Batiment $batiment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('batiment_delete', array('id' => $batiment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
