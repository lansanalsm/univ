<?php

namespace univBundle\Controller;

use univBundle\Entity\FraisInscription;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Fraisinscription controller.
 *
 */
class FraisInscriptionController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fraisInscriptions = $em->getRepository('univBundle:FraisInscription')->findAll();

        return $this->render('fraisinscription/index.html.twig', array(
            'fraisInscriptions' => $fraisInscriptions,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function newAction(Request $request)
    {
        $fraisInscription = new Fraisinscription();
        $form = $this->createForm('univBundle\Form\FraisInscriptionType', $fraisInscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraisInscription);
            $em->flush();

            return $this->redirectToRoute('fraisinscription_show', array('id' => $fraisInscription->getId()));
        }

        return $this->render('fraisinscription/new.html.twig', array(
            'fraisInscription' => $fraisInscription,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function showAction(FraisInscription $fraisInscription)
    {
        $deleteForm = $this->createDeleteForm($fraisInscription);

        return $this->render('fraisinscription/show.html.twig', array(
            'fraisInscription' => $fraisInscription,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function editAction(Request $request, FraisInscription $fraisInscription)
    {
        $deleteForm = $this->createDeleteForm($fraisInscription);
        $editForm = $this->createForm('univBundle\Form\FraisInscriptionType', $fraisInscription);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $mserror = "Operation effectuer avec success";
            $errorMessage = '<span style="color: #ffb069;font-weight: bold ;font-size: larger">' . $mserror . '</span>';
            $this->get('session')->getFlashBag()->add('success', $errorMessage);
            return $this->redirectToRoute('fraisinscription_index');
        }

        return $this->render('fraisinscription/edit.html.twig', array(
            'fraisInscription' => $fraisInscription,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * */
    public function deleteAction(Request $request, FraisInscription $fraisInscription)
    {
        $form = $this->createDeleteForm($fraisInscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisInscription);
            $em->flush();
        }

        return $this->redirectToRoute('fraisinscription_index');
    }

    /**
     * Creates a form to delete a fraisInscription entity.
     *
     * @param FraisInscription $fraisInscription The fraisInscription entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FraisInscription $fraisInscription)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fraisinscription_delete', array('id' => $fraisInscription->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
