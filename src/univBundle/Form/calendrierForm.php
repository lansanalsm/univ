<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TimeType;


class calendrierForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matiere', EntityType::class, array(
                'class' => 'univBundle\Entity\Matiers',
                'choice_label' => 'Matiere',
                'placeholder' => '=== Sélectionner la matiere ==='))
            ->add('enseignant', EntityType::class, array(
                'class' => 'univBundle\Entity\enseignant',
                'choice_label' => 'NomPrenomTelephone',
                'placeholder' => '=== Sélectionner un enseignant ==='))
            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'Departement',
                'placeholder' => '=== Sélectionner un departement ==='))
            ->add('salle', EntityType::class, array(
                'class' => 'univBundle\Entity\Salles',
                'choice_label' => 'numSalle',
                'placeholder' => '=== Sélectionner une salle ==='))
            ->add('jour', EntityType::class, array(
                'class' => 'univBundle\Entity\jourEtude',
                'choice_label' => 'Jour',
                'placeholder' => '=== Sélectionner un jour ==='))
            ->add('debut', TimeType::class)
            ->add('fin', TimeType::class,array())
            ->add('licence', EntityType::class, array(
                'class' => 'univBundle\Entity\Licence',
                'choice_label' => 'name', 'mapped' => false,
                'placeholder' => 'Sélectionner une Licence'))
            ->add('semestre', EntityType::class, array(
                'class' => 'univBundle\Entity\Semetre','mapped' => false,'required' => true,
                'choice_label' => 'name',
                'placeholder' => 'Sélectionner un semestre'))

        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Calendrier'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_calendrier';
    }


}
