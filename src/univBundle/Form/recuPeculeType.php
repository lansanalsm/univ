<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class recuPeculeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('datepaie', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
                ))
            ->add('sessions', EntityType::class, array('class' => 'univBundle\Entity\Sessions',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('S')
                        ->where('S.encour = :encour')
                        ->setParameter('encour', 1)
                        ->orderBy('S.id', 'ASC');
                }, 'choice_label' => 'sessions', 'required' => true,'disabled'=>true
            ))
//            ->add('mois', EntityType::class, array(
//                'class' => 'univBundle\Entity\mois',
//                'choice_label' => 'name',
//                'placeholder' => '=== Choisisez un mois ==='))

            ->add('etudiant',EntityType::class,
                array(
                    'class' => 'univBundle\Entity\Etudiants',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('E');
                    }, 'choice_label' => 'matricule', 'required' => true,'disabled'=>true
                )
            )
//            ->add('mois')
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\recuPecule'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_recupecule';
    }


}
