<?php

namespace univBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversiteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomUniversite')
            ->add('nomPrenomRecteur')
            ->add('nomPrenomDirecteurProgramme')
            ->add('nomPrenomViceRecteurChargerEtude')
            ->add('nomPrenomViceRecteurChargerRecherche')
            ->add('adresse')
            ->add('phone1')
            ->add('phone2')
            ->add('email',EmailType::class)
            ->add('boitePostal')
            ->add('siteWeb')
            ->add('couverture', MediaType::class, array('data_class' => 'univBundle\Entity\Media', 'required' => false))
            ->add('logo', MediaType::class, array('data_class' => 'univBundle\Entity\Media', 'required' => false))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Universite'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_universite';
    }


}
