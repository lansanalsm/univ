<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('prenom')
            ->add('tel1')
            ->add('tel2')
            ->add('adresse')
            ->add('email', EmailType::class)
            ->add('faculte', EntityType::class, array(
                'class' => 'univBundle\Entity\Facultes',
                'choice_label' => 'name', 'mapped' => false, 'required' => false,
                'placeholder' => '=== Choisisez une faculte ==='))
            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'name', 'required' => false,
                'placeholder' => '=== Sélectionner un departement ==='))
            ->add('fonction', EntityType::class, array(
                'class' => 'univBundle\Entity\Fonction',
                'choice_label' => 'name',
                'placeholder' => '=== Choisisez une fonction ==='))
            ->add('GradeAcademique', EntityType::class, array(
                'class' => 'univBundle\Entity\GradeAcademique',
                'choice_label' => 'name','required' => false,
                'placeholder' => '=== Choisisez un grade ==='))
            ->add('typePersonnel', EntityType::class, array(
                'class' => 'univBundle\Entity\typePersonnel',
                'choice_label' => 'name','required' => false,
                'placeholder' => '=== Choisisez un type ==='))
            ->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'ROLE_SUPERVISEUR' => 'ROLE_SUPERVISEUR',
                    'ROLE_CHEF_DEPARTEMENT' => 'ROLE_CHEF_DEPARTEMENT',
                    'ROLE_DIRECTEUR_PROGRAMME'=>'ROLE_DIRECTEUR_PROGRAMME',
                    'ROLE_SCOLARITE' => 'ROLE_SCOLARITE',
                    'ROLE_COMPTABILITE' => 'ROLE_COMPTABILITE',
//                    'ROLE_ENSEGNANT' => 'ROLE_ENSEGNANT',
                ),
                'mapped' => false,
                'required' => true, 'placeholder' => '========== Choisisez un role =========='
            ))
            ->add('image', MediaType::class, array('data_class' => 'univBundle\Entity\Media', 'required' => false));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Personnel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_personnel';
    }


}
