<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateInscription', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
            ))
//            ->add('annee')
//            ->add('etudiant')
//            ->add('licence')
//            ->add('image', MediaType::class, array('data_class' => 'univBundle\Entity\Media', 'required' => false,'mapped'=>false))
            ->add('licence', EntityType::class, array('class' => 'univBundle\Entity\Licence', 'choice_label' => 'name', 'placeholder' => 'Sélectionner une licence'))
            ->add('sessions', EntityType::class,
                array('class' => 'univBundle\Entity\Sessions',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('S')
                            ->where('S.encour = :encour')
                            ->setParameter('encour', 1)
                            ->orderBy('S.id', 'ASC');
                    }, 'choice_label' => 'sessions', 'required' => true,
                )
            )
//            ->add('sessions', EntityType::class, array('class' => 'univBundle\Entity\Sessions', 'choice_label' => 'sessions', 'placeholder' => 'Sélectionner une sessions'))
            ->add('concentration', EntityType::class, array('class' => 'univBundle\Entity\concentration', 'choice_label' => 'name','required' => false,))//            ->add('sessions')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Inscription'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_inscription';
    }


}
