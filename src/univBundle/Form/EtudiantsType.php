<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EtudiantsType extends AbstractType
{
//    private $encour;
//    public function __construct($encour = null)
//    {
//        $sessionEncour = $this->get('monServices')->getSessionEncour(1);
//        $this->encour = $sessionEncour;
//    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('matricule',TextType::class , array('required' => true, 'attr' => array('readonly' => true)))
            ->add('nomPrenom', TextType::class)
            ->add('sexe', ChoiceType::class, array(
                'choices' => array(
                    'Masculin' => 'Masculin',
                    'Feminin' => 'Feminin'),
                'mapped' => true,
                'required' => true,
            ))
            ->add('filiation', TextType::class)
            ->add('contact', TextType::class ,array('required' => false))
            ->add('nationnalite', TextType::class)
            ->add('dateNaissance', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(), 'required' => true, 'mapped' => false,
            ))
            ->add('image', MediaType::class, array('data_class' => 'univBundle\Entity\Media', 'required' => false))
            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'name',
                'placeholder' => '=== Sélectionner un departement ==='))
            ->add('licence', EntityType::class, array(
                'class' => 'univBundle\Entity\Licence',
                'choice_label' => 'name',
                'placeholder' => '=== Sélectionner un niveau ==='))
            ->add('statut', ChoiceType::class, array(
                'choices' => array(
                    'Non Boursier' => 'Non Boursier',
                    'Boursier' => 'Boursier'
                ),
                'mapped' => true,
                'required' => true,
            ))
            ->add('concentration', EntityType::class, array('class' => 'univBundle\Entity\concentration', 'choice_label' => 'name', 'placeholder' => 'Sélectionner une concentration', 'mapped' => false, 'required' => false))
//            ->add('Semestre', EntityType::class, array(
//                'class' => 'univBundle\Entity\semetre',
//                'choice_label' => 'name',
////                'placeholder' => '=== Sélectionner un Semestre ==='))
            ->add('PV', TextType::class, array('mapped' => false, 'required' => true))
//            ->add('Sessions', EntityType::class, array(
//                'class' => 'univBundle\Entity\Sessions',
//                'choice_label' => 'sessions','mapped'=>false,'required'=>true,
//                'placeholder' => '=== Sélectionner un Semestre ==='))

            ->add('Sessions', EntityType::class, array(
                    'class' => 'univBundle\Entity\Sessions',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('S')
                            ->where('S.encour = :encour')
                            ->setParameter('encour', 1)
                            ->orderBy('S.id', 'ASC');
                    }, 'choice_label' => 'sessions', 'required' => true, 'mapped' => false,
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Etudiants'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_etudiants';
    }


}
