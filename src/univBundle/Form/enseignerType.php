<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class enseignerType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matiers', EntityType::class, array(
                'class' => 'univBundle\Entity\Matiers',
                'choice_label' => 'name', 'required' => true,
                'placeholder' => '=== Sélectionner une matiere ==='))
//            ->add('Sessions', EntityType::class, array(
//                'class' => 'univBundle\Entity\Sessions',
//                'choice_label' => 'sessions','required'=>true,
//                'placeholder' => '=== Sélectionner une Session ==='))
            ->add('licence', EntityType::class, array(
                'class' => 'univBundle\Entity\Licence',
                'choice_label' => 'name', 'mapped' => false, 'required' => true,
                'placeholder' => '=== Sélectionner un niveau ==='))
            ->add('semestre', EntityType::class, array(
                'class' => 'univBundle\Entity\Semetre',
                'choice_label' => 'name', 'mapped' => false, 'required' => true,
                'placeholder' => '=== Sélectionner un semestre ==='));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\enseigner'
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_enseigner';
    }


}
