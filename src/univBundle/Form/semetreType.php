<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class semetreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('licence', EntityType::class, array(
                    'class' => 'univBundle\Entity\Licence',
                    'choice_label' => 'name',
                    'placeholder' => 'Sélectionner une licence'))

            ->add('module', EntityType::class, array(
                'class' => 'univBundle\Entity\Module',
                'choice_label' => 'name',
                'placeholder' => '=== Sélectionner un Module ==='))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Semetre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_semetre';
    }


}
