<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionNonBoussierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateInscription', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
            ))
            ->add('licence', EntityType::class, array('class' => 'univBundle\Entity\Licence', 'choice_label' => 'name', 'placeholder' => 'Sélectionner une licence'))
            ->add('sessions', EntityType::class,
                array('class' => 'univBundle\Entity\Sessions',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('S')
                            ->where('S.encour = :encour')
                            ->setParameter('encour', 1)
                            ->orderBy('S.id', 'ASC');
                    }, 'choice_label' => 'sessions', 'required' => true,
                )
            )
//            ->add('sessions', EntityType::class, array('class' => 'univBundle\Entity\Sessions', 'choice_label' => 'sessions', 'placeholder' => 'Sélectionner une sessions'))
            ->add('concentration', EntityType::class, array('class' => 'univBundle\Entity\concentration', 'choice_label' => 'name'))//            ->add('sessions')
//            ->add('datepaiement', DateType::class, array(
//                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
//                'data' => new \DateTime(),'mapped'=>false,
//            ))
//            ->add('montantPayer')
            ->add('sessions', EntityType::class, array('class' => 'univBundle\Entity\Sessions',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('S')
                        ->where('S.encour = :encour')
                        ->setParameter('encour', 1)
                        ->orderBy('S.id', 'ASC');
                }, 'choice_label' => 'sessions', 'required' => true,
            ))
            ->add('fraisScolarite', IntegerType::class, array('required' => true, 'attr' => array('min' => 0, 'step' => "any"),'mapped'=>false,'required'=>true))
            ->add('numeroRecu', TextType::class,array('mapped'=>false,'required'=>true))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Inscription'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_inscription';
    }


}
