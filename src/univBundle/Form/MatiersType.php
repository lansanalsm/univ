<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatiersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('code')
            ->add('name')
            ->add('nombreHeure', IntegerType::class, array('required' => true, 'attr' => array('min' => 45, 'max' => 72),))
            ->add('nombreCredit', IntegerType::class, array('required' => true, 'attr' => array('min' => 1, 'max' => 6),))
            ->add('baremInf', NumberType::class, array('required' => true, 'attr' => array('min' => 0, 'max' => 10),))
            ->add('baremSup', NumberType::class, array('required' => true, 'attr' => array('min' => 0, 'max' => 10),))
            ->add('typeMatiere', ChoiceType::class, array(
                'choices' => array(
                    'Fondamentale' =>'Fondamentale',
                    'Optionnelle' => 'Optionnelle',
                ),
                'mapped' => true,
                'required' => true, 'placeholder' => '========== Choisisez un type =========='
            ))


            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'name',
                'placeholder' => 'Sélectionner un departement'))
            ->add('licence', EntityType::class, array(
                'class' => 'univBundle\Entity\Licence',
                'choice_label' => 'name', 'mapped' => false, 'required' => true,
                'placeholder' => 'Sélectionner une Licence'))
            ->add('semestre', EntityType::class, array(
                'class' => 'univBundle\Entity\Semetre',
                'choice_label' => 'name',
                'placeholder' => 'Sélectionner un semestre'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Matiers'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_matiers';
    }


}
