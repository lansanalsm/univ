<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmploieType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('heurDebut', TimeType::class,
                            array('placeholder' => array(
                                    'hour' => 'Heures', 'minute' => 'Minutes', 'seconde' => 'Secondes',
                )))
            ->add('heurFin', TimeType::class,
                array('placeholder' => array(
                    'heure' => 'Heure', 'minute' => 'Minute', 'seconde' => 'Seconde',
                )))
            ->add('datejour', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data' => new \DateTime()))
            ->add('salles',EntityType::class, array(
                'class' => 'univBundle\Entity\Salles',
                'choice_label' => 'name',
                'placeholder' => '=== Choisisez une salle ==='))
            ->add('matiers', EntityType::class, array(
                'class' => 'univBundle\Entity\Matiers',
                'choice_label' => 'name',
                'placeholder' => '=== Choisisez une matiere ==='))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Emploie'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_emploie';
    }


}
