<?php

namespace univBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class detailCongeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data' => new \DateTime()))
            ->add('dateFin', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data' => new \DateTime()))
            ->add('conge', congeType::class, array('data_class' => 'univBundle\Entity\conge','mapped'=>true))
//            ->add('etudiant')
//            ->add('sessions')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\detailConge'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_detailconge';
    }


}
