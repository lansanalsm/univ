<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class enseignantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('prenom')
            ->add('diplome')
            ->add('telephone')
            ->add('contact')
            ->add('email',EmailType::class)
//            ->add('departement', EntityType::class, array(
//                'class' => 'univBundle\Entity\departement',
//                'choice_label' => 'name',
//                'placeholder' => '=== Sélectionner un departement ==='))
            ->add('GradeAcademique', EntityType::class, array(
                'class' => 'univBundle\Entity\GradeAcademique',
                'choice_label' => 'name',
                'placeholder' => '=== Choisisez une grade ==='))
            ->add('typeEnseignant', EntityType::class, array(
                'class' => 'univBundle\Entity\typeEnseignant',
                'choice_label' => 'name',
                'placeholder' => '=== Choisisez un type ==='))

//            ->add('startTime', TimeType::class, array('mapped' =>false,
//                'placeholder' => array(
//                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
//                )
//            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\enseignant'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_enseignant';
    }


}
