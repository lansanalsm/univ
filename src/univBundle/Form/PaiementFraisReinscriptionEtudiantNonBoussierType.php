<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaiementFraisReinscriptionEtudiantNonBoussierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('datepaiement')
//            ->add('montantPayer')
//            ->add('sessions')
            ->add('datepaiement', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
            ))
            ->add('fraisScolarite', IntegerType::class, array('required' => true, 'attr' => array('min' => 0, 'step' => "any"),))
            ->add('numeroRecu', TextType::class)
//            ->add('departement', EntityType::class, array('class' => 'univBundle\Entity\departement',
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('S')
//                        ->orderBy('S.id', 'ASC');
//                }, 'choice_label' => 'name', 'required' => true,'mapped'=>false,'choice_value'=>'code',
//                'placeholder' => '=== Sélectionner un departement ==='
//            ))
            ->add('sessions', EntityType::class, array('class' => 'univBundle\Entity\Sessions',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('S')
                        ->where('S.encour = :encour')
                        ->setParameter('encour', 1)
                        ->orderBy('S.id', 'ASC');
                }, 'choice_label' => 'sessions', 'required' => true,
            ))
//            ->add('flagInscription')
//            ->add('admisBac')
//            ->add('inscriptions')
//            ->add('etudiant')
//            ->add('fraisInscription')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Paiement'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_paiement';
    }


}
