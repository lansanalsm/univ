<?php

namespace univBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecycleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('pv',IntegerType::class, array('required' => true, 'attr' => array('min' =>1),))
            ->add('nomPrenom',TextType::class)
            ->add('dateNaissance', DateType::class, array(
                'widget' => 'single_text', 'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(), 'required' => true, 'mapped' => false,
            ))
            ->add('sexe', ChoiceType::class, array(
                'choices' => array(
                    'Masculin' => 'Masculin',
                    'Feminin' => 'Feminin'),
                'mapped' => true,
                'required' => true,
            ))
            ->add('filiation',TextType::class)
//            ->add('centre',TextType::class)
            ->add('origine',TextType::class)
            ->add('options',TextType::class)
            ->add('statut', ChoiceType::class, array(
                'choices' => array(
                    'Non Boussier' => 'Non Bourssier',
                ),
                'mapped' => true,
                'required' => true,
            ))
            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'name',
                'placeholder' => '=== Sélectionner un departement ==='))
            ->add('sessions',IntegerType::class, array('required' => true, 'attr' => array('min' =>2015),))
//            ->add('etat')
            ->add('contact',TextType::class)
            ->add('nationnalite',TextType::class)
            ->add('licence', EntityType::class, array(
                'class' => 'univBundle\Entity\Licence',
                'choice_label' => 'name','mapped'=>false,
                'placeholder' => '=== Sélectionner un niveau ==='))


        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\AdminBac'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_adminbac';
    }


}
