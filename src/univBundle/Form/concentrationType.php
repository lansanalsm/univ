<?php

namespace univBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class concentrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('faculte', EntityType::class, array(
                'class' => 'univBundle\Entity\Facultes',
                'choice_label' => 'name', 'mapped' => false, 'required' => true,
                'placeholder' => '=== Sélectionner une faculte ==='))
            ->add('departement', EntityType::class, array(
                'class' => 'univBundle\Entity\departement',
                'choice_label' => 'name',
                'placeholder' => '=== Sélectionner un departement ==='));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\concentration'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_concentration';
    }


}
