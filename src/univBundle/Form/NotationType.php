<?php

namespace univBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyenneCours', IntegerType::class, array('required' => true, 'attr' => array('min' => 0, 'max' => 10, 'step' => "any"),))
            ->add('moyenneExament', IntegerType::class, array('required' => true, 'attr' => array('min' => 0, 'max' => 10, 'step' => "any"),))
//            ->add('session1')
//            ->add('session2')
//            ->add('dateNotation')
//            ->add('flagSession1')
//            ->add('flagSession2')
//            ->add('etudiant')
//            ->add('licence')
//            ->add('semestre')
//            ->add('matiers')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'univBundle\Entity\Notation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'univbundle_notation';
    }


}
