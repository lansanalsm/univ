<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
// Validation
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\Mapping\UniqueConstraint;
/**
 * recuPecule
 *
 * @ORM\Table(name="recu_pecule",
 * uniqueConstraints={
 *        @UniqueConstraint(name="etudiant_session_mois_unique",
 *            columns={"etudiants_id","sessions_id","mois_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="univBundle\Repository\recuPeculeRepository")
 */
class recuPecule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepaie", type="datetime")
     */
    private $datepaie;

    /**
     * @var float
     *
     * @ORM\Column(name="montantRecu", type="float")
     */
    private $montantRecu;

    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="recupecule")
     * @ORM\JoinColumn(name="etudiants_id", referencedColumnName="id",nullable=true)
     */
    private $etudiant;

    /**
     * @ORM\ManyToOne(targetEntity="pecule", inversedBy="recupecule")
     * @ORM\JoinColumn(name="pecule_id", referencedColumnName="id")
     */
    private $pecule;

    /**
     * @ORM\ManyToOne(targetEntity="mois", inversedBy="recupecule")
     * @ORM\JoinColumn(name="mois_id", referencedColumnName="id",nullable=true)
     */
    private $mois;
    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="recupecule")
     * @ORM\JoinColumn(name="sessions_id", referencedColumnName="id")
     */
    private $sessions;



    /**
     * @Assert\Callback
     */
    public function isContentValid(ExecutionContextInterface $context)
    {

        // $em = $this->getDoctrine()->getManager();

        $mois= $this->getMois();
        $etudiant = $this->getEtudiant();
        $session = $this->getSessions();

        global $kernel;
        $em = $kernel->getContainer()->get('doctrine')->getManager();
        $all_recuPecule = $em->getRepository('univBundle:recuPecule')->findAll();
        foreach ($all_recuPecule as $recuPecule) {
            if( $mois == $recuPecule->getMois() && $etudiant == $recuPecule->getEtudiant() && $session== $recuPecule->getSessions()) {
                $context
                    ->buildViolation('Cet etudiant a deja recu son pucule pour ce mois ')
                    ->atPath('mois')
                    ->addViolation() ;
                break;
            }


        }

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datepaie
     *
     * @param \DateTime $datepaie
     *
     * @return recuPecule
     */
    public function setDatepaie($datepaie)
    {
        $this->datepaie = $datepaie;

        return $this;
    }

    /**
     * Get datepaie
     *
     * @return \DateTime
     */
    public function getDatepaie()
    {
        return $this->datepaie;
    }

    /**
     * Set montantRecu
     *
     * @param float $montantRecu
     *
     * @return recuPecule
     */
    public function setMontantRecu($montantRecu)
    {
        $this->montantRecu = $montantRecu;

        return $this;
    }

    /**
     * Get montantRecu
     *
     * @return float
     */
    public function getMontantRecu()
    {
        return $this->montantRecu;
    }



    /**
     * Set etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return recuPecule
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set pecule
     *
     * @param \univBundle\Entity\pecule $pecule
     *
     * @return recuPecule
     */
    public function setPecule(\univBundle\Entity\pecule $pecule = null)
    {
        $this->pecule = $pecule;

        return $this;
    }

    /**
     * Get pecule
     *
     * @return \univBundle\Entity\pecule
     */
    public function getPecule()
    {
        return $this->pecule;
    }

    /**
     * Set mois
     *
     * @param \univBundle\Entity\mois $mois
     *
     * @return recuPecule
     */
    public function setMois(\univBundle\Entity\mois $mois = null)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return \univBundle\Entity\mois
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return recuPecule
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }
}
