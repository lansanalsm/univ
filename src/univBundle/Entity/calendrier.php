<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

// Validation
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


/**
 * Calendrier
 *
 * @ORM\Table(name="calendrier")
 * @ORM\Entity(repositoryClass="univBundle\Repository\CalendrierRepository")
 */
class calendrier
{   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="Matiers", inversedBy="calendriers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matiere;

     /**
     * @ORM\ManyToOne(targetEntity="Enseignant", inversedBy="calendriers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $enseignant;


     /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="calendriers")
     * @ORM\JoinColumn(nullable=false)
     */
     private $departement;

   
     /**
     * @ORM\ManyToOne(targetEntity="Salles", inversedBy="calendriers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $salle;

     /**
     * @ORM\ManyToOne(targetEntity="jourEtude", inversedBy="calendriers")
     * @ORM\JoinColumn(nullable=false)
     */

    private $jour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="debut", type="time")
     */
    private $debut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="time")
     */
    private $fin;

    // public function __construct()
    // {
    // // Par défaut, la date de l'annonce est la date d'aujourd'hui
    // $this->date = new \Datetime();
    // }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matiere
     *
     * @param string $matiere
     *
     * @return Calendrier
     */
    public function setMatiere($matiere)
    {
        $this->matiere = $matiere;

        return $this;
    }

    /**
     * Get matiere
     *
     * @return string
     */
    public function getMatiere()
    {
        return $this->matiere;
    }

    /**
     * Set enseignant
     *
     * @param string $enseignant
     *
     * @return Calendrier
     */
    public function setEnseignant( $enseignant)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return string
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }
        /**
     * Set departement
     *
     * @param string $departement
     *
     * @return Calendrier
     */
     public function setDepartement( $departement)
     {
         $this->departement = $departement;
 
         return $this;
     }
 
     /**
      * Get enseignant
      *
      * @return string
      */
     public function getDepartement()
     {
         return $this->departement;
     }

    /**
     * Set classe
     *
     * @param string $classe
     *
     * @return Calendrier
     */
    public function setSalle( $salle)
    {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get classe
     *
     * @return string
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * Set jour
     *
     * @param string $jour
     *
     * @return Calendrier
     */
    public function setJour( $jour)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * Get jour
     *
     * @return string
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Set debut
     *
     * @param \DateTime $debut
     *
     * @return Calendrier
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;

        return $this;
    }

    /**
     * Get debut
     *
     * @return \DateTime
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     *
     * @return Calendrier
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }

     /**
     * Get duree
     *
     * @return int
     */
     public function getDuree()
     {
         return $this->fin - $this->debut ;
     }

   /**
   * @Assert\Callback
   */
  public function isContentValid(ExecutionContextInterface $context)
  {

    // $em = $this->getDoctrine()->getManager();
    
    $matiere = $this->getMatiere();
    $enseignant = $this->getEnseignant();
    $departement = $this->getDepartement();
    $salle = $this->getSalle();
    $jour = $this->getJour();
    $debut = $this->getDebut(); //->format('H:i:s');
    $fin = $this->getFin();


    //recupere tous les calendrier pour pouvoir faire les comparaison entre
    // dans le but d'eviter les duplicata

    global $kernel; 
    $em = $kernel->getContainer()->get('doctrine')->getManager();
    $matiere_obj = $em->getRepository('univBundle:Matiers')->find($matiere);

    // if ($matiere_obj->getId()) {

    // }
    
    $all_calendrier = $em->getRepository('univBundle:calendrier')->findAll();

    foreach ($all_calendrier as $calendrier) {

        // enseignant
        if(($debut >= $calendrier->getDebut() && $fin <= $calendrier->getFin()) && ($enseignant == $calendrier->getEnseignant() && $jour == $calendrier->getJour())) {
            $context
            ->buildViolation('Cet enseignant est dans une salle pendant cet interval selectionne')
            ->atPath('enseignant')
            ->addViolation() ;
            break;
        }

        // salle
        if(($debut >= $calendrier->getDebut() && $fin <= $calendrier->getFin()) && ($salle == $calendrier->getSalle() && $jour == $calendrier->getJour())) {
            $context
            ->buildViolation('Cette salle est occupée pendant cet interval selectionne')
            ->atPath('salle')
            ->addViolation() ;
            break;
        }

         // departement
         if(($debut >= $calendrier->getDebut() && $fin <= $calendrier->getFin()) && ($departement == $calendrier->getDepartement() && $jour == $calendrier->getJour())) {
            $context
            ->buildViolation('Cette salle est occupée pendant cet interval selectionne')
            ->atPath('departement')
            ->addViolation() ;
            break;
        }
        // les deux heures
        if ($debut == $fin ) {
            $context
            ->buildViolation("L'heure du debut et de la fin ne peuvent pas etre egales")
            ->atPath('fin')
            ->addViolation() ;
            break;
        }
    }
  
}

}
