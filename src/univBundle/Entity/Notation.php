<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Notation
 *
 * @ORM\Table(name="notation",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_notation_etudiant_matiere",
 *            columns={"etudiant_id", "matieres_id","session_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="univBundle\Repository\NotationRepository")
 */
class Notation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="notation")
     * @ORM\JoinColumn(name="session_id", referencedColumnName="id",nullable=true)
     */
    private $sessions;

    /**
     * @ORM\OneToMany(targetEntity="detailNotation", mappedBy="notation")
     */
    private $detailnotation;

    public function __construct()
    {
        $this->dateNotation = new \DateTime();
        $this->detailnotation = new ArrayCollection();
//        $dateJ= new \DateTime()
        $this->dateDelaitNotation=$this->dateNotation->add(new \DateInterval('P1D'));
    }

    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="notation")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id" ,nullable=true)
     */
    private $etudiant;


    /**
     * @ORM\ManyToOne(targetEntity="Licence", inversedBy="notation")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id" ,nullable=true)
     */
    private $licence;


    /**
     * @ORM\ManyToOne(targetEntity="Semetre", inversedBy="notation")
     * @ORM\JoinColumn(name="Semestre_id", referencedColumnName="id" ,nullable=true)
     */
    private $semestre;

    /**
     * @ORM\ManyToOne(targetEntity="Matiers", inversedBy="notation")
     * @ORM\JoinColumn(name="matieres_id", referencedColumnName="id" ,nullable=true)
     */
    private $matiers;


    /**
     * @var float|null
     *
     * @ORM\Column(name="moyenneCours", type="float", nullable=true)
     */
    private $moyenneCours;

    /**
     * @var float
     *
     * @ORM\Column(name="moyenneExament", type="float", nullable=true)
     */
    private $moyenneExament;

    /**
     * @var float|null
     *
     * @ORM\Column(name="session1", type="float", nullable=true)
     */
    private $session1;

    /**
     * @var float
     *
     * @ORM\Column(name="session2", type="float", nullable=true)
     */
    private $session2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateNotation", type="datetime", nullable=true)
     */
    private $dateNotation;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateLeverSession", type="datetime", nullable=true)
     */
    private $dateLeverSession;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateDelaitNotation", type="datetime", nullable=true)
     */
    private $dateDelaitNotation;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagSession1", type="boolean", nullable=true)
     */
    private $flagSession1;


    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagDelait", type="boolean", nullable=true)
     */
    private $flagDelait;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagSession2", type="boolean", nullable=true)
     */
    private $flagSession2;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moyenneCours.
     *
     * @param float|null $moyenneCours
     *
     * @return Notation
     */
    public function setMoyenneCours($moyenneCours = null)
    {
        $this->moyenneCours = $moyenneCours;

        return $this;
    }

    /**
     * Get moyenneCours.
     *
     * @return float|null
     */
    public function getMoyenneCours()
    {
        return $this->moyenneCours;
    }

    /**
     * Set moyenneExament.
     *
     * @param float $moyenneExament
     *
     * @return Notation
     */
    public function setMoyenneExament($moyenneExament)
    {
        $this->moyenneExament = $moyenneExament;

        return $this;
    }

    /**
     * Get moyenneExament.
     *
     * @return float
     */
    public function getMoyenneExament()
    {
        return $this->moyenneExament;
    }

    /**
     * Set session1.
     *
     * @param float|null $session1
     *
     * @return Notation
     */
    public function setSession1($session1 = null)
    {
        $this->session1 = $session1;

        return $this;
    }

    /**
     * Get session1.
     *
     * @return float|null
     */
    public function getSession1()
    {
        return $this->session1;
    }

    /**
     * Set session2.
     *
     * @param float $session2
     *
     * @return Notation
     */
    public function setSession2($session2)
    {
        $this->session2 = $session2;

        return $this;
    }

    /**
     * Get session2.
     *
     * @return float
     */
    public function getSession2()
    {
        return $this->session2;
    }

    /**
     * Set dateNotation.
     *
     * @param \DateTime|null $dateNotation
     *
     * @return Notation
     */
    public function setDateNotation($dateNotation = null)
    {
        $this->dateNotation = $dateNotation;

        return $this;
    }

    /**
     * Get dateNotation.
     *
     * @return \DateTime|null
     */
    public function getDateNotation()
    {
        return $this->dateNotation;
    }

    /**
     * Set flagSession1.
     *
     * @param bool|null $flagSession1
     *
     * @return Notation
     */
    public function setFlagSession1($flagSession1 = null)
    {
        $this->flagSession1 = $flagSession1;

        return $this;
    }

    /**
     * Get flagSession1.
     *
     * @return bool|null
     */
    public function getFlagSession1()
    {
        return $this->flagSession1;
    }

    /**
     * Set flagSession2.
     *
     * @param bool|null $flagSession2
     *
     * @return Notation
     */
    public function setFlagSession2($flagSession2 = null)
    {
        $this->flagSession2 = $flagSession2;

        return $this;
    }

    /**
     * Get flagSession2.
     *
     * @return bool|null
     */
    public function getFlagSession2()
    {
        return $this->flagSession2;
    }

    /**
     * Set etudiant.
     *
     * @param \univBundle\Entity\Etudiants|null $etudiant
     *
     * @return Notation
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant.
     *
     * @return \univBundle\Entity\Etudiants|null
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set matiers.
     *
     * @param \univBundle\Entity\Matiers|null $matiers
     *
     * @return Notation
     */
    public function setMatiers(\univBundle\Entity\Matiers $matiers = null)
    {
        $this->matiers = $matiers;

        return $this;
    }

    /**
     * Get matiers.
     *
     * @return \univBundle\Entity\Matiers|null
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set licence.
     *
     * @param \univBundle\Entity\Licence|null $licence
     *
     * @return Notation
     */
    public function setLicence(\univBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence.
     *
     * @return \univBundle\Entity\Licence|null
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set semestre.
     *
     * @param \univBundle\Entity\Semetre|null $semestre
     *
     * @return Notation
     */
    public function setSemestre(\univBundle\Entity\Semetre $semestre = null)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre.
     *
     * @return \univBundle\Entity\Semetre|null
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Set flagDelait
     *
     * @param boolean $flagDelait
     *
     * @return Notation
     */
    public function setFlagDelait($flagDelait)
    {
        $this->flagDelait = $flagDelait;

        return $this;
    }

    /**
     * Get flagDelait
     *
     * @return boolean
     */
    public function getFlagDelait()
    {
        return $this->flagDelait;
    }

    /**
     * Set dateDelaitNotation
     *
     * @param \DateTime $dateDelaitNotation
     *
     * @return Notation
     */
    public function setDateDelaitNotation($dateDelaitNotation)
    {
        $this->dateDelaitNotation = $dateDelaitNotation;

        return $this;
    }

    /**
     * Get dateDelaitNotation
     *
     * @return \DateTime
     */
    public function getDateDelaitNotation()
    {
        return $this->dateDelaitNotation;
    }

    /**
     * Set dateLeverSession
     *
     * @param \DateTime $dateLeverSession
     *
     * @return Notation
     */
    public function setDateLeverSession($dateLeverSession)
    {
        $this->dateLeverSession = $dateLeverSession;

        return $this;
    }

    /**
     * Get dateLeverSession
     *
     * @return \DateTime
     */
    public function getDateLeverSession()
    {
        return $this->dateLeverSession;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return Notation
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Add detailnotation
     *
     * @param \univBundle\Entity\detailNotation $detailnotation
     *
     * @return Notation
     */
    public function addDetailnotation(\univBundle\Entity\detailNotation $detailnotation)
    {
        $this->detailnotation[] = $detailnotation;

        return $this;
    }

    /**
     * Remove detailnotation
     *
     * @param \univBundle\Entity\detailNotation $detailnotation
     */
    public function removeDetailnotation(\univBundle\Entity\detailNotation $detailnotation)
    {
        $this->detailnotation->removeElement($detailnotation);
    }

    /**
     * Get detailnotation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailnotation()
    {
        return $this->detailnotation;
    }
}
