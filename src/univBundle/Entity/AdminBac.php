<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
/**
 * AdminBac
 *
 * @ORM\Table(name="admin_bac",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_pvSession_niveau",
 *            columns={"sessions", "pv"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="univBundle\Repository\AdminBacRepository")
 */
class AdminBac
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="admisBac")
     */
    private $paiement;

    public function __construct()
    {
        $this->paiement = new ArrayCollection();

    }


    /**
     * @var string|null
     *
     * @ORM\Column(name="pv", type="string",length=150,nullable=true)
     */
    private $pv;

    /**
     * @var text|null
     *
     * @ORM\Column(name="nomPrenom", type="string",length=150)
     */
    private $nomPrenom;


    /**
     * @var float
     *
     * @ORM\Column(name="fraisFormation", type="float", nullable=true)
     */
    private $fraisFormation;


    /**
     * @var float
     *
     * @ORM\Column(name="avancePaye", type="float", nullable=true)
     */
    private $avancePaye;


    /**
     * @var float
     *
     * @ORM\Column(name="ResteApaye", type="float", nullable=true)
     */
    private $ResteApaye;


    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $filiation;

    /**
     * @ORM\Column(type="string",length=150,  nullable=true)
     */
    private $centre;

    /**
     * @ORM\Column(type="string",length=150)
     */
    private $origine;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $options;

    /**
     * @ORM\Column(type="string",length=150,  nullable=true)
     */
    private $idnational;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $statut;


    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $oldStatuBeforPaiement;



    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $oldDepartementBeforPaiement;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $departement;

    /**
     * @ORM\Column(type="string",length=150,nullable=true)
     */
    private $universite;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $codeDep;

    /**
     * @ORM\Column(type="string",length=150)
     */
    private $sessions;

    /**
     * @ORM\Column(type="string",length=150 ,nullable=true)
     */
    private $etat;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $flagIncrition;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $flagPaiement;

    /**
     * @ORM\Column(type="string",length=150,  nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $nationnalite;

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $niveau;


    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $codeNiveau;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pv
     *
     * @param string $pv
     *
     * @return AdminBac
     */
    public function setPv($pv)
    {
        $this->pv = $pv;

        return $this;
    }

    /**
     * Get pv
     *
     * @return string
     */
    public function getPv()
    {
        return $this->pv;
    }

    /**
     * Set nomPrenom
     *
     * @param string $nomPrenom
     *
     * @return AdminBac
     */
    public function setNomPrenom($nomPrenom)
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    /**
     * Get nomPrenom
     *
     * @return string
     */
    public function getNomPrenom()
    {
        return $this->nomPrenom;
    }

    /**
     * Set fraisFormation
     *
     * @param float $fraisFormation
     *
     * @return AdminBac
     */
    public function setFraisFormation($fraisFormation)
    {
        $this->fraisFormation = $fraisFormation;

        return $this;
    }

    /**
     * Get fraisFormation
     *
     * @return float
     */
    public function getFraisFormation()
    {
        return $this->fraisFormation;
    }

    /**
     * Set avancePaye
     *
     * @param float $avancePaye
     *
     * @return AdminBac
     */
    public function setAvancePaye($avancePaye)
    {
        $this->avancePaye = $avancePaye;

        return $this;
    }

    /**
     * Get avancePaye
     *
     * @return float
     */
    public function getAvancePaye()
    {
        return $this->avancePaye;
    }

    /**
     * Set resteApaye
     *
     * @param float $resteApaye
     *
     * @return AdminBac
     */
    public function setResteApaye($resteApaye)
    {
        $this->ResteApaye = $resteApaye;

        return $this;
    }

    /**
     * Get resteApaye
     *
     * @return float
     */
    public function getResteApaye()
    {
        return $this->ResteApaye;
    }

    /**
     * Set dateNaissance
     *
     * @param string $dateNaissance
     *
     * @return AdminBac
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return string
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return AdminBac
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set filiation
     *
     * @param string $filiation
     *
     * @return AdminBac
     */
    public function setFiliation($filiation)
    {
        $this->filiation = $filiation;

        return $this;
    }

    /**
     * Get filiation
     *
     * @return string
     */
    public function getFiliation()
    {
        return $this->filiation;
    }

    /**
     * Set centre
     *
     * @param string $centre
     *
     * @return AdminBac
     */
    public function setCentre($centre)
    {
        $this->centre = $centre;

        return $this;
    }

    /**
     * Get centre
     *
     * @return string
     */
    public function getCentre()
    {
        return $this->centre;
    }

    /**
     * Set origine
     *
     * @param string $origine
     *
     * @return AdminBac
     */
    public function setOrigine($origine)
    {
        $this->origine = $origine;

        return $this;
    }

    /**
     * Get origine
     *
     * @return string
     */
    public function getOrigine()
    {
        return $this->origine;
    }

    /**
     * Set options
     *
     * @param string $options
     *
     * @return AdminBac
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set idnational
     *
     * @param string $idnational
     *
     * @return AdminBac
     */
    public function setIdnational($idnational)
    {
        $this->idnational = $idnational;

        return $this;
    }

    /**
     * Get idnational
     *
     * @return string
     */
    public function getIdnational()
    {
        return $this->idnational;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return AdminBac
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set oldStatuBeforPaiement
     *
     * @param string $oldStatuBeforPaiement
     *
     * @return AdminBac
     */
    public function setOldStatuBeforPaiement($oldStatuBeforPaiement)
    {
        $this->oldStatuBeforPaiement = $oldStatuBeforPaiement;

        return $this;
    }

    /**
     * Get oldStatuBeforPaiement
     *
     * @return string
     */
    public function getOldStatuBeforPaiement()
    {
        return $this->oldStatuBeforPaiement;
    }

    /**
     * Set oldDepartementBeforPaiement
     *
     * @param string $oldDepartementBeforPaiement
     *
     * @return AdminBac
     */
    public function setOldDepartementBeforPaiement($oldDepartementBeforPaiement)
    {
        $this->oldDepartementBeforPaiement = $oldDepartementBeforPaiement;

        return $this;
    }

    /**
     * Get oldDepartementBeforPaiement
     *
     * @return string
     */
    public function getOldDepartementBeforPaiement()
    {
        return $this->oldDepartementBeforPaiement;
    }

    /**
     * Set departement
     *
     * @param string $departement
     *
     * @return AdminBac
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return string
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set universite
     *
     * @param string $universite
     *
     * @return AdminBac
     */
    public function setUniversite($universite)
    {
        $this->universite = $universite;

        return $this;
    }

    /**
     * Get universite
     *
     * @return string
     */
    public function getUniversite()
    {
        return $this->universite;
    }

    /**
     * Set codeDep
     *
     * @param string $codeDep
     *
     * @return AdminBac
     */
    public function setCodeDep($codeDep)
    {
        $this->codeDep = $codeDep;

        return $this;
    }

    /**
     * Get codeDep
     *
     * @return string
     */
    public function getCodeDep()
    {
        return $this->codeDep;
    }

    /**
     * Set sessions
     *
     * @param string $sessions
     *
     * @return AdminBac
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return string
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return AdminBac
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set flagIncrition
     *
     * @param boolean $flagIncrition
     *
     * @return AdminBac
     */
    public function setFlagIncrition($flagIncrition)
    {
        $this->flagIncrition = $flagIncrition;

        return $this;
    }

    /**
     * Get flagIncrition
     *
     * @return boolean
     */
    public function getFlagIncrition()
    {
        return $this->flagIncrition;
    }

    /**
     * Set flagPaiement
     *
     * @param boolean $flagPaiement
     *
     * @return AdminBac
     */
    public function setFlagPaiement($flagPaiement)
    {
        $this->flagPaiement = $flagPaiement;

        return $this;
    }

    /**
     * Get flagPaiement
     *
     * @return boolean
     */
    public function getFlagPaiement()
    {
        return $this->flagPaiement;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return AdminBac
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set nationnalite
     *
     * @param string $nationnalite
     *
     * @return AdminBac
     */
    public function setNationnalite($nationnalite)
    {
        $this->nationnalite = $nationnalite;

        return $this;
    }

    /**
     * Get nationnalite
     *
     * @return string
     */
    public function getNationnalite()
    {
        return $this->nationnalite;
    }

    /**
     * Set niveau
     *
     * @param string $niveau
     *
     * @return AdminBac
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set codeNiveau
     *
     * @param string $codeNiveau
     *
     * @return AdminBac
     */
    public function setCodeNiveau($codeNiveau)
    {
        $this->codeNiveau = $codeNiveau;

        return $this;
    }

    /**
     * Get codeNiveau
     *
     * @return string
     */
    public function getCodeNiveau()
    {
        return $this->codeNiveau;
    }

    /**
     * Add paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     *
     * @return AdminBac
     */
    public function addPaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement[] = $paiement;

        return $this;
    }

    /**
     * Remove paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     */
    public function removePaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement->removeElement($paiement);
    }

    /**
     * Get paiement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiement()
    {
        return $this->paiement;
    }
}
