<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Inscription
 *
 * @ORM\Table(name="inscription",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_inscription_niveau_Session_Etudiant",
 *            columns={"etudiant_id", "licence_id","session_id"}
),
@UniqueConstraint(name="unisite_inscription_session_etudiant",
 *            columns={"etudiant_id","session_id"}
)
 *    }
 * )
 * @ORM\Entity(repositoryClass="univBundle\Repository\InscriptionRepository")
 */
class Inscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="concentration", inversedBy="inscription")
     * @ORM\JoinColumn(name="concentration_id", referencedColumnName="id", nullable=true)
     */
    private $concentration;

    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="inscription")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id")
     */
    private $etudiant;


    /**
     * @ORM\OneToOne(targetEntity="Paiement", inversedBy="inscriptions",cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $paiement;

    /**
     * @var \DateTime
     * @ORM\Column(name="dateInscription", type="datetime", nullable=false)
     */
    private $dateInscription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="annee", type="string", length=150, nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Licence", inversedBy="inscription")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id", nullable=false)
     */
    private $licence;


    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="inscription")
     * @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     */
    private $sessions;


    public function __construct()
    {
        $this->dateInscription = new \DateTime();
        $this->annee = date_format(new \DateTime(), "Y");
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateInscription.
     *
     * @param \DateTime $dateInscription
     *
     * @return Inscription
     */
    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    /**
     * Get dateInscription.
     *
     * @return \DateTime
     */
    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    /**
     * Set etudiant.
     *
     * @param \univBundle\Entity\Etudiants|null $etudiant
     *
     * @return Inscription
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant.
     *
     * @return \univBundle\Entity\Etudiants|null
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set licence.
     *
     * @param \univBundle\Entity\Licence|null $licence
     *
     * @return Inscription
     */
    public function setLicence(\univBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence.
     *
     * @return \univBundle\Entity\Licence|null
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set annee.
     *
     * @param string|null $annee
     *
     * @return Inscription
     */
    public function setAnnee($annee = null)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string|null
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set sessions.
     *
     * @param \univBundle\Entity\Sessions|null $sessions
     *
     * @return Inscription
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions.
     *
     * @return \univBundle\Entity\Sessions|null
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set concentration
     *
     * @param \univBundle\Entity\concentration $concentration
     *
     * @return Inscription
     */
    public function setConcentration(\univBundle\Entity\concentration $concentration = null)
    {
        $this->concentration = $concentration;

        return $this;
    }

    /**
     * Get concentration
     *
     * @return \univBundle\Entity\concentration
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    /**
     * Set paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     *
     * @return Inscription
     */
    public function setPaiement(\univBundle\Entity\Paiement $paiement = null)
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * Get paiement
     *
     * @return \univBundle\Entity\Paiement
     */
    public function getPaiement()
    {
        return $this->paiement;
    }
}
