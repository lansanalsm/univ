<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * jourEtude
 *
 * @ORM\Table(name="jour_etude")
 * @ORM\Entity(repositoryClass="univBundle\Repository\jourEtudeRepository")
 */
class jourEtude
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=60, unique=true)
     */
    private $nom;




    /**
     * @ORM\OneToMany(targetEntity="calendrier", mappedBy="jour")
     */
    private $calendriers;


    public function __construct()
    {
        $this->calendriers = new ArrayCollection();

    }

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, unique=true)
     */
    private $code;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return jourEtude
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
    
    /**
     * Get nom
     *
     * @return string
     */
     public function getNom()
     {
         return $this->nom;
     }

    /**
     * Get jour
     *
     * @return string
     */
    public function getJour()
    {
        return $this->getNom();
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return jourEtude
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     *
     * @return jourEtude
     */
    public function addCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers[] = $calendrier;

        return $this;
    }

    /**
     * Remove calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     */
    public function removeCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers->removeElement($calendrier);
    }

    /**
     * Get calendriers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendriers()
    {
        return $this->calendriers;
    }
}
