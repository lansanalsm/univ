<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * dossier
 *
 * @ORM\Table(name="dossier")
 * @ORM\Entity(repositoryClass="univBundle\Repository\dossierRepository")
 */
class dossier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $image;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image.
     *
     * @param \univBundle\Entity\Media|null $image
     *
     * @return dossier
     */
    public function setImage(\univBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \univBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
