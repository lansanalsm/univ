<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Facultes
 *
 * @ORM\Table(name="facultes")
 * @UniqueEntity(fields="code",message="Ce code existe déja ")
 * @UniqueEntity(fields="name",message="Ce nom existe déja ")
 * @ORM\Entity(repositoryClass="univBundle\Repository\FacultesRepository")
 */
class Facultes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="departement", mappedBy="faculte")
     */
    private $departement;


    public function __construct()
    {
        $this->departement = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, unique=true)
     */
    private $code;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Facultes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Facultes
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add departement.
     *
     * @param \univBundle\Entity\departement $departement
     *
     * @return Facultes
     */
    public function addDepartement(\univBundle\Entity\departement $departement)
    {
        $this->departement[] = $departement;

        return $this;
    }

    /**
     * Remove departement.
     *
     * @param \univBundle\Entity\departement $departement
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDepartement(\univBundle\Entity\departement $departement)
    {
        return $this->departement->removeElement($departement);
    }

    /**
     * Get departement.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartement()
    {
        return $this->departement;
    }
}
