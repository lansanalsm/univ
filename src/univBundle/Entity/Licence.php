<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Licence
 *
 * @ORM\Table(name="licence")
 * @ORM\Entity(repositoryClass="univBundle\Repository\LicenceRepository")
 */
class Licence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Semetre", mappedBy="licence")
     */
    private $semestre;

    /**
     * @ORM\OneToMany(targetEntity="Inscription", mappedBy="licence")
     */
    private $inscription;

    /**
     * @ORM\OneToMany(targetEntity="Etudiants", mappedBy="licence")
     */
    private $etudiant;

    /**
     * @ORM\OneToMany(targetEntity="Notation", mappedBy="licence")
     */
    private $notation;

    public function __construct()
    {
        $this->inscription = new ArrayCollection();
        $this->semestre = new ArrayCollection();
        $this->notation = new ArrayCollection();
        $this->etudiant = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, unique=true)
     */
    private $code;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Licence
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Licence
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add semestre.
     *
     * @param \univBundle\Entity\Semetre $semestre
     *
     * @return Licence
     */
    public function addSemestre(\univBundle\Entity\Semetre $semestre)
    {
        $this->semestre[] = $semestre;

        return $this;
    }

    /**
     * Remove semestre.
     *
     * @param \univBundle\Entity\Semetre $semestre
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSemestre(\univBundle\Entity\Semetre $semestre)
    {
        return $this->semestre->removeElement($semestre);
    }

    /**
     * Get semestre.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Add inscription.
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return Licence
     */
    public function addInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription.
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInscription(\univBundle\Entity\Inscription $inscription)
    {
        return $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Add notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return Licence
     */
    public function addNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation[] = $notation;

        return $this;
    }

    /**
     * Remove notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotation(\univBundle\Entity\Notation $notation)
    {
        return $this->notation->removeElement($notation);
    }

    /**
     * Get notation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Add etudiant.
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return Licence
     */
    public function addEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant[] = $etudiant;

        return $this;
    }

    /**
     * Remove etudiant.
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        return $this->etudiant->removeElement($etudiant);
    }

    /**
     * Get etudiant.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }
}
