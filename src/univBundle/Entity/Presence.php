<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Presence
 *
 * @ORM\Table(name="presence")
 * @ORM\Entity(repositoryClass="univBundle\Repository\PresenceRepository")
 */
class Presence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepresence", type="datetime")
     */
    private $datepresence;

    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="presence")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id" ,nullable=true)
     */
    private $etudiant;


    /**
     * @ORM\ManyToOne(targetEntity="Matiers", inversedBy="presence")
     * @ORM\JoinColumn(name="matieres_id", referencedColumnName="id" ,nullable=true)
     */
    private $matiers;


    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="presence")
     * @ORM\JoinColumn(name="Sessions_id", referencedColumnName="id",nullable=true)
     */
    private $sessions;


    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagConge", type="boolean", nullable=true)
     */
    private $flagConge;




    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagPresence", type="boolean", nullable=true)
     */
    private $flagPresence;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagAbsence", type="boolean", nullable=true)
     */
    private $flagAbsence;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagMaladie", type="boolean", nullable=true)
     */
    private $flagMaladie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datepresence
     *
     * @param \DateTime $datepresence
     *
     * @return Presence
     */
    public function setDatepresence($datepresence)
    {
        $this->datepresence = $datepresence;

        return $this;
    }

    /**
     * Get datepresence
     *
     * @return \DateTime
     */
    public function getDatepresence()
    {
        return $this->datepresence;
    }

    /**
     * Set flagConge
     *
     * @param boolean $flagConge
     *
     * @return Presence
     */
    public function setFlagConge($flagConge)
    {
        $this->flagConge = $flagConge;

        return $this;
    }

    /**
     * Get flagConge
     *
     * @return boolean
     */
    public function getFlagConge()
    {
        return $this->flagConge;
    }

    /**
     * Set flagMaladie
     *
     * @param boolean $flagMaladie
     *
     * @return Presence
     */
    public function setFlagMaladie($flagMaladie)
    {
        $this->flagMaladie = $flagMaladie;

        return $this;
    }

    /**
     * Get flagMaladie
     *
     * @return boolean
     */
    public function getFlagMaladie()
    {
        return $this->flagMaladie;
    }

    /**
     * Set etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return Presence
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set matiers
     *
     * @param \univBundle\Entity\Matiers $matiers
     *
     * @return Presence
     */
    public function setMatiers(\univBundle\Entity\Matiers $matiers = null)
    {
        $this->matiers = $matiers;

        return $this;
    }

    /**
     * Get matiers
     *
     * @return \univBundle\Entity\Matiers
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return Presence
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set flagPresence
     *
     * @param boolean $flagPresence
     *
     * @return Presence
     */
    public function setFlagPresence($flagPresence)
    {
        $this->flagPresence = $flagPresence;

        return $this;
    }

    /**
     * Get flagPresence
     *
     * @return boolean
     */
    public function getFlagPresence()
    {
        return $this->flagPresence;
    }

    /**
     * Set flagAbsence
     *
     * @param boolean $flagAbsence
     *
     * @return Presence
     */
    public function setFlagAbsence($flagAbsence)
    {
        $this->flagAbsence = $flagAbsence;

        return $this;
    }

    /**
     * Get flagAbsence
     *
     * @return boolean
     */
    public function getFlagAbsence()
    {
        return $this->flagAbsence;
    }
}
