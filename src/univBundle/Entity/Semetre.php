<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Semetre
 *
 * @ORM\Table(name="semetre")
 * @ORM\Entity(repositoryClass="univBundle\Repository\SemetreRepository")
 */
class Semetre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Licence", inversedBy="semestre")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id")
     */
    private $licence;

    /**
     * @ORM\OneToMany(targetEntity="Matiers", mappedBy="semestre")
     */
    private $matiers;

    /**
     * @ORM\OneToMany(targetEntity="Notation", mappedBy="semestre")
     */
    private $notation;

    /**
     * @ORM\OneToMany(targetEntity="Etudiants", mappedBy="semestre")
     */
    private $etudiant;


    /**
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="semestre")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    private $module;

    public function __construct()
    {

        $this->matiers = new ArrayCollection();
        $this->notation = new ArrayCollection();
        $this->etudiant = new ArrayCollection();

    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Semetre
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set licence
     *
     * @param \univBundle\Entity\Licence $licence
     *
     * @return Semetre
     */
    public function setLicence(\univBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \univBundle\Entity\Licence
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Add matier
     *
     * @param \univBundle\Entity\Matiers $matier
     *
     * @return Semetre
     */
    public function addMatier(\univBundle\Entity\Matiers $matier)
    {
        $this->matiers[] = $matier;

        return $this;
    }

    /**
     * Remove matier
     *
     * @param \univBundle\Entity\Matiers $matier
     */
    public function removeMatier(\univBundle\Entity\Matiers $matier)
    {
        $this->matiers->removeElement($matier);
    }

    /**
     * Get matiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Add notation
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return Semetre
     */
    public function addNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation[] = $notation;

        return $this;
    }

    /**
     * Remove notation
     *
     * @param \univBundle\Entity\Notation $notation
     */
    public function removeNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation->removeElement($notation);
    }

    /**
     * Get notation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Add etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return Semetre
     */
    public function addEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant[] = $etudiant;

        return $this;
    }

    /**
     * Remove etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     */
    public function removeEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant->removeElement($etudiant);
    }

    /**
     * Get etudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set module
     *
     * @param \univBundle\Entity\Module $module
     *
     * @return Semetre
     */
    public function setModule(\univBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \univBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
}
