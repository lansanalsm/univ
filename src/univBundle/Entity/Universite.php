<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Universite
 *
 * @ORM\Table(name="universite")
 * @ORM\Entity(repositoryClass="univBundle\Repository\UniversiteRepository")
 */
class Universite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $couverture;


    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="nomUniversite", type="string", length=150, nullable=true, unique=true)
     */
    private $nomUniversite;

    /**
     * @var string
     *
     * @ORM\Column(name="boitePostal", type="string", length=150, nullable=true, unique=true)
     */
    private $boitePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="siteWeb", type="string", length=150, nullable=true, unique=true)
     */
    private $siteWeb;



    /**
     * @var string
     *
     * @ORM\Column(name="nomPrenomRecteur", type="string", length=150, nullable=true, unique=true)
     */
    private $nomPrenomRecteur;

    /**
     * @var string
     *
     * @ORM\Column(name="nomPrenomDirecteurProgramme", type="string", length=150, nullable=true, unique=true)
     */
    private $nomPrenomDirecteurProgramme;


    /**
     * @var string
     *
     * @ORM\Column(name="nomPrenomViceRecteurChargerEtude", type="string", length=150, nullable=true, unique=true)
     */
    private $nomPrenomViceRecteurChargerEtude;

    /**
     * @var string
     *
     * @ORM\Column(name="nomPrenomViceRecteurChargerRecherche", type="string", length=150, nullable=true, unique=true)
     */
    private $nomPrenomViceRecteurChargerRecherche;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=150)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="phone1", type="string", length=150)
     */
    private $phone1;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=150)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150)
     */
    private $email;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomUniversite
     *
     * @param string $nomUniversite
     *
     * @return Universite
     */
    public function setNomUniversite($nomUniversite)
    {
        $this->nomUniversite = $nomUniversite;

        return $this;
    }

    /**
     * Get nomUniversite
     *
     * @return string
     */
    public function getNomUniversite()
    {
        return $this->nomUniversite;
    }

    /**
     * Set nomPrenomRecteur
     *
     * @param string $nomPrenomRecteur
     *
     * @return Universite
     */
    public function setNomPrenomRecteur($nomPrenomRecteur)
    {
        $this->nomPrenomRecteur = $nomPrenomRecteur;

        return $this;
    }

    /**
     * Get nomPrenomRecteur
     *
     * @return string
     */
    public function getNomPrenomRecteur()
    {
        return $this->nomPrenomRecteur;
    }

    /**
     * Set nomPrenomDirecteurProgramme
     *
     * @param string $nomPrenomDirecteurProgramme
     *
     * @return Universite
     */
    public function setNomPrenomDirecteurProgramme($nomPrenomDirecteurProgramme)
    {
        $this->nomPrenomDirecteurProgramme = $nomPrenomDirecteurProgramme;

        return $this;
    }

    /**
     * Get nomPrenomDirecteurProgramme
     *
     * @return string
     */
    public function getNomPrenomDirecteurProgramme()
    {
        return $this->nomPrenomDirecteurProgramme;
    }

    /**
     * Set nomPrenomViceRecteurChargerEtude
     *
     * @param string $nomPrenomViceRecteurChargerEtude
     *
     * @return Universite
     */
    public function setNomPrenomViceRecteurChargerEtude($nomPrenomViceRecteurChargerEtude)
    {
        $this->nomPrenomViceRecteurChargerEtude = $nomPrenomViceRecteurChargerEtude;

        return $this;
    }

    /**
     * Get nomPrenomViceRecteurChargerEtude
     *
     * @return string
     */
    public function getNomPrenomViceRecteurChargerEtude()
    {
        return $this->nomPrenomViceRecteurChargerEtude;
    }

    /**
     * Set nomPrenomViceRecteurChargerRecherche
     *
     * @param string $nomPrenomViceRecteurChargerRecherche
     *
     * @return Universite
     */
    public function setNomPrenomViceRecteurChargerRecherche($nomPrenomViceRecteurChargerRecherche)
    {
        $this->nomPrenomViceRecteurChargerRecherche = $nomPrenomViceRecteurChargerRecherche;

        return $this;
    }

    /**
     * Get nomPrenomViceRecteurChargerRecherche
     *
     * @return string
     */
    public function getNomPrenomViceRecteurChargerRecherche()
    {
        return $this->nomPrenomViceRecteurChargerRecherche;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Universite
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set phone1
     *
     * @param string $phone1
     *
     * @return Universite
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Universite
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Universite
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set couverture
     *
     * @param \univBundle\Entity\Media $couverture
     *
     * @return Universite
     */
    public function setCouverture(\univBundle\Entity\Media $couverture = null)
    {
        $this->couverture = $couverture;

        return $this;
    }

    /**
     * Get couverture
     *
     * @return \univBundle\Entity\Media
     */
    public function getCouverture()
    {
        return $this->couverture;
    }

    /**
     * Set logo
     *
     * @param \univBundle\Entity\Media $logo
     *
     * @return Universite
     */
    public function setLogo(\univBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \univBundle\Entity\Media
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set boitePostal
     *
     * @param string $boitePostal
     *
     * @return Universite
     */
    public function setBoitePostal($boitePostal)
    {
        $this->boitePostal = $boitePostal;

        return $this;
    }

    /**
     * Get boitePostal
     *
     * @return string
     */
    public function getBoitePostal()
    {
        return $this->boitePostal;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return Universite
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }
}
