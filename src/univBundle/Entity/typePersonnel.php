<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * typePersonnel
 *
 * @ORM\Table(name="type_personnel")
 * @ORM\Entity(repositoryClass="univBundle\Repository\typePersonnelRepository")
 */
class typePersonnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Personnel", mappedBy="typePersonnel")
     */
    private $personnel;

    public function __construct()
    {
        $this->personnel = new ArrayCollection();
    }


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return typePersonnel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     *
     * @return typePersonnel
     */
    public function addPersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel[] = $personnel;

        return $this;
    }

    /**
     * Remove personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     */
    public function removePersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel->removeElement($personnel);
    }

    /**
     * Get personnel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }
}
