<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Matiers
 *
 * @ORM\Table(name="matiers",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_matiers_departement_semestre",
 *            columns={"departement_id", "semetre_id","name"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="univBundle\Repository\MatiersRepository")
 */
class Matiers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Emploie", mappedBy="matiers")
     */
    private $emploie;


    /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="matiers")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id")
     */
    private $departement;


    /**
     * @ORM\ManyToOne(targetEntity="Semetre", inversedBy="matiers")
     * @ORM\JoinColumn(name="semetre_id", referencedColumnName="id")
     */
    private $semestre;

//
//    /**
//     * @ORM\ManyToOne(targetEntity="Licence", inversedBy="matiers")
//     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id")
//     */
//    private $licence;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, unique=true,nullable=true)
     */
    private $code;


    /**
     * @var integer
     *
     * @ORM\Column(name="nombreCredit", type="integer",nullable=true)
     */
    private $nombreCredit;


    /**
     * @var string
     *
     * @ORM\Column(name="typeMatiere", type="string", length=150)
     */
    private $typeMatiere;


    /**
     * @ORM\OneToMany(targetEntity="Notation", mappedBy="matiers")
     */
    private $notation;


    /**
     * @ORM\OneToMany(targetEntity="enseigner", mappedBy="matiers")
     */
    private $enseigner;


    /**
     * @ORM\OneToMany(targetEntity="calendrier", mappedBy="matiere")
     */
    private $calendriers;

    /**
     * @ORM\OneToMany(targetEntity="Presence", mappedBy="matiers")
     */
    private $presence;

    /**
     * @ORM\OneToMany(targetEntity="detailNotation", mappedBy="matiers")
     */
    private $detailnotation;


    public function __construct()
    {
        $this->detailnotation = new ArrayCollection();
        $this->notation = new ArrayCollection();
        $this->enseigner = new ArrayCollection();
        $this->emploie = new ArrayCollection();
        $this->calendriers = new ArrayCollection();
        $this->presence = new ArrayCollection();
        $this->baremInf = 3.5;
        $this->baremSup = 5;
        $this->nombreCredit=6;
        $this->nombreHeure=72;

    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="nombreHeure", type="integer")
     */
    private $nombreHeure;


    /**
     * @var float|null
     *
     * @ORM\Column(name="baremInf", type="float", nullable=true)
     */
    private $baremInf;


    /**
     * @var float|null
     *
     * @ORM\Column(name="baremSup", type="float", nullable=true)
     */
    private $baremSup;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Matiers
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Matiers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get matiers.
     *
     * @return string
     */
    public function getMatiere()
    {
        return $this->getName();
    }
    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nombreHeure.
     *
     * @param int $nombreHeure
     *
     * @return Matiers
     */
    public function setNombreHeure($nombreHeure)
    {
        $this->nombreHeure = $nombreHeure;

        return $this;
    }

    /**
     * Get nombreHeure.
     *
     * @return int
     */
    public function getNombreHeure()
    {
        return $this->nombreHeure;
    }

    /**
     * Set departement.
     *
     * @param \univBundle\Entity\departement|null $departement
     *
     * @return Matiers
     */
    public function setDepartement(\univBundle\Entity\departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement.
     *
     * @return \univBundle\Entity\departement|null
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set semestre.
     *
     * @param \univBundle\Entity\Semetre|null $semestre
     *
     * @return Matiers
     */
    public function setSemestre(\univBundle\Entity\Semetre $semestre = null)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre.
     *
     * @return \univBundle\Entity\Semetre|null
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Add notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return Matiers
     */
    public function addNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation[] = $notation;

        return $this;
    }

    /**
     * Remove notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotation(\univBundle\Entity\Notation $notation)
    {
        return $this->notation->removeElement($notation);
    }

    /**
     * Get notation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Add enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     *
     * @return Matiers
     */
    public function addEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner[] = $enseigner;

        return $this;
    }

    /**
     * Remove enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     */
    public function removeEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner->removeElement($enseigner);
    }

    /**
     * Get enseigner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseigner()
    {
        return $this->enseigner;
    }

    /**
     * Add emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     *
     * @return Matiers
     */
    public function addEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie[] = $emploie;

        return $this;
    }

    /**
     * Remove emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     */
    public function removeEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie->removeElement($emploie);
    }

    /**
     * Get emploie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmploie()
    {
        return $this->emploie;
    }

    /**
     * Set baremInf
     *
     * @param float $baremInf
     *
     * @return Matiers
     */
    public function setBaremInf($baremInf)
    {
        $this->baremInf = $baremInf;

        return $this;
    }

    /**
     * Get baremInf
     *
     * @return float
     */
    public function getBaremInf()
    {
        return $this->baremInf;
    }

    /**
     * Set baremSup
     *
     * @param float $baremSup
     *
     * @return Matiers
     */
    public function setBaremSup($baremSup)
    {
        $this->baremSup = $baremSup;

        return $this;
    }

    /**
     * Get baremSup
     *
     * @return float
     */
    public function getBaremSup()
    {
        return $this->baremSup;
    }

    /**
     * Add calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     *
     * @return Matiers
     */
    public function addCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers[] = $calendrier;

        return $this;
    }

    /**
     * Remove calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     */
    public function removeCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers->removeElement($calendrier);
    }

    /**
     * Get calendriers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendriers()
    {
        return $this->calendriers;
    }

    /**
     * Add presence
     *
     * @param \univBundle\Entity\Presence $presence
     *
     * @return Matiers
     */
    public function addPresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence[] = $presence;

        return $this;
    }

    /**
     * Remove presence
     *
     * @param \univBundle\Entity\Presence $presence
     */
    public function removePresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence->removeElement($presence);
    }

    /**
     * Get presence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Set nombreCredit
     *
     * @param integer $nombreCredit
     *
     * @return Matiers
     */
    public function setNombreCredit($nombreCredit)
    {
        $this->nombreCredit = $nombreCredit;

        return $this;
    }

    /**
     * Get nombreCredit
     *
     * @return integer
     */
    public function getNombreCredit()
    {
        return $this->nombreCredit;
    }

    /**
     * Set typeMatiere
     *
     * @param string $typeMatiere
     *
     * @return Matiers
     */
    public function setTypeMatiere($typeMatiere)
    {
        $this->typeMatiere = $typeMatiere;

        return $this;
    }

    /**
     * Get typeMatiere
     *
     * @return string
     */
    public function getTypeMatiere()
    {
        return $this->typeMatiere;
    }

    /**
     * Add detailnotation
     *
     * @param \univBundle\Entity\detailNotation $detailnotation
     *
     * @return Matiers
     */
    public function addDetailnotation(\univBundle\Entity\detailNotation $detailnotation)
    {
        $this->detailnotation[] = $detailnotation;

        return $this;
    }

    /**
     * Remove detailnotation
     *
     * @param \univBundle\Entity\detailNotation $detailnotation
     */
    public function removeDetailnotation(\univBundle\Entity\detailNotation $detailnotation)
    {
        $this->detailnotation->removeElement($detailnotation);
    }

    /**
     * Get detailnotation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailnotation()
    {
        return $this->detailnotation;
    }
}
