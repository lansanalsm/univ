<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiement
 *
 * @ORM\Table(name="paiement")
 * @ORM\Entity(repositoryClass="univBundle\Repository\PaiementRepository")
 */
class Paiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="AdminBac", inversedBy="paiement")
     * @ORM\JoinColumn(name="admisBac_id", referencedColumnName="id",nullable=true)
     */
    private $admisBac;


    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="paiement")
     * @ORM\JoinColumn(name="sessions_id", referencedColumnName="id")
     */
    private $sessions;


    /**
     * @ORM\OneToOne(targetEntity="Inscription", mappedBy="paiement",cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $inscriptions;


    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="paiement")
     * @ORM\JoinColumn(name="etudiants_id", referencedColumnName="id",nullable=true)
     */
    private $etudiant;


    /**
     * @ORM\ManyToOne(targetEntity="FraisInscription", inversedBy="paiement")
     * @ORM\JoinColumn(name="fraisInscription_id", referencedColumnName="id")
     */
    private $fraisInscription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepaiement", type="datetime")
     */
    private $datepaiement;

    /**
     * @var float
     *
     * @ORM\Column(name="montantPayer", type="float", nullable=true)
     */
    private $montantPayer;


    /**
     * @var float
     *
     * @ORM\Column(name="fraisScolarite", type="string", nullable=true)
     */
    private $fraisScolarite;


    /**
     * @var string
     *
     * @ORM\Column(name="numeroRecu", type="string", nullable=true)
     */
    private $numeroRecu;

    /**
     * @var bool
     *
     * @ORM\Column(name="flagInscription", type="boolean", nullable=true)
     */
    private $flagInscription;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datepaiement
     *
     * @param \DateTime $datepaiement
     *
     * @return Paiement
     */
    public function setDatepaiement($datepaiement)
    {
        $this->datepaiement = $datepaiement;

        return $this;
    }

    /**
     * Get datepaiement
     *
     * @return \DateTime
     */
    public function getDatepaiement()
    {
        return $this->datepaiement;
    }

    /**
     * Set montantPayer
     *
     * @param float $montantPayer
     *
     * @return Paiement
     */
    public function setMontantPayer($montantPayer)
    {
        $this->montantPayer = $montantPayer;

        return $this;
    }

    /**
     * Get montantPayer
     *
     * @return float
     */
    public function getMontantPayer()
    {
        return $this->montantPayer;
    }

    /**
     * Set fraisScolarite
     *
     * @param string $fraisScolarite
     *
     * @return Paiement
     */
    public function setFraisScolarite($fraisScolarite)
    {
        $this->fraisScolarite = $fraisScolarite;

        return $this;
    }

    /**
     * Get fraisScolarite
     *
     * @return string
     */
    public function getFraisScolarite()
    {
        return $this->fraisScolarite;
    }

    /**
     * Set numeroRecu
     *
     * @param string $numeroRecu
     *
     * @return Paiement
     */
    public function setNumeroRecu($numeroRecu)
    {
        $this->numeroRecu = $numeroRecu;

        return $this;
    }

    /**
     * Get numeroRecu
     *
     * @return string
     */
    public function getNumeroRecu()
    {
        return $this->numeroRecu;
    }

    /**
     * Set flagInscription
     *
     * @param boolean $flagInscription
     *
     * @return Paiement
     */
    public function setFlagInscription($flagInscription)
    {
        $this->flagInscription = $flagInscription;

        return $this;
    }

    /**
     * Get flagInscription
     *
     * @return boolean
     */
    public function getFlagInscription()
    {
        return $this->flagInscription;
    }

    /**
     * Set admisBac
     *
     * @param \univBundle\Entity\AdminBac $admisBac
     *
     * @return Paiement
     */
    public function setAdmisBac(\univBundle\Entity\AdminBac $admisBac = null)
    {
        $this->admisBac = $admisBac;

        return $this;
    }

    /**
     * Get admisBac
     *
     * @return \univBundle\Entity\AdminBac
     */
    public function getAdmisBac()
    {
        return $this->admisBac;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return Paiement
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set inscriptions
     *
     * @param \univBundle\Entity\Inscription $inscriptions
     *
     * @return Paiement
     */
    public function setInscriptions(\univBundle\Entity\Inscription $inscriptions = null)
    {
        $this->inscriptions = $inscriptions;

        return $this;
    }

    /**
     * Get inscriptions
     *
     * @return \univBundle\Entity\Inscription
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }

    /**
     * Set etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return Paiement
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set fraisInscription
     *
     * @param \univBundle\Entity\FraisInscription $fraisInscription
     *
     * @return Paiement
     */
    public function setFraisInscription(\univBundle\Entity\FraisInscription $fraisInscription = null)
    {
        $this->fraisInscription = $fraisInscription;

        return $this;
    }

    /**
     * Get fraisInscription
     *
     * @return \univBundle\Entity\FraisInscription
     */
    public function getFraisInscription()
    {
        return $this->fraisInscription;
    }
}
