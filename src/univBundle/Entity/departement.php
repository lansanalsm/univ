<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * departement
 *
 * @ORM\Table(name="departement")
 * @ORM\Entity(repositoryClass="univBundle\Repository\departementRepository")
 */
class departement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Etudiants", mappedBy="departement")
     */
    private $etudiant;

    /**
     * @ORM\ManyToOne(targetEntity="Facultes", inversedBy="departement")
     * @ORM\JoinColumn(name="facute_id", referencedColumnName="id")
     */
    private $faculte;


    /**
     * @ORM\OneToMany(targetEntity="Matiers", mappedBy="departement")
     */
    private $matiers;



    /**
     * @ORM\OneToMany(targetEntity="calendrier", mappedBy="departement")
     */
    private $calendriers;


    /**
     * @ORM\OneToMany(targetEntity="concentration", mappedBy="departement")
     */
    private $concentration;

    /**
     * @ORM\OneToMany(targetEntity="Personnel", mappedBy="departement")
     */
    private $personnel;

    /**
     * @ORM\OneToMany(targetEntity="enseignant", mappedBy="departement")
     */
    private $enseignant;

    public function __construct()
    {
        $this->etudiant = new ArrayCollection();
        $this->enseignant = new ArrayCollection();
        $this->personnel = new ArrayCollection();
        $this->matiers = new ArrayCollection();
        $this->concentration = new ArrayCollection();
        $this->calendriers = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, unique=true)
     */
    private $code;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return departement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get departement.
     *
     * @return string
     */
     public function getDepartement()
     {
         return $this->getName();
     }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return departement
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add etudiant.
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return departement
     */
    public function addEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant[] = $etudiant;

        return $this;
    }

    /**
     * Remove etudiant.
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        return $this->etudiant->removeElement($etudiant);
    }

    /**
     * Get etudiant.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Add matier.
     *
     * @param \univBundle\Entity\Matiers $matier
     *
     * @return departement
     */
    public function addMatier(\univBundle\Entity\Matiers $matier)
    {
        $this->matiers[] = $matier;

        return $this;
    }

    /**
     * Remove matier.
     *
     * @param \univBundle\Entity\Matiers $matier
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMatier(\univBundle\Entity\Matiers $matier)
    {
        return $this->matiers->removeElement($matier);
    }

    /**
     * Get matiers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set faculte.
     *
     * @param \univBundle\Entity\Facultes|null $faculte
     *
     * @return departement
     */
    public function setFaculte(\univBundle\Entity\Facultes $faculte = null)
    {
        $this->faculte = $faculte;

        return $this;
    }

    /**
     * Get faculte.
     *
     * @return \univBundle\Entity\Facultes|null
     */
    public function getFaculte()
    {
        return $this->faculte;
    }

    /**
     * Add concentration
     *
     * @param \univBundle\Entity\concentration $concentration
     *
     * @return departement
     */
    public function addConcentration(\univBundle\Entity\concentration $concentration)
    {
        $this->concentration[] = $concentration;

        return $this;
    }

    /**
     * Remove concentration
     *
     * @param \univBundle\Entity\concentration $concentration
     */
    public function removeConcentration(\univBundle\Entity\concentration $concentration)
    {
        $this->concentration->removeElement($concentration);
    }

    /**
     * Get concentration
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    /**
     * Add personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     *
     * @return departement
     */
    public function addPersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel[] = $personnel;

        return $this;
    }

    /**
     * Remove personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     */
    public function removePersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel->removeElement($personnel);
    }

    /**
     * Get personnel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * Add enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     *
     * @return departement
     */
    public function addEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant[] = $enseignant;

        return $this;
    }

    /**
     * Remove enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     */
    public function removeEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant->removeElement($enseignant);
    }

    /**
     * Get enseignant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }

    /**
     * Add calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     *
     * @return departement
     */
    public function addCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers[] = $calendrier;

        return $this;
    }

    /**
     * Remove calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     */
    public function removeCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers->removeElement($calendrier);
    }

    /**
     * Get calendriers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendriers()
    {
        return $this->calendriers;
    }
}
