<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * mois
 *
 * @ORM\Table(name="mois")
 * @ORM\Entity(repositoryClass="univBundle\Repository\moisRepository")
 */
class mois
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="recuPecule", mappedBy="mois")
     */
    private $recupecule;

    public function __construct()
    {

        $this->recupecule = new ArrayCollection();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="code", type="integer", unique=true)
     */
    private $code;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return mois
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return mois
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     *
     * @return mois
     */
    public function addRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule[] = $recupecule;

        return $this;
    }

    /**
     * Remove recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     */
    public function removeRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule->removeElement($recupecule);
    }

    /**
     * Get recupecule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecupecule()
    {
        return $this->recupecule;
    }
}
