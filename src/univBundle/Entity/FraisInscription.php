<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * FraisInscription
 *
 * @ORM\Table(name="frais_inscription")
 * @ORM\Entity(repositoryClass="univBundle\Repository\FraisInscriptionRepository")
 */
class FraisInscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;


    /**
     * @var float
     *
     * @ORM\Column(name="montantReinscription", type="float")
     */
    private $montantReinscription;


    /**
     * @var bool
     *
     * @ORM\Column(name="encour", type="boolean", nullable=true)
     */
    private $encour;


    /**
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="fraisInscription")
     */
    private $paiement;

    public function __construct()
    {
        $this->paiement = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return FraisInscription
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Add paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     *
     * @return FraisInscription
     */
    public function addPaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement[] = $paiement;

        return $this;
    }

    /**
     * Remove paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     */
    public function removePaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement->removeElement($paiement);
    }

    /**
     * Get paiement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Set encour
     *
     * @param boolean $encour
     *
     * @return FraisInscription
     */
    public function setEncour($encour)
    {
        $this->encour = $encour;

        return $this;
    }

    /**
     * Get encour
     *
     * @return boolean
     */
    public function getEncour()
    {
        return $this->encour;
    }

    /**
     * Set montantReinscription
     *
     * @param float $montantReinscription
     *
     * @return FraisInscription
     */
    public function setMontantReinscription($montantReinscription)
    {
        $this->montantReinscription = $montantReinscription;

        return $this;
    }

    /**
     * Get montantReinscription
     *
     * @return float
     */
    public function getMontantReinscription()
    {
        return $this->montantReinscription;
    }
}
