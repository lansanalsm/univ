<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * detailConge
 *
 * @ORM\Table(name="detail_conge")
 * @ORM\Entity(repositoryClass="univBundle\Repository\detailCongeRepository")
 */
class detailConge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="conge", inversedBy="detailConge")
     * @ORM\JoinColumn(name="conge_id", referencedColumnName="id")
     */
    private $conge;


    /**
     * @ORM\ManyToOne(targetEntity="Etudiants", inversedBy="detailConge")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id")
     */
    private $etudiant;



    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="detailConge")
     * @ORM\JoinColumn(name="Sessions_id", referencedColumnName="id")
     */
    private $sessions;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=true)
     */
    private $dateFin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return detailConge
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return detailConge
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set conge
     *
     * @param \univBundle\Entity\conge $conge
     *
     * @return detailConge
     */
    public function setConge(\univBundle\Entity\conge $conge = null)
    {
        $this->conge = $conge;

        return $this;
    }

    /**
     * Get conge
     *
     * @return \univBundle\Entity\conge
     */
    public function getConge()
    {
        return $this->conge;
    }

    /**
     * Set etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return detailConge
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return detailConge
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }
}
