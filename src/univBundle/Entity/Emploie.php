<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Emploie
 *
 * @ORM\Table(name="emploie")
 * @ORM\Entity(repositoryClass="univBundle\Repository\EmploieRepository")
 */
class Emploie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="Salles", inversedBy="emploie")
     * @ORM\JoinColumn(name="salles_id", referencedColumnName="id")
     */
    private $salles;



    /**
     * @ORM\ManyToOne(targetEntity="Matiers", inversedBy="emploie")
     * @ORM\JoinColumn(name="matiere_id", referencedColumnName="id")
     */
    private $matiers;



    /**
     * @ORM\ManyToOne(targetEntity="Sessions", inversedBy="emploie")
     * @ORM\JoinColumn(name="sessions_id", referencedColumnName="id")
     */
    private $sessions;



    /**
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="emploie")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id",nullable=true)
     */
    private $module;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heurDebut", type="time")
     */
    private $heurDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heurFin", type="time")
     */
    private $heurFin;


    /**
     * @var \String
     *
     * @ORM\Column(name="interval",type="string",nullable=true)
     */
    private $interval;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datejour", type="datetime")
     */
    private $datejour;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set heurDebut
     *
     * @param \DateTime $heurDebut
     *
     * @return Emploie
     */
    public function setHeurDebut($heurDebut)
    {
        $this->heurDebut = $heurDebut;

        return $this;
    }

    /**
     * Get heurDebut
     *
     * @return \DateTime
     */
    public function getHeurDebut()
    {
        return $this->heurDebut;
    }

    /**
     * Set heurFin
     *
     * @param \DateTime $heurFin
     *
     * @return Emploie
     */
    public function setHeurFin($heurFin)
    {
        $this->heurFin = $heurFin;

        return $this;
    }

    /**
     * Get heurFin
     *
     * @return \DateTime
     */
    public function getHeurFin()
    {
        return $this->heurFin;
    }

    /**
     * Set datejour
     *
     * @param \DateTime $datejour
     *
     * @return Emploie
     */
    public function setDatejour($datejour)
    {
        $this->datejour = $datejour;

        return $this;
    }

    /**
     * Get datejour
     *
     * @return \DateTime
     */
    public function getDatejour()
    {
        return $this->datejour;
    }

    /**
     * Set salles
     *
     * @param \univBundle\Entity\Salles $salles
     *
     * @return Emploie
     */
    public function setSalles(\univBundle\Entity\Salles $salles = null)
    {
        $this->salles = $salles;

        return $this;
    }

    /**
     * Get salles
     *
     * @return \univBundle\Entity\Salles
     */
    public function getSalles()
    {
        return $this->salles;
    }

    /**
     * Set matiers
     *
     * @param \univBundle\Entity\Matiers $matiers
     *
     * @return Emploie
     */
    public function setMatiers(\univBundle\Entity\Matiers $matiers = null)
    {
        $this->matiers = $matiers;

        return $this;
    }

    /**
     * Get matiers
     *
     * @return \univBundle\Entity\Matiers
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return Emploie
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set module
     *
     * @param \univBundle\Entity\Module $module
     *
     * @return Emploie
     */
    public function setModule(\univBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \univBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set interval
     *
     * @param string $interval
     *
     * @return Emploie
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * Get interval
     *
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }
}
