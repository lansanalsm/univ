<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * pecule
 *
 * @ORM\Table(name="pecule")
 * @ORM\Entity(repositoryClass="univBundle\Repository\peculeRepository")
 */
class pecule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="recuPecule", mappedBy="pecule")
     */
    private $recupecule;

    public function __construct()
    {

        $this->recupecule = new ArrayCollection();
    }

    /**
     * @var float
     *
     * @ORM\Column(name="valeurl1", type="float")
     */
    private $valeurl1;

    /**
     * @var float
     *
     * @ORM\Column(name="valeurl2", type="float")
     */
    private $valeurl2;

    /**
     * @var float
     *
     * @ORM\Column(name="valeurl3", type="float")
     */
    private $valeurl3;

    /**
     * @var float
     *
     * @ORM\Column(name="valeur4", type="float")
     */
    private $valeur4;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valeurl1
     *
     * @param float $valeurl1
     *
     * @return pecule
     */
    public function setValeurl1($valeurl1)
    {
        $this->valeurl1 = $valeurl1;

        return $this;
    }

    /**
     * Get valeurl1
     *
     * @return float
     */
    public function getValeurl1()
    {
        return $this->valeurl1;
    }

    /**
     * Set valeurl2
     *
     * @param float $valeurl2
     *
     * @return pecule
     */
    public function setValeurl2($valeurl2)
    {
        $this->valeurl2 = $valeurl2;

        return $this;
    }

    /**
     * Get valeurl2
     *
     * @return float
     */
    public function getValeurl2()
    {
        return $this->valeurl2;
    }

    /**
     * Set valeurl3
     *
     * @param float $valeurl3
     *
     * @return pecule
     */
    public function setValeurl3($valeurl3)
    {
        $this->valeurl3 = $valeurl3;

        return $this;
    }

    /**
     * Get valeurl3
     *
     * @return float
     */
    public function getValeurl3()
    {
        return $this->valeurl3;
    }

    /**
     * Set valeur4
     *
     * @param float $valeur4
     *
     * @return pecule
     */
    public function setValeur4($valeur4)
    {
        $this->valeur4 = $valeur4;

        return $this;
    }

    /**
     * Get valeur4
     *
     * @return float
     */
    public function getValeur4()
    {
        return $this->valeur4;
    }

    /**
     * Add recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     *
     * @return pecule
     */
    public function addRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule[] = $recupecule;

        return $this;
    }

    /**
     * Remove recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     */
    public function removeRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule->removeElement($recupecule);
    }

    /**
     * Get recupecule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecupecule()
    {
        return $this->recupecule;
    }
}
