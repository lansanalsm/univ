<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * GradeAcademique
 *
 * @ORM\Table(name="grade_academique")
 * @ORM\Entity(repositoryClass="univBundle\Repository\GradeAcademiqueRepository")
 */
class GradeAcademique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Personnel", mappedBy="GradeAcademique")
     */
    private $personnel;

    /**
     * @ORM\OneToMany(targetEntity="enseignant", mappedBy="GradeAcademique")
     */
    private $enseignant;

    public function __construct()
    {
        $this->personnel = new ArrayCollection();
        $this->enseignant = new ArrayCollection();

    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GradeAcademique
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     *
     * @return GradeAcademique
     */
    public function addPersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel[] = $personnel;

        return $this;
    }

    /**
     * Remove personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     */
    public function removePersonnel(\univBundle\Entity\Personnel $personnel)
    {
        $this->personnel->removeElement($personnel);
    }

    /**
     * Get personnel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * Add enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     *
     * @return GradeAcademique
     */
    public function addEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant[] = $enseignant;

        return $this;
    }

    /**
     * Remove enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     */
    public function removeEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant->removeElement($enseignant);
    }

    /**
     * Get enseignant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }
}
