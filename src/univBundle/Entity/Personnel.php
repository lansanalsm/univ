<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Personnel
 *
 * @ORM\Table(name="personnel",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_personnel_departement",
 *            columns={"departement_id", "fonction_id"})
 *    }
 * )
 * @UniqueEntity(fields="email",message="Cet email existe déja ")
 * @UniqueEntity(fields="tel1",message="Ce numero existe déja ")
 * @UniqueEntity(fields="tel2",message="Ce  numero existe déja ")
 * @ORM\Entity(repositoryClass="univBundle\Repository\PersonnelRepository")
 */
class Personnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Utilisateurs", inversedBy="personnel",cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $utilisateur;


    /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="personnel")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id",nullable=true)
     */
    private $departement;


    /**
     * @ORM\ManyToOne(targetEntity="GradeAcademique", inversedBy="personnel")
     * @ORM\JoinColumn(name="GradeAcademique_id", referencedColumnName="id",nullable=true)
     */
    private $GradeAcademique;


    /**
     * @ORM\ManyToOne(targetEntity="typePersonnel", inversedBy="personnel")
     * @ORM\JoinColumn(name="typePersonnel_id", referencedColumnName="id",nullable=true)
     */
    private $typePersonnel;


    /**
     * @ORM\ManyToOne(targetEntity="Fonction", inversedBy="personnel")
     * @ORM\JoinColumn(name="fonction_id", referencedColumnName="id")
     */
    private $fonction;


    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="string", length=150)
     */
    private $roles;


    /**
     * @var string
     *
     * @ORM\Column(name="tempwd", type="string", length=150,nullable=true)
     */
    private $tempwd;


    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=150)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="tel1", type="string", length=150, unique=true)
     */
    private $tel1;

    /**
     * @var string
     *
     * @ORM\Column(name="tel2", type="string", length=150, nullable=true, unique=true)
     */
    private $tel2;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=150, nullable=true)
     */
    private $adresse;


    /**
     * @ORM\Column(type="string",unique=true)
     */
    private $email;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roles
     *
     * @param string $roles
     *
     * @return Personnel
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Personnel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Personnel
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     *
     * @return Personnel
     */
    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get tel1
     *
     * @return string
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     *
     * @return Personnel
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get tel2
     *
     * @return string
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Personnel
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Personnel
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set departement
     *
     * @param \univBundle\Entity\departement $departement
     *
     * @return Personnel
     */
    public function setDepartement(\univBundle\Entity\departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \univBundle\Entity\departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set gradeAcademique
     *
     * @param \univBundle\Entity\GradeAcademique $gradeAcademique
     *
     * @return Personnel
     */
    public function setGradeAcademique(\univBundle\Entity\GradeAcademique $gradeAcademique = null)
    {
        $this->GradeAcademique = $gradeAcademique;

        return $this;
    }

    /**
     * Get gradeAcademique
     *
     * @return \univBundle\Entity\GradeAcademique
     */
    public function getGradeAcademique()
    {
        return $this->GradeAcademique;
    }

    /**
     * Set typePersonnel
     *
     * @param \univBundle\Entity\typePersonnel $typePersonnel
     *
     * @return Personnel
     */
    public function setTypePersonnel(\univBundle\Entity\typePersonnel $typePersonnel = null)
    {
        $this->typePersonnel = $typePersonnel;

        return $this;
    }

    /**
     * Get typePersonnel
     *
     * @return \univBundle\Entity\typePersonnel
     */
    public function getTypePersonnel()
    {
        return $this->typePersonnel;
    }

    /**
     * Set fonction
     *
     * @param \univBundle\Entity\Fonction $fonction
     *
     * @return Personnel
     */
    public function setFonction(\univBundle\Entity\Fonction $fonction = null)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return \univBundle\Entity\Fonction
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set image
     *
     * @param \univBundle\Entity\Media $image
     *
     * @return Personnel
     */
    public function setImage(\univBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \univBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Personnel
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tempwd
     *
     * @param string $tempwd
     *
     * @return Personnel
     */
    public function setTempwd($tempwd)
    {
        $this->tempwd = $tempwd;

        return $this;
    }

    /**
     * Get tempwd
     *
     * @return string
     */
    public function getTempwd()
    {
        return $this->tempwd;
    }
}
