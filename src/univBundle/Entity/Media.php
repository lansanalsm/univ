<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity(repositoryClass="univBundle\Repository\MediaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * toString
     * @return string
     */
    public function __toString()
    {
        return $this->getAssetPath();
    }


    /**
     * @ORM\OneToOne(targetEntity="univBundle\Entity\dossier", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $dossier;


    /**
     * @var \DateTime
     *
     * @ORM\COlumn(name="updated_at",type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        $this->updateAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
    }


    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @ORM\Column(type="string",length=150, nullable=true)
     */
    private $path;

    private $file;


    public function getUploadRootDir()
    {
        return __dir__ . '/../../../web/uploads';
        // return __DIR__.'/../../../web/'.$this->getUploadDir();

    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;

    }

    public function getAssetPath()
    {
        return 'uploads/' . $this->path;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        $this->tempFile = $this->getAbsolutePath();
        $this->oldFile = $this->getPath();
        $this->updateAt = new \DateTime();

        if (null !== $this->file)
            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->file->guessExtension();

//        .'.xlsx'     $this->path = sha1(uniqid(mt_rand(), true));
//            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->file->guessExtension();
    }


    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->file) {
            $this->file->move($this->getUploadRootDir(), $this->path);
            unset($this->file);

//            if ($this->oldFile != null) unlink($this->tempFile);
        }
    }


    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        $this->tempFile = $this->getAbsolutePath();
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (file_exists($this->tempFile)) unlink($this->tempFile);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return MediaSliders
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Set dossier
     *
     * @param \univBundle\Entity\Dossier $dossier
     * @return Media
     */
    public function setDossier(\univBundle\Entity\dossier $dossier = null)
    {
        $this->dossier = $dossier;

        return $this;
    }

    /**
     * Get dossier
     *
     * @return \univBundle\Entity\Dossier
     */
    public function getDossier()
    {
        return $this->dossier;
    }
}
