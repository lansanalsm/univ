<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Salles
 *
 * @ORM\Table(name="salles")
 * @ORM\Entity(repositoryClass="univBundle\Repository\SallesRepository")
 */
class Salles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Batiment", inversedBy="salle")
     * @ORM\JoinColumn(name="batiement_id", referencedColumnName="id")
     */
    private $batiment;

    /**
     * @ORM\OneToMany(targetEntity="calendrier", mappedBy="salle")
     */
    private $calendriers;



    /**
     * @ORM\OneToMany(targetEntity="Emploie", mappedBy="salles")
     */
    private $emploie;

    public function __construct()
    {
        $this->emploie = new ArrayCollection();
        $this->calendriers = new ArrayCollection();

    }

    /**
     * @var string
     *
     * @ORM\Column(name="numSalle", type="string", length=150)
     */
    private $numSalle;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="nombrePlace", type="integer")
     */
    private $nombrePlace;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numSalle
     *
     * @param string $numSalle
     *
     * @return Salles
     */
    public function setNumSalle($numSalle)
    {
        $this->numSalle = $numSalle;

        return $this;
    }

    /**
     * Get numSalle
     *
     * @return string
     */
    public function getNumSalle()
    {
        return $this->numSalle;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Salles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nombrePlace
     *
     * @param integer $nombrePlace
     *
     * @return Salles
     */
    public function setNombrePlace($nombrePlace)
    {
        $this->nombrePlace = $nombrePlace;

        return $this;
    }

    /**
     * Get nombrePlace
     *
     * @return integer
     */
    public function getNombrePlace()
    {
        return $this->nombrePlace;
    }

    /**
     * Set batiment
     *
     * @param \univBundle\Entity\Batiment $batiment
     *
     * @return Salles
     */
    public function setBatiment(\univBundle\Entity\Batiment $batiment = null)
    {
        $this->batiment = $batiment;

        return $this;
    }

    /**
     * Get batiment
     *
     * @return \univBundle\Entity\Batiment
     */
    public function getBatiment()
    {
        return $this->batiment;
    }

    /**
     * Add calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     *
     * @return Salles
     */
    public function addCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers[] = $calendrier;

        return $this;
    }

    /**
     * Remove calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     */
    public function removeCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers->removeElement($calendrier);
    }

    /**
     * Get calendriers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendriers()
    {
        return $this->calendriers;
    }

    /**
     * Add emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     *
     * @return Salles
     */
    public function addEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie[] = $emploie;

        return $this;
    }

    /**
     * Remove emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     */
    public function removeEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie->removeElement($emploie);
    }

    /**
     * Get emploie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmploie()
    {
        return $this->emploie;
    }
}
