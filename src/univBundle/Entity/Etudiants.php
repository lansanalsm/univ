<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Etudiants
 *
 * @ORM\Table(name="etudiants")
 * @ORM\Entity(repositoryClass="univBundle\Repository\EtudiantsRepository")
 */
class Etudiants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var Image
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    private $image;


    /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="etudiant")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id")
     */
    private $departement;


    /**
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="etudiant")
     */
    private $paiement;

    /**
     * @ORM\ManyToOne(targetEntity="Licence", inversedBy="etudiant")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id", nullable=true)
     */
    private $licence;

    /**
     * @ORM\ManyToOne(targetEntity="Semetre", inversedBy="etudiant")
     * @ORM\JoinColumn(name="semestre_id", referencedColumnName="id", nullable=true)
     */
    private $semestre;

    /**
     * @ORM\ManyToOne(targetEntity="Roles", inversedBy="etudiant")
     * @ORM\JoinColumn(name="roles_id", referencedColumnName="id", nullable=true)
     */
    private $roles;


    /**
     * @ORM\ManyToOne(targetEntity="concentration", inversedBy="etudiant")
     * @ORM\JoinColumn(name="concentration_id", referencedColumnName="id", nullable=true)
     */
    private $concentration;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Utilisateurs", inversedBy="etudiants",cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $utilisateur;


    /**
     * @ORM\OneToMany(targetEntity="Inscription", mappedBy="etudiant")
     */
    private $inscription;

    /**
     * @ORM\OneToMany(targetEntity="Notation", mappedBy="etudiant")
     */
    private $notation;



    /**
     * @ORM\OneToMany(targetEntity="detailConge", mappedBy="etudiant")
     */
    private $detailConge;

    /**
     * @ORM\OneToMany(targetEntity="recuPecule", mappedBy="etudiant")
     */
    private $recupecule;

        /**
     * @ORM\OneToMany(targetEntity="Presence", mappedBy="etudiant")
     */
    private $presence;

    public function __construct()
    {
        $this->inscription = new ArrayCollection();
        $this->notation = new ArrayCollection();
        $this->paiement = new ArrayCollection();
        $this->recupecule = new ArrayCollection();
        $this->detailConge = new ArrayCollection();
        $this->presence = new ArrayCollection();
    }


    /**
     * @var string|null
     *
     * @ORM\Column(name="matricule", type="string", length=150, nullable=true, unique=true)
     */
    private $matricule;


    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="nomPrenom", type="string", length=150)
     */
    private $nomPrenom;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $filiation;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $nationnalite;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $dateNaissance;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricule.
     *
     * @param string|null $matricule
     *
     * @return Etudiants
     */
    public function setMatricule($matricule = null)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule.
     *
     * @return string|null
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set nomPrenom.
     *
     * @param string $nomPrenom
     *
     * @return Etudiants
     */
    public function setNomPrenom($nomPrenom)
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    /**
     * Get nomPrenom.
     *
     * @return string
     */
    public function getNomPrenom()
    {
        return $this->nomPrenom;
    }

    /**
     * Set sexe.
     *
     * @param string|null $sexe
     *
     * @return Etudiants
     */
    public function setSexe($sexe = null)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe.
     *
     * @return string|null
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set filiation.
     *
     * @param string|null $filiation
     *
     * @return Etudiants
     */
    public function setFiliation($filiation = null)
    {
        $this->filiation = $filiation;

        return $this;
    }

    /**
     * Get filiation.
     *
     * @return string|null
     */
    public function getFiliation()
    {
        return $this->filiation;
    }

    /**
     * Set contact.
     *
     * @param string|null $contact
     *
     * @return Etudiants
     */
    public function setContact($contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return string|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set nationnalite.
     *
     * @param string|null $nationnalite
     *
     * @return Etudiants
     */
    public function setNationnalite($nationnalite = null)
    {
        $this->nationnalite = $nationnalite;

        return $this;
    }

    /**
     * Get nationnalite.
     *
     * @return string|null
     */
    public function getNationnalite()
    {
        return $this->nationnalite;
    }

    /**
     * Set dateNaissance.
     *
     * @param string|null $dateNaissance
     *
     * @return Etudiants
     */
    public function setDateNaissance($dateNaissance = null)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance.
     *
     * @return string|null
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set image.
     *
     * @param \univBundle\Entity\Media|null $image
     *
     * @return Etudiants
     */
    public function setImage(\univBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \univBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set departement.
     *
     * @param \univBundle\Entity\departement|null $departement
     *
     * @return Etudiants
     */
    public function setDepartement(\univBundle\Entity\departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement.
     *
     * @return \univBundle\Entity\departement|null
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Add inscription.
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return Etudiants
     */
    public function addInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription.
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInscription(\univBundle\Entity\Inscription $inscription)
    {
        return $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set statut.
     *
     * @param string|null $statut
     *
     * @return Etudiants
     */
    public function setStatut($statut = null)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut.
     *
     * @return string|null
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Add notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return Etudiants
     */
    public function addNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation[] = $notation;

        return $this;
    }

    /**
     * Remove notation.
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotation(\univBundle\Entity\Notation $notation)
    {
        return $this->notation->removeElement($notation);
    }

    /**
     * Get notation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set licence.
     *
     * @param \univBundle\Entity\Licence|null $licence
     *
     * @return Etudiants
     */
    public function setLicence(\univBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence.
     *
     * @return \univBundle\Entity\Licence|null
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set semestre.
     *
     * @param \univBundle\Entity\Semetre|null $semestre
     *
     * @return Etudiants
     */
    public function setSemestre(\univBundle\Entity\Semetre $semestre = null)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre.
     *
     * @return \univBundle\Entity\Semetre|null
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Set roles
     *
     * @param \univBundle\Entity\Roles $roles
     *
     * @return Etudiants
     */
    public function setRoles(\univBundle\Entity\Roles $roles = null)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return \univBundle\Entity\Roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Etudiants
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set concentration
     *
     * @param \univBundle\Entity\concentration $concentration
     *
     * @return Etudiants
     */
    public function setConcentration(\univBundle\Entity\concentration $concentration = null)
    {
        $this->concentration = $concentration;

        return $this;
    }

    /**
     * Get concentration
     *
     * @return \univBundle\Entity\concentration
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    /**
     * Add detailConge
     *
     * @param \univBundle\Entity\detailConge $detailConge
     *
     * @return Etudiants
     */
    public function addDetailConge(\univBundle\Entity\detailConge $detailConge)
    {
        $this->detailConge[] = $detailConge;

        return $this;
    }

    /**
     * Remove detailConge
     *
     * @param \univBundle\Entity\detailConge $detailConge
     */
    public function removeDetailConge(\univBundle\Entity\detailConge $detailConge)
    {
        $this->detailConge->removeElement($detailConge);
    }

    /**
     * Get detailConge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailConge()
    {
        return $this->detailConge;
    }

    /**
     * Add paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     *
     * @return Etudiants
     */
    public function addPaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement[] = $paiement;

        return $this;
    }

    /**
     * Remove paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     */
    public function removePaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement->removeElement($paiement);
    }

    /**
     * Get paiement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Add recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     *
     * @return Etudiants
     */
    public function addRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule[] = $recupecule;

        return $this;
    }

    /**
     * Remove recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     */
    public function removeRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule->removeElement($recupecule);
    }

    /**
     * Get recupecule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecupecule()
    {
        return $this->recupecule;
    }

    /**
     * Add presence
     *
     * @param \univBundle\Entity\Presence $presence
     *
     * @return Etudiants
     */
    public function addPresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence[] = $presence;

        return $this;
    }

    /**
     * Remove presence
     *
     * @param \univBundle\Entity\Presence $presence
     */
    public function removePresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence->removeElement($presence);
    }

    /**
     * Get presence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresence()
    {
        return $this->presence;
    }
}
