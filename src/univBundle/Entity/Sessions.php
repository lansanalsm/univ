<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Sessions
 *
 * @ORM\Table(name="sessions")
 * @Gedmo\Loggable
 * @UniqueEntity(fields="sessions",message="Cette sessions existe déja ")
 * @ORM\Entity(repositoryClass="univBundle\Repository\SessionsRepository")
 */
class Sessions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Paiement", mappedBy="sessions")
     */
    private $paiement;




    /**
     * @ORM\OneToMany(targetEntity="Inscription", mappedBy="sessions")
     */
    private $inscription;

    /**
     * @var bool
     *
     * @ORM\Column(name="encour", type="boolean", nullable=true)
     */
    private $encour;
    /**
     * @ORM\OneToMany(targetEntity="enseigner", mappedBy="sessions")
     */
    private $enseigner;


    /**
     * @ORM\OneToMany(targetEntity="Emploie", mappedBy="sessions")
     */
    private $emploie;

    /**
     * @ORM\OneToMany(targetEntity="programmer", mappedBy="sessions")
     */
    private $programmer;


    /**
     * @ORM\OneToMany(targetEntity="detailConge", mappedBy="sessions")
     */
    private $detailConge;


    /**
     * @ORM\OneToMany(targetEntity="recuPecule", mappedBy="sessions")
     */
    private $recupecule;

    /**
     * @ORM\OneToMany(targetEntity="Presence", mappedBy="sessions")
     */
    private $presence;


    /**
     * @ORM\OneToMany(targetEntity="Notation", mappedBy="sessions")
     */
    private $notation;

    public function __construct()
    {
        $this->notation = new ArrayCollection();
        $this->presence = new ArrayCollection();
        $this->inscription = new ArrayCollection();
        $this->enseigner = new ArrayCollection();
        $this->emploie = new ArrayCollection();
        $this->programmer = new ArrayCollection();
        $this->detailConge = new ArrayCollection();
        $this->paiement = new ArrayCollection();
        $this->recupecule = new ArrayCollection();
    }


    /**
     * @var string
     *
     * @ORM\Column(name="sessions", type="string", length=150, unique=true)
     * @Gedmo\Versioned
     */
    private $sessions;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set encour
     *
     * @param boolean $encour
     *
     * @return Sessions
     */
    public function setEncour($encour)
    {
        $this->encour = $encour;

        return $this;
    }

    /**
     * Get encour
     *
     * @return boolean
     */
    public function getEncour()
    {
        return $this->encour;
    }

    /**
     * Set sessions
     *
     * @param string $sessions
     *
     * @return Sessions
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return string
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Add inscription
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return Sessions
     */
    public function addInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \univBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Add enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     *
     * @return Sessions
     */
    public function addEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner[] = $enseigner;

        return $this;
    }

    /**
     * Remove enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     */
    public function removeEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner->removeElement($enseigner);
    }

    /**
     * Get enseigner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseigner()
    {
        return $this->enseigner;
    }

    /**
     * Add emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     *
     * @return Sessions
     */
    public function addEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie[] = $emploie;

        return $this;
    }

    /**
     * Remove emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     */
    public function removeEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie->removeElement($emploie);
    }

    /**
     * Get emploie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmploie()
    {
        return $this->emploie;
    }

    /**
     * Add programmer
     *
     * @param \univBundle\Entity\programmer $programmer
     *
     * @return Sessions
     */
    public function addProgrammer(\univBundle\Entity\programmer $programmer)
    {
        $this->programmer[] = $programmer;

        return $this;
    }

    /**
     * Remove programmer
     *
     * @param \univBundle\Entity\programmer $programmer
     */
    public function removeProgrammer(\univBundle\Entity\programmer $programmer)
    {
        $this->programmer->removeElement($programmer);
    }

    /**
     * Get programmer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProgrammer()
    {
        return $this->programmer;
    }

    /**
     * Add detailConge
     *
     * @param \univBundle\Entity\detailConge $detailConge
     *
     * @return Sessions
     */
    public function addDetailConge(\univBundle\Entity\detailConge $detailConge)
    {
        $this->detailConge[] = $detailConge;

        return $this;
    }

    /**
     * Remove detailConge
     *
     * @param \univBundle\Entity\detailConge $detailConge
     */
    public function removeDetailConge(\univBundle\Entity\detailConge $detailConge)
    {
        $this->detailConge->removeElement($detailConge);
    }

    /**
     * Get detailConge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailConge()
    {
        return $this->detailConge;
    }

    /**
     * Add paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     *
     * @return Sessions
     */
    public function addPaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement[] = $paiement;

        return $this;
    }

    /**
     * Remove paiement
     *
     * @param \univBundle\Entity\Paiement $paiement
     */
    public function removePaiement(\univBundle\Entity\Paiement $paiement)
    {
        $this->paiement->removeElement($paiement);
    }

    /**
     * Get paiement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Add recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     *
     * @return Sessions
     */
    public function addRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule[] = $recupecule;

        return $this;
    }

    /**
     * Remove recupecule
     *
     * @param \univBundle\Entity\recuPecule $recupecule
     */
    public function removeRecupecule(\univBundle\Entity\recuPecule $recupecule)
    {
        $this->recupecule->removeElement($recupecule);
    }

    /**
     * Get recupecule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecupecule()
    {
        return $this->recupecule;
    }

    /**
     * Add presence
     *
     * @param \univBundle\Entity\Presence $presence
     *
     * @return Sessions
     */
    public function addPresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence[] = $presence;

        return $this;
    }

    /**
     * Remove presence
     *
     * @param \univBundle\Entity\Presence $presence
     */
    public function removePresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence->removeElement($presence);
    }

    /**
     * Get presence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Add notation
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return Sessions
     */
    public function addNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation[] = $notation;

        return $this;
    }

    /**
     * Remove notation
     *
     * @param \univBundle\Entity\Notation $notation
     */
    public function removeNotation(\univBundle\Entity\Notation $notation)
    {
        $this->notation->removeElement($notation);
    }

    /**
     * Get notation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotation()
    {
        return $this->notation;
    }
}
