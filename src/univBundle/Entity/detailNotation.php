<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * detailNotation
 *
 * @ORM\Table(name="detail_notation")
 * @ORM\Entity(repositoryClass="univBundle\Repository\detailNotationRepository")
 */
class detailNotation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Matiers", inversedBy="detailnotation")
     * @ORM\JoinColumn(name="matieres_id", referencedColumnName="id" ,nullable=true)
     */
    private $matiers;


    /**
     * @ORM\ManyToOne(targetEntity="Notation", inversedBy="detailnotation")
     * @ORM\JoinColumn(name="notation_id", referencedColumnName="id" ,nullable=true)
     */
    private $notation;



    /**
     * @var float|null
     *
     * @ORM\Column(name="moyenneCours", type="float", nullable=true)
     */
    private $moyenneCours;

    /**
     * @var float
     *
     * @ORM\Column(name="moyenneExament", type="float", nullable=true)
     */
    private $moyenneExament;

    /**
     * @var float|null
     *
     * @ORM\Column(name="session1", type="float", nullable=true)
     */
    private $session1;

    /**
     * @var float
     *
     * @ORM\Column(name="session2", type="float", nullable=true)
     */
    private $session2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateNotation", type="datetime", nullable=true)
     */
    private $dateNotation;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateLeverSession", type="datetime", nullable=true)
     */
    private $dateLeverSession;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateDelaitNotation", type="datetime", nullable=true)
     */
    private $dateDelaitNotation;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagSession1", type="boolean", nullable=true)
     */
    private $flagSession1;


    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagDelait", type="boolean", nullable=true)
     */
    private $flagDelait;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="flagSession2", type="boolean", nullable=true)
     */
    private $flagSession2;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moyenneCours
     *
     * @param float $moyenneCours
     *
     * @return detailNotation
     */
    public function setMoyenneCours($moyenneCours)
    {
        $this->moyenneCours = $moyenneCours;

        return $this;
    }

    /**
     * Get moyenneCours
     *
     * @return float
     */
    public function getMoyenneCours()
    {
        return $this->moyenneCours;
    }

    /**
     * Set moyenneExament
     *
     * @param float $moyenneExament
     *
     * @return detailNotation
     */
    public function setMoyenneExament($moyenneExament)
    {
        $this->moyenneExament = $moyenneExament;

        return $this;
    }

    /**
     * Get moyenneExament
     *
     * @return float
     */
    public function getMoyenneExament()
    {
        return $this->moyenneExament;
    }

    /**
     * Set session1
     *
     * @param float $session1
     *
     * @return detailNotation
     */
    public function setSession1($session1)
    {
        $this->session1 = $session1;

        return $this;
    }

    /**
     * Get session1
     *
     * @return float
     */
    public function getSession1()
    {
        return $this->session1;
    }

    /**
     * Set session2
     *
     * @param float $session2
     *
     * @return detailNotation
     */
    public function setSession2($session2)
    {
        $this->session2 = $session2;

        return $this;
    }

    /**
     * Get session2
     *
     * @return float
     */
    public function getSession2()
    {
        return $this->session2;
    }

    /**
     * Set dateNotation
     *
     * @param \DateTime $dateNotation
     *
     * @return detailNotation
     */
    public function setDateNotation($dateNotation)
    {
        $this->dateNotation = $dateNotation;

        return $this;
    }

    /**
     * Get dateNotation
     *
     * @return \DateTime
     */
    public function getDateNotation()
    {
        return $this->dateNotation;
    }

    /**
     * Set dateLeverSession
     *
     * @param \DateTime $dateLeverSession
     *
     * @return detailNotation
     */
    public function setDateLeverSession($dateLeverSession)
    {
        $this->dateLeverSession = $dateLeverSession;

        return $this;
    }

    /**
     * Get dateLeverSession
     *
     * @return \DateTime
     */
    public function getDateLeverSession()
    {
        return $this->dateLeverSession;
    }

    /**
     * Set dateDelaitNotation
     *
     * @param \DateTime $dateDelaitNotation
     *
     * @return detailNotation
     */
    public function setDateDelaitNotation($dateDelaitNotation)
    {
        $this->dateDelaitNotation = $dateDelaitNotation;

        return $this;
    }

    /**
     * Get dateDelaitNotation
     *
     * @return \DateTime
     */
    public function getDateDelaitNotation()
    {
        return $this->dateDelaitNotation;
    }

    /**
     * Set flagSession1
     *
     * @param boolean $flagSession1
     *
     * @return detailNotation
     */
    public function setFlagSession1($flagSession1)
    {
        $this->flagSession1 = $flagSession1;

        return $this;
    }

    /**
     * Get flagSession1
     *
     * @return boolean
     */
    public function getFlagSession1()
    {
        return $this->flagSession1;
    }

    /**
     * Set flagDelait
     *
     * @param boolean $flagDelait
     *
     * @return detailNotation
     */
    public function setFlagDelait($flagDelait)
    {
        $this->flagDelait = $flagDelait;

        return $this;
    }

    /**
     * Get flagDelait
     *
     * @return boolean
     */
    public function getFlagDelait()
    {
        return $this->flagDelait;
    }

    /**
     * Set flagSession2
     *
     * @param boolean $flagSession2
     *
     * @return detailNotation
     */
    public function setFlagSession2($flagSession2)
    {
        $this->flagSession2 = $flagSession2;

        return $this;
    }

    /**
     * Get flagSession2
     *
     * @return boolean
     */
    public function getFlagSession2()
    {
        return $this->flagSession2;
    }

    /**
     * Set matiers
     *
     * @param \univBundle\Entity\Matiers $matiers
     *
     * @return detailNotation
     */
    public function setMatiers(\univBundle\Entity\Matiers $matiers = null)
    {
        $this->matiers = $matiers;

        return $this;
    }

    /**
     * Get matiers
     *
     * @return \univBundle\Entity\Matiers
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set notation
     *
     * @param \univBundle\Entity\Notation $notation
     *
     * @return detailNotation
     */
    public function setNotation(\univBundle\Entity\Notation $notation = null)
    {
        $this->notation = $notation;

        return $this;
    }

    /**
     * Get notation
     *
     * @return \univBundle\Entity\Notation
     */
    public function getNotation()
    {
        return $this->notation;
    }
}
