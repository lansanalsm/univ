<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoriqueAffectationMatiere
 *
 * @ORM\Table(name="historique_affectation_matiere")
 * @ORM\Entity(repositoryClass="univBundle\Repository\HistoriqueAffectationMatiereRepository")
 */
class HistoriqueAffectationMatiere
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return HistoriqueAffectationMatiere
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
