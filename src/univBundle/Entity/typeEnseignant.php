<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * typeEnseignant
 *
 * @ORM\Table(name="type_enseignant")
 * @ORM\Entity(repositoryClass="univBundle\Repository\typeEnseignantRepository")
 */
class typeEnseignant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="enseignant", mappedBy="typeEnseignant")
     */
    private $enseignant;

    public function __construct()
    {
        $this->enseignant = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return typeEnseignant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     *
     * @return typeEnseignant
     */
    public function addEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant[] = $enseignant;

        return $this;
    }

    /**
     * Remove enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     */
    public function removeEnseignant(\univBundle\Entity\enseignant $enseignant)
    {
        $this->enseignant->removeElement($enseignant);
    }

    /**
     * Get enseignant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }
}
