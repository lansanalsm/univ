<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * enseignant
 *
 * @ORM\Table(name="enseignant")
 * @UniqueEntity(fields="email",message="Cet email existe déja ")
 * @UniqueEntity(fields="telephone",message="Cet numero existe déja ")
 * @ORM\Entity(repositoryClass="univBundle\Repository\enseignantRepository")
 */
class enseignant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Utilisateurs", inversedBy="enseignant",cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $utilisateur;


    /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="enseignant")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id",nullable=true)
     */
    private $departement;

    /**
     * @ORM\OneToMany(targetEntity="enseigner", mappedBy="enseignant")
     */
    private $enseigner;

    /**
     * @ORM\OneToMany(targetEntity="calendrier", mappedBy="enseignant")
     */
    private $calendriers;


//    /**
//     * @ORM\OneToMany(targetEntity="Presence", mappedBy="enseignant")
//     */
//    private $presence;

    public function __construct()
    {
        $this->enseigner = new ArrayCollection();
        $this->calendriers = new ArrayCollection();
//        $this->presence = new ArrayCollection();
    }


    /**
     * @ORM\ManyToOne(targetEntity="GradeAcademique", inversedBy="enseignant")
     * @ORM\JoinColumn(name="GradeAcademique_id", referencedColumnName="id",nullable=true)
     */
    private $GradeAcademique;

    /**
     * @ORM\ManyToOne(targetEntity="typeEnseignant", inversedBy="enseignant")
     * @ORM\JoinColumn(name="typeEnseignant_id", referencedColumnName="id",nullable=true)
     */
    private $typeEnseignant;

    /**
     * @ORM\Column(type="string",  nullable=true, length=150)
     */
    private $contact;

    /**
     * @ORM\Column(type="string",unique=true, length=150)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string",unique=true, length=150)
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=150)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="diplome", type="string", length=150, nullable=true)
     */
    private $diplome;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return enseignant
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return enseignant
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return enseignant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return enseignant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

     /**
     * Get nameComplete
     *
     * @return string
     */
     public function getNameComplete()
     {
         return ($this->prenom." ".$this->name);
     }

     /**
     * Get enseignant
     *
     * @return string
     */
     public function getEnseignant()
     {
         return $this->getName();
     }

    public function getNomPrenomTelephone()
    {
        return $this->getName().'  '.$this->getPrenom()."   (Tel: " .$this->getTelephone()." )";
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return enseignant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set diplome
     *
     * @param string $diplome
     *
     * @return enseignant
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return string
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateur
     *
     * @return enseignant
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set departement
     *
     * @param \univBundle\Entity\departement $departement
     *
     * @return enseignant
     */
    public function setDepartement(\univBundle\Entity\departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \univBundle\Entity\departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Add enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     *
     * @return enseignant
     */
    public function addEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner[] = $enseigner;

        return $this;
    }

    /**
     * Remove enseigner
     *
     * @param \univBundle\Entity\enseigner $enseigner
     */
    public function removeEnseigner(\univBundle\Entity\enseigner $enseigner)
    {
        $this->enseigner->removeElement($enseigner);
    }

    /**
     * Get enseigner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseigner()
    {
        return $this->enseigner;
    }

    /**
     * Set gradeAcademique
     *
     * @param \univBundle\Entity\GradeAcademique $gradeAcademique
     *
     * @return enseignant
     */
    public function setGradeAcademique(\univBundle\Entity\GradeAcademique $gradeAcademique = null)
    {
        $this->GradeAcademique = $gradeAcademique;

        return $this;
    }

    /**
     * Get gradeAcademique
     *
     * @return \univBundle\Entity\GradeAcademique
     */
    public function getGradeAcademique()
    {
        return $this->GradeAcademique;
    }

    /**
     * Set typeEnseignant
     *
     * @param \univBundle\Entity\typeEnseignant $typeEnseignant
     *
     * @return enseignant
     */
    public function setTypeEnseignant(\univBundle\Entity\typeEnseignant $typeEnseignant = null)
    {
        $this->typeEnseignant = $typeEnseignant;

        return $this;
    }

    /**
     * Get typeEnseignant
     *
     * @return \univBundle\Entity\typeEnseignant
     */
    public function getTypeEnseignant()
    {
        return $this->typeEnseignant;
    }

    /**
     * Add calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     *
     * @return enseignant
     */
    public function addCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers[] = $calendrier;

        return $this;
    }

    /**
     * Remove calendrier
     *
     * @param \univBundle\Entity\calendrier $calendrier
     */
    public function removeCalendrier(\univBundle\Entity\calendrier $calendrier)
    {
        $this->calendriers->removeElement($calendrier);
    }

    /**
     * Get calendriers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendriers()
    {
        return $this->calendriers;
    }

    /**
     * Add presence
     *
     * @param \univBundle\Entity\Presence $presence
     *
     * @return enseignant
     */
    public function addPresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence[] = $presence;

        return $this;
    }

    /**
     * Remove presence
     *
     * @param \univBundle\Entity\Presence $presence
     */
    public function removePresence(\univBundle\Entity\Presence $presence)
    {
        $this->presence->removeElement($presence);
    }

    /**
     * Get presence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresence()
    {
        return $this->presence;
    }
}
