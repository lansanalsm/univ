<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Blocs
 *
 * @ORM\Table(name="blocs")
 * @ORM\Entity(repositoryClass="univBundle\Repository\BlocsRepository")
 */
class Blocs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Batiment", mappedBy="blocs")
     */
    private $batiment;

    public function __construct()
    {
        $this->batiment = new ArrayCollection();

    }


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreBatment", type="string", length=150)
     */
    private $nombreBatment;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Blocs
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nombreBatment
     *
     * @param string $nombreBatment
     *
     * @return Blocs
     */
    public function setNombreBatment($nombreBatment)
    {
        $this->nombreBatment = $nombreBatment;

        return $this;
    }

    /**
     * Get nombreBatment
     *
     * @return string
     */
    public function getNombreBatment()
    {
        return $this->nombreBatment;
    }

    /**
     * Add batiment
     *
     * @param \univBundle\Entity\Batiment $batiment
     *
     * @return Blocs
     */
    public function addBatiment(\univBundle\Entity\Batiment $batiment)
    {
        $this->batiment[] = $batiment;

        return $this;
    }

    /**
     * Remove batiment
     *
     * @param \univBundle\Entity\Batiment $batiment
     */
    public function removeBatiment(\univBundle\Entity\Batiment $batiment)
    {
        $this->batiment->removeElement($batiment);
    }

    /**
     * Get batiment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBatiment()
    {
        return $this->batiment;
    }
}
