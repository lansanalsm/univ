<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Batiment
 *
 * @ORM\Table(name="batiment")
 * @ORM\Entity(repositoryClass="univBundle\Repository\BatimentRepository")
 */
class Batiment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Salles", mappedBy="batiment")
     */
    private $salle;

    public function __construct()
    {
        $this->salle = new ArrayCollection();

    }



    /**
     * @ORM\ManyToOne(targetEntity="Blocs", inversedBy="batiment")
     * @ORM\JoinColumn(name="blocs_id", referencedColumnName="id")
     */
    private $blocs;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Batiment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add salle
     *
     * @param \univBundle\Entity\Salles $salle
     *
     * @return Batiment
     */
    public function addSalle(\univBundle\Entity\Salles $salle)
    {
        $this->salle[] = $salle;

        return $this;
    }

    /**
     * Remove salle
     *
     * @param \univBundle\Entity\Salles $salle
     */
    public function removeSalle(\univBundle\Entity\Salles $salle)
    {
        $this->salle->removeElement($salle);
    }

    /**
     * Get salle
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * Set blocs
     *
     * @param \univBundle\Entity\Blocs $blocs
     *
     * @return Batiment
     */
    public function setBlocs(\univBundle\Entity\Blocs $blocs = null)
    {
        $this->blocs = $blocs;

        return $this;
    }

    /**
     * Get blocs
     *
     * @return \univBundle\Entity\Blocs
     */
    public function getBlocs()
    {
        return $this->blocs;
    }
}
