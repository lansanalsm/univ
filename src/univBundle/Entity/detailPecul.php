<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * detailPecul
 *
 * @ORM\Table(name="detail_pecul")
 * @ORM\Entity(repositoryClass="univBundle\Repository\detailPeculRepository")
 */
class detailPecul
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="pecule")
     * @ORM\JoinColumn(name="pecule_id", referencedColumnName="id")
     */
    private $pecule;


    /**
     * @ORM\ManyToOne(targetEntity="mois", inversedBy="recupecule")
     * @ORM\JoinColumn(name="mois_id", referencedColumnName="id",nullable=true)
     */
    private $mois;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pecule
     *
     * @param \univBundle\Entity\pecule $pecule
     *
     * @return detailPecul
     */
    public function setPecule(\univBundle\Entity\pecule $pecule = null)
    {
        $this->pecule = $pecule;

        return $this;
    }

    /**
     * Get pecule
     *
     * @return \univBundle\Entity\pecule
     */
    public function getPecule()
    {
        return $this->pecule;
    }

    /**
     * Set mois
     *
     * @param \univBundle\Entity\mois $mois
     *
     * @return detailPecul
     */
    public function setMois(\univBundle\Entity\mois $mois = null)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return \univBundle\Entity\mois
     */
    public function getMois()
    {
        return $this->mois;
    }
}
