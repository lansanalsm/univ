<?php
/**
 * Created by PhpStorm.
 * User: Kante
 * Date: 09/11/2018
 * Time: 12:58
 */

namespace univBundle\Entity;


class noteN
{
private $matricule;
private $nomPrenom;
private $moyenneCours;
private $moyenneExamen;

    /**
     * @return mixed
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * @param mixed $matricule
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
    }

    /**
     * @return mixed
     */
    public function getNomPrenom()
    {
        return $this->nomPrenom;
    }

    /**
     * @param mixed $nomPrenom
     */
    public function setNomPrenom($nomPrenom)
    {
        $this->nomPrenom = $nomPrenom;
    }

    /**
     * @return mixed
     */
    public function getMoyenneCours()
    {
        return $this->moyenneCours;
    }

    /**
     * @param mixed $moyenneCours
     */
    public function setMoyenneCours($moyenneCours)
    {
        $this->moyenneCours = $moyenneCours;
    }

    /**
     * @return mixed
     */
    public function getMoyenneExamen()
    {
        return $this->moyenneExamen;
    }

    /**
     * @param mixed $moyenneExamen
     */
    public function setMoyenneExamen($moyenneExamen)
    {
        $this->moyenneExamen = $moyenneExamen;
    }


}