<?php

namespace univBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resultat
 *
 * @ORM\Table(name="resultat")
 * @ORM\Entity(repositoryClass="univBundle\Repository\ResultatRepository")
 */
class Resultat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Etudiants")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id" ,nullable=false)
     */
    private $etudiant;


    /**
     * @ORM\ManyToOne(targetEntity="Licence")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id" ,nullable=false)
     */
    private $licence;


    /**
     * @ORM\ManyToOne(targetEntity="Semetre")
     * @ORM\JoinColumn(name="Semestre_id", referencedColumnName="id" ,nullable=false)
     */
    private $semestre;

    /**
     * @ORM\ManyToOne(targetEntity="Matiers")
     * @ORM\JoinColumn(name="matieres_id", referencedColumnName="id" ,nullable=true)
     */
    private $matiers;


    /**
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     */
    private $sessions;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;


    /**
     * @var float
     *
     * @ORM\Column(name="moyenneGenerale", type="float")
     */
    private $moyenneGenerale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCalcul", type="date")
     */
    private $dateCalcul;


    public function __construct()
    {
        $this->dateCalcul = new \DateTime(); ;
    }

    public static function createResultat($etudiant,$licence,$semestre,$matiere,$session,$moyenneGenerale,$user){
        $resultat=new Resultat();
        $resultat->setEtudiant($etudiant);
        $resultat->setLicence($licence);
        $resultat->setSemestre($semestre);
        $resultat->setMatiers($matiere);
        $resultat->setSessions($session);
        $resultat->setMoyenneGenerale($moyenneGenerale);
        $resultat->setUtilisateur($user);
        return $resultat;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moyenneGenerale
     *
     * @param float $moyenneGenerale
     *
     * @return Resultat
     */
    public function setMoyenneGenerale($moyenneGenerale)
    {
        $this->moyenneGenerale = $moyenneGenerale;

        return $this;
    }

    /**
     * Get moyenneGenerale
     *
     * @return float
     */
    public function getMoyenneGenerale()
    {
        return $this->moyenneGenerale;
    }

    /**
     * Set dateCalcul
     *
     * @param \DateTime $dateCalcul
     *
     * @return Resultat
     */
    public function setDateCalcul($dateCalcul)
    {
        $this->dateCalcul = $dateCalcul;

        return $this;
    }

    /**
     * Get dateCalcul
     *
     * @return \DateTime
     */
    public function getDateCalcul()
    {
        return $this->dateCalcul;
    }

    /**
     * Set etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return Resultat
     */
    public function setEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set licence
     *
     * @param \univBundle\Entity\Licence $licence
     *
     * @return Resultat
     */
    public function setLicence(\univBundle\Entity\Licence $licence)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \univBundle\Entity\Licence
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set semestre
     *
     * @param \univBundle\Entity\Semetre $semestre
     *
     * @return Resultat
     */
    public function setSemestre(\univBundle\Entity\Semetre $semestre)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre
     *
     * @return \univBundle\Entity\Semetre
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Set matiers
     *
     * @param \univBundle\Entity\Matiers $matiers
     *
     * @return Resultat
     */
    public function setMatiers(\univBundle\Entity\Matiers $matiers = null)
    {
        $this->matiers = $matiers;

        return $this;
    }

    /**
     * Get matiers
     *
     * @return \univBundle\Entity\Matiers
     */
    public function getMatiers()
    {
        return $this->matiers;
    }

    /**
     * Set sessions
     *
     * @param \univBundle\Entity\Sessions $sessions
     *
     * @return Resultat
     */
    public function setSessions(\univBundle\Entity\Sessions $sessions = null)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return \univBundle\Entity\Sessions
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Resultat
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateurs $utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
