<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * typeConge
 *
 * @ORM\Table(name="type_conge")
 * @ORM\Entity(repositoryClass="univBundle\Repository\typeCongeRepository")
 */
class typeConge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="conge", mappedBy="typeConge")
     */
    private $conge;

    public function __construct()
    {
        $this->conge = new ArrayCollection();
    }


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return typeConge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add conge
     *
     * @param \univBundle\Entity\conge $conge
     *
     * @return typeConge
     */
    public function addConge(\univBundle\Entity\conge $conge)
    {
        $this->conge[] = $conge;

        return $this;
    }

    /**
     * Remove conge
     *
     * @param \univBundle\Entity\conge $conge
     */
    public function removeConge(\univBundle\Entity\conge $conge)
    {
        $this->conge->removeElement($conge);
    }

    /**
     * Get conge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConge()
    {
        return $this->conge;
    }
}
