<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 *
 * @ORM\Table(name="module")
 * @ORM\Entity(repositoryClass="univBundle\Repository\ModuleRepository")
 */
class Module
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="programmer", mappedBy="module")
     */
       private $programmer;

    /**
     * @ORM\OneToMany(targetEntity="Semetre", mappedBy="module")
     */
    private $semestre;


    /**
     * @ORM\OneToMany(targetEntity="Emploie", mappedBy="module")
     */
    private $emploie;

    public function __construct()
    {
        $this->semestre=new  ArrayCollection();
        $this->module = new ArrayCollection();
        $this->emploie = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Module
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add programmer
     *
     * @param \univBundle\Entity\programmer $programmer
     *
     * @return Module
     */
    public function addProgrammer(\univBundle\Entity\programmer $programmer)
    {
        $this->programmer[] = $programmer;

        return $this;
    }

    /**
     * Remove programmer
     *
     * @param \univBundle\Entity\programmer $programmer
     */
    public function removeProgrammer(\univBundle\Entity\programmer $programmer)
    {
        $this->programmer->removeElement($programmer);
    }

    /**
     * Get programmer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProgrammer()
    {
        return $this->programmer;
    }

    /**
     * Add semestre
     *
     * @param \univBundle\Entity\Semetre $semestre
     *
     * @return Module
     */
    public function addSemestre(\univBundle\Entity\Semetre $semestre)
    {
        $this->semestre[] = $semestre;

        return $this;
    }

    /**
     * Remove semestre
     *
     * @param \univBundle\Entity\Semetre $semestre
     */
    public function removeSemestre(\univBundle\Entity\Semetre $semestre)
    {
        $this->semestre->removeElement($semestre);
    }

    /**
     * Get semestre
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Add emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     *
     * @return Module
     */
    public function addEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie[] = $emploie;

        return $this;
    }

    /**
     * Remove emploie
     *
     * @param \univBundle\Entity\Emploie $emploie
     */
    public function removeEmploie(\univBundle\Entity\Emploie $emploie)
    {
        $this->emploie->removeElement($emploie);
    }

    /**
     * Get emploie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmploie()
    {
        return $this->emploie;
    }
}
