<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * concentration
 *
 * @ORM\Table(name="concentration",
 * uniqueConstraints={
 *        @UniqueConstraint(name="unisite_conentration_departement",
 *            columns={"name", "departement_id"})
 *    })
 * @ORM\Entity(repositoryClass="univBundle\Repository\concentrationRepository")
 */
class concentration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="departement", inversedBy="concentration")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id")
     */
    private $departement;

    /**
     * @ORM\OneToMany(targetEntity="Etudiants", mappedBy="concentration")
     */
    private $etudiant;

    /**
     * @ORM\OneToMany(targetEntity="Inscription", mappedBy="concentration")
     */
    private $inscription;

    public function __construct()
    {
        $this->etudiant = new ArrayCollection();
        $this->inscription = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return concentration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set departement
     *
     * @param \univBundle\Entity\departement $departement
     *
     * @return concentration
     */
    public function setDepartement(\univBundle\Entity\departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \univBundle\Entity\departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Add etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     *
     * @return concentration
     */
    public function addEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant[] = $etudiant;

        return $this;
    }

    /**
     * Remove etudiant
     *
     * @param \univBundle\Entity\Etudiants $etudiant
     */
    public function removeEtudiant(\univBundle\Entity\Etudiants $etudiant)
    {
        $this->etudiant->removeElement($etudiant);
    }

    /**
     * Get etudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Add inscription
     *
     * @param \univBundle\Entity\Inscription $inscription
     *
     * @return concentration
     */
    public function addInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \univBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\univBundle\Entity\Inscription $inscription)
    {
        $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscription()
    {
        return $this->inscription;
    }
}
