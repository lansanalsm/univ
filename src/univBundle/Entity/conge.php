<?php

namespace univBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * conge
 *
 * @ORM\Table(name="conge")
 * @ORM\Entity(repositoryClass="univBundle\Repository\congeRepository")
 */
class conge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="DetailConge", mappedBy="conge")
     */
    private $detailConge;

    public function __construct()
    {

        $this->detailConge = new ArrayCollection();
    }


    /**
     * @ORM\ManyToOne(targetEntity="typeConge", inversedBy="conge")
     * @ORM\JoinColumn(name="typeConge_id", referencedColumnName="id")
     */
    private $typeConge;


    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=150)
     */
    private $titre;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer",nullable=true)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return conge
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return conge
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return conge
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Add detailConge
     *
     * @param \univBundle\Entity\DetailConge $detailConge
     *
     * @return conge
     */
    public function addDetailConge(\univBundle\Entity\DetailConge $detailConge)
    {
        $this->detailConge[] = $detailConge;

        return $this;
    }

    /**
     * Remove detailConge
     *
     * @param \univBundle\Entity\DetailConge $detailConge
     */
    public function removeDetailConge(\univBundle\Entity\DetailConge $detailConge)
    {
        $this->detailConge->removeElement($detailConge);
    }

    /**
     * Get detailConge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailConge()
    {
        return $this->detailConge;
    }

    /**
     * Set typeConge
     *
     * @param \univBundle\Entity\typeConge $typeConge
     *
     * @return conge
     */
    public function setTypeConge(\univBundle\Entity\typeConge $typeConge = null)
    {
        $this->typeConge = $typeConge;

        return $this;
    }

    /**
     * Get typeConge
     *
     * @return \univBundle\Entity\typeConge
     */
    public function getTypeConge()
    {
        return $this->typeConge;
    }
}
