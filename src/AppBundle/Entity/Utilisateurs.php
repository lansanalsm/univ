<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * Utilisateurs
 *
 * @ORM\Table(name="utilisateurs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UtilisateursRepository")
 */
class Utilisateurs extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity="univBundle\Entity\Personnel", mappedBy="utilisateur", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $personnel;


    /**
     * @var string
     *
     * @ORM\Column(name="rolesUser", type="string", length=150,nullable=true)
     */
    private $rolesUser;


    /**
     * @ORM\OneToOne(targetEntity="univBundle\Entity\enseignant", mappedBy="utilisateur", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $enseignant;

    /**
     * @ORM\OneToOne(targetEntity="univBundle\Entity\Etudiants", mappedBy="utilisateur", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $etudiants;


    /**
     * @ORM\Column(name="nom", type="string", length=150, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(name="locked", type="boolean", nullable=true)
     */
    private $locked;


    /**
     * @ORM\Column(name="prenom", type="string", length=150, nullable=true)
     */
    private $prenom;

    public function _construct()
    {
        $this->locked = false;
    }

    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    public function isLocked()
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * @return mixed
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * Set nom.
     *
     * @param string|null $nom
     *
     * @return Utilisateurs
     */
    public function setNom($nom = null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string|null $prenom
     *
     * @return Utilisateurs
     */
    public function setPrenom($prenom = null)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string|null
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set personnel
     *
     * @param \univBundle\Entity\Personnel $personnel
     *
     * @return Utilisateurs
     */
    public function setPersonnel(\univBundle\Entity\Personnel $personnel = null)
    {
        $this->personnel = $personnel;

        return $this;
    }

    /**
     * Get personnel
     *
     * @return \univBundle\Entity\Personnel
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * Set etudiants
     *
     * @param \univBundle\Entity\Etudiants $etudiants
     *
     * @return Utilisateurs
     */
    public function setEtudiants(\univBundle\Entity\Etudiants $etudiants = null)
    {
        $this->etudiants = $etudiants;

        return $this;
    }

    /**
     * Get etudiants
     *
     * @return \univBundle\Entity\Etudiants
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * Set enseignant
     *
     * @param \univBundle\Entity\enseignant $enseignant
     *
     * @return Utilisateurs
     */
    public function setEnseignant(\univBundle\Entity\enseignant $enseignant = null)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return \univBundle\Entity\enseignant
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }

    /**
     * Set rolesUser
     *
     * @param string $rolesUser
     *
     * @return Utilisateurs
     */
    public function setRolesUser($rolesUser)
    {
        $this->rolesUser = $rolesUser;

        return $this;
    }

    /**
     * Get rolesUser
     *
     * @return string
     */
    public function getRolesUser()
    {
        return $this->rolesUser;
    }
}
